# Workflows for automatic evaluation of bioassays (AutomatizedBioassayScreening)
Details on how to use provided R scripts which can be used for the analysis of cell-based bioassay raw data for a variety of high-throughput assays in a 384-well plate format. Description of the files needed. 

---

# Structure of the environment

The environment has a distinct hierarchy of folders and files. 

1. The main folder is the `project` folder which acts as main environment and can be created at the desired path.
   1. This project folder contains a `Summary.xlsx` which has two sheets.
   - The first sheet  `Sample_List` has to be filled with the green columns.
      - Sample_description: A place for comments and more detailled sample descriptions.
      - Sample_ID: An unique ID of a sample. If an ID is occuring several times, one has to define if those measurements are merged or split by defining the respective `yaml` file in the assay folders. 
      - Unit: The concentration unit used.
      - Excluded_wells: The wells of the respective row from position 1-22 to be excluded from analysis in the format 1,2,n or "all" to excluded the row (recommended instead of deleting the sample from the dosing sheet)
      - Assay_ID: The ID used for respective assays, i.e. AREc32 or MMP. Can be extended as desired. Needs to match the names of the `assay` folders. If the respective sample was measured in this assay, one has to add a "x" to the respective Assay_ID column. 
   - The orange columns will be filled in by the scripts. Filled-in files will get the suffix "_Assay_ID_filled, where the Assay_ID will be the respective Assay_ID as defined by the user. 
      - Measurmement: Will be the name of the folder containing the raw data measurememnts.
      -  Plate: The number of the plate.
      - Position: The row of the plate where the sample was dosed.

2. The `assay` folders which have specified names of the respective assays, i.e. AREc32 or MMP.
   1. All folders within this `assay` folder are considered `measurements` containing raw data files. So please avoid adding any other folders besides actual measurements. 

      1. Each `measurement` includes the `dosing.xlsx` which is defining how the samples were dosed. The sheet sample_data is the only one which needs to be filled in, whilst the other sheets are linked and used by the scripts for dosed concentrations. Concentrations are calculated for two scenarios, linear or serial dilution per plate. 

         Green columns have to be filled in. Sample_description, Sample_ID, and Sample_concentration_unit can directly be copied and pasted into the `Summary.xlsx`.
         - Plate: The number of the plate. Currently limited to 5 plates per `measurement`.
         - Col.: The column of the 384-well plate.
         - Sample_description: A place for comments and more detailled sample descriptions.
         - Sample_ID: An unique ID of a sample. If an ID is occuring several times, one has to define if those measurements are merged or split by defining the respective `yaml` file in the assay folders. 
         - Sample_concentration_unit: The concentration unit used.
         - Dosing: Define if the script should use the linear or serial dilution pattern of the respective plate. Also affects calculations of the lowest and highest concentrations dosed.
         - Sample_concentration_value: The concentration value of the initial sample in the sample_concentration_unit.
         - Blow_down?: Was the sample evaporated before adding medium? If not empty, the calculations will be done assuming full evaporation of the added sample volume.
         - target_conc_assay: What is the targeted highest concentration dosed to the cells? (mutually exclusive with target_µL_of_sample).
         - target_µL_of_sample: What is the volume of sample added to the dosing vial? (mutually exclusive with target_conc_assay).
         - µL_medium: The amount of assay medium added to the dosing vial.
         - Dilution: The ratio of dosed sample to original sample. Helps identifying necessary dilutions if target_conc_assay is not within a feasable range of volumes.

         Orange columns are calculated by equations within the sheet.
         - µL_total: The final volume of the dosing vial.
         - µL_of_sample: The volume of sample added to the dosing vial.
         - conc_dosing_vial: The concentration of the sample in the dosing vial.
         - high_conc_assay: The highest concentration dosed to the assay plate.
         - low_conc_assay: The lowest concentration dosed to the assay plate.
   2. A "Plots" folder which will be generated by the R script if it is not already exisiting
      - Contains .PDF files which show the final plots
         - Left (A) plot is a summary of all measured data points of both, effect and cytotoxicity 
         - Middle (B) plot shows the data and fit used to derive effect concentrations (selected enpoint)
         - Right (C) plot shows the data and fit used to derive inhibitory concentrations (cytotoxicity)
         - If both linear and log-logistic plots are to be plotted, the upper row (A - C) will plot the values on a linear scale whilst the lower row (D - F) will show the values on a logarithmic scale

         2. The raw data in the formats of `.txt` or `.xlsx` files
            - .txt: Should contain the information of incubation time (t24h or t48h) as well as the plate (_p1, _p2, ...). 
            - .xlsx: One sheet per plate and timepoint and should contain a label (*"Image"* for MMP, *"RawData"* for all other assays) and the plate (_p1, _p2, ...).

   3. The "Automated_bioassay_evaluation.R" script

   4. The `yaml` file defining the analysis settings
      - Assay_ID: The Assay_ID that is matching both the assay folder and ID in the `Summary.xlsx`
      - Input_format: Defines how the raw data is imported as this is assay-dependent.
	 - `GeneBLAzer` for all -BLA assays, `Neurotoxicity` for SH-SY5Y, `MMP` for MMP, `AREc32` for AREc32, `AhR-CALUX` for AhR-CALUX
      - Output_format: The unit of the effect response needs to be defined as plots and calculations will differ.
	 - `percent_activity` for all responses in %, `induction_ratio` for all responses as induction ratios
      - benchmark_effect: The effect level F used to derive the effect concentration ECF or ECIRF
      - benchmark_cytotoxicity: The cytotoxicity level F used to derive the inhibitory concentration ICF
      - Reference: The ID of the reference compound used in the assay. This has to match the IDs of used in the `Summary.xlsx` or `dosing.xlsx`
      - combine_repeats: Boolean if data of reoccuring IDs should be merged (TRUE) or analyzed seperately (FALSE)
      - re_evaluate_everything: Boolean if the script should always re-analyze all samples (TRUE) or only new samples and added repeats (FALSE)
      - cytotoxicity_cutoff: Define if you want to trim the concentration-response curve to a maximum of the measured ICF (concentration at F% cytotoxicity as defined by benchmark_cytotoxicity)
      - use_precipitation_filter: Boolean if the precipitation filter should be active, which excludes data that shows increased cell viability due to imaging artifacts such as precipitation.
      - threshold_precipitation_filter: The value of cytotoxicity that should trigger the filter. Should be a negative value such as -10. 
      - minimum_nr_of_datapoints_above_threshold: The nr of independent concentrations above the set effect threshold. It is recommended to have at least 2. Lowers the confidence if not enough datapoints are present in a sample.
      - confidence_threshold: The value of confidence that needs to be exceeded to define a calculated benchmark concentration as confident. 
      - Summary_model_effects: Which model should be used for the final summary of effects (log, log-lin or linear)
      - Summary_model_cytotoxicity: Which model should be used for the final summary of cytotoxicity (log, log-lin or linear)
      - red: the 0-255 value of red in RGB coloring of the effect color
      - green: the 0-255 value of green in the RGB coloring of the effect color
      - blue: the 0-255 value of blue in the RGB coloring of the effect color
      - Endpoint: The y axis label of the measured effect
      - plot_cytotoxicity: Boolean if the summary plots should use cytotoxicity (TRUE) or cell viability (FALSE)
      - which_plots: Define if you want to only plot the "linear", "log", or "all" plots. 
      - x_axis_font_size: The size of the labels of the x axis
      - y_axis_font_size: The size of the labels of the y axis
      - use_letters_for_subplots: Define if you want to plot the respective letters (A - C, A - F) on the subplots.
      - linear_cutoff_effect: The maximum effect level included for linear regression
      - linear_cutoff_cytotoxicity: The maximum cytotoxicity level included for linear regression
      - log_concentration_unexposed_controls: A virtual log concentration value used for plotting the background measured for the unexposed cells. The linear counterpart will be used for linear regression.
      - min_y_axis_limits_effects: Defines the minimum y axis limits of effects for linear plots
      - min_y_axis_limits_cytotoxicity: Defines the minimum y axis limits of cytotoxicity for linear plots
      - linear_y_axis_limits_effects: Defines the maximum y axis limits of effects for linear plots
      - linear_y_axis_limits_cytotoxicity: Defines the maximum y axis limits of cytotoxicity for linear plots
      - log_y_axis_limits_effects: Defines the maximum y axis limits of effects for log-logistic plots
      - log_y_axis_limits_cytotoxicity: Defines the maximum y axis limits of cytotoxicity for log-logistic plots


# Setup of the environment

1) Create the `project` folder wherever you like and with whatever name. It is suggested to have a folder which contains all your projects to later on have an easier work environment for inter-project analyses. 
2) Add a `Summary.xlsx` to the `project` folder. This repository provides a template "Summary_project_name.xlsx". It is recommended to use your `project` folders name, but the script will automatically use the `project` name after evaluation. 
3) Add a `bioassay` folder. The name of the `bioassay` folder needs to match the Assay_ID used in the `Summary.xlsx`, i.e. AREc32 or MMP. 
4) Add the `measurememnts` to your `assay` folder. These folders should contain the `dosing.xlsx` as well as .xlsx and .txt of the raw data. A measurement is currently limited to 5 plates and nees to be split with restarting plates (so i.e. measurement_X_1 has plates 1 - 5, and measurement_X_2 has the plates 6 - 8 but are filled in as plates 1 - 3. 
 - **.txt: Should contain the information of incubation time (t24h or t48h) as well as the plate (_p1, _p2, ...).** 
 - **.xlsx: One sheet per plate and timepoint and should contain a label ("Image" for MMP, "RawData" for all other assays) and the plate (_p1, _p2, ...).**
5) Update `Summary.xlsx` in accordance with the `dosing.xlsx`, so that Sample_description, Sample_ID, and Unit/Sample_concentration_unit match and select the bioassays that have been measured per sample
6) Add the respective yaml file to the `bioassay` folder and define the parameters as needed. Examples are provide in the respective assay folders in this repository.

# Data evaluation

Add the respective R script designed for the assay and run all lines. The script will generate a "Plots" folder within the `bioassay` folder and a `Summary_Assay_ID_filled.xlsx` will be generated showin plate coordinates and benchmark concentrations of all selected samples. You have to define in the `yaml` file if repeats should be merged or analyzed seperately.

The main steps are:

1) Calculation of response values (% relative to control/reference or induction ratio)
2) simple linear regression up to the specified cutoff and calculation of standard error based on error propagation
3) log-logisitc regressin using the tcpl workflow
 - Check for negative cytotoxicity and trimming of usable concentrations assuming precipitation
 - Fit of log-logistic model and calculation of AICs for constant (cnst), gain-loss (gnls) and hill (hill) model
 - Derive best-fitting model where differences >3 of AIC are considered significant
  - If constant, the sample is considered inactive and no further analysis is performed
  - If gain-loss, the maximum effect is identified and the concentration-response trimmed until this maximum defines the highest concentration. Note: gain-loss is not applied to cytotoxicity
  - Hill is considered the most optimal model
 - Calculate the distance of the individual data points to the fit and perform outlier removal via rosner test if at least five independent values define the curve
 - Apply a hill model and calculate the benchmark concentration with standard error of based on the errors of the relative EC50 and hill-slope according to tcpl via error propagation.
 4) Calculate linear coefficient of determination (R2) or Cox-Snell pseudo R2 as value for goodness of fit
 5) Define confidence factor which is dependent on if the nubmer of independent concentrations above and below the calculated benchmark concentration exceeds a pre-set value. If not, the confidence factor is set to 0.5, otherwise remains 1.
 6) Calculate the confidence of the fit to derive the benchmark activity concentration AC via: $`(1-|SE_(AC)/AC|)*factor*R^2`$

# Data interpretation

The filled in `Summary_Assay_ID_filled.xlsx` will contain several columns with results on the second sheet. Assay_ID is the ID which is used for the respective assay, i.e. "AREc32" or "MMP"
 - Sample_ID: An unique ID of a sample. If an ID is occuring several times, one has to define if those measurements are merged or split by defining the respective `yaml` file in the assay folders. 
 - Unit: The concentration unit used.
 - Assay_ID_linmodel_ECF: The ECF value derived using linear regression
 - Assay_ID_SE_linmodel_ECF: The standard error of the ECF value derived using linear regression
 - Assay_ID_R2_linmodel_ECF: The coefficient of determination of the linear regression line used to derive ECF
 - Assay_ID_linmodel_confidence_ECF: The confidence of the ECF derived via linear regression
 - Assay_ID_logmodel_ECF: The ECF value derived using log-logistic regression
 - Assay_ID_SE_logmodel_ECF: The standard error of the ECF value derived using log-logistic regression
 - Assay_ID_R2_logmodel_ECF: The Cox-Snell coefficient of determination of the log-logistic fit used to derive ECF
 - Assay_ID_logmodel_lin_ECF: The ECF from a hill model fitted on a linear scale
 - Assay_ID_SE_logmodel_lin_ECF: The standard error of the ECF value derived from a hill model fitted on a linear scale
 - Assay_ID_logmodel_confidence_ECF: The confidence of the ECF derived via log-logistic regression
 - Assay_ID_Effect_dev_Control: Boolean if the measured effect values differ significantly from the control of unexposed cells

 - Assay_ID_linmodel_ICF: The ICF value derived using linear regression
 - Assay_ID_SE_linmodel_ICF: The standard error of the ICF value derived using linear regression
 - Assay_ID_R2_linmodel_ICF: The coefficient of determination of the linear regression line used to derive ICF
 - Assay_ID_linmodel_confidence_ICF: The confidence of the ICF derived via linear regression
 - Assay_ID_logmodel_ICF: The ICF value derived using log-logistic regression
 - Assay_ID_SE_logmodel_ICF: The standard error of the ICF value derived using log-logistic regression
 - Assay_ID_R2_logmodel_ICF: The Cox-Snell coefficient of determination of the log-logistic fit used to derive ICF
 - Assay_ID_logmodel_lin_ICF: The ICF from a hill model fitted on a linear scale
 - Assay_ID_SE_logmodel_lin_ICF: The standard error of the ICF value derived from a hill model fitted on a linear scale
 - Assay_ID_logmodel_confidence_ICF: The confidence of the ICF derived via log-logistic regression
 - Assay_ID_Effect_dev_Control: Boolean if the measured cytotoxicity values differ significantly from the control of unexposed cells

For simplification, the `Summary_Assay_ID_filled.xlsx` gives out a `Summary` sheet which tries to simplify the results.
 - Sample: An unique ID of a sample. If an ID is occuring several times, one has to define if those measurements are merged or split by defining the respective `yaml` file in the assay folders. 
 - Unit: The concentration unit used.
 - log_ECF or ECF: The (log) ECF value in the unit specified for this sample.
 - log_SE_ECF or SE_ECF: The (log) standard error of the ECF in the unit specified for this sample.
 - Confident_log_ECF? or Confident_ECF?: Boolean if the confidence for the (log) ECF is high enough and considered valid.
 - log_ICF or ICF: The (log) ICF value in the unit specified for this sample.
 - log_SE_ICF or SE_ICF: The (log) standard error of the ICF in the unit specified for this sample.
 - Confident_log_ICF? or Confident_ICF?: Boolean if the confidence for the (log) ICF is high enough and considered valid.
