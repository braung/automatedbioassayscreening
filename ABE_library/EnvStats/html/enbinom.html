<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Probability Parameter of a Negative Binomial...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for enbinom {EnvStats}"><tr><td>enbinom {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Probability Parameter of a Negative Binomial Distribution
</h2>

<h3>Description</h3>

<p>Estimate the probability parameter of a 
<a href="../../stats/help/NegBinomial.html">negative binomial distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  enbinom(x, size, method = "mle/mme")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>vector of non-negative integers indicating the number of trials that took place 
<em>before</em> <code>size</code> &ldquo;successes&rdquo; occurred.  (The total number of 
trials that took place is <code>x+1</code>).  Missing (<code>NA</code>), undefined (<code>NaN</code>), 
and infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.  If 
<code>length(x)=n</code> and <code>n</code> is greater than 1, it is assumed that <code>x</code> 
represents observations from <code>n</code> separate negative binomial experiments that 
all had the same probability of success (<code>prob</code>), but possibly different 
values of <code>size</code>.
</p>
</td></tr>
<tr valign="top"><td><code>size</code></td>
<td>

<p>vector of positive integers indicating the number of &ldquo;successes&rdquo; that 
must be observed before the trials are stopped.  Missing (<code>NA</code>), 
undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed 
but will be removed.  The length of <code>size</code> must be 1 or else the same 
length as <code>x</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  Possible values are: <br /> 
<code>"mle/mme"</code> (maximum likelihood and method of moments; the default) and <br />
<code>"mvue"</code> (minimum variance unbiased).  <br />
You cannot use <code>method="mvue"</code> if 
the sum of the elements in <code>size</code> is 1.  See the DETAILS section for more 
information on these estimation methods.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of <i>n</i> 
independent observations from <a href="../../stats/help/NegBinomial.html">negative binomial distributions</a> 
with parameters <code>prob=</code><i>p</i> and <code>size=</code><i>\underline{k}</i>, where 
where <i>\underline{k} = c(k_1, k_2, &hellip;, k_n)</i> is a vector of <i>n</i> 
(possibly different) values.
</p>
<p>It can be shown (e.g., Forbes et al., 2011) that if <i>X</i> is defined as:
</p>
<p style="text-align: center;"><i>X = &sum;^n_{i = 1} x_i</i></p>

<p>then <i>X</i> is an observation from a 
<a href="../../stats/help/NegBinomial.html">negative binomial distribution</a> with 
parameters <code>prob=</code><i>p</i> and <code>size=</code><i>K</i>, where
</p>
<p style="text-align: center;"><i>K = &sum;^n_{i = 1} k_i</i></p>

<p><em>Estimation</em> <br />
The maximum likelihood and method of moments estimator (mle/mme) of 
<i>p</i> is given by:
</p>
<p style="text-align: center;"><i>\hat{p}_{mle} = \frac{K}{X + K}</i></p>

<p>and the minimum variance unbiased estimator (mvue) of <i>p</i> is given by:
</p>
<p style="text-align: center;"><i>\hat{p}_{mvue} = \frac{K - 1}{X + K - 1}</i></p>

<p>(Forbes et al., 2011).  Note that the mvue of <i>p</i> is not defined for 
<i>K=1</i>.
</p>


<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other information. <br /> 
See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/help/NegBinomial.html">negative binomial distribution</a> has its roots in 
a gambling game where participants would bet on the number of tosses of a 
coin necessary to achieve a fixed number of heads.  The negative binomial 
distribution has been applied in a wide variety of fields, including accident 
statistics, birth-and-death processes, and modeling spatial distributions of 
biological organisms.
</p>
<p>The <a href="../../stats/help/Geometric.html">geometric distribution</a> with parameter <code>prob=</code><i>p</i> 
is a special case of the negative binomial distribution with parameters 
<code>size=1</code> and <code>prob=</code><i>p</i>.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and A. Kemp. (1992).  
<em>Univariate Discrete Distributions</em>.  Second Edition. John Wiley and Sons, 
New York, Chapter 5.
</p>


<h3>See Also</h3>

<p><a href="../../stats/help/NegBinomial.html">NegBinomial</a>, <code><a href="../../EnvStats/help/egeom.html">egeom</a></code>, <a href="../../stats/help/Geometric.html">Geometric</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate an observation from a negative binomial distribution with 
  # parameters size=2 and prob=0.2, then estimate the parameter prob. 
  # Note: the call to set.seed simply allows you to reproduce this example. 
  # Also, the only parameter that is estimated is prob; the parameter 
  # size is supplied in the call to enbinom.  The parameter size is printed in 
  # order to show all of the parameters associated with the distribution.

  set.seed(250) 
  dat &lt;- rnbinom(1, size = 2, prob = 0.2) 
  dat
  #[1] 5

  enbinom(dat, size = 2)
  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Negative Binomial
  #
  #Estimated Parameter(s):          size = 2.0000000
  #                                 prob = 0.2857143
  #
  #Estimation Method:               mle/mme for 'prob'
  #
  #Data:                            dat, 2
  #
  #Sample Size:                     1

  #----------

  # Generate 3 observations from negative binomial distributions with 
  # parameters size=c(2,3,4) and prob=0.2, then estimate the parameter 
  # prob using the mvue. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  size.vec &lt;- 2:4 
  set.seed(250) 
  dat &lt;- rnbinom(3, size = size.vec, prob = 0.2) 
  dat 
  #[1]  5 19 12 

  enbinom(dat, size = size.vec, method = "mvue") 
  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Negative Binomial
  #
  #Estimated Parameter(s):          size = 9.0000000
  #                                 prob = 0.1818182
  #
  #Estimation Method:               mvue for 'prob'
  #
  #Data:                            dat, size.vec
  #
  #Sample Size:                     3

  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
