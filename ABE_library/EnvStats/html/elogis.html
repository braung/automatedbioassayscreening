<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Parameters of a Logistic Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for elogis {EnvStats}"><tr><td>elogis {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Parameters of a Logistic Distribution
</h2>

<h3>Description</h3>

<p>Estimate the location and scale parameters of a 
<a href="../../stats/help/Logistic.html">logistic distribution</a>, and optionally construct a 
confidence interval for the location parameter.
</p>


<h3>Usage</h3>

<pre>
  elogis(x, method = "mle", ci = FALSE, ci.type = "two-sided", 
    ci.method = "normal.approx", conf.level = 0.95)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  Possible values are 
<code>"mle"</code> (maximum likelihood; the default), <code>"mme"</code> (methods of moments), 
and <code>"mmue"</code> (method of moments based on the unbiased estimator of variance).  
See the DETAILS section for more information on these estimation methods. 
</p>
</td></tr>
<tr valign="top"><td><code>ci</code></td>
<td>

<p>logical scalar indicating whether to compute a confidence interval for the 
location or scale parameter.  The default value is <code>FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.type</code></td>
<td>

<p>character string indicating what kind of confidence interval to compute.  The 
possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.method</code></td>
<td>

<p>character string indicating what method to use to construct the confidence interval 
for the location or scale parameter.  Currently, the only possible value is 
<code>"normal.approx"</code> (the default).  See the DETAILS section for more information.  
This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the confidence interval.  
The default value is <code>conf.level=0.95</code>. This argument is ignored if 
<code>ci=FALSE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of 
<i>n</i> observations from an <a href="../../stats/help/Logistic.html">logistic distribution</a> with 
parameters <code>location=</code><i>&eta;</i> and <code>scale=</code><i>&theta;</i>.
</p>
<p><b>Estimation</b> <br />
</p>
<p><em>Maximum Likelihood Estimation</em> (<code>method="mle"</code>) <br />
The maximum likelihood estimators (mle's) of <i>&eta;</i> and <i>&theta;</i> are 
the solutions of the simultaneous equations (Forbes et al., 2011):
</p>
<p style="text-align: center;"><i>&sum;_{i=1}^{n} \frac{1}{1 + e^{z_i}} = \frac{n}{2} \;\;\;\; (1)</i></p>

<p style="text-align: center;"><i>&sum;_{i=1}^{n} z_i \, [\frac{1 - e^{z_i}}{1 + e^{z_i}} = n \;\;\;\; (2)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>z_i = \frac{x_i - \hat{eta}_{mle}}{\hat{&theta;}_{mle}} \;\;\;\; (3)</i></p>

<p><em>Method of Moments Estimation</em> (<code>method="mme"</code>) <br />
The method of moments estimators (mme's) of <i>&eta;</i> and <i>&theta;</i> are 
given by:
</p>
<p style="text-align: center;"><i>\hat{&eta;}_{mme} = \bar{x} \;\;\;\; (4)</i></p>

<p style="text-align: center;"><i>\hat{&theta;}_{mme} = \frac{&radic;{3}}{&pi;} s_m \;\;\;\; (5)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>\bar{x} = &sum;_{i=1}^n x_i \;\;\;\; (6)</i></p>

<p style="text-align: center;"><i>s_m^2 = \frac{1}{n} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\; (7)</i></p>

<p>that is, <i>s_m</i> denotes the square root of the method of moments estimator 
of variance.
</p>
<p><em>Method of Moments Estimators Based on the Unbiased Estimator of Variance</em> (<code>method="mmue"</code>) <br />
These estimators are exactly the same as the method of moments estimators given in 
equations (4-7) above, except that the method of moments estimator of variance in 
equation (7) is replaced with the unbiased estimator of variance:
</p>
<p style="text-align: center;"><i>s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\; (8)</i></p>

<p><br />
</p>
<p><b>Confidence Intervals</b> <br />
When <code>ci=TRUE</code>, an approximate <i>(1-&alpha;)</i>100% confidence intervals 
for <i>&eta;</i> can be constructed assuming the distribution of the estimator of 
<i>&eta;</i> is approximately normally distributed.  A two-sided confidence 
interval is constructed as:
</p>
<p style="text-align: center;"><i>[\hat{&eta;} - t(n-1, 1-&alpha;/2) \hat{&sigma;}_{\hat{&eta;}}, \, \hat{&eta;} + t(n-1, 1-&alpha;/2) \hat{&sigma;}_{\hat{&eta;}}]</i></p>

<p>where <i>t(&nu;, p)</i> is the <i>p</i>'th quantile of 
<a href="../../stats/help/TDist.html">Student's t-distribution</a> with 
<i>&nu;</i> degrees of freedom, and the quantity 
</p>
<p style="text-align: center;"><i>\hat{&sigma;}_{\hat{&eta;}} = \frac{&pi; \hat{&theta;}}{&radic;{3n}} \;\;\;\; (9)</i></p>

<p>denotes the estimated asymptotic standard deviation of the estimator of <i>&eta;</i>.
</p>
<p>One-sided confidence intervals for <i>&eta;</i> and <i>&theta;</i> are computed in 
a similar fashion.
</p>


<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other information. <br /> 
See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/help/Logistic.html">logistic distribution</a> is defined on the real line 
and is unimodal and symmetric about its location parameter (the mean).  It has 
longer tails than a normal (Gaussian) distribution.  It is used to model growth 
curves and bioassay data.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995). 
<em>Continuous Univariate Distributions, Volume 2</em>. 
Second Edition. John Wiley and Sons, New York.
</p>


<h3>See Also</h3>

<p><a href="../../stats/help/Logistic.html">Logistic</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a logistic distribution with 
  # parameters location=0 and scale=1, then estimate the parameters 
  # and construct a 90% confidence interval for the location parameter. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rlogis(20) 
  elogis(dat, ci = TRUE, conf.level = 0.9) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Logistic
  #
  #Estimated Parameter(s):          location = -0.2181845
  #                                 scale    =  0.8152793
  #
  #Estimation Method:               mle
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Confidence Interval for:         location
  #
  #Confidence Interval Method:      Normal Approximation
  #                                 (t Distribution)
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                90%
  #
  #Confidence Interval:             LCL = -0.7899382
  #                                 UCL =  0.3535693
  
  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
