<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Shape and Scale Parameters for a Gamma Distribution...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for egammaCensored {EnvStats}"><tr><td>egammaCensored {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Shape and Scale Parameters for a Gamma Distribution Based on Type I Censored Data
</h2>

<h3>Description</h3>

<p>Estimate the shape and scale parameters of a
<a href="../../stats/help/GammaDist.html">gamma distribution</a> given a
sample of data that has been subjected to Type I censoring, and optionally
construct a confidence interval for the mean.
</p>


<h3>Usage</h3>

<pre>
  egammaCensored(x, censored, method = "mle", censoring.side = "left",
    ci = FALSE, ci.method = "profile.likelihood", ci.type = "two-sided",
    conf.level = 0.95, n.bootstraps = 1000, pivot.statistic = "z",
    ci.sample.size = sum(!censored))
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.  Missing (<code>NA</code>), undefined (<code>NaN</code>), and
infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>censored</code></td>
<td>

<p>numeric or logical vector indicating which values of <code>x</code> are censored.
This must be the same length as <code>x</code>.  If the mode of <code>censored</code> is
<code>"logical"</code>, <code>TRUE</code> values correspond to elements of <code>x</code> that
are censored, and <code>FALSE</code> values correspond to elements of <code>x</code> that
are not censored.  If the mode of <code>censored</code> is <code>"numeric"</code>,
it must contain only <code>1</code>'s and <code>0</code>'s; <code>1</code> corresponds to
<code>TRUE</code> and <code>0</code> corresponds to <code>FALSE</code>.  Missing (<code>NA</code>)
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  Currently, the only
available method is maximum likelihood (<code>method="mle"</code>).
</p>
</td></tr>
<tr valign="top"><td><code>censoring.side</code></td>
<td>

<p>character string indicating on which side the censoring occurs.  The possible
values are <code>"left"</code> (the default) and <code>"right"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci</code></td>
<td>

<p>logical scalar indicating whether to compute a confidence interval for the
mean.  The default value is <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.method</code></td>
<td>

<p>character string indicating what method to use to construct the confidence interval
for the mean.  The possible values are <code>"profile.likelihood"</code>
(profile likelihood; the default),
<code>"normal.approx"</code> (normal approximation),
and
<code>"bootstrap"</code> (based on bootstrapping).
See the DETAILS section for more information.
This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.type</code></td>
<td>

<p>character string indicating what kind of confidence interval to compute.  The
possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and
<code>"upper"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the confidence interval.
The default value is <code>conf.level=0.95</code>. This argument is ignored if
<code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>n.bootstraps</code></td>
<td>

<p>numeric scalar indicating how many bootstraps to use to construct the
confidence interval for the mean when <code>ci.type="bootstrap"</code>.  This
argument is ignored if <code>ci=FALSE</code> and/or <code>ci.method</code> does not
equal <code>"bootstrap"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>pivot.statistic</code></td>
<td>

<p>character string indicating which pivot statistic to use in the construction
of the confidence interval for the mean when <code>ci.method="normal.approx"</code> or
<code>ci.method="normal.approx.w.cov"</code> (see the DETAILS section).  The possible
values are <code>pivot.statistic="z"</code> (the default) and <code>pivot.statistic="t"</code>.
When <code>pivot.statistic="t"</code> you may supply the argument
<code>ci.sample size</code> (see below).  The argument <code>pivot.statistic</code> is
ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.sample.size</code></td>
<td>

<p>numeric scalar indicating what sample size to assume to construct the
confidence interval for the mean if <code>pivot.statistic="t"</code> and <br />
<code>ci.method="normal.approx"</code>.  The default value is the number of
uncensored observations.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> or <code>censored</code> contain any missing (<code>NA</code>), undefined (<code>NaN</code>) or
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to
performing the estimation.
</p>
<p>Let <i>\underline{x}</i> denote a vector of <i>N</i> observations from a
<a href="../../stats/help/GammaDist.html">gamma distribution</a> with parameters
<code>shape=</code><i>&kappa;</i> and <code>scale=</code><i>&theta;</i>.
The relationship between these parameters and the mean <i>&mu;</i>
and coefficient of variation <i>&tau;</i> of this distribution is given by:
</p>
<p style="text-align: center;"><i>&kappa; = &tau;^{-2} \;\;\;\;\;\; (1)</i></p>

<p style="text-align: center;"><i>&theta; = &mu;/&kappa; \;\;\;\;\;\; (2)</i></p>

<p style="text-align: center;"><i>&mu; = &kappa; \; &theta; \;\;\;\;\;\; (3)</i></p>

<p style="text-align: center;"><i>&tau; = &kappa;^{-1/2} \;\;\;\;\;\; (4)</i></p>

<p>Assume <i>n</i> (<i>0 &lt; n &lt; N</i>) of these
observations are known and <i>c</i> (<i>c=N-n</i>) of these observations are
all censored below (left-censored) or all censored above (right-censored) at
<i>k</i> fixed censoring levels
</p>
<p style="text-align: center;"><i>T_1, T_2, &hellip;, T_k; \; k &ge; 1 \;\;\;\;\;\; (5)</i></p>

<p>For the case when <i>k &ge; 2</i>, the data are said to be Type I
<b><em>multiply censored</em></b>.  For the case when <i>k=1</i>,
set <i>T = T_1</i>.  If the data are left-censored
and all <i>n</i> known observations are greater
than or equal to <i>T</i>, or if the data are right-censored and all <i>n</i>
known observations are less than or equal to <i>T</i>, then the data are
said to be Type I <b><em>singly censored</em></b> (Nelson, 1982, p.7), otherwise
they are considered to be Type I multiply censored.
</p>
<p>Let <i>c_j</i> denote the number of observations censored below or above censoring
level <i>T_j</i> for <i>j = 1, 2, &hellip;, k</i>, so that
</p>
<p style="text-align: center;"><i>&sum;_{i=1}^k c_j = c \;\;\;\;\;\; (6)</i></p>

<p>Let <i>x_{(1)}, x_{(2)}, &hellip;, x_{(N)}</i> denote the &ldquo;ordered&rdquo; observations,
where now &ldquo;observation&rdquo; means either the actual observation (for uncensored
observations) or the censoring level (for censored observations).  For
right-censored data, if a censored observation has the same value as an
uncensored one, the uncensored observation should be placed first.
For left-censored data, if a censored observation has the same value as an
uncensored one, the censored observation should be placed first.
</p>
<p>Note that in this case the quantity <i>x_{(i)}</i> does not necessarily represent
the <i>i</i>'th &ldquo;largest&rdquo; observation from the (unknown) complete sample.
</p>
<p>Finally, let <i>&Omega;</i> (omega) denote the set of <i>n</i> subscripts in the
&ldquo;ordered&rdquo; sample that correspond to uncensored observations.
<br />
</p>
<p><b>Estimation</b> <br />
</p>
<p><em>Maximum Likelihood Estimation</em> (<code>method="mle"</code>) <br />
For Type I left censored data, the likelihood function is given by:
</p>
<p style="text-align: center;"><i>L(&kappa;, &theta; | \underline{x}) = {N \choose c_1 c_2 &hellip; c_k n} &prod;_{j=1}^k [F(T_j)]^{c_j} &prod;_{i \in &Omega;} f[x_{(i)}] \;\;\;\;\;\; (7)</i></p>

<p>where <i>f</i> and <i>F</i> denote the probability density function (pdf) and
cumulative distribution function (cdf) of the population
(Cohen, 1963; Cohen, 1991, pp.6, 50).  That is,
</p>
<p style="text-align: center;"><i>f(t) =  \frac{t^{&kappa;-1} e^{-t/&theta;}}{&theta;^&kappa; &Gamma;(&kappa;)} \;\;\;\;\;\; (8)</i></p>

<p>(Johnson et al., 1994, p.343).  For left singly censored data, Equation (7)
simplifies to:
</p>
<p style="text-align: center;"><i>L(&kappa;, &theta; | \underline{x}) = {N \choose c} [F(T)]^{c} &prod;_{i = c+1}^n f[x_{(i)}] \;\;\;\;\;\; (9)</i></p>

<p>Similarly, for Type I right censored data, the likelihood function is given by:
</p>
<p style="text-align: center;"><i>L(&kappa;, &theta; | \underline{x}) = {N \choose c_1 c_2 &hellip; c_k n} &prod;_{j=1}^k [1 - F(T_j)]^{c_j} &prod;_{i \in &Omega;} f[x_{(i)}] \;\;\;\;\;\; (10)</i></p>

<p>and for right singly censored data this simplifies to:
</p>
<p style="text-align: center;"><i>L(&kappa;, &theta; | \underline{x}) = {N \choose c} [1 - F(T)]^{c} &prod;_{i = 1}^n f[x_{(i)}] \;\;\;\;\;\; (11)</i></p>

<p>The maximum likelihood estimators are computed by minimizing the
negative log-likelihood function.
<br />
</p>
<p><b>Confidence Intervals</b> <br />
This section explains how confidence intervals for the mean <i>&mu;</i> are
computed.
<br />
</p>
<p><em>Likelihood Profile</em> (<code>ci.method="profile.likelihood"</code>) <br />
This method was proposed by Cox (1970, p.88), and Venzon and Moolgavkar (1988)
introduced an efficient method of computation.  This method is also discussed by
Stryhn and Christensen (2003) and Royston (2007).
The idea behind this method is to invert the likelihood-ratio test to obtain a
confidence interval for the mean <i>&mu;</i> while treating the coefficient of variation
<i>&tau;</i> as a nuisance parameter.  Equation (7) above
shows the form of the likelihood function <i>L(&mu;, &tau; | \underline{x})</i> for
multiply left-censored data and Equation (10) shows the function for
multiply right-censored data, where <i>&mu;</i> and <i>&tau;</i> are defined by
Equations (3) and (4).
</p>
<p>Following Stryhn and Christensen (2003), denote the maximum likelihood estimates
of the mean and coefficient of variation by <i>(&mu;^*, &tau;^*)</i>.  The likelihood
ratio test statistic (<i>G^2</i>) of the hypothesis <i>H_0: &mu; = &mu;_0</i>
(where <i>&mu;_0</i> is a fixed value) equals the drop in <i>2 log(L)</i> between the
&ldquo;full&rdquo; model and the reduced model with <i>&mu;</i> fixed at <i>&mu;_0</i>, i.e.,
</p>
<p style="text-align: center;"><i>G^2 = 2 \{log[L(&mu;^*, &tau;^*)] - log[L(&mu;_0, &tau;_0^*)]\} \;\;\;\;\;\; (12)</i></p>

<p>where <i>&tau;_0^*</i> is the maximum likelihood estimate of <i>&tau;</i> for the
reduced model (i.e., when <i>&mu; = &mu;_0</i>).  Under the null hypothesis,
the test statistic <i>G^2</i> follows a
<a href="../../stats/help/Chisquare.html">chi-squared distribution</a> with 1 degree of freedom.
</p>
<p>Alternatively, we may
express the test statistic in terms of the profile likelihood function <i>L_1</i>
for the mean <i>&mu;</i>, which is obtained from the usual likelihood function by
maximizing over the parameter <i>&tau;</i>, i.e.,
</p>
<p style="text-align: center;"><i>L_1(&mu;) = max_{&tau;} L(&mu;, &tau;) \;\;\;\;\;\; (13)</i></p>

<p>Then we have
</p>
<p style="text-align: center;"><i>G^2 = 2 \{log[L_1(&mu;^*)] - log[L_1(&mu;_0)]\} \;\;\;\;\;\; (14)</i></p>

<p>A two-sided <i>(1-&alpha;)100\%</i> confidence interval for the mean <i>&mu;</i>
consists of all values of <i>&mu;_0</i> for which the test is not significant at
level <i>alpha</i>:
</p>
<p style="text-align: center;"><i>&mu;_0: G^2 &le; &chi;^2_{1, {1-&alpha;}} \;\;\;\;\;\; (15)</i></p>

<p>where <i>&chi;^2_{&nu;, p}</i> denotes the <i>p</i>'th quantile of the
<a href="../../stats/help/Chisquare.html">chi-squared distribution</a> with <i>&nu;</i> degrees of freedom.
One-sided lower and one-sided upper confidence intervals are computed in a similar
fashion, except that the quantity <i>1-&alpha;</i> in Equation (15) is replaced with
<i>1-2&alpha;</i>.
<br />
</p>
<p><em>Normal Approximation</em> (<code>ci.method="normal.approx"</code>) <br />
This method constructs approximate <i>(1-&alpha;)100\%</i> confidence intervals for
<i>&mu;</i> based on the assumption that the estimator of <i>&mu;</i> is
approximately normally distributed.  That is, a two-sided <i>(1-&alpha;)100\%</i>
confidence interval for <i>&mu;</i> is constructed as:
</p>
<p style="text-align: center;"><i>[\hat{&mu;} - t_{1-&alpha;/2, m-1}\hat{&sigma;}_{\hat{&mu;}}, \; \hat{&mu;} + t_{1-&alpha;/2, m-1}\hat{&sigma;}_{\hat{&mu;}}] \;\;\;\; (16)</i></p>

<p>where <i>\hat{&mu;}</i> denotes the estimate of <i>&mu;</i>,
<i>\hat{&sigma;}_{\hat{&mu;}}</i> denotes the estimated asymptotic standard
deviation of the estimator of <i>&mu;</i>, <i>m</i> denotes the assumed sample
size for the confidence interval, and <i>t_{p,&nu;}</i> denotes the <i>p</i>'th
quantile of <a href="../../stats/help/TDist.html">Student's t-distribuiton</a> with <i>&nu;</i>
degrees of freedom.  One-sided confidence intervals are computed in a
similar fashion.
</p>
<p>The argument <code>ci.sample.size</code> determines the value of <i>m</i> and by
default is equal to the number of uncensored observations.
This is simply an ad-hoc method of constructing
confidence intervals and is not based on any published theoretical results.
</p>
<p>When <code>pivot.statistic="z"</code>, the <i>p</i>'th quantile from the
<a href="../../stats/help/Normal.html">standard normal distribution</a> is used in place of the
<i>p</i>'th quantile from Student's t-distribution.
</p>
<p>The standard deviation of the mle of <i>&mu;</i> is
estimated based on the inverse of the Fisher Information matrix.
<br />
</p>
<p><em>Bootstrap and Bias-Corrected Bootstrap Approximation</em> (<code>ci.method="bootstrap"</code>) <br />
The bootstrap is a nonparametric method of estimating the distribution
(and associated distribution parameters and quantiles) of a sample statistic,
regardless of the distribution of the population from which the sample was drawn.
The bootstrap was introduced by Efron (1979) and a general reference is
Efron and Tibshirani (1993).
</p>
<p>In the context of deriving an approximate <i>(1-&alpha;)100\%</i> confidence
interval for the population mean <i>&mu;</i>, the bootstrap can be broken down into the
following steps:
</p>

<ol>
<li><p> Create a bootstrap sample by taking a random sample of size <i>N</i> from
the observations in <i>\underline{x}</i>, where sampling is done with
replacement.  Note that because sampling is done with replacement, the same
element of <i>\underline{x}</i> can appear more than once in the bootstrap
sample.  Thus, the bootstrap sample will usually not look exactly like the
original sample (e.g., the number of censored observations in the bootstrap
sample will often differ from the number of censored observations in the
original sample).
</p>
</li>
<li><p> Estimate <i>&mu;</i> based on the bootstrap sample created in Step 1, using
the same method that was used to estimate <i>&mu;</i> using the original
observations in <i>\underline{x}</i>. Because the bootstrap sample usually
does not match the original sample, the estimate of <i>&mu;</i> based on the
bootstrap sample will usually differ from the original estimate based on
<i>\underline{x}</i>.
</p>
</li>
<li><p> Repeat Steps 1 and 2 <i>B</i> times, where <i>B</i> is some large number.
For the function <code>egammaCensored</code>, the number of bootstraps <i>B</i> is
determined by the argument <code>n.bootstraps</code> (see the section ARGUMENTS above).
The default value of <code>n.bootstraps</code> is <code>1000</code>.
</p>
</li>
<li><p> Use the <i>B</i> estimated values of <i>&mu;</i> to compute the empirical
cumulative distribution function of this estimator of <i>&mu;</i> (see
<code><a href="../../EnvStats/help/ecdfPlot.html">ecdfPlot</a></code>), and then create a confidence interval for <i>&mu;</i>
based on this estimated cdf.
</p>
</li></ol>

<p>The two-sided percentile interval (Efron and Tibshirani, 1993, p.170) is computed as:
</p>
<p style="text-align: center;"><i>[\hat{G}^{-1}(\frac{&alpha;}{2}), \; \hat{G}^{-1}(1-\frac{&alpha;}{2})] \;\;\;\;\;\; (17)</i></p>

<p>where <i>\hat{G}(t)</i> denotes the empirical cdf evaluated at <i>t</i> and thus
<i>\hat{G}^{-1}(p)</i> denotes the <i>p</i>'th empirical quantile, that is,
the <i>p</i>'th quantile associated with the empirical cdf.  Similarly, a one-sided lower
confidence interval is computed as:
</p>
<p style="text-align: center;"><i>[\hat{G}^{-1}(&alpha;), \; &infin;] \;\;\;\;\;\; (18)</i></p>

<p>and a one-sided upper confidence interval is computed as:
</p>
<p style="text-align: center;"><i>[0, \; \hat{G}^{-1}(1-&alpha;)] \;\;\;\;\;\; (19)</i></p>

<p>The function <code>egammaCensored</code> calls the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> function <code><a href="../../stats/help/quantile.html">quantile</a></code>
to compute the empirical quantiles used in Equations (17)-(19).
</p>
<p>The percentile method bootstrap confidence interval is only first-order
accurate (Efron and Tibshirani, 1993, pp.187-188), meaning that the probability
that the confidence interval will contain the true value of <i>&mu;</i> can be
off by <i>k/&radic;{N}</i>, where <i>k</i>is some constant.  Efron and Tibshirani
(1993, pp.184-188) proposed a bias-corrected and accelerated interval that is
second-order accurate, meaning that the probability that the confidence interval
will contain the true value of <i>&mu;</i> may be off by <i>k/N</i> instead of
<i>k/&radic;{N}</i>.  The two-sided bias-corrected and accelerated confidence interval is
computed as:
</p>
<p style="text-align: center;"><i>[\hat{G}^{-1}(&alpha;_1), \; \hat{G}^{-1}(&alpha;_2)] \;\;\;\;\;\; (20)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>&alpha;_1 = &Phi;[\hat{z}_0 + \frac{\hat{z}_0 + z_{&alpha;/2}}{1 - \hat{a}(z_0 + z_{&alpha;/2})}] \;\;\;\;\;\; (21)</i></p>

<p style="text-align: center;"><i>&alpha;_2 = &Phi;[\hat{z}_0 + \frac{\hat{z}_0 + z_{1-&alpha;/2}}{1 - \hat{a}(z_0 + z_{1-&alpha;/2})}] \;\;\;\;\;\; (22)</i></p>

<p style="text-align: center;"><i>\hat{z}_0 = &Phi;^{-1}[\hat{G}(\hat{&mu;})] \;\;\;\;\;\; (23)</i></p>

<p style="text-align: center;"><i>\hat{a} = \frac{&sum;_{i=1}^N (\hat{&mu;}_{(\cdot)} - \hat{&mu;}_{(i)})^3}{6[&sum;_{i=1}^N (\hat{&mu;}_{(\cdot)} - \hat{&mu;}_{(i)})^2]^{3/2}} \;\;\;\;\;\; (24)</i></p>

<p>where the quantity <i>\hat{&mu;}_{(i)}</i> denotes the estimate of <i>&mu;</i> using
all the values in <i>\underline{x}</i> except the <i>i</i>'th one, and
</p>
<p style="text-align: center;"><i>\hat{&mu;}{(\cdot)} = \frac{1}{N} &sum;_{i=1}^N \hat{&mu;_{(i)}} \;\;\;\;\;\; (25)</i></p>

<p>A one-sided lower confidence interval is given by:
</p>
<p style="text-align: center;"><i>[\hat{G}^{-1}(&alpha;_1), \; &infin;] \;\;\;\;\;\; (26)</i></p>

<p>and a one-sided upper confidence interval is given by:
</p>
<p style="text-align: center;"><i>[0, \; \hat{G}^{-1}(&alpha;_2)] \;\;\;\;\;\; (27)</i></p>

<p>where <i>&alpha;_1</i> and <i>&alpha;_2</i> are computed as for a two-sided confidence
interval, except <i>&alpha;/2</i> is replaced with <i>&alpha;</i> in Equations (21) and (22).
</p>
<p>The constant <i>\hat{z}_0</i> incorporates the bias correction, and the constant
<i>\hat{a}</i> is the acceleration constant.  The term &ldquo;acceleration&rdquo; refers
to the rate of change of the standard error of the estimate of <i>&mu;</i> with
respect to the true value of <i>&mu;</i> (Efron and Tibshirani, 1993, p.186).  For a
normal (Gaussian) distribution, the standard error of the estimate of <i>&mu;</i>
does not depend on the value of <i>&mu;</i>, hence the acceleration constant is not
really necessary.
</p>
<p>When <code>ci.method="bootstrap"</code>, the function <code>egammaCensored</code> computes both
the percentile method and bias-corrected and accelerated method
bootstrap confidence intervals.
</p>


<h3>Value</h3>

<p>a list of class <code>"estimateCensored"</code> containing the estimated parameters
and other information.  See <code><a href="../../EnvStats/help/estimateCensored.object.html">estimateCensored.object</a></code> for details.
</p>


<h3>Note</h3>

<p>A sample of data contains censored observations if some of the observations are
reported only as being below or above some censoring level.  In environmental
data analysis, Type I left-censored data sets are common, with values being
reported as &ldquo;less than the detection limit&rdquo; (e.g., Helsel, 2012).  Data
sets with only one censoring level are called <em>singly censored</em>; data sets with
multiple censoring levels are called <em>multiply</em> or <em>progressively censored</em>.
</p>
<p>Statistical methods for dealing with censored data sets have a long history in the
field of survival analysis and life testing.  More recently, researchers in the
environmental field have proposed alternative methods of computing estimates and
confidence intervals in addition to the classical ones such as maximum likelihood
estimation.  Helsel (2012, Chapter 6) gives an excellent review of past studies
of the properties of various estimators for parameters of a normal or lognormal
distribution based on censored environmental data.
</p>
<p>In practice, it is better to use a confidence interval for the mean or a
joint confidence region for the mean and standard deviation (or coefficient of
variation), rather than rely on a single point-estimate of the mean.
Few studies have been done to evaluate the performance of methods for constructing
confidence intervals for the mean or joint confidence regions for the mean and
coefficient of variation of a gamma distribution when data are subjected to
single or multiple censoring.
See, for example, Singh et al. (2006).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Cohen, A.C. (1963).  Progressively Censored Samples in Life Testing.
<em>Technometrics</em> <b>5</b>, 327&ndash;339
</p>
<p>Cohen, A.C. (1991).  <em>Truncated and Censored Samples</em>.  Marcel Dekker,
New York, New York, 312pp.
</p>
<p>Cox, D.R. (1970).  <em>Analysis of Binary Data</em>.  Chapman &amp; Hall, London.  142pp.
</p>
<p>Efron, B. (1979).  Bootstrap Methods: Another Look at the Jackknife.
<em>The Annals of Statistics</em> <b>7</b>, 1&ndash;26.
</p>
<p>Efron, B., and R.J. Tibshirani. (1993).  <em>An Introduction to the Bootstrap</em>.
Chapman and Hall, New York, 436pp.
</p>
<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).
<em>Statistical Distributions, Fourth Edition</em>.
John Wiley and Sons, Hoboken, NJ.
</p>
<p>Helsel, D.R. (2012). <em>Statistics for Censored Environmental Data Using Minitab and R,
Second Edition</em>.  John Wiley &amp; Sons, Hoboken, New Jersey.
</p>
<p>Johnson, N.L., S. Kotz, and N. Balakrishnan. (1994).
<em>Continuous Univariate Distributions, Volume 1</em>. Second Edition.
John Wiley and Sons, New York, Chapter 17.
</p>
<p>Millard, S.P., P. Dixon, and N.K. Neerchal. (2014; in preparation).
<em>Environmental Statistics with R</em>.  CRC Press, Boca Raton, Florida.
</p>
<p>Nelson, W. (1982).  <em>Applied Life Data Analysis</em>.
John Wiley and Sons, New York, 634pp.
</p>
<p>Royston, P. (2007).  Profile Likelihood for Estimation and Confdence Intervals.
<em>The Stata Journal</em> <b>7</b>(3), pp. 376&ndash;387.
</p>
<p>Singh, A., R. Maichle, and S. Lee. (2006).  <em>On the Computation of a 95%
Upper Confidence Limit of the Unknown Population Mean Based Upon Data Sets
with Below Detection Limit Observations</em>.  EPA/600/R-06/022, March 2006.
Office of Research and Development, U.S. Environmental Protection Agency,
Washington, D.C.
</p>
<p>Stryhn, H., and J. Christensen. (2003).  <em>Confidence Intervals by the Profile
Likelihood Method, with Applications in Veterinary Epidemiology</em>.  Contributed paper
at ISVEE X (November 2003, Chile).
<a href="https://gilvanguedes.com/wp-content/uploads/2019/05/Profile-Likelihood-CI.pdf">https://gilvanguedes.com/wp-content/uploads/2019/05/Profile-Likelihood-CI.pdf</a>.
</p>
<p>Venzon, D.J., and S.H. Moolgavkar. (1988).  A Method for Computing
Profile-Likelihood-Based Confidence Intervals.  <em>Journal of the Royal
Statistical Society, Series C (Applied Statistics)</em> <b>37</b>(1), pp. 87&ndash;94.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/egammaAltCensored.html">egammaAltCensored</a></code>, <a href="../../stats/help/GammaDist.html">GammaDist</a>, <code><a href="../../EnvStats/help/egamma.html">egamma</a></code>,
<code><a href="../../EnvStats/help/estimateCensored.object.html">estimateCensored.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Chapter 15 of USEPA (2009) gives several examples of estimating the mean
  # and standard deviation of a lognormal distribution on the log-scale using
  # manganese concentrations (ppb) in groundwater at five background wells.
  # In EnvStats these data are stored in the data frame
  # EPA.09.Ex.15.1.manganese.df.

  # Here we will estimate the shape and scale parameters using
  # the data ON THE ORIGINAL SCALE, using the MLE and
  # assuming a gamma distribution.

  # First look at the data:
  #-----------------------

  EPA.09.Ex.15.1.manganese.df

  #   Sample   Well Manganese.Orig.ppb Manganese.ppb Censored
  #1       1 Well.1                 &lt;5           5.0     TRUE
  #2       2 Well.1               12.1          12.1    FALSE
  #3       3 Well.1               16.9          16.9    FALSE
  #...
  #23      3 Well.5                3.3           3.3    FALSE
  #24      4 Well.5                8.4           8.4    FALSE
  #25      5 Well.5                 &lt;2           2.0     TRUE

  longToWide(EPA.09.Ex.15.1.manganese.df,
    "Manganese.Orig.ppb", "Sample", "Well",
    paste.row.name = TRUE)

  #         Well.1 Well.2 Well.3 Well.4 Well.5
  #Sample.1     &lt;5     &lt;5     &lt;5    6.3   17.9
  #Sample.2   12.1    7.7    5.3   11.9   22.7
  #Sample.3   16.9   53.6   12.6     10    3.3
  #Sample.4   21.6    9.5  106.3     &lt;2    8.4
  #Sample.5     &lt;2   45.9   34.5   77.2     &lt;2


  # Now estimate the shape and scale parameters
  # using the MLE, and compute a confidence interval
  # for the mean using the profile-likelihood method.
  #---------------------------------------------------

  with(EPA.09.Ex.15.1.manganese.df,
    egammaCensored(Manganese.ppb, Censored, ci = TRUE))

  #Results of Distribution Parameter Estimation
  #Based on Type I Censored Data
  #--------------------------------------------
  #
  #Assumed Distribution:            Gamma
  #
  #Censoring Side:                  left
  #
  #Censoring Level(s):              2 5
  #
  #Estimated Parameter(s):          shape =  0.6370043
  #                                 scale = 30.8707533
  #
  #Estimation Method:               MLE
  #
  #Data:                            Manganese.ppb
  #
  #Censoring Variable:              Censored
  #
  #Sample Size:                     25
  #
  #Percent Censored:                24%
  #
  #Confidence Interval for:         mean
  #
  #Confidence Interval Method:      Profile Likelihood
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL = 12.25151
  #                                 UCL = 34.35332

  #----------

  # Compare the confidence interval for the mean
  # based on assuming a lognormal distribution versus
  # assuming a gamma distribution.

  with(EPA.09.Ex.15.1.manganese.df,
    elnormAltCensored(Manganese.ppb, Censored,
      ci = TRUE))$interval$limits
  #     LCL      UCL
  #12.37629 69.87694

  with(EPA.09.Ex.15.1.manganese.df,
    egammaCensored(Manganese.ppb, Censored,
      ci = TRUE))$interval$limits
  #     LCL      UCL
  #12.25151 34.35332
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
