<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Probability-Weighted Moments</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for pwMoment {EnvStats}"><tr><td>pwMoment {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Probability-Weighted Moments
</h2>

<h3>Description</h3>

<p>Estimate the <i>1jk</i>'th probability-weighted moment from a random sample, 
where either <i>j = 0</i>, <i>k = 0</i>, or both.
</p>


<h3>Usage</h3>

<pre>
  pwMoment(x, j = 0, k = 0, method = "unbiased", 
    plot.pos.cons = c(a = 0.35, b = 0), na.rm = FALSE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations. 
</p>
</td></tr>
<tr valign="top"><td><code>j, k</code></td>
<td>

<p>non-negative integers specifying the order of the moment.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying what method to use to compute the 
probability-weighted moment.  The possible values are <code>"unbiased"</code> 
(method based on the U-statistic; the default), or <code>"plotting.position"</code> 
(method based on the plotting position formula).  See the DETAILS section for 
more information.
</p>
</td></tr>
<tr valign="top"><td><code>plot.pos.cons</code></td>
<td>

<p>numeric vector of length 2 specifying the constants used in the formula for the 
plotting positions when <code>method="plotting.position"</code>.  The default value is 
<code>plot.pos.cons=c(a=0.35, b=0)</code>.  If this vector has a names attribute with 
the value <code>c("a","b")</code> or <code>c("b","a")</code>, then the elements will be 
matched by name in the formula for computing the plotting positions.  Otherwise, 
the first element is mapped to the name <code>"a"</code> and the second element to the 
name <code>"b"</code>.  See the DETAILS section for more information.  This argument is 
ignored if <code>method="ubiased"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>na.rm</code></td>
<td>

<p>logical scalar indicating whether to remove missing values from <code>x</code>.  
If <code>na.rm=FALSE</code> (the default) and <code>x</code> contains missing values, 
then a missing value (<code>NA</code>) is returned.  If <code>na.rm=TRUE</code>, missing 
values are removed from <code>x</code> prior to computing the probability-weighted 
moment.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The definition of a probability-weighted moment, introduced by 
Greenwood et al. (1979), is as follows.  Let <i>X</i> denote a random variable 
with cdf <i>F</i>, and let <i>x(p)</i> denote the <i>p</i>'th quantile of the 
distribution.  Then the <i>ijk</i>'th probability-weighted moment is given by:
</p>
<p style="text-align: center;"><i>M(i, j, k) = E[X^i F^j (1 - F)^k] = \int^1_0 [x(F)]^i F^j (1 - F)^k \, dF</i></p>

<p>where <i>i</i>, <i>j</i>, and <i>k</i> are real numbers.  Note that if <i>i</i> is a 
nonnegative integer, then <i>M(i, 0, 0)</i> is the conventional <i>i</i>'th moment 
about the origin.
</p>
<p>Greenwood et al. (1979) state that in the special case where <i>i</i>, <i>j</i>, and 
<i>k</i> are nonnegative integers:
</p>
<p style="text-align: center;"><i>M(i, j, k) = B(j + 1, k + 1) E[X^i_{j+1, j+k+1}]</i></p>

<p>where <i>B(a, b)</i> denotes the <a href="../../stats/help/Beta.html">beta function</a> evaluated at 
<i>a</i> and <i>b</i>, and 
</p>
<p style="text-align: center;"><i>E[X^i_{j+1, j+k+1}]</i></p>
     
<p>denotes the <i>i</i>'th moment about the origin of the <i>(j + 1)</i>'th order 
statistic for a sample of size <i>(j + k + 1)</i>. In particular, 
</p>
<p style="text-align: center;"><i>M(1, 0, k) = \frac{1}{k+1} E[X_{1, k+1}]</i></p>

<p style="text-align: center;"><i>M(1, j, 0) = \frac{1}{j+1} E[X_{j+1, j+1}]</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>E[X_{1, k+1}]</i></p>

<p>denotes the expected value of the first order statistic (i.e., the minimum) in a 
sample of size <i>(k + 1)</i>, and 
</p>
<p style="text-align: center;"><i>E[X_{j+1, j+1}]</i></p>
 
<p>denotes the expected value of the <i>(j+1)</i>'th order statistic (i.e., the maximum) 
in a sample of size <i>(j+1)</i>.
</p>
<p><em>Unbiased Estimators</em> (<code>method="unbiased"</code>) <br />
Landwehr et al. (1979) show that, given a random sample of <i>n</i> values from 
some arbitrary distribution, an unbiased, distribution-free, and parameter-free 
estimator of <i>M(1, 0, k)</i> is given by:
</p>
<p style="text-align: center;"><i>\hat{M}(1, 0, k) = \frac{1}{n} &sum;^{n-k}_{i=1} x_{i,n} \frac{{n-i \choose k}}{{n-1 \choose k}}</i></p>

<p>where the quantity <i>x_{i,n}</i> denotes the <i>i</i>'th order statistic in the 
random sample of size <i>n</i>.  Hosking et al. (1985) note that this estimator is 
closely related to U-statistics (Hoeffding, 1948; Lehmann, 1975, pp. 362-371).  
Hosking et al. (1985) note that an unbiased, distribution-free, and parameter-free 
estimator of <i>M(1, j, 0)</i> is given by:
</p>
<p style="text-align: center;"><i>\hat{M}(1, j, 0) = \frac{1}{n} &sum;^n_{i=j+1} x_{i,n} \frac{{i-1 \choose j}}{{n-1 \choose j}}</i></p>

<p><br />
</p>
<p><em>Plotting-Position Estimators</em> (<code>method="plotting.position"</code>) <br />
Hosking et al. (1985) propose alternative estimators of <i>M(1, 0, k)</i> and 
<i>M(1, j, 0)</i> based on plotting positions:
</p>
<p style="text-align: center;"><i>\hat{M}(1, 0, k) = \frac{1}{n} &sum;^n_{i=1} (1 - p_{i,n})^k x_{i,n}</i></p>

<p style="text-align: center;"><i>\hat{M}(1, j, 0) = \frac{1}{n} &sum;^n_{i=1} p_{i,n}^j x_{i,n}</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>p_{i,n} = \hat{F}(x_{i,n})</i></p>

<p>denotes the plotting position of the <i>i</i>'th order statistic in the random 
sample of size <i>n</i>, that is, a distribution-free estimate of the cdf of 
<i>X</i> evaluated at the <i>i</i>'th order statistic.  Typically, plotting 
positions have the form:
</p>
<p style="text-align: center;"><i>p_{i,n} = \frac{i-a}{n+b}</i></p>

<p>where <i>b &gt; -a &gt; -1</i>.  For this form of plotting position, the 
plotting-position estimators are asymptotically equivalent to the U-statistic 
estimators.
</p>


<h3>Value</h3>

<p>A numeric scalar&ndash;the value of the <i>1jk</i>'th probability-weighted moment 
as defined by Greenwood et al. (1979).
</p>


<h3>Note</h3>

<p>Greenwood et al. (1979) introduced the concept of probability-weighted moments 
as a tool to derive estimates of distribution parameters for distributions that 
can be (perhaps only be) expressed in inverse form.  The term &ldquo;inverse form&rdquo; 
simply means that instead of characterizing the distribution by the formula for 
its cumulative distribution function (cdf), the distribution is characterized by 
the formula for the <i>p</i>'th quantile (<i>0 &le; p &le; 1</i>).
</p>
<p>For distributions that can only be expressed in inverse form, moment estimates of 
their parameters are not available, and maximum likelihood estimates are not easy 
to compute.  Greenwood et al. (1979) show that in these cases, it is often possible 
to derive expressions for the distribution parameters in terms of 
probability-weighted moments.  Thus, for these cases the distribution parameters 
can be estimated based on the sample probability-weighted moments, which are fairly 
easy to compute.  Furthermore, for distributions whose parameters can be expressed 
as functions of conventional moments, the method of probability-weighted moments 
provides an alternative to method of moments and maximum likelihood estimators.
</p>
<p>Landwehr et al. (1979) use the method of probability-weighted moments to estimate 
the parameters of the <a href="../../EnvStats/help/EVD.html">Type I Extreme Value (Gumbel) distribution</a>.
</p>
<p>Hosking et al. (1985) use the method of probability-weighted moments to estimate 
the parameters of the <a href="../../EnvStats/help/GEVD.html">generalized extreme value distribution</a>.
</p>
<p>Hosking (1990) and Hosking and Wallis (1995) show the relationship between 
probabiity-weighted moments and <a href="../../EnvStats/help/lMoment.html">L-moments</a>.
</p>
<p>Hosking and Wallis (1995) recommend using the unbiased estimators of 
probability-weighted moments for almost all applications.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Greenwood, J.A., J.M. Landwehr, N.C. Matalas, and J.R. Wallis. (1979).  
Probability Weighted Moments: Definition and Relation to Parameters of Several 
Distributions Expressible in Inverse Form.  <em>Water Resources Research</em> 
<b>15</b>(5), 1049&ndash;1054. 
</p>
<p>Hoeffding, W. (1948).  A Class of Statistics with Asymptotically Normal 
Distribution.  <em>Annals of Mathematical Statistics</em> <b>19</b>, 293&ndash;325.
</p>
<p>Hosking, J.R.M. (1990).  L-Moments: Analysis and Estimation of Distributions 
Using Linear Combinations of Order Statistics.  <em>Journal of the Royal 
Statistical Society, Series B</em> <b>52</b>(1), 105&ndash;124.
</p>
<p>Hosking, J.R.M., and J.R. Wallis (1995).  A Comparison of Unbiased and 
Plotting-Position Estimators of L Moments.  <em>Water Resources Research</em> 
<b>31</b>(8), 2019&ndash;2025.
</p>
<p>Hosking, J.R.M., J.R. Wallis, and E.F. Wood. (1985).  Estimation of the 
Generalized Extreme-Value Distribution by the Method of 
Probability-Weighted Moments.  <em>Technometrics</em> <b>27</b>(3), 251&ndash;261.
</p>
<p>Landwehr, J.M., N.C. Matalas, and J.R. Wallis. (1979).  Probability Weighted 
Moments Compared With Some Traditional Techniques in Estimating Gumbel 
Parameters and Quantiles.  <em>Water Resources Research</em> <b>15</b>(5), 
1055&ndash;1064.
</p>
<p>Lehmann, E.L. (1975).  <em>Nonparametrics: Statistical Methods Based on Ranks</em>.  
Holden-Day, Oakland, CA, pp.362-371.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/eevd.html">eevd</a></code>, <code><a href="../../EnvStats/help/egevd.html">egevd</a></code>, <code><a href="../../EnvStats/help/lMoment.html">lMoment</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a generalized extreme value distribution 
  # with parameters location=10, scale=2, and shape=.25, then compute the 
  # 0'th, 1'st and 2'nd probability-weighted moments. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rgevd(20, location = 10, scale = 2, shape = 0.25) 

  pwMoment(dat) 
  #[1] 10.59556
 
  pwMoment(dat, 1) 
  #[1] 5.798481
  
  pwMoment(dat, 2) 
  #[1] 4.060574
  
  pwMoment(dat, k = 1) 
  #[1] 4.797081
 
  pwMoment(dat, k = 2) 
  #[1] 3.059173
 
  pwMoment(dat, 1, method = "plotting.position") 
  # [1] 5.852913
 
  pwMoment(dat, 1, method = "plotting.position", 
    plot.pos = c(.325, 1)) 
  #[1] 5.586817 

  #----------

  # Clean Up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
