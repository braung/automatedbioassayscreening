<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Half-Width of Confidence Interval for Normal Distribution...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for ciNormHalfWidth {EnvStats}"><tr><td>ciNormHalfWidth {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Half-Width of Confidence Interval for Normal Distribution Mean or Difference Between Two Means
</h2>

<h3>Description</h3>

<p>Compute the half-width of a confidence interval for the mean of a normal 
distribution or the difference between two means, given the sample size(s), 
estimated standard deviation, and confidence level.
</p>


<h3>Usage</h3>

<pre>
  ciNormHalfWidth(n.or.n1, n2 = n.or.n1, 
    sigma.hat = 1, conf.level = 0.95, 
    sample.type = ifelse(missing(n2), "one.sample", "two.sample"))
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n.or.n1</code></td>
<td>

<p>numeric vector of sample sizes.  When <code>sample.type="one.sample"</code>, 
this argument denotes <i>n</i>, the number of observations in the single sample.  
When <code>sample.type="two.sample"</code>, this argument denotes <i>n_1</i>, 
the number of observations from group 1.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) values are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>n2</code></td>
<td>

<p>numeric vector of sample sizes for group 2.  The default value is the value of <code>n.or.n1</code>.  
This argument is ignored when <code>sample.type="one.sample"</code>.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) values are not allowed. 
</p>
</td></tr>
<tr valign="top"><td><code>sigma.hat</code></td>
<td>

<p>numeric vector specifying the value(s) of the estimated standard deviation(s).
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>numeric vector of numbers between 0 and 1 indicating the confidence level 
associated with the confidence interval(s).  The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>sample.type</code></td>
<td>

<p>character string indicating whether this is a one-sample <br />
(<code>sample.type="one.sample"</code>) or two-sample <br />
(<code>sample.type="two.sample"</code>) confidence interval.  <br />
When <code>sample.type="one.sample"</code>, the computed half-width is based on 
a confidence interval for a single mean.  <br />
When <code>sample.type="two.sample"</code>, the computed half-width is based on 
a confidence interval for the difference between two means.  <br />
The default value is <code>sample.type="one.sample"</code> unless the argument 
<code>n2</code> is supplied.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If the arguments <code>n.or.n1</code>, <code>n2</code>, <code>sigma.hat</code>, and 
<code>conf.level</code> are not all the same length, they are replicated to be the same length 
as the length of the longest argument. 
</p>
<p><em>One-Sample Case</em> (<code>sample.type="one.sample"</code>) <br />
Let <i>\underline{x} = x_1, x_2, &hellip;, x_n</i> denote a vector of <i>n</i> 
observations from a normal distribution with mean <i>&mu;</i> and standard deviation 
<i>&sigma;</i>.  A two-sided <i>(1-&alpha;)100\%</i> confidence interval for <i>&mu;</i> 
is given by:
</p>
<p style="text-align: center;"><i>[\hat{&mu;} - t(n-1, 1-&alpha;/2) \frac{\hat{&sigma;}}{&radic;{n}}, \, \hat{&mu;} + t(n-1, 1-&alpha;/2) \frac{\hat{&sigma;}}{&radic;{n}}] \;\;\;\;\;\; (1)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>\hat{&mu;} = \bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\;\;\; (2)</i></p>

<p style="text-align: center;"><i>\hat{&sigma;}^2 = s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\;\;\; (3)</i></p>

<p>and <i>t(&nu;, p)</i> is the <i>p</i>'th quantile of 
<a href="../../stats/help/TDist.html">Student's t-distribution</a> with <i>&nu;</i> degrees of freedom 
(Zar, 2010; Gilbert, 1987; Ott, 1995; Helsel and Hirsch, 1992).  Thus, the 
half-width of this confidence interval is given by:
</p>
<p style="text-align: center;"><i>HW = t(n-1, 1-&alpha;/2) \frac{\hat{&sigma;}}{&radic;{n}} \;\;\;\;\;\; (4)</i></p>

<p><em>Two-Sample Case</em> (<code>sample.type="two.sample"</code>) <br />
Let <i>\underline{x}_1 = x_{11}, x_{12}, &hellip;, x_{1n_1}</i> denote a vector of 
<i>n_1</i> observations from a normal distribution with mean <i>&mu;_1</i> and 
standard deviation <i>&sigma;</i>, and let 
<i>\underline{x}_2 = x_{21}, x_{22}, &hellip;, x_{2n_2}</i> denote a vector of 
<i>n_2</i> observations from a normal distribution with mean <i>&mu;_2</i> and 
standard deviation <i>&sigma;</i>.  A two-sided <i>(1-&alpha;)100\%</i> confidence 
interval for <i>&mu;_1 - &mu;_2</i> is given by:
</p>
<p style="text-align: center;"><i>[(\hat{&mu;}_1 - \hat{&mu;}_2) - t(n_1 + n_2 - 2, 1-&alpha;/2) \hat{&sigma;} &radic;{\frac{1}{n_1} + \frac{1}{n_2}}, \, (\hat{&mu;}_1 - \hat{&mu;}_2) + t(n_1 + n_2 - 2, 1-&alpha;/2) \hat{&sigma;} &radic;{\frac{1}{n_1} + \frac{1}{n_2}}] \;\;\;\;\;\; (5)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>\hat{&mu;}_1 = \bar{x}_1 = \frac{1}{n_1} &sum;_{i=1}^{n_1} x_{1i} \;\;\;\;\;\; (6)</i></p>

<p style="text-align: center;"><i>\hat{&mu;}_2 = \bar{x}_2 = \frac{1}{n_2} &sum;_{i=1}^{n_2} x_{2i} \;\;\;\;\;\; (7)</i></p>

<p style="text-align: center;"><i>\hat{&sigma;}^2 = s_p^2 = \frac{(n_1 - 1) s_1^2 + (n_2 - 1) s_2^2}{n_1 + n_2 - 2} \;\;\;\;\;\; (8)</i></p>

<p style="text-align: center;"><i>s_1^2 = \frac{1}{n_1 - 1} &sum;_{i=1}^{n_1} (x_{1i} - \bar{x}_1)^2 \;\;\;\;\;\; (9)</i></p>

<p style="text-align: center;"><i>s_2^2 = \frac{1}{n_2 - 1} &sum;_{i=1}^{n_2} (x_{2i} - \bar{x}_2)^2 \;\;\;\;\;\; (10)</i></p>

<p>(Zar, 2010, p.142; Helsel and Hirsch, 1992, p.135, 
Berthouex and Brown, 2002, pp.157&ndash;158).  Thus, the half-width of this confidence 
interval is given by:
</p>
<p style="text-align: center;"><i>HW = t(n_1 + n_2 - 2, 1-&alpha;/2) \hat{&sigma;} &radic;{\frac{1}{n_1} + \frac{1}{n_2}} \;\;\;\;\;\; (11)</i></p>

<p>Note that for the two-sample case, the function <code>ciNormHalfWidth</code> assumes the 
two populations have the same standard deviation.
</p>


<h3>Value</h3>

<p>a numeric vector of half-widths.
</p>


<h3>Note</h3>

<p>The normal distribution and lognormal distribution are probably the two most frequently used 
distributions to model environmental data.  In order to make any kind of probability 
statement about a normally-distributed population (of chemical concentrations for example), 
you have to first estimate the mean and standard deviation (the population parameters) of the 
distribution.  Once you estimate these parameters, it is often useful to characterize the 
uncertainty in the estimate of the mean.  This is done with confidence intervals.
</p>
<p>In the course of designing a sampling program, an environmental scientist may wish to determine 
the relationship between sample size, confidence level, and half-width if one of the objectives 
of the sampling program is to produce confidence intervals.  The functions 
<code>ciNormHalfWidth</code>, <code><a href="../../EnvStats/help/ciNormN.html">ciNormN</a></code>, and <code><a href="../../EnvStats/help/plotCiNormDesign.html">plotCiNormDesign</a></code> 
can be used to investigate these relationships for the case of normally-distributed observations.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). 
<em>Statistics for Environmental Engineers</em>.  Second Edition.   
Lewis Publishers, Boca Raton, FL.
</p>
<p>Gilbert, R.O. (1987). <em>Statistical Methods for Environmental Pollution Monitoring</em>. 
Van Nostrand Reinhold, New York, NY.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). 
<em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York, NY, Chapter 7.
</p>
<p>Millard, S.P., and N. Neerchal. (2001).  <em>Environmental Statistics with S-PLUS</em>.  
CRC Press, Boca Raton, FL.
</p>
<p>Ott, W.R. (1995). <em>Environmental Statistics and Data Analysis</em>. 
Lewis Publishers, Boca Raton, FL.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C. p.21-3.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. 
Fifth Edition. Prentice-Hall, Upper Saddle River, NJ, 
Chapters 7 and 8.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/ciNormN.html">ciNormN</a></code>, <code><a href="../../EnvStats/help/plotCiNormDesign.html">plotCiNormDesign</a></code>, <code><a href="../../stats/html/Normal.html">Normal</a></code>, 
<code><a href="../../EnvStats/help/enorm.html">enorm</a></code>, <code><a href="../../stats/html/t.test.html">t.test</a></code> <br />
<a href="../../EnvStats/help/Estimating+20Distribution+20Parameters.html">Estimating Distribution Parameters</a>. 
</p>


<h3>Examples</h3>

<pre>
  # Look at how the half-width of a one-sample confidence interval 
  # decreases with increasing sample size:

  seq(5, 30, by = 5) 
  #[1] 5 10 15 20 25 30 

  hw &lt;- ciNormHalfWidth(n.or.n1 = seq(5, 30, by = 5)) 

  round(hw, 2) 
  #[1] 1.24 0.72 0.55 0.47 0.41 0.37

  #----------------------------------------------------------------

  # Look at how the half-width of a one-sample confidence interval 
  # increases with increasing estimated standard deviation:

  seq(0.5, 2, by = 0.5) 
  #[1] 0.5 1.0 1.5 2.0 

  hw &lt;- ciNormHalfWidth(n.or.n1 = 20, sigma.hat = seq(0.5, 2, by = 0.5)) 

  round(hw, 2) 
  #[1] 0.23 0.47 0.70 0.94

  #----------------------------------------------------------------

  # Look at how the half-width of a one-sample confidence interval 
  # increases with increasing confidence level:

  seq(0.5, 0.9, by = 0.1) 
  #[1] 0.5 0.6 0.7 0.8 0.9 

  hw &lt;- ciNormHalfWidth(n.or.n1 = 20, conf.level = seq(0.5, 0.9, by = 0.1)) 

  round(hw, 2) 
  #[1] 0.15 0.19 0.24 0.30 0.39

  #==========

  # Modifying the example on pages 21-4 to 21-5 of USEPA (2009), 
  # determine how adding another four months of observations to 
  # increase the sample size from 4 to 8 will affect the half-width 
  # of a two-sided 95% confidence interval for the Aldicarb level at 
  # the first compliance well.
  #  
  # Use the estimated standard deviation from the first four months 
  # of data.  (The data are stored in EPA.09.Ex.21.1.aldicarb.df.) 
  # Note that the half-width changes from 34% of the observed mean to 
  # 18% of the observed mean by increasing the sample size from 
  # 4 to 8.

  EPA.09.Ex.21.1.aldicarb.df
  #   Month   Well Aldicarb.ppb
  #1      1 Well.1         19.9
  #2      2 Well.1         29.6
  #3      3 Well.1         18.7
  #4      4 Well.1         24.2
  #...

  mu.hat &lt;- with(EPA.09.Ex.21.1.aldicarb.df, 
    mean(Aldicarb.ppb[Well=="Well.1"]))

  mu.hat 
  #[1] 23.1 

  sigma.hat &lt;- with(EPA.09.Ex.21.1.aldicarb.df, 
    sd(Aldicarb.ppb[Well=="Well.1"]))

  sigma.hat 
  #[1] 4.93491 

  hw.4 &lt;- ciNormHalfWidth(n.or.n1 = 4, sigma.hat = sigma.hat) 

  hw.4 
  #[1] 7.852543 

  hw.8 &lt;- ciNormHalfWidth(n.or.n1 = 8, sigma.hat = sigma.hat) 

  hw.8 
  #[1] 4.125688 

  100 * hw.4/mu.hat 
  #[1] 33.99369 

  100 * hw.8/mu.hat 
  #[1] 17.86012

  #==========

  # Clean up
  #---------
  rm(hw, mu.hat, sigma.hat, hw.4, hw.8)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
