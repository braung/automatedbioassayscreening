<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: The Empirical Distribution Based on a Set of Observations</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for Empirical {EnvStats}"><tr><td>Empirical {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
The Empirical Distribution Based on a Set of Observations
</h2>

<h3>Description</h3>

<p>Density, distribution function, quantile function, and random generation for 
the empirical distribution based on a set of observations
</p>


<h3>Usage</h3>

<pre>
  demp(x, obs, discrete = FALSE, density.arg.list = NULL)
  pemp(q, obs, discrete = FALSE, 
    prob.method = ifelse(discrete, "emp.probs", "plot.pos"), 
    plot.pos.con = 0.375) 
  qemp(p, obs, discrete = FALSE, 
    prob.method = ifelse(discrete, "emp.probs", "plot.pos"), 
    plot.pos.con = 0.375)
  remp(n, obs)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>q</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>vector of probabilities between 0 and 1.
</p>
</td></tr>
<tr valign="top"><td><code>n</code></td>
<td>

<p>sample size.  If <code>length(n)</code> is larger than 1, then <code>length(n)</code> 
random values are returned.
</p>
</td></tr>
<tr valign="top"><td><code>obs</code></td>
<td>

<p>numeric vector of observations.  Missing (<code>NA</code>), undefined (<code>NaN</code>), and 
infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>discrete</code></td>
<td>

<p>logical scalar indicating whether the assumed parent distribution of <code>x</code> is 
discrete (<code>discrete=TRUE</code>) or continuous (<code>discrete=FALSE</code>).  The 
default value is <code>FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>density.arg.list</code></td>
<td>

<p>list with arguments to the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> <code><a href="../../stats/html/density.html">density</a></code> function.  The default value is 
<code>NULL</code>.  (See the help file for <code><a href="../../stats/html/density.html">density</a></code> 
for more information on the arguments to density.)  The argument 
<code>density.arg.list</code> is ignored if <code>discrete=TRUE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>prob.method</code></td>
<td>

<p>character string indicating what method to use to compute the empirical 
probabilities.  Possible values are <code>"emp.probs"</code> (empirical probabilities, 
default if <code>discrete=TRUE</code>) and <code>"plot.pos"</code> (plotting positions, 
default if <code>discrete=FALSE</code>).  See the DETAILS section for more explanation.
</p>
</td></tr>
<tr valign="top"><td><code>plot.pos.con</code></td>
<td>

<p>numeric scalar between 0 and 1 containing the value of the plotting position 
constant.  The default value is <code>plot.pos.con=0.375</code>.  See the DETAILS 
section for more information. This argument is ignored if 
<code>prob.method="emp.probs"</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Let <i>x_1, x_2, &hellip;, x_n</i> denote a random sample of n observations 
from some unknown probability distribution (i.e., the elements of the argument 
<code>obs</code>), and let <i>x_{(i)}</i> denote the <i>i^{th}</i> order statistic, that is, 
the <i>i^{th}</i> largest observation, for <i>i = 1, 2, &hellip;, n</i>.
</p>
<p><em>Estimating Density</em> <br />
The function <code>demp</code> computes the empirical probability density function.  If 
the observations are assumed to come from a discrete distribution, the probability 
density (mass) function is estimated by:
</p>
<p style="text-align: center;"><i>\hat{f}(x) = \widehat{Pr}(X = x) = \frac{&sum;^n_{i=1} I_{[x]}(x_i)}{n}</i></p>

<p>where <i>I</i> is the indicator function: 
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <i>I_{[x]}(y) =</i> </td><td style="text-align: left;"> <i>1</i> </td><td style="text-align: left;"> if <i>y = x</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                       </td><td style="text-align: left;"> <i>0</i> </td><td style="text-align: left;"> if <i>y \ne x</i> 
  </td>
</tr>

</table>

<p>That is, the estimated probability of observing the value <i>x</i> is simply the 
observed proportion of observations equal to <i>x</i>.
</p>
<p>If the observations are assumed to come from a continuous distribution, the 
function <code>demp</code> calls the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> function <code><a href="../../stats/html/density.html">density</a></code> to compute the 
estimated density based on the values specified in the argument <code>obs</code>, 
and then uses linear interpolation to estimate the density at the values 
specified in the argument <code>x</code>.  See the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> help file for 
<code><a href="../../stats/html/density.html">density</a></code> for more information on how the empirical density is 
computed in the continuous case.
</p>
<p><em>Estimating Probabilities</em> <br />
The function <code>pemp</code> computes the estimated cumulative distribution function 
(cdf), also called the empirical cdf (ecdf).  If the observations are assumed to 
come from a discrete distribution, the value of the cdf evaluated at the <i>i^{th}</i> 
order statistic is usually estimated by: 
</p>
<p style="text-align: center;"><i>\hat{F}[x_{(i)}] = \widehat{Pr}(X &le; x_{(i)}) = \hat{p}_i = 
    \frac{&sum;^n_{j=1} I_{(-&infin;, x_{(i)}]}(x_j)}{n}</i></p>

<p>where: 
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <i>I_{(-&infin;, x]}(y) =</i> </td><td style="text-align: left;"> <i>1</i> </td><td style="text-align: left;"> if <i>y &le; x</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                                </td><td style="text-align: left;"> <i>0</i> </td><td style="text-align: left;"> if <i>y &gt; x</i> 
  </td>
</tr>

</table>

<p>(D'Agostino, 1986a).  That is, the estimated value of the cdf at the <i>i^{th}</i> 
order statistic is simply the observed proportion of observations less than or 
equal to the <i>i^{th}</i> order statistic.  This estimator is sometimes called the 
&ldquo;empirical probabilities&rdquo; estimator and is intuitively appealing.  
The function <code>pemp</code> uses the above equations to compute the empirical cdf when 
<code>prob.method="emp.probs"</code>.
</p>
<p>For any general value of <i>x</i>, when the observations are assumed to come from a 
discrete distribution, the value of the cdf is estimated by:  
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <i>\hat{F}(x) =</i> </td><td style="text-align: left;"> <i>0</i>         </td><td style="text-align: left;"> if <i>x &lt; x_{(1)}</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                       </td><td style="text-align: left;"> <i>\hat{p}_i</i> </td><td style="text-align: left;"> if <i>x_{(i)} &le; x &lt; x_{(i+1)}</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                       </td><td style="text-align: left;"> <i>1</i>         </td><td style="text-align: left;"> if <i>x &ge; x_{(n)}</i> 
  </td>
</tr>

</table>
  
<p>The function <code>pemp</code> uses the above equation when <code>discrete=TRUE</code>.
</p>
<p>If the observations are assumed to come from a continuous distribution, the value 
of the cdf evaluated at the <i>i^{th}</i> order statistic is usually estimated by: 
</p>
<p style="text-align: center;"><i>\hat{F}[x_{(i)}] = \hat{p}_i = \frac{i - a}{n - 2a + 1}</i></p>

<p>where <i>a</i> denotes the plotting position constant and <i>0 &le; a &le; 1</i> 
(Cleveland, 1993, p.18; D'Agostino, 1986a, pp.8,25).  The estimators defined by 
the above equation are called <em>plotting positions</em> and are used to construct 
<a href="../../EnvStats/help/qqPlot.html">probability plots</a>.  The function <code>pemp</code> uses the above equation 
when <br /> 
<code>prob.method="plot.pos"</code>.
</p>
<p>For any general value of <i>x</i>, the value of the cdf is estimated by linear 
interpolation: 
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <i>\hat{F}(x) =</i> </td><td style="text-align: left;"> <i>\hat{p}_1</i> </td><td style="text-align: left;"> if <i>x &lt; x_{(1)}</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                       </td><td style="text-align: left;"> <i>(1 - r)\hat{p}_i + r\hat{p}_{i+1}</i> </td><td style="text-align: left;"> if <i>x_{(i)} &le; x &lt; x_{(i+1)}</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                       </td><td style="text-align: left;"> <i>\hat{p}_n</i> </td><td style="text-align: left;"> if <i>x &ge; x_{(n)}</i> 
  </td>
</tr>

</table>

<p>where
</p>
<p style="text-align: center;"><i>r = \frac{x - x_{(i)}}{x_{(i+1)} - x_{(i)}}</i></p>

<p>(Chambers et al., 1983).  The function <code>pemp</code> uses the above two equations 
when <code>discrete=FALSE</code>.
</p>
<p><em>Estimating Quantiles</em> <br />
The function <code>qemp</code> computes the estimated quantiles based on the observed 
data.  If the observations are assumed to come from a discrete distribution, the 
<i>p^{th}</i> quantile is usually estimated by: 
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <i>\hat{x}_p =</i> </td><td style="text-align: left;"> <i>x_{(1)}</i> </td><td style="text-align: left;"> if <i>p &le; \hat{p}_1</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                      </td><td style="text-align: left;"> <i>x_{(i)}</i> </td><td style="text-align: left;"> if <i>\hat{p}_{i-1} &lt; p &le; \hat{p}_i</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                      </td><td style="text-align: left;"> <i>x_n</i>     </td><td style="text-align: left;"> if <i>p &gt; \hat{p}_n</i> 
  </td>
</tr>

</table>

<p>The function <code>qemp</code> uses the above equation when <code>discrete=TRUE</code>.
</p>
<p>If the observations are assumed to come from a continuous distribution, the 
<i>p^{th}</i> quantile is usually estimated by linear interpolation: 
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <i>\hat{x}_p =</i> </td><td style="text-align: left;"> <i>x_{(1)}</i> </td><td style="text-align: left;"> if <i>p &le; \hat{p}_1</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                      </td><td style="text-align: left;"> <i>(1 - r)x_{(i-1)} + rx_{(i)}</i> </td><td style="text-align: left;"> if <i>\hat{p}_{i-1} &lt; p &le; \hat{p}_i</i>, </td>
</tr>
<tr>
 <td style="text-align: left;">
                      </td><td style="text-align: left;"> <i>x_n</i> </td><td style="text-align: left;"> if <i>p &gt; \hat{p}_n</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
  </td>
</tr>

</table>

<p>where
</p>
<p style="text-align: center;"><i>r = \frac{p - \hat{p}_{i-1}}{\hat{p}_i - \hat{p}_{i-1}}</i></p>

<p>The function <code>qemp</code> uses the above two equations when <code>discrete=FALSE</code>.
</p>
<p><em>Generating Random Numbers From the Empirical Distribution</em> <br />
The function <code>remp</code> simply calls the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> function <code><a href="../../base/html/sample.html">sample</a></code> to 
sample the elements of <code>obs</code> with replacement.
</p>


<h3>Value</h3>

<p>density (<code>demp</code>), probability (<code>pemp</code>), quantile (<code>qemp</code>), or 
random sample (<code>remp</code>) for the empirical distribution based on the data 
contained in the vector <code>obs</code>.
</p>


<h3>Note</h3>

<p>The function <code>demp</code> let's you perform nonparametric density estimation.  
The function <code>pemp</code> computes the value of the empirical cumulative 
distribution function (ecdf) for user-specified quantiles.  The ecdf is a 
nonparametric estimate of the true cdf (see <code><a href="../../EnvStats/help/ecdfPlot.html">ecdfPlot</a></code>).  The 
function <code>qemp</code> computes nonparametric estimates of quantiles 
(see the help files for <code><a href="../../EnvStats/help/eqnpar.html">eqnpar</a></code> and <code><a href="../../stats/html/quantile.html">quantile</a></code>).  
The function <code>remp</code> let's you sample a set of observations with replacement, 
which is often done while bootstrapping or performing some other kind of 
Monte Carlo simulation.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Chambers, J.M., W.S. Cleveland, B. Kleiner, and P.A. Tukey. (1983).  
<em>Graphical Methods for Data Analysis</em>.  Duxbury Press, Boston, MA, 
pp.11&ndash;16.
</p>
<p>Cleveland, W.S. (1993).  <em>Visualizing Data</em>.  Hobart Press, Summit, 
New Jersey, 360pp.
</p>
<p>D'Agostino, R.B. (1986a).  Graphical Analysis.  
In: D'Agostino, R.B., and M.A. Stephens, eds. <em>Goodness-of Fit Techniques</em>. 
Marcel Dekker, New York, Chapter 2, pp.7&ndash;62.
</p>
<p>Scott, D. W. (1992).  
<em>Multivariate Density Estimation:  Theory, Practice and Visualization</em>.  
John Wiley and Sons, New York. 
</p>
<p>Sheather, S. J. and Jones M. C. (1991).  A Reliable Data-Based Bandwidth Selection 
Method for Kernel Density Estimation.  
<em>Journal of the Royal Statististical Society B</em>, 683&ndash;690. 
</p>
<p>Silverman, B.W. (1986).  <em>Density Estimation for Statistics and Data Analysis</em>.  
Chapman and Hall, London.
</p>
<p>Wegman, E.J. (1972).  Nonparametric Probability Density Estimation.  
<em>Technometrics</em> <b>14</b>, 533-546.
</p>


<h3>See Also</h3>

<p><code><a href="../../stats/html/density.html">density</a></code>, <code><a href="../../stats/html/approxfun.html">approx</a></code>, <code><a href="../../EnvStats/help/epdfPlot.html">epdfPlot</a></code>, 
<code><a href="../../EnvStats/help/ecdfPlot.html">ecdfPlot</a></code>, <code><a href="../../EnvStats/help/cdfCompare.html">cdfCompare</a></code>, <code><a href="../../stats/html/qqnorm.html">qqplot</a></code>, 
<code><a href="../../EnvStats/help/eqnpar.html">eqnpar</a></code>, <code><a href="../../stats/html/quantile.html">quantile</a></code>, <code><a href="../../base/html/sample.html">sample</a></code>, <br />
<code><a href="../../EnvStats/help/simulateVector.html">simulateVector</a></code>, <code><a href="../../EnvStats/help/simulateMvMatrix.html">simulateMvMatrix</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Create a set of 100 observations from a gamma distribution with 
  # parameters shape=4 and scale=5. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(3) 
  obs &lt;- rgamma(100, shape=4, scale=5)

  # Now plot the empirical distribution (with a histogram) and the true distribution:

  dev.new()
  hist(obs, col = "cyan", xlim = c(0, 65), freq = FALSE, 
    ylab = "Relative Frequency") 

  pdfPlot('gamma', list(shape = 4, scale = 5), add = TRUE) 

  box()

  # Now plot the empirical distribution (based on demp) with the 
  # true distribution:

  x &lt;- qemp(p = seq(0, 1, len = 100), obs = obs) 
  y &lt;- demp(x, obs) 

  dev.new()
  plot(x, y, xlim = c(0, 65), type = "n", 
    xlab = "Value of Random Variable", 
    ylab = "Relative Frequency") 
  lines(x, y, lwd = 2, col = "cyan") 

  pdfPlot('gamma', list(shape = 4, scale = 5), add = TRUE)

  # Alternatively, you can create the above plot with the function 
  # epdfPlot:

  dev.new()
  epdfPlot(obs, xlim = c(0, 65), epdf.col = "cyan", 
    xlab = "Value of Random Variable", 
    main = "Empirical and Theoretical PDFs")

  pdfPlot('gamma', list(shape = 4, scale = 5), add = TRUE)

  


  # Clean Up
  #---------
  rm(obs, x, y)

</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
