<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: The Extreme Value (Gumbel) Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for EVD {EnvStats}"><tr><td>EVD {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
The Extreme Value (Gumbel) Distribution
</h2>

<h3>Description</h3>

<p>Density, distribution function, quantile function, and random generation 
for the (largest) extreme value distribution.
</p>


<h3>Usage</h3>

<pre>
  devd(x, location = 0, scale = 1)
  pevd(q, location = 0, scale = 1)
  qevd(p, location = 0, scale = 1)
  revd(n, location = 0, scale = 1)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>q</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>vector of probabilities between 0 and 1.
</p>
</td></tr>
<tr valign="top"><td><code>n</code></td>
<td>

<p>sample size.  If <code>length(n)</code> is larger than 1, then <code>length(n)</code> 
random values are returned.
</p>
</td></tr>
<tr valign="top"><td><code>location</code></td>
<td>

<p>vector of location parameters.
</p>
</td></tr>
<tr valign="top"><td><code>scale</code></td>
<td>

<p>vector of positive scale parameters.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Let <i>X</i> be an extreme value random variable with parameters 
<code>location=</code><i>&eta;</i> and <code>scale=</code><i>&theta;</i>.  
The density function of <i>X</i> is given by:
</p>
<p style="text-align: center;"><i>f(x; &eta;, &theta;) = \frac{1}{&theta;} e^{-(x-&eta;)/&theta;} exp[-e^{-(x-&eta;)/&theta;}]</i></p>

<p>where <i>-&infin; &lt; x, &eta; &lt; &infin;</i> and <i>&theta; &gt; 0</i>.
</p>
<p>The cumulative distribution function of <i>X</i> is given by:
</p>
<p style="text-align: center;"><i>F(x; &eta;, &theta;) = exp[-e^{-(x-&eta;)/&theta;}]</i></p>

<p>The <i>p^{th}</i> quantile of <i>X</i> is given by:
</p>
<p style="text-align: center;"><i>x_{p} = &eta; - &theta; log[-log(p)]</i></p>

<p>The mode, mean, variance, skew, and kurtosis of <i>X</i> are given by:
</p>
<p style="text-align: center;"><i>Mode(X) = &eta;</i></p>

<p style="text-align: center;"><i>E(X) = &eta; + &epsilon; &theta;</i></p>

<p style="text-align: center;"><i>Var(X) = &theta;^2 &pi;^2 / 6</i></p>

<p style="text-align: center;"><i>Skew(X) = &radic;{&beta;_1} = 1.139547</i></p>

<p style="text-align: center;"><i>Kurtosis(X) = &beta;_2 = 5.4</i></p>

<p>where <i>&epsilon;</i> denotes <a href="../../EnvStats/help/EulersConstant.html">Euler's constant</a>, 
which is equivalent to <code>-<a href="../../base/html/Special.html">digamma</a>(1)</code>.
</p>


<h3>Value</h3>

<p>density (<code>devd</code>), probability (<code>pevd</code>), quantile (<code>qevd</code>), or 
random sample (<code>revd</code>) for the extreme value distribution with 
location parameter(s) determined by <code>location</code> and scale 
parameter(s) determined by <code>scale</code>.
</p>


<h3>Note</h3>

<p>There are three families of extreme value distributions.  The one 
described here is the Type I, also called the Gumbel extreme value 
distribution or simply Gumbel distribution.  The name 
&ldquo;extreme value&rdquo; comes from the fact that this distribution is 
the limiting distribution (as <i>n</i> approaches infinity) of the 
greatest value among <i>n</i> independent random variables each 
having the same continuous distribution.
</p>
<p>The Gumbel extreme value distribution is related to the 
<a href="../../stats/help/Exponential.html">exponential distribution</a> as follows. 
Let <i>Y</i> be an <a href="../../stats/help/Exponential.html">exponential</a> random variable 
with parameter <code>rate=</code><i>&lambda;</i>.  Then <i>X = &eta; - log(Y)</i> 
has an extreme value distribution with parameters 
<code>location=</code><i>&eta;</i> and <code>scale=</code><i>1/&lambda;</i>.
</p>
<p>The distribution described above and used by <code>devd</code>, <code>pevd</code>, 
<code>qevd</code>, and <code>revd</code> is the <em>largest</em> extreme value 
distribution.  The smallest extreme value distribution is the limiting 
distribution (as <i>n</i> approaches infinity) of the smallest value among 
<i>n</i> independent random variables each having the same continuous distribution. 
If <i>X</i> has a largest extreme value distribution with parameters <br />
<code>location=</code><i>&eta;</i> and <code>scale=</code><i>&theta;</i>, then 
<i>Y = -X</i> has a smallest extreme value distribution with parameters 
<code>location=</code><i>-&eta;</i> and <code>scale=</code><i>&theta;</i>.  The smallest 
extreme value distribution is related to the 
<a href="../../stats/help/Weibull.html">Weibull distribution</a> as follows.  
Let <i>Y</i> be a <a href="../../stats/help/Weibull.html">Weibull random variable</a> with parameters 
<code>shape=</code><i>&beta;</i> and <code>scale=</code><i>&alpha;</i>.  Then <i>X = log(Y)</i> 
has a smallest extreme value distribution with parameters <code>location=</code><i>log(&alpha;)</i> 
and <code>scale=</code><i>1/&beta;</i>.
</p>
<p>The extreme value distribution has been used extensively to model the distribution 
of streamflow, flooding, rainfall, temperature, wind speed, and other 
meteorological variables, as well as material strength and life data.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995). 
<em>Continuous Univariate Distributions, Volume 2</em>. 
Second Edition. John Wiley and Sons, New York.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/eevd.html">eevd</a></code>, <code><a href="../../EnvStats/help/GEVD.html">GEVD</a></code>, 
<a href="../../EnvStats/help/Probability+20Distributions+20and+20Random+20Numbers.html">Probability Distributions and Random Numbers</a>.
</p>


<h3>Examples</h3>

<pre>
  # Density of an extreme value distribution with location=0, scale=1, 
  # evaluated at 0.5:

  devd(.5) 
  #[1] 0.3307043

  #----------

  # The cdf of an extreme value distribution with location=1, scale=2, 
  # evaluated at 0.5:

  pevd(.5, 1, 2) 
  #[1] 0.2769203

  #----------

  # The 25'th percentile of an extreme value distribution with 
  # location=-2, scale=0.5:

  qevd(.25, -2, 0.5) 
  #[1] -2.163317

  #----------

  # Random sample of 4 observations from an extreme value distribution with 
  # location=5, scale=2. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(20) 
  revd(4, 5, 2) 
  #[1] 9.070406 7.669139 4.511481 5.903675
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
