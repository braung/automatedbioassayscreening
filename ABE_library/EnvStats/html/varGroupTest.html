<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Test for Homogeneity of Variance Among Two or More Groups</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for varGroupTest {EnvStats}"><tr><td>varGroupTest {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Test for Homogeneity of Variance Among Two or More Groups
</h2>

<h3>Description</h3>

<p>Test the null hypothesis that the variances of two or more normal distributions are the same using 
Levene's or Bartlett's test.
</p>


<h3>Usage</h3>

<pre>
varGroupTest(object, ...)

## S3 method for class 'formula'
varGroupTest(object, data = NULL, subset, 
  na.action = na.pass, ...)

## Default S3 method:
varGroupTest(object, group, test = "Levene", 
  correct = TRUE, data.name = NULL, group.name = NULL, 
  parent.of.data = NULL, subset.expression = NULL, ...)

## S3 method for class 'data.frame'
varGroupTest(object, ...)

## S3 method for class 'matrix'
varGroupTest(object, ...)

## S3 method for class 'list'
varGroupTest(object, ...)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>object</code></td>
<td>

<p>an object containing data for 2 or more groups whose variances are to be compared.  
In the default method, the argument <code>object</code> must be a numeric vector.  
When <code>object</code> is a data frame, all columns must be numeric.  
When <code>object</code> is a matrix, it must be a numeric matrix.  
When <code>object</code> is a list, all components must be numeric vectors.
In the formula method, a symbolic specification of the form <code>y ~ g</code> 
can be given, indicating the observations in the vector <code>y</code> are to be grouped 
according to the levels of the factor <code>g</code>.  Missing (<code>NA</code>), undefined (<code>NaN</code>), 
and infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>data</code></td>
<td>

<p>when <code>object</code> is a formula, <code>data</code> specifies an optional data frame, list or 
environment (or object coercible by <code>as.data.frame</code> to a data frame) containing the 
variables in the model.  If not found in <code>data</code>, the variables are taken from 
<code>environment(formula)</code>, typically the environment from which <br />
<code>summaryStats</code> is called.
</p>
</td></tr>
<tr valign="top"><td><code>subset</code></td>
<td>

<p>when <code>object</code> is a formula, <code>subset</code> specifies an optional vector specifying 
a subset of observations to be used.
</p>
</td></tr>
<tr valign="top"><td><code>na.action</code></td>
<td>

<p>when <code>object</code> is a formula, <code>na.action</code> specifies a function which indicates 
what should happen when the data contain <code>NA</code>s. The default is <code><a href="../../stats/html/na.fail.html">na.pass</a></code>.
</p>
</td></tr>
<tr valign="top"><td><code>group</code></td>
<td>

<p>when <code>object</code> is a numeric vector, <code>group</code> is a factor or character vector 
indicating which group each observation belongs to.  When <code>object</code> is a matrix or data frame
this argument is ignored and the columns define the groups.  When <code>object</code> is a list 
this argument is ignored and the components define the groups.  When <code>object</code> is a formula, 
this argument is ignored and the right-hand side of the formula specifies the grouping variable.
</p>
</td></tr>
<tr valign="top"><td><code>test</code></td>
<td>

<p>character string indicating which test to use.  The possible values are <code>"Levene"</code> 
(Levene's test; the default) and <code>"Bartlett"</code> (Bartlett's test).
</p>
</td></tr>
<tr valign="top"><td><code>correct</code></td>
<td>

<p>logical scalar indicating whether to use the correction factor for Bartlett's test.  
The default value is <code>correct=TRUE</code>.  This argument is ignored if <code>test="Levene"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>data.name</code></td>
<td>

<p>character string indicating the name of the data used for the group variance test.  
The default value is <code>data.name=deparse(substitute(object))</code>.
</p>
</td></tr>
<tr valign="top"><td><code>group.name</code></td>
<td>

<p>character string indicating the name of the data used to create the groups.
The default value is <code>group.name=deparse(substitute(group))</code>.
</p>
</td></tr>
<tr valign="top"><td><code>parent.of.data</code></td>
<td>

<p>character string indicating the source of the data used for the group variance test.
</p>
</td></tr>
<tr valign="top"><td><code>subset.expression</code></td>
<td>

<p>character string indicating the expression used to subset the data.
</p>
</td></tr>
<tr valign="top"><td><code>...</code></td>
<td>

<p>additional arguments affecting the group variance test.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The function <code>varGroupTest</code> performs Levene's or Bartlett's test for 
homogeneity of variance among two or more groups.  The <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> function <code><a href="../../stats/html/var.test.html">var.test</a></code> 
compares two variances.
</p>
<p>Bartlett's test is very sensitive to the assumption of normality and will tend to give 
significant results even when the null hypothesis is true if the underlying distributions 
have long tails (e.g., are leptokurtic).  Levene's test is almost as powerful as Bartlett's 
test when the underlying distributions are normal, and unlike Bartlett's test it tends to 
maintain the assumed <i>alpha</i>-level when the underlying distributions are not normal 
(Snedecor and Cochran, 1989, p.252; Milliken and Johnson, 1992, p.22; Conover et al., 1981).  
Thus, Levene's test is generally recommended over Bartlett's test.
</p>


<h3>Value</h3>

<p>a list of class <code>"htest"</code> containing the results of the group variance test.  
Objects of class <code>"htest"</code> have special printing and plotting methods.  
See the help file for <code><a href="../../EnvStats/help/htest.object.html">htest.object</a></code> for details.
</p>


<h3>Note</h3>

<p>Chapter 11 of USEPA (2009) discusses using Levene's test to test the assumption of 
equal variances between monitoring wells or to test that the variance is stable over time 
when performing intrawell tests.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Conover, W.J., M.E. Johnson, and M.M. Johnson. (1981). A Comparative Study of Tests for Homogeneity 
of Variances, with Applications to the Outer Continental Shelf Bidding Data. 
<em>Technometrics</em> <b>23</b>(4), 351-361.
</p>
<p>Davis, C.B. (1994). Environmental Regulatory Statistics. In Patil, G.P., and C.R. Rao, eds., 
<em>Handbook of Statistics, Vol. 12: Environmental Statistics</em>. North-Holland, Amsterdam, 
a division of Elsevier, New York, NY, Chapter 26, 817-865.
</p>
<p>Milliken, G.A., and D.E. Johnson. (1992). <em>Analysis of Messy Data, Volume I: Designed Experiments</em>. 
Chapman &amp; Hall, New York.
</p>
<p>Snedecor, G.W., and W.G. Cochran. (1989). <em>Statistical Methods, Eighth Edition</em>. 
Iowa State University Press, Ames Iowa.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2010).  <em>Errata Sheet - March 2009 Unified Guidance</em>.
EPA 530/R-09-007a, August 9, 2010.  Office of Resource Conservation and Recovery, Program Information and Implementation Division.
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. Fifth Edition. 
Prentice-Hall, Upper Saddle River, NJ.
</p>


<h3>See Also</h3>

<p><code><a href="../../stats/html/var.test.html">var.test</a></code>, <code><a href="../../EnvStats/help/varTest.html">varTest</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Example 11-2 of USEPA (2009, page 11-7) gives an example of 
  # testing the assumption of equal variances across wells for arsenic  
  # concentrations (ppb) in groundwater collected at 6 monitoring 
  # wells over 4 months.  The data for this example are stored in 
  # EPA.09.Ex.11.1.arsenic.df.

  head(EPA.09.Ex.11.1.arsenic.df)
  #  Arsenic.ppb Month Well
  #1        22.9     1    1
  #2         3.1     2    1
  #3        35.7     3    1
  #4         4.2     4    1
  #5         2.0     1    2
  #6         1.2     2    2

  longToWide(EPA.09.Ex.11.1.arsenic.df, "Arsenic.ppb", "Month", "Well", 
    paste.row.name = TRUE, paste.col.name = TRUE)
  #        Well.1 Well.2 Well.3 Well.4 Well.5 Well.6
  #Month.1   22.9    2.0    2.0    7.8   24.9    0.3
  #Month.2    3.1    1.2  109.4    9.3    1.3    4.8
  #Month.3   35.7    7.8    4.5   25.9    0.8    2.8
  #Month.4    4.2   52.0    2.5    2.0   27.0    1.2

  varGroupTest(Arsenic.ppb ~ Well, data = EPA.09.Ex.11.1.arsenic.df)

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 Ratio of each pair of variances = 1
  #
  #Alternative Hypothesis:          At least one variance differs
  #
  #Test Name:                       Levene's Test for
  #                                 Homogenity of Variance
  #
  #Estimated Parameter(s):          Well.1 =  246.8158
  #                                 Well.2 =  592.6767
  #                                 Well.3 = 2831.4067
  #                                 Well.4 =  105.2967
  #                                 Well.5 =  207.4467
  #                                 Well.6 =    3.9025
  #
  #Data:                            Arsenic.ppb
  #
  #Grouping Variable:               Well
  #
  #Data Source:                     EPA.09.Ex.11.1.arsenic.df
  #
  #Sample Sizes:                    Well.1 = 4
  #                                 Well.2 = 4
  #                                 Well.3 = 4
  #                                 Well.4 = 4
  #                                 Well.5 = 4
  #                                 Well.6 = 4
  #
  #Test Statistic:                  F = 4.564176
  #
  #Test Statistic Parameters:       num df   =  5
  #                                 denom df = 18
  #
  #P-value:                         0.007294084
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
