<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Parameters of a Beta Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for ebeta {EnvStats}"><tr><td>ebeta {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Parameters of a Beta Distribution
</h2>

<h3>Description</h3>

<p>Estimate the shape parameters of a <a href="../../stats/help/Beta.html">beta distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  ebeta(x, method = "mle")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.  All observations must be between greater than 
0 and less than 1. 
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  The possible values are 
<code>"mle"</code> (maximum likelihood; the default), <code>"mme"</code> (method of moments), 
and <code>"mmue"</code> (method of moments based on the unbiased estimator of variance).  
See the DETAILS section for more information on these estimation methods. 
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of <i>n</i> observations 
from a <a href="../../stats/help/Beta.html">beta distribution</a> with parameters 
<code>shape1=</code><i>&nu;</i> and <code>shape2=</code><i>&omega;</i>.
</p>
<p><em>Maximum Likelihood Estimation</em> (<code>method="mle"</code>) <br />
The maximum likelihood estimators (mle's) of the shape parameters <i>&nu;</i> and 
<i>&omega;</i> are the solutions of the simultaneous equations:
</p>
<p style="text-align: center;"><i>&Psi;(\hat{&nu;}) - &Psi;(\hat{&nu;} + \hat{&omega;}) = (1/n) &sum;_{i=1}^{n} log(x_i)</i></p>

<p style="text-align: center;"><i>&Psi;(\hat{&nu;}) - &Psi;(\hat{&nu;} + \hat{&omega;}) = (1/n) &sum;_{i=1}^{n} log(1 - x_i)</i></p>

<p>where <i>&Psi;()</i> is the <a href="../../base/help/Special.html">digamma function</a> (Forbes et al., 2011).  
</p>
<p><em>Method of Moments Estimators</em> (<code>method="mme"</code>)<br />
The method of moments estimators (mme's) of the shape parameters <i>&nu;</i> and 
<i>&omega;</i> are given by (Forbes et al., 2011):
</p>
<p style="text-align: center;"><i>\hat{&nu;} = \bar{x} \{ [ \bar{x}(1 - \bar{x}) / s_{m}^2] - 1 \}</i></p>

<p style="text-align: center;"><i>\hat{&omega;} = (1 - \bar{x}) \{ [ \bar{x}(1 - \bar{x}) / s_{m}^2] - 1 \}</i></p>

<p>where
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i; \, s_{m}^2 = \frac{1}{n} &sum;_{i=1}^n (x_i - \bar{x})^2</i></p>

<p><em>Method of Moments Estimators Based on the Unbiased Estimator of Variance</em> (<code>method="mmue"</code>) <br />
These estimators are the same as the method of moments estimators except that 
the method of moments estimator of variance is replaced with the unbiased estimator 
of variance:
</p>
<p style="text-align: center;"><i>s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2</i></p>



<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other information.  
See <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>


<h3>Note</h3>

<p>The beta distribution takes real values between 0 and 1.  Special cases of the 
beta are the <a href="../../stats/help/Uniform.html">Uniform</a>[0,1] when <code>shape1=1</code> and 
<code>shape2=1</code>, and the arcsin distribution when <code>shape1=0.5</code> and <br />
<code>shape2=0.5</code>.  The arcsin distribution appears in the theory of random walks.  
The beta distribution is used in Bayesian analyses as a conjugate to the binomial 
distribution.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995). 
<em>Continuous Univariate Distributions, Volume 2</em>. 
Second Edition. John Wiley and Sons, New York.
</p>


<h3>See Also</h3>

<p><a href="../../stats/help/Beta.html">Beta</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a beta distribution with parameters 
  # shape1=2 and shape2=4, then estimate the parameters via 
  # maximum likelihood. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rbeta(20, shape1 = 2, shape2 = 4) 
  ebeta(dat) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Beta
  #
  #Estimated Parameter(s):          shape1 =  5.392221
  #                                 shape2 = 11.823233
  #
  #Estimation Method:               mle
  #
  #Data:                            dat
  #
  #Sample Size:                     20

  #==========

  # Repeat the above, but use the method of moments estimators:

  ebeta(dat, method = "mme")

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Beta
  #
  #Estimated Parameter(s):          shape1 =  5.216311
  #                                 shape2 = 11.461341
  #
  #Estimation Method:               mme
  #
  #Data:                            dat
  #
  #Sample Size:                     20

  #==========

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
