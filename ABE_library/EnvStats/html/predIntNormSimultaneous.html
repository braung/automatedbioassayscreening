<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Simultaneous Prediction Interval for a Normal Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for predIntNormSimultaneous {EnvStats}"><tr><td>predIntNormSimultaneous {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Simultaneous Prediction Interval for a Normal Distribution
</h2>

<h3>Description</h3>

<p>Estimate the mean and standard deviation of a
<a href="../../stats/help/Normal.html">normal distribution</a>, and construct a simultaneous prediction
interval for the next <i>r</i> sampling &ldquo;occasions&rdquo;, based on one of three
possible rules: <i>k</i>-of-<i>m</i>, California, or Modified California.
</p>


<h3>Usage</h3>

<pre>
  predIntNormSimultaneous(x, n.mean = 1, k = 1, m = 2, r = 1, rule = "k.of.m",
    delta.over.sigma = 0, pi.type = "upper", conf.level = 0.95,
    K.tol = .Machine$double.eps^0.5)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>a numeric vector of observations, or an object resulting from a call to an estimating
function that assumes a normal (Gaussian) distribution (e.g., <code><a href="../../EnvStats/help/enorm.html">enorm</a></code>,
<code><a href="../../EnvStats/help/eqnorm.html">eqnorm</a></code>, <code><a href="../../EnvStats/help/enormCensored.html">enormCensored</a></code>, etc.).  If <code>x</code> is a
numeric vector, missing (<code>NA</code>), undefined (<code>NaN</code>), and
infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>n.mean</code></td>
<td>

<p>positive integer specifying the sample size associated with the future averages.
The default value is <code>n.mean=1</code> (i.e., individual observations).  Note that all
future averages must be based on the same sample size.
</p>
</td></tr>
<tr valign="top"><td><code>k</code></td>
<td>

<p>for the <i>k</i>-of-<i>m</i> rule (<code>rule="k.of.m"</code>), a positive integer
specifying the minimum number of observations (or averages) out of <i>m</i>
observations (or averages) (all obtained on one future sampling &ldquo;occassion&rdquo;)
the prediction interval should contain with confidence level <code>conf.level</code>.
The default value is <code>k=1</code>.  This argument is ignored when the argument
<code>rule</code> is not equal to <code>"k.of.m"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>m</code></td>
<td>

<p>positive integer specifying the maximum number of future observations (or
averages) on one future sampling &ldquo;occasion&rdquo;.
The default value is <code>m=2</code>, except when <code>rule="Modified.CA"</code>, in which
case this argument is ignored and <code>m</code> is automatically set equal to <code>4</code>.
</p>
</td></tr>
<tr valign="top"><td><code>r</code></td>
<td>

<p>positive integer specifying the number of future sampling &ldquo;occasions&rdquo;.
The default value is <code>r=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>rule</code></td>
<td>

<p>character string specifying which rule to use.  The possible values are
<code>"k.of.m"</code> (<i>k</i>-of-<i>m</i> rule; the default), <code>"CA"</code> (California rule),
and <code>"Modified.CA"</code> (modified California rule).
See the DETAILS section below for more information.
</p>
</td></tr>
<tr valign="top"><td><code>delta.over.sigma</code></td>
<td>

<p>numeric scalar indicating the ratio <i>&Delta;/&sigma;</i>.  The quantity
<i>&Delta;</i> (delta) denotes the difference between the mean of the population
that was sampled to construct the prediction interval, and the mean of the
population that will be sampled to produce the future observations.  The quantity
<i>&sigma;</i> (sigma) denotes the population standard deviation for both populations.
See the DETAILS section below for more information.  The default value is
<code>delta.over.sigma=0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>pi.type</code></td>
<td>

<p>character string indicating what kind of prediction interval to compute.
The possible values are <code>pi.type="upper"</code> (the default),
<code>pi.type="lower"</code> and <code>pi.type="two-sided"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the prediction interval.
The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>K.tol</code></td>
<td>

<p>numeric scalar indicating the tolerance to use in the nonlinear search algorithm to
compute <i>K</i>.  The default value is <code>K.tol=.Machine$double.eps^(1/2)</code>.
For many applications, the value of <i>K</i> needs to be known only to the second
decimal place, in which case setting <code>K.tol=1e-4</code> will speed up computation a
bit.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p><em>What is a Simultaneous Prediction Interval?</em> <br />
A prediction interval for some population is an interval on the real line constructed
so that it will contain <i>k</i> future observations from that population
with some specified probability <i>(1-&alpha;)100\%</i>, where
<i>0 &lt; &alpha; &lt; 1</i> and <i>k</i> is some pre-specified positive integer.
The quantity <i>(1-&alpha;)100\%</i> is called
the confidence coefficient or confidence level associated with the prediction
interval.  The function <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code> computes a standard prediction
interval based on a sample from a <a href="../../stats/help/Normal.html">normal distribution</a>.
</p>
<p>The function <code>predIntNormSimultaneous</code> computes a simultaneous prediction
interval that will contain a certain number of future observations with probability
<i>(1-&alpha;)100\%</i> for each of <i>r</i> future sampling &ldquo;occasions&rdquo;,
where <i>r</i> is some pre-specified positive integer.  The quantity <i>r</i> may
refer to <i>r</i> distinct future sampling occasions in time, or it may for example
refer to sampling at <i>r</i> distinct locations on one future sampling occasion,
assuming that the population standard deviation is the same at all of the <i>r</i>
distinct locations.
</p>
<p>The function <code>predIntNormSimultaneous</code> computes a simultaneous prediction
interval based on one of three possible rules:
</p>

<ul>
<li><p> For the <i>k</i>-of-<i>m</i> rule (<code>rule="k.of.m"</code>), at least <i>k</i> of
the next <i>m</i> future observations will fall in the prediction
interval with probability <i>(1-&alpha;)100\%</i> on each of the <i>r</i> future
sampling occasions.  If obserations are being taken sequentially, for a particular
sampling occasion, up to <i>m</i> observations may be taken, but once
<i>k</i> of the observations fall within the prediction interval, sampling can stop.
Note:  When <i>k=m</i> and <i>r=1</i>, the results of <code>predIntNormSimultaneous</code>
are equivalent to the results of <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>.
</p>
</li>
<li><p> For the California rule (<code>rule="CA"</code>), with probability
<i>(1-&alpha;)100\%</i>, for each of the <i>r</i> future sampling occasions, either
the first observation will fall in the prediction interval, or else all of the next
<i>m-1</i> observations will fall in the prediction interval. That is, if the first
observation falls in the prediction interval then sampling can stop.  Otherwise,
<i>m-1</i> more observations must be taken.
</p>
</li>
<li><p> For the Modified California rule (<code>rule="Modified.CA"</code>), with probability
<i>(1-&alpha;)100\%</i>, for each of the <i>r</i> future sampling occasions, either the
first observation will fall in the prediction interval, or else at least 2 out of
the next 3 observations will fall in the prediction interval.  That is, if the first
observation falls in the prediction interval then sampling can stop.  Otherwise, up
to 3 more observations must be taken.
</p>
</li></ul>

<p>Simultaneous prediction intervals can be extended to using averages (means) in place
of single observations (USEPA, 2009, Chapter 19).  That is, you can create a
simultaneous prediction interval
that will contain a specified number of averages (based on which rule you choose) on
each of <i>r</i> future sampling occassions, where each each average is based on
<i>w</i> individual observations.  For the function <code>predIntNormSimultaneous</code>,
the argument <code>n.mean</code> corresponds to <i>w</i>.
<br />
</p>
<p><em>The Form of a Prediction Interval</em> <br />
Let <i>\underline{x} = x_1, x_2, &hellip;, x_n</i> denote a vector of <i>n</i>
observations from a <a href="../../stats/help/Normal.html">normal distribution</a> with parameters
<code>mean=</code><i>&mu;</i> and <code>sd=</code><i>&sigma;</i>.  Also, let <i>w</i> denote the
sample size associated with the future averages (i.e., <code>n.mean=</code><i>w</i>).
When <i>w=1</i>, each average is really just a single observation, so in the rest of
this help file the term &ldquo;averages&rdquo; will replace the phrase
&ldquo;observations or averages&rdquo;.
</p>
<p>For a normal distribution, the form of a two-sided <i>(1-&alpha;)100\%</i>
prediction interval is:
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, \bar{x} + Ks] \;\;\;\;\;\; (1)</i></p>

<p>where <i>\bar{x}</i> denotes the sample mean:
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\;\;\; (2)</i></p>

<p><i>s</i> denotes the sample standard deviation:
</p>
<p style="text-align: center;"><i>s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\;\;\; (3)</i></p>

<p>and <i>K</i> denotes a constant that depends on the sample size <i>n</i>, the
confidence level, the number of future sampling occassions <i>r</i>, and the
sample size associated with the future averages, <i>w</i>.  Do not confuse the
constant <i>K</i> (uppercase K) with the number of future averages <i>k</i>
(lowercase k) in the <i>k</i>-of-<i>m</i> rule.  The symbol <i>K</i> is used here
to be consistent with the notation used for tolerance intervals
(see <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code>).
</p>
<p>Similarly, the form of a one-sided lower prediction interval is:
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, &infin;] \;\;\;\;\;\; (4)</i></p>

<p>and the form of a one-sided upper prediction interval is:
</p>
<p style="text-align: center;"><i>[-&infin;, \bar{x} + Ks] \;\;\;\;\;\; (5)</i></p>

<p><b>Note:</b>  For simultaneous prediction intervals, only lower
(<code>pi.type="lower"</code>) and upper <br />
(<code>pi.type="upper"</code>) prediction
intervals are available.
</p>
<p>The derivation of the constant <i>K</i> is explained in the help file for
<code><a href="../../EnvStats/help/predIntNormSimultaneousK.html">predIntNormSimultaneousK</a></code>.
<br />
</p>
<p><em>Prediction Intervals are Random Intervals</em> <br />
A prediction interval is a <em>random</em> interval; that is, the lower and/or
upper bounds are random variables computed based on sample statistics in the
baseline sample.  Prior to taking one specific baseline sample, the probability
that the prediction interval will perform according to the rule chosen is
<i>(1-&alpha;)100\%</i>.  Once a specific baseline sample is taken and the prediction
interval based on that sample is computed, the probability that that prediction
interval will perform according to the rule chosen is not necessarily
<i>(1-&alpha;)100\%</i>, but it should be close.  See the help file for
<code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code> for more information.
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, <code>predIntNormSimultaneous</code> returns a list of
class <code>"estimate"</code> containing the estimated parameters, the prediction interval,
and other information.  See the help file for <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function,
<code>predIntNormSimultaneous</code> returns a list whose class is the same as <code>x</code>.
The list contains the same components as <code>x</code>, as well as a component called
<code>interval</code> containing the prediction interval information.
If <code>x</code> already has a component called <code>interval</code>, this component is
replaced with the prediction interval information.
</p>


<h3>Note</h3>

<p><em>Motivation</em> <br />
Prediction and tolerance intervals have long been applied to quality control and
life testing problems (Hahn, 1970b,c; Hahn and Nelson, 1973).  In the context of
environmental statistics, prediction intervals are useful for analyzing data from
groundwater detection monitoring programs at hazardous and solid waste facilities.
</p>
<p>One of the main statistical problems that plague groundwater monitoring programs at
hazardous and solid waste facilities is the requirement of testing several wells and
several constituents at each well on each sampling occasion.  This is an obvious
multiple comparisons problem, and the naive approach of using a standard t-test at
a conventional <i>&alpha;</i>-level (e.g., 0.05 or 0.01) for each test leads to a
very high probability of at least one significant result on each sampling occasion,
when in fact no contamination has occurred.  This problem was pointed out years ago
by Millard (1987) and others.
</p>
<p>Davis and McNichols (1987) proposed simultaneous prediction intervals as a way of
controlling the facility-wide false positive rate (FWFPR) while maintaining adequate
power to detect contamination in the groundwater.  Because of the ubiquitous presence
of spatial variability, it is usually best to use simultaneous prediction intervals
at each well (Davis, 1998a).  That is, by constructing prediction intervals based on
background (pre-landfill) data on each well, and comparing future observations at a
well to the prediction interval for that particular well.  In each of these cases,
the individual <i>&alpha;</i>-level at each well is equal to the FWFRP divided by the
product of the number of wells and constituents.
</p>
<p>Often, observations at downgradient wells are not available prior to the
construction and operation of the landfill.  In this case, upgradient well data can
be combined to create a background prediction interval, and observations at each
downgradient well can be compared to this prediction interval.  If spatial
variability is present and a major source of variation, however, this method is not
really valid (Davis, 1994; Davis, 1998a).
</p>
<p>Chapter 19 of USEPA (2009) contains an extensive discussion of using the
<i>1</i>-of-<i>m</i> rule and the Modified California rule.
</p>
<p>Chapters 1 and 3 of Gibbons et al. (2009) discuss simultaneous prediction intervals
for the normal and lognormal distributions, respectively.
<br />
</p>
<p><em>The k-of-m Rule</em> <br />
For the <i>k</i>-of-<i>m</i> rule, Davis and McNichols (1987) give tables with
&ldquo;optimal&rdquo; choices of <i>k</i> (in terms of best power for a given overall
confidence level) for selected values of <i>m</i>, <i>r</i>, and <i>n</i>.  They found
that the optimal ratios of <i>k</i> to <i>m</i> (i.e., <i>k/m</i>) are generally small,
in the range of 15-50%.
<br />
</p>
<p><em>The California Rule</em> <br />
The California rule was mandated in that state for groundwater monitoring at waste
disposal facilities when resampling verification is part of the statistical program
(Barclay's Code of California Regulations, 1991).  The California code mandates a
&ldquo;California&rdquo; rule with <i>m &ge; 3</i>.  The motivation for this rule may have
been a desire to have a majority of the observations in bounds (Davis, 1998a).  For
example, for a <i>k</i>-of-<i>m</i> rule with <i>k=1</i> and <i>m=3</i>, a monitoring
location will pass if the first observation is out of bounds, the second resample
is out of bounds, but the last resample is in bounds, so that 2 out of 3 observations
are out of bounds.  For the California rule with <i>m=3</i>, either the first
observation must be in bounds, or the next 2 observations must be in bounds in order
for the monitoring location to pass.
</p>
<p>Davis (1998a) states that if the FWFPR is kept constant, then the California rule
offers little increased power compared to the <i>k</i>-of-<i>m</i> rule, and can
actually decrease the power of detecting contamination.
<br />
</p>
<p><em>The Modified California Rule</em> <br />
The Modified California Rule was proposed as a compromise between a 1-of-<i>m</i>
rule and the California rule.  For a given FWFPR, the Modified California rule
achieves better power than the California rule, and still requires at least as many
observations in bounds as out of bounds, unlike a 1-of-<i>m</i> rule.
<br />
</p>
<p><em>Different Notations Between Different References</em> <br />
For the <i>k</i>-of-<i>m</i> rule described in this help file, both
Davis and McNichols (1987) and USEPA (2009, Chapter 19) use the variable
<i>p</i> instead of <i>k</i> to represent the minimum number
of future observations the interval should contain on each of the <i>r</i> sampling
occasions.
</p>
<p>Gibbons et al. (2009, Chapter 1) presents extensive lists of the value of
<i>K</i> for both <i>k</i>-of-<i>m</i> rules and California rules.  Gibbons et al.'s
notation reverses the meaning of <i>k</i> and <i>r</i> compared to the notation used
in this help file.  That is, in Gibbons et al.'s notation, <i>k</i> represents the
number of future sampling occasions or monitoring wells, and <i>r</i> represents the
minimum number of observations the interval should contain on each sampling occasion.
</p>
<p>USEPA (2009, Chapter 19) uses <i>p</i> in place of <i>k</i>.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p><b>Barclay's California Code of Regulations</b>. (1991). Title 22,
Section 66264.97 [concerning hazardous waste facilities] and Title 23,
Section 2550.7(e)(8) [concerning solid waste facilities].
Barclay's Law Publishers, San Francisco, CA.
</p>
<p>Davis, C.B. (1998a).  <em>Ground-Water Statistics &amp; Regulations:  Principles,
Progress and Problems</em>.  Second Edition.  Environmetrics &amp; Statistics Limited,
Henderson, NV.
</p>
<p>Davis, C.B. (1998b). Personal Communication, September 3, 1998.
</p>
<p>Davis, C.B., and R.J. McNichols. (1987).  One-sided Intervals for at Least <i>p</i>
of <i>m</i> Observations from a Normal Population on Each of <i>r</i> Future Occasions.
<em>Technometrics</em> <b>29</b>, 359&ndash;370.
</p>
<p>Fertig, K.W., and N.R. Mann. (1977).  One-Sided Prediction Intervals for at Least
<i>p</i> Out of <i>m</i> Future Observations From a Normal Population.
<em>Technometrics</em> <b>19</b>, 167&ndash;177.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009).
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.
John Wiley &amp; Sons, Hoboken.
</p>
<p>Hahn, G.J. (1969). Factors for Calculating Two-Sided Prediction Intervals for
Samples from a Normal Distribution.
<em>Journal of the American Statistical Association</em> <b>64</b>(327), 878-898.
</p>
<p>Hahn, G.J. (1970a). Additional Factors for Calculating Prediction Intervals for
Samples from a Normal Distribution.
<em>Journal of the American Statistical Association</em> <b>65</b>(332), 1668-1676.
</p>
<p>Hahn, G.J. (1970b). Statistical Intervals for a Normal Population, Part I: Tables,
Examples and Applications. <em>Journal of Quality Technology</em> <b>2</b>(3), 115-125.
</p>
<p>Hahn, G.J. (1970c). Statistical Intervals for a Normal Population, Part II:
Formulas, Assumptions, Some Derivations. <em>Journal of Quality Technology</em>
<b>2</b>(4), 195-206.
</p>
<p>Hahn, G.J., and W.Q. Meeker. (1991). <em>Statistical Intervals: A Guide for
Practitioners</em>.  John Wiley and Sons, New York.
</p>
<p>Hahn, G., and W. Nelson. (1973). A Survey of Prediction Intervals and Their
Applications.  <em>Journal of Quality Technology</em> <b>5</b>, 178-188.
</p>
<p>Hall, I.J., and R.R. Prairie. (1973).  One-Sided Prediction Intervals to Contain at
Least <i>m</i> Out of <i>k</i> Future Observations.
<em>Technometrics</em> <b>15</b>, 897&ndash;914.
</p>
<p>Millard, S.P. (1987).  Environmental Monitoring, Statistics, and the Law:  Room for
Improvement (with Comment).  <em>The American Statistician</em> <b>41</b>(4), 249&ndash;259.
</p>
<p>Millard, S.P., and Neerchal, N.K. (2001). <em>Environmental Statistics with S-PLUS</em>.
CRC Press, Boca Raton, Florida.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2010).  <em>Errata Sheet - March 2009 Unified Guidance</em>.
EPA 530/R-09-007a, August 9, 2010.  Office of Resource Conservation and Recovery, Program Information and Implementation Division.
U.S. Environmental Protection Agency, Washington, D.C.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/predIntNormSimultaneousK.html">predIntNormSimultaneousK</a></code>,
<code><a href="../../EnvStats/help/predIntNormSimultaneousTestPower.html">predIntNormSimultaneousTestPower</a></code>,
<code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>, <br />
<code><a href="../../EnvStats/help/predIntLnormSimultaneous.html">predIntLnormSimultaneous</a></code>, <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code>,
<a href="../../stats/html/Normal.html">Normal</a>, <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>, <code><a href="../../EnvStats/help/enorm.html">enorm</a></code>
</p>


<h3>Examples</h3>

<pre>
  # Generate 8 observations from a normal distribution with parameters
  # mean=10 and sd=2, then use predIntNormSimultaneous to estimate the
  # mean and standard deviation of the true distribution and construct an
  # upper 95% prediction interval to contain at least 1 out of the next
  # 3 observations.
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(479)
  dat &lt;- rnorm(8, mean = 10, sd = 2)

  predIntNormSimultaneous(dat, k = 1, m = 3)


  #----------

  # Repeat the above example, but do it in two steps.  First create a list called
  # est.list containing information about the estimated parameters, then create the
  # prediction interval.

  est.list &lt;- enorm(dat)
  est.list

  predIntNormSimultaneous(est.list, k = 1, m = 3)

  #----------

  # Compare the 95% 1-of-3 upper prediction interval to the California and
  # Modified California prediction intervals.  Note that the upper prediction
  # bound for the Modified California rule is between the bound for the
  # 1-of-3 rule bound and the bound for the California rule.

  predIntNormSimultaneous(dat, k = 1, m = 3)$interval$limits["UPL"]

  predIntNormSimultaneous(dat, m = 3, rule = "CA")$interval$limits["UPL"]

  predIntNormSimultaneous(dat, rule = "Modified.CA")$interval$limits["UPL"]

  #----------

  # Show how the upper bound on an upper 95% simultaneous prediction limit increases
  # as the number of future sampling occasions r increases.  Here, we'll use the
  # 1-of-3 rule.

  predIntNormSimultaneous(dat, k = 1, m = 3)$interval$limits["UPL"]

  predIntNormSimultaneous(dat, k = 1, m = 3, r = 10)$interval$limits["UPL"]

  #----------

  # Compare the upper simultaneous prediction limit for the 1-of-3 rule
  # based on individual observations versus based on means of order 4.

  predIntNormSimultaneous(dat, k = 1, m = 3)$interval$limits["UPL"]

  predIntNormSimultaneous(dat, n.mean = 4, k = 1,
    m = 3)$interval$limits["UPL"]

  #==========

  # Example 19-1 of USEPA (2009, p. 19-17) shows how to compute an
  # upper simultaneous prediction limit for the 1-of-3 rule for
  # r = 2 future sampling occasions.  The data for this example are
  # stored in EPA.09.Ex.19.1.sulfate.df.

  # We will pool data from 4 background wells that were sampled on
  # a number of different occasions, giving us a sample size of
  # n = 25 to use to construct the prediction limit.

  # There are 50 compliance wells and we will monitor 10 different
  # constituents at each well at each of the r=2 future sampling
  # occasions.  To determine the confidence level we require for
  # the simultaneous prediction interval, USEPA (2009) recommends
  # setting the individual Type I Error level at each well to

  # 1 - (1 - SWFPR)^(1 / (Number of Constituents * Number of Wells))

  # which translates to setting the confidence limit to

  # (1 - SWFPR)^(1 / (Number of Constituents * Number of Wells))

  # where SWFPR = site-wide false positive rate.  For this example, we
  # will set SWFPR = 0.1.  Thus, the confidence level is given by:

  nc &lt;- 10
  nw &lt;- 50
  SWFPR &lt;- 0.1
  conf.level &lt;- (1 - SWFPR)^(1 / (nc * nw))

  conf.level

  #----------

  # Look at the data:

  names(EPA.09.Ex.19.1.sulfate.df)

  EPA.09.Ex.19.1.sulfate.df[,
    c("Well", "Date", "Sulfate.mg.per.l", "log.Sulfate.mg.per.l")]

  # Construct the upper simultaneous prediction limit for the
  # 1-of-3 plan based on the log-transformed sulfate data

  log.Sulfate &lt;- EPA.09.Ex.19.1.sulfate.df$log.Sulfate.mg.per.l

  pred.int.list.log &lt;-
    predIntNormSimultaneous(x = log.Sulfate, k = 1, m = 3, r = 2,
      rule = "k.of.m", pi.type = "upper", conf.level = conf.level)

  pred.int.list.log


  # Now exponentiate the prediction interval to get the limit on
  # the original scale

  exp(pred.int.list.log$interval$limits["UPL"])

  #==========

  #two-sided
  predIntNormSimultaneous(x = log.Sulfate, k = 1, m = 3, r = 2,
      rule = "k.of.m", pi.type = "two-sided", conf.level = conf.level)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
