<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Prediction Interval for a Lognormal Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for predIntLnorm {EnvStats}"><tr><td>predIntLnorm {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Prediction Interval for a Lognormal Distribution
</h2>

<h3>Description</h3>

<p>Estimate the mean and standard deviation on the log-scale for a 
<a href="../../stats/help/Lognormal.html">lognormal distribution</a>, or estimate the mean 
and coefficient of variation for a 
<a href="../../EnvStats/help/LognormalAlt.html">lognormal distribution (alternative parameterization)</a>, 
and construct a prediction interval for the next <i>k</i> observations or 
next set of <i>k</i> geometric means.
</p>


<h3>Usage</h3>

<pre>
  predIntLnorm(x, n.geomean = 1, k = 1, method = "Bonferroni", 
    pi.type = "two-sided", conf.level = 0.95)

  predIntLnormAlt(x, n.geomean = 1, k = 1, method = "Bonferroni", 
    pi.type = "two-sided", conf.level = 0.95, est.arg.list = NULL)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>For <code>predIntLnorm</code>, <code>x</code> can be a numeric vector of positive observations, 
or an object resulting from a call to an estimating function that assumes a 
lognormal distribution (i.e., <code><a href="../../EnvStats/help/elnorm.html">elnorm</a></code> or <code><a href="../../EnvStats/help/elnormCensored.html">elnormCensored</a></code>).  
You <em>cannot</em> supply objects resulting from a call to estimating functions that 
use the alternative parameterization such as <code><a href="../../EnvStats/help/elnormAlt.html">elnormAlt</a></code> or 
<code><a href="../../EnvStats/help/elnormAltCensored.html">elnormAltCensored</a></code>.
</p>
<p>For <code>predIntLnormAlt</code>, a numeric vector of positive observations.  
</p>
<p>If <code>x</code> is a numeric vector, 
missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>n.geomean</code></td>
<td>

<p>positive integer specifying the sample size associated with the <i>k</i> future 
geometric means.  The default value is <code>n.geomean=1</code> (i.e., individual 
observations).  Note that all future geometric means must be based on the same 
sample size.
</p>
</td></tr>
<tr valign="top"><td><code>k</code></td>
<td>

<p>positive integer specifying the number of future observations or geometric means the 
prediction interval should contain with confidence level <code>conf.level</code>.  
The default value is <code>k=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method to use if the number of future observations 
(<code>k</code>) is greater than 1.  The possible values are <code>method="Bonferroni"</code> 
(approximate method based on Bonferonni inequality; the default), and <br />
<code>method="exact"</code> (exact method due to Dunnett, 1955).  See the DETAILS section 
of <code><a href="../../EnvStats/help/predIntNormK.html">predIntNormK</a></code> for more information.  
This argument is ignored if <code>k=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>pi.type</code></td>
<td>

<p>character string indicating what kind of prediction interval to compute.  
The possible values are <code>pi.type="two-sided"</code> (the default), 
<code>pi.type="lower"</code>, and <code>pi.type="upper"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the prediction interval.  
The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>est.arg.list</code></td>
<td>

<p>for <code>predIntLnormAlt</code>, a list containing arguments to pass to the function <br />
<code><a href="../../EnvStats/help/elnormAlt.html">elnormAlt</a></code> for estimating the mean and coefficient of variation.  
The default value is <code>est.arg.list=NULL</code>, which implies the default values 
will be used in the call to <code><a href="../../EnvStats/help/elnormAlt.html">elnormAlt</a></code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The function <code>predIntLnorm</code> returns a prediction interval as well as 
estimates of the meanlog and sdlog parameters.  
The function <code>predIntLnormAlt</code> returns a prediction interval as well as 
estimates of the mean and coefficient of variation.
</p>
<p>A prediction interval for a lognormal distribution is constructed by taking the 
natural logarithm of the observations and constructing a prediction interval 
based on the normal (Gaussian) distribution by calling <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>.  
These prediction limits are then exponentiated to produce a prediction interval on 
the original scale of the data.
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, a list of class 
<code>"estimate"</code> containing the estimated parameters, the prediction interval, 
and other information.  See the help file for <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function, 
<code>predIntLnorm</code> returns a list whose class is the same as <code>x</code>.  
The list contains the same components as <code>x</code>, as well as a component called 
<code>interval</code> containing the prediction interval information.  
If <code>x</code> already has a component called <code>interval</code>, this component is 
replaced with the prediction interval information.
</p>


<h3>Note</h3>

<p>Prediction and tolerance intervals have long been applied to quality control and 
life testing problems (Hahn, 1970b,c; Hahn and Nelson, 1973; Krishnamoorthy and Mathew, 2009).  
In the context of environmental statistics, prediction intervals are useful for 
analyzing data from groundwater detection monitoring programs at hazardous and 
solid waste facilities (e.g., Gibbons et al., 2009; Millard and Neerchal, 2001; 
USEPA, 2009).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). <em>Statistics for Environmental Engineers</em>. 
Lewis Publishers, Boca Raton.
</p>
<p>Dunnett, C.W. (1955). A Multiple Comparisons Procedure for Comparing Several Treatments 
with a Control. <em>Journal of the American Statistical Association</em> <b>50</b>, 1096-1121.
</p>
<p>Dunnett, C.W. (1964). New Tables for Multiple Comparisons with a Control. 
<em>Biometrics</em> <b>20</b>, 482-491.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009). 
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.  
John Wiley &amp; Sons, Hoboken.
</p>
<p>Hahn, G.J. (1969). Factors for Calculating Two-Sided Prediction Intervals for 
Samples from a Lognormal Distribution. 
<em>Journal of the American Statistical Association</em> <b>64</b>(327), 878-898.
</p>
<p>Hahn, G.J. (1970a). Additional Factors for Calculating Prediction Intervals for 
Samples from a Lognormal Distribution. 
<em>Journal of the American Statistical Association</em> <b>65</b>(332), 1668-1676.
</p>
<p>Hahn, G.J. (1970b). Statistical Intervals for a Lognormal Population, Part I: Tables, 
Examples and Applications. <em>Journal of Quality Technology</em> <b>2</b>(3), 115-125.
</p>
<p>Hahn, G.J. (1970c). Statistical Intervals for a Lognormal Population, Part II: 
Formulas, Assumptions, Some Derivations. <em>Journal of Quality Technology</em> 
<b>2</b>(4), 195-206.
</p>
<p>Hahn, G.J., and W.Q. Meeker. (1991). <em>Statistical Intervals: A Guide for Practitioners</em>. 
John Wiley and Sons, New York.
</p>
<p>Hahn, G., and W. Nelson. (1973). A Survey of Prediction Intervals and Their Applications. 
<em>Journal of Quality Technology</em> <b>5</b>, 178-188.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). <em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (2002). <em>Statistical Methods in Water Resources</em>. 
Techniques of Water Resources Investigations, Book 4, chapter A3. U.S. Geological Survey. 
(available on-line at:  <a href="http://pubs.usgs.gov/twri/twri4a3/">http://pubs.usgs.gov/twri/twri4a3/</a>).
</p>
<p>Krishnamoorthy K., and T. Mathew. (2009). 
<em>Statistical Tolerance Regions: Theory, Applications, and Computation</em>. 
John Wiley and Sons, Hoboken.
</p>
<p>Millard, S.P., and Neerchal, N.K. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, Florida.
</p>
<p>Miller, R.G. (1981a). <em>Simultaneous Statistical Inference</em>. McGraw-Hill, New York.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2010).  <em>Errata Sheet - March 2009 Unified Guidance</em>.
EPA 530/R-09-007a, August 9, 2010.  Office of Resource Conservation and Recovery, Program Information and Implementation Division.
U.S. Environmental Protection Agency, Washington, D.C.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/elnorm.html">elnorm</a></code>, <code><a href="../../EnvStats/help/elnormAlt.html">elnormAlt</a></code>, 
<code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>, <code><a href="../../EnvStats/help/predIntNormK.html">predIntNormK</a></code>, 
<code><a href="../../EnvStats/help/predIntLnormSimultaneous.html">predIntLnormSimultaneous</a></code>, <br />
<code><a href="../../EnvStats/help/predIntLnormAltSimultaneous.html">predIntLnormAltSimultaneous</a></code>, 
<code><a href="../../EnvStats/help/tolIntLnorm.html">tolIntLnorm</a></code>,  <code><a href="../../EnvStats/help/tolIntLnormAlt.html">tolIntLnormAlt</a></code>, 
<a href="../../stats/html/Lognormal.html">Lognormal</a>, <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a lognormal distribution with parameters 
  # meanlog=0 and sdlog=1.  The exact two-sided 90% prediction interval for 
  # k=1 future observation is given by: [exp(-1.645), exp(1.645)] = [0.1930, 5.181]. 
  # Use predIntLnorm to estimate the distribution parameters, and construct a 
  # two-sided 90% prediction interval.
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(47) 
  dat &lt;- rlnorm(20, meanlog = 0, sdlog = 1) 
  predIntLnorm(dat, conf = 0.9)

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Lognormal
  #
  #Estimated Parameter(s):          meanlog = -0.1035722
  #                                 sdlog   =  0.9106429
  #
  #Estimation Method:               mvue
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Prediction Interval Method:      exact
  #
  #Prediction Interval Type:        two-sided
  #
  #Confidence Level:                90%
  #
  #Number of Future Observations:   1
  #
  #Prediction Interval:             LPL = 0.1795898
  #                                 UPL = 4.5264399

  #----------

  # Repeat the above example, but do it in two steps.  
  # First create a list called est.list containing information about the 
  # estimated parameters, then create the prediction interval.

  est.list &lt;- elnorm(dat) 

  est.list 
  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Lognormal
  #
  #Estimated Parameter(s):          meanlog = -0.1035722
  #                                 sdlog   =  0.9106429
  #
  #Estimation Method:               mvue
  #
  #Data:                            dat
  #
  #Sample Size:                     20

  predIntLnorm(est.list, conf = 0.9)
  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Lognormal
  #
  #Estimated Parameter(s):          meanlog = -0.1035722
  #                                 sdlog   =  0.9106429
  #
  #Estimation Method:               mvue
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Prediction Interval Method:      exact
  #
  #Prediction Interval Type:        two-sided
  #
  #Confidence Level:                90%
  #
  #Number of Future Observations:   1
  #
  #Prediction Interval:             LPL = 0.1795898
  #                                 UPL = 4.5264399

  #----------

  # Using the same data from the first example, create a one-sided 
  # upper 99% prediction limit for the next 3 geometric means of order 2 
  # (i.e., each of the 3 future geometric means is based on a sample size 
  # of 2 future observations).

  predIntLnorm(dat, n.geomean = 2, k = 3, conf.level = 0.99, 
    pi.type = "upper")

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Lognormal
  #
  #Estimated Parameter(s):          meanlog = -0.1035722
  #                                 sdlog   =  0.9106429
  #
  #Estimation Method:               mvue
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Prediction Interval Method:      Bonferroni
  #
  #Prediction Interval Type:        upper
  #
  #Confidence Level:                99%
  #
  #Number of Future
  #Geometric Means:                 3
  #
  #Sample Size for
  #Geometric Means:                 2
  #
  #Prediction Interval:             LPL = 0.000000
  #                                 UPL = 7.047571

  #----------

  # Compare the result above that is based on the Bonferroni method 
  # with the exact method

  predIntLnorm(dat, n.geomean = 2, k = 3, conf.level = 0.99, 
    pi.type = "upper", method = "exact")$interval$limits["UPL"]

  #    UPL 
  #7.00316

  #----------

  # Clean up
  rm(dat, est.list)
  
  #--------------------------------------------------------------------

  # Example 18-2 of USEPA (2009, p.18-15) shows how to construct a 99% 
  # upper prediction interval for the log-scale mean of 4 future observations 
  # (future mean of order 4) assuming a lognormal distribution based on 
  # chrysene concentrations (ppb) in groundwater at 2 background wells.  
  # Data were collected once per month over 4 months at the 2 background 
  # wells, and also at a compliance well.  
  # The question to be answered is whether there is evidence of 
  # contamination at the compliance well.

  # Here we will follow the example, but look at the geometric mean 
  # instead of the log-scale mean.

  #----------

  # The data for this example are stored in EPA.09.Ex.18.2.chrysene.df.

  EPA.09.Ex.18.2.chrysene.df

  #   Month   Well  Well.type Chrysene.ppb
  #1      1 Well.1 Background          6.9
  #2      2 Well.1 Background         27.3
  #3      3 Well.1 Background         10.8
  #4      4 Well.1 Background          8.9
  #5      1 Well.2 Background         15.1
  #6      2 Well.2 Background          7.2
  #7      3 Well.2 Background         48.4
  #8      4 Well.2 Background          7.8
  #9      1 Well.3 Compliance         68.0
  #10     2 Well.3 Compliance         48.9
  #11     3 Well.3 Compliance         30.1
  #12     4 Well.3 Compliance         38.1 

  Chrysene.bkgd &lt;- with(EPA.09.Ex.18.2.chrysene.df, 
    Chrysene.ppb[Well.type == "Background"])
  Chrysene.cmpl &lt;- with(EPA.09.Ex.18.2.chrysene.df, 
    Chrysene.ppb[Well.type == "Compliance"])


  #----------

  # A Shapiro-Wilks goodness-of-fit test for normality indicates  
  # we should reject the assumption of normality and assume a 
  # lognormal distribution for the background well data:

  gofTest(Chrysene.bkgd)

  #Results of Goodness-of-Fit Test
  #-------------------------------
  #
  #Test Method:                     Shapiro-Wilk GOF
  #
  #Hypothesized Distribution:       Normal
  #
  #Estimated Parameter(s):          mean = 16.55000
  #                                 sd   = 14.54441
  #
  #Estimation Method:               mvue
  #
  #Data:                            Chrysene.bkgd
  #
  #Sample Size:                     8
  #
  #Test Statistic:                  W = 0.7289006
  #
  #Test Statistic Parameter:        n = 8
  #
  #P-value:                         0.004759859
  #
  #Alternative Hypothesis:          True cdf does not equal the
  #                                 Normal Distribution.

  gofTest(Chrysene.bkgd, dist = "lnorm")

  #Results of Goodness-of-Fit Test
  #-------------------------------
  #
  #Test Method:                     Shapiro-Wilk GOF
  #
  #Hypothesized Distribution:       Lognormal
  #
  #Estimated Parameter(s):          meanlog = 2.5533006
  #                                 sdlog   = 0.7060038
  #
  #Estimation Method:               mvue
  #
  #Data:                            Chrysene.bkgd
  #
  #Sample Size:                     8
  #
  #Test Statistic:                  W = 0.8546352
  #
  #Test Statistic Parameter:        n = 8
  #
  #P-value:                         0.1061057
  #
  #Alternative Hypothesis:          True cdf does not equal the
  #                                 Lognormal Distribution.

  #----------

  # Here is the one-sided 99% upper prediction limit for 
  # a geometric mean based on 4 future observations:

  predIntLnorm(Chrysene.bkgd, n.geomean = 4, k = 1, 
    conf.level = 0.99, pi.type = "upper")

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Lognormal
  #
  #Estimated Parameter(s):          meanlog = 2.5533006
  #                                 sdlog   = 0.7060038
  #
  #Estimation Method:               mvue
  #
  #Data:                            Chrysene.bkgd
  #
  #Sample Size:                     8
  #
  #Prediction Interval Method:      exact
  #
  #Prediction Interval Type:        upper
  #
  #Confidence Level:                99%
  #
  #Number of Future
  #Geometric Means:                 1
  #
  #Sample Size for
  #Geometric Means:                 4
  #
  #Prediction Interval:             LPL =  0.00000
  #                                 UPL = 46.96613

  UPL &lt;- predIntLnorm(Chrysene.bkgd, n.geomean = 4, k = 1, 
    conf.level = 0.99, pi.type = "upper")$interval$limits["UPL"]

  UPL
  #     UPL 
  #46.96613
  
  # Is there evidence of contamination at the compliance well?

  geoMean(Chrysene.cmpl)
  #[1] 44.19034

  # Since the geometric mean at the compliance well is less than 
  # the upper prediction limit, there is no evidence of contamination.

  #----------

  # Cleanup
  #--------

  rm(Chrysene.bkgd, Chrysene.cmpl, UPL)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
