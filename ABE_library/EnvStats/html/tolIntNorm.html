<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Tolerance Interval for a Normal Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for tolIntNorm {EnvStats}"><tr><td>tolIntNorm {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Tolerance Interval for a Normal Distribution
</h2>

<h3>Description</h3>

<p>Construct a <i>&beta;</i>-content or <i>&beta;</i>-expectation tolerance 
interval for a <a href="../../stats/help/Normal.html">normal distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  tolIntNorm(x, coverage = 0.95, cov.type = "content", 
    ti.type = "two-sided", conf.level = 0.95, method = "exact")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations, or an object resulting from a call to an 
estimating function that assumes a normal (Gaussian) distribution 
(i.e., <code><a href="../../EnvStats/help/enorm.html">enorm</a></code> or <code><a href="../../EnvStats/help/enormCensored.html">enormCensored</a></code>).  
If <code>x</code> is a numeric vector, 
missing (<code>NA</code>), undefined (<code>NaN</code>), and 
infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>coverage</code></td>
<td>

<p>a scalar between 0 and 1 indicating the desired coverage of the tolerance interval.  
The default value is <code>coverage=0.95</code>.  If <code>cov.type="expectation"</code>, 
this argument is ignored.
</p>
</td></tr>
<tr valign="top"><td><code>cov.type</code></td>
<td>

<p>character string specifying the coverage type for the tolerance interval.  
The possible values are <code>"content"</code> (<i>&beta;</i>-content; the default), and 
<code>"expectation"</code> (<i>&beta;</i>-expectation).  See the DETAILS section for more 
information.
</p>
</td></tr>
<tr valign="top"><td><code>ti.type</code></td>
<td>

<p>character string indicating what kind of tolerance interval to compute.  
The possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level associated with the tolerance 
interval.  The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>for the case of a two-sided tolerance interval, a character string specifying the method for 
constructing the tolerance interval.  This argument is ignored if <code>ti.type="lower"</code> or 
<code>ti.type="upper"</code>.  The possible values are <br />
<code>"exact"</code> (the default) and <code>"wald.wolfowitz"</code> (the Wald-Wolfowitz approximation).  
See the DETAILS section for more information.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>A tolerance interval for some population is an interval on the real line constructed so as to 
contain <i>100 &beta; \%</i> of the population (i.e., <i>100 &beta; \%</i> of all 
future observations), where <i>0 &lt; &beta; &lt; 1</i>.  The quantity <i>100 &beta; \%</i> is called 
the <em>coverage</em>.
</p>
<p>There are two kinds of tolerance intervals (Guttman, 1970):
</p>
 
<ul>
<li><p> A <i>&beta;</i>-content tolerance interval with confidence level <i>100(1-&alpha;)\%</i> is 
constructed so that it contains at least <i>100 &beta; \%</i> of the population (i.e., the 
coverage is at least <i>100 &beta; \%</i>) with probability <i>100(1-&alpha;)\%</i>, where 
<i>0 &lt; &alpha; &lt; 1</i>. The quantity <i>100(1-&alpha;)\%</i> is called the confidence level or 
confidence coefficient associated with the tolerance interval.
</p>
</li>
<li><p> A <i>&beta;</i>-expectation tolerance interval is constructed so that the <em>average</em> coverage of 
the interval is <i>100 &beta; \%</i>.
</p>
</li></ul>
 
<p><b>Note:</b> A <i>&beta;</i>-expectation tolerance interval with coverage <i>100 &beta; \%</i> is 
equivalent to a prediction interval for one future observation with associated confidence level 
<i>100 &beta; \%</i>.  Note that there is no explicit confidence level associated with a 
<i>&beta;</i>-expectation tolerance interval.  If a <i>&beta;</i>-expectation tolerance interval is 
treated as a <i>&beta;</i>-content tolerance interval, the confidence level associated with this 
tolerance interval is usually around 50% (e.g., Guttman, 1970, Table 4.2, p.76).  
</p>
<p>For a normal distribution, the form of a two-sided <i>100(1-&alpha;)\%</i> tolerance 
interval is: 
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, \, \bar{x} + Ks]</i></p>
 
<p>where <i>\bar{x}</i> denotes the sample 
mean, <i>s</i> denotes the sample standard deviation, and <i>K</i> denotes a constant 
that depends on the sample size <i>n</i>, the coverage, and, for a <i>&beta;</i>-content 
tolerance interval (but not a <i>&beta;</i>-expectation tolerance interval), the 
confidence level.  
</p>
<p>Similarly, the form of a one-sided lower tolerance interval is: 
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, \, &infin;]</i></p>
 
<p>and the form of a one-sided upper tolerance interval is:
</p>
<p style="text-align: center;"><i>[-&infin;, \, \bar{x} + Ks]</i></p>
 
<p>but <i>K</i> differs for one-sided versus two-sided tolerance intervals.  
The derivation of the constant <i>K</i> is explained in the help file for 
<code><a href="../../EnvStats/help/tolIntNormK.html">tolIntNormK</a></code>.
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, <code>tolIntNorm</code> returns a list of class 
<code>"estimate"</code> containing the estimated parameters, a component called 
<code>interval</code> containing the tolerance interval information, and other 
information.  See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function, <code>tolIntNorm</code> 
returns a list whose class is the same as <code>x</code>.  The list contains the same 
components as <code>x</code>.  If <code>x</code> already has a component called 
<code>interval</code>, this component is replaced with the tolerance interval 
information.
</p>


<h3>Note</h3>

<p>Tolerance intervals have long been applied to quality control and 
life testing problems (Hahn, 1970b,c; Hahn and Meeker, 1991; Krishnamoorthy and Mathew, 2009).  
References that discuss tolerance intervals in the context of environmental monitoring include:  
Berthouex and Brown (2002, Chapter 21), Gibbons et al. (2009), 
Millard and Neerchal (2001, Chapter 6), Singh et al. (2010b), and USEPA (2009).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). <em>Statistics for Environmental Engineers</em>. 
Lewis Publishers, Boca Raton.
</p>
<p>Draper, N., and H. Smith. (1998). <em>Applied Regression Analysis</em>. Third Edition. 
John Wiley and Sons, New York.
</p>
<p>Ellison, B.E. (1964). On Two-Sided Tolerance Intervals for a Normal Distribution. 
<em>Annals of Mathematical Statistics</em> <b>35</b>, 762-772.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009). 
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.  
John Wiley &amp; Sons, Hoboken.
</p>
<p>Guttman, I. (1970). <em>Statistical Tolerance Regions: Classical and Bayesian</em>. 
Hafner Publishing Co., Darien, CT.
</p>
<p>Hahn, G.J. (1970b). Statistical Intervals for a Normal Population, Part I: Tables, Examples 
and Applications. <em>Journal of Quality Technology</em> <b>2</b>(3), 115-125.
</p>
<p>Hahn, G.J. (1970c). Statistical Intervals for a Normal Population, Part II: Formulas, Assumptions, 
Some Derivations. <em>Journal of Quality Technology</em> <b>2</b>(4), 195-206.
</p>
<p>Hahn, G.J., and W.Q. Meeker. (1991). <em>Statistical Intervals: A Guide for Practitioners</em>. 
John Wiley and Sons, New York.
</p>
<p>Krishnamoorthy K., and T. Mathew. (2009). 
<em>Statistical Tolerance Regions: Theory, Applications, and Computation</em>. 
John Wiley and Sons, Hoboken.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton.
</p>
<p>Odeh, R.E., and D.B. Owen. (1980). <em>Tables for Normal Tolerance Limits, Sampling Plans, 
and Screening</em>. Marcel Dekker, New York.
</p>
<p>Owen, D.B. (1962). <em>Handbook of Statistical Tables</em>. Addison-Wesley, Reading, MA.
</p>
<p>Singh, A., R. Maichle, and N. Armbya. (2010a). 
<em>ProUCL Version 4.1.00 User Guide (Draft)</em>. EPA/600/R-07/041, May 2010. 
Office of Research and Development, U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Singh, A., N. Armbya, and A. Singh. (2010b). 
<em>ProUCL Version 4.1.00 Technical Guide (Draft)</em>. EPA/600/R-07/041, May 2010.  
Office of Research and Development, U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2010).  <em>Errata Sheet - March 2009 Unified Guidance</em>.
EPA 530/R-09-007a, August 9, 2010.  Office of Resource Conservation and Recovery, Program Information and Implementation Division.
U.S. Environmental Protection Agency, Washington, D.C. 
</p>
<p>Wald, A., and J. Wolfowitz. (1946). Tolerance Limits for a Normal Distribution. 
<em>Annals of Mathematical Statistics</em> <b>17</b>, 208-215.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/tolIntNormK.html">tolIntNormK</a></code>, <code><a href="../../EnvStats/help/tolIntLnorm.html">tolIntLnorm</a></code>, <code><a href="../../stats/html/Normal.html">Normal</a></code>, 
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>, <code><a href="../../EnvStats/help/enorm.html">enorm</a></code>, <code><a href="../../EnvStats/help/eqnorm.html">eqnorm</a></code>, 
<code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>, <a href="../../EnvStats/help/Tolerance+20Intervals.html">Tolerance Intervals</a>, 
<a href="../../EnvStats/help/Estimating+20Distribution+20Parameters.html">Estimating Distribution Parameters</a>, <a href="../../EnvStats/help/Estimating+20Distribution+20Quantiles.html">Estimating Distribution Quantiles</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a normal distribution with parameters 
  # mean=10 and sd=2, then create a tolerance interval. 
  # (Note: the call to set.seed simply allows you to reproduce this 
  # example.)

  set.seed(250) 
  dat &lt;- rnorm(20, mean = 10, sd = 2) 
  tolIntNorm(dat)

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Normal
  #
  #Estimated Parameter(s):          mean = 9.861160
  #                                 sd   = 1.180226
  #
  #Estimation Method:               mvue
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Tolerance Interval Coverage:     95%
  #
  #Coverage Type:                   content
  #
  #Tolerance Interval Method:       Exact
  #
  #Tolerance Interval Type:         two-sided
  #
  #Confidence Level:                95%
  #
  #Tolerance Interval:              LTL =  6.603328
  #                                 UTL = 13.118993

  #----------

  # Clean up
  rm(dat)
  
  #--------------------------------------------------------------------

  # Example 17-3 of USEPA (2009, p. 17-17) shows how to construct a 
  # beta-content upper tolerance limit with 95% coverage and 95% 
  # confidence  using chrysene data and assuming a lognormal distribution.  
  # The data for this example are stored in EPA.09.Ex.17.3.chrysene.df, 
  # which contains chrysene concentration data (ppb) found in water 
  # samples obtained from two background wells (Wells 1 and 2) and 
  # three compliance wells (Wells 3, 4, and 5).  The tolerance limit 
  # is based on the data from the background wells.

  # Here we will first take the log of the data and  
  # then construct the tolerance interval; note however that it is 
  # easier to call the function tolIntLnorm instead using the 
  # original data.

  head(EPA.09.Ex.17.3.chrysene.df)
  #  Month   Well  Well.type Chrysene.ppb
  #1     1 Well.1 Background         19.7
  #2     2 Well.1 Background         39.2
  #3     3 Well.1 Background          7.8
  #4     4 Well.1 Background         12.8
  #5     1 Well.2 Background         10.2
  #6     2 Well.2 Background          7.2

  longToWide(EPA.09.Ex.17.3.chrysene.df, "Chrysene.ppb", "Month", "Well")
  #  Well.1 Well.2 Well.3 Well.4 Well.5
  #1   19.7   10.2   68.0   26.8   47.0
  #2   39.2    7.2   48.9   17.7   30.5
  #3    7.8   16.1   30.1   31.9   15.0
  #4   12.8    5.7   38.1   22.2   23.4

  tol.int.list &lt;- with(EPA.09.Ex.17.3.chrysene.df, 
    tolIntNorm(log(Chrysene.ppb[Well.type == "Background"]), 
    ti.type = "upper", coverage = 0.95, conf.level = 0.95))

  tol.int.list

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Normal
  #
  #Estimated Parameter(s):          mean = 2.5085773
  #                                 sd   = 0.6279479
  #
  #Estimation Method:               mvue
  #
  #Data:                            log(Chrysene.ppb[Well.type == "Background"])
  #
  #Sample Size:                     8
  #
  #Tolerance Interval Coverage:     95%
  #
  #Coverage Type:                   content
  #
  #Tolerance Interval Method:       Exact
  #
  #Tolerance Interval Type:         upper
  #
  #Confidence Level:                95%
  #
  #Tolerance Interval:              LTL =     -Inf
  #                                 UTL = 4.510032

  # Compute the upper tolerance interaval on the original scale
  # by exponentiating the upper tolerance limit:

  exp(tol.int.list$interval$limits["UTL"])
  #    UTL 
  #90.9247

  #----------

  # Clean up

  rm(tol.int.list)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
