<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Predict Concentration Using Calibration</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for inversePredictCalibrate {EnvStats}"><tr><td>inversePredictCalibrate {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Predict Concentration Using Calibration 
</h2>

<h3>Description</h3>

<p>Predict concentration using a calibration line (or curve) and inverse regression.
</p>


<h3>Usage</h3>

<pre>
  inversePredictCalibrate(object, obs.y = NULL, 
    n.points = ifelse(is.null(obs.y), 100, length(obs.y)), 
    intervals = FALSE, coverage = 0.99, simultaneous = FALSE, 
    individual = FALSE, trace = FALSE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>object</code></td>
<td>

<p>an object of class <code>"calibrate"</code> that is the result of calling the function <br />
<code><a href="../../EnvStats/help/calibrate.html">calibrate</a></code>.
</p>
</td></tr>
<tr valign="top"><td><code>obs.y</code></td>
<td>

<p>optional numeric vector of observed values for the machine signal.  
The default value is <code>obs.y=NULL</code>, in which case <code>obs.y</code> is set equal to 
a vector of values (of length <code>n.points</code>) ranging from the minimum to the maximum of 
the fitted values from the calibrate object.
</p>
</td></tr>
<tr valign="top"><td><code>n.points</code></td>
<td>

<p>optional integer indicating the number of points at which to predict concentrations 
(i.e., perform inverse regression). The default value is <code>n.points=100</code>. 
This argument is ignored when <code>obs.y</code> is supplied.
</p>
</td></tr>
<tr valign="top"><td><code>intervals</code></td>
<td>

<p>optional logical scalar indicating whether to compute confidence intervals for 
the predicted concentrations. The default value is <code>intervals=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>coverage</code></td>
<td>

<p>optional numeric scalar between 0 and 1 indicating the confidence level associated with 
the confidence intervals for the predicted concentrations. 
The default value is <code>coverage=0.99</code>.
</p>
</td></tr>
<tr valign="top"><td><code>simultaneous</code></td>
<td>

<p>optional logical scalar indicating whether to base the confidence intervals 
for the predicted values on simultaneous or non-simultaneous prediction limits. 
The default value is <code>simultaneous=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>individual</code></td>
<td>

<p>optional logical scalar indicating whether to base the confidence intervals for the predicted values 
on prediction limits for the mean (<code>individual=FALSE</code>) or prediction limits for 
an individual observation (<code>individual=TRUE</code>).  
The default value is <code>individual=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>trace</code></td>
<td>

<p>optional logical scalar indicating whether to print out (trace) the progress of 
the inverse prediction for each of the specified values of <code>obs.y</code>. 
The default value is <code>trace=FALSE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>A simple and frequently used calibration model is a straight line where the 
response variable <i>S</i> denotes the signal of the machine and the 
predictor variable <i>C</i> denotes the true concentration in the physical 
sample.  The error term is assumed to follow a normal distribution with 
mean 0.  Note that the average value of the signal for a blank (<i>C = 0</i>) 
is the intercept.  Other possible calibration models include higher order 
polynomial models such as a quadratic or cubic model.
</p>
<p>In a typical setup, a small number of samples (e.g., <i>n = 6</i>) with known 
concentrations are measured and the signal is recorded.  A sample with no 
chemical in it, called a blank, is also measured.  (You have to be careful 
to define exactly what you mean by a &ldquo;blank.&rdquo;  A blank could mean 
a container from the lab that has nothing in it but is prepared in a similar 
fashion to containers with actual samples in them.  Or it could mean a 
field blank: the container was taken out to the field and subjected to the 
same process that all other containers were subjected to, except a physical 
sample of soil or water was not placed in the container.)  Usually, 
replicate measures at the same known concentrations are taken.  
(The term &ldquo;replicate&rdquo; must be well defined to distinguish between for 
example the same physical samples that are measured more than once vs. two 
different physical samples of the same known concentration.)
</p>
<p>The function <code><a href="../../EnvStats/help/calibrate.html">calibrate</a></code> initially fits a linear calibration 
line or curve.  Once the calibration line is fit, samples with unknown 
concentrations are measured and their signals are recorded.  In order to 
produce estimated concentrations, you have to use inverse regression to 
map the signals to the estimated concentrations.  We can quantify the 
uncertainty in the estimated concentration by combining inverse regression 
with prediction limits for the signal <i>S</i>.
</p>


<h3>Value</h3>

<p>A numeric matrix containing the results of the inverse calibration. 
The first two columns are labeled <code>obs.y</code> and <code>pred.x</code> containing 
the values of the argument <code>obs.y</code> and the predicted values of <code>x</code> 
(the concentration), respectively. If <code>intervals=TRUE</code>, then the matrix also 
contains the columns <code>lpl.x</code> and <code>upl.x</code> corresponding to the lower and 
upper prediction limits for <code>x</code>.  Also, if <code>intervals=TRUE</code>, then the 
matrix has the attributes <code>coverage</code> (the value of the argument <code>coverage</code>) 
and <code>simultaneous</code> (the value of the argument <code>simultaneous</code>).
</p>


<h3>Note</h3>

<p>Almost always the process of determining the concentration of a chemical in 
a soil, water, or air sample involves using some kind of machine that 
produces a signal, and this signal is related to the concentration of the 
chemical in the physical sample. The process of relating the machine signal 
to the concentration of the chemical is called <strong>calibration</strong> 
(see <code><a href="../../EnvStats/help/calibrate.html">calibrate</a></code>). Once calibration has been performed, 
estimated concentrations in physical samples with unknown concentrations 
are computed using inverse regression.  The uncertainty in the process used 
to estimate the concentration may be quantified with decision, detection, 
and quantitation limits.
</p>
<p>In practice, only the point estimate of concentration is reported (along 
with a possible qualifier), without confidence bounds for the true 
concentration <i>C</i>. This is most unfortunate because it gives the 
impression that there is no error associated with the reported concentration. 
Indeed, both the International Organization for Standardization (ISO) and 
the International Union of Pure and Applied Chemistry (IUPAC) recommend 
always reporting both the estimated concentration and the uncertainty 
associated with this estimate (Currie, 1997).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Currie, L.A. (1997). Detection: International Update, and Some Emerging Di-Lemmas Involving Calibration, the Blank, and Multiple Detection Decisions. 
<em>Chemometrics and Intelligent Laboratory Systems</em> <b>37</b>, 151&ndash;181.
</p>
<p>Draper, N., and H. Smith. (1998). <em>Applied Regression Analysis</em>. Third Edition. 
John Wiley and Sons, New York, Chapter 3 and p.335.
</p>
<p>Hubaux, A., and G. Vos. (1970). Decision and Detection Limits for Linear Calibration Curves. 
<em>Annals of Chemistry</em> <b>42</b>, 849&ndash;855.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, FL, pp.562&ndash;575.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/pointwise.html">pointwise</a></code>, <code><a href="../../EnvStats/help/calibrate.html">calibrate</a></code>, <code><a href="../../EnvStats/help/detectionLimitCalibrate.html">detectionLimitCalibrate</a></code>, <code><a href="../../stats/html/lm.html">lm</a></code>
</p>


<h3>Examples</h3>

<pre>
  # The data frame EPA.97.cadmium.111.df contains calibration data 
  # for cadmium at mass 111 (ng/L) that appeared in 
  # Gibbons et al. (1997b) and were provided to them by the U.S. EPA.  
  # Here we 
  # 1. Display a plot of these data along with the fitted calibration 
  #    line and 99% non-simultaneous prediction limits. 
  # 2. Then based on an observed signal of 60 from a sample with 
  #    unknown concentration, we use the calibration line to estimate 
  #    the true concentration and use the prediction limits to compute 
  #    confidence bounds for the true concentration. 
  # An observed signal of 60 results in an estimated value of cadmium 
  # of 59.97 ng/L and a confidence interval of [53.83, 66.15]. 
  # See Millard and Neerchal (2001, pp.566-569) for more details on 
  # this example.

  Cadmium &lt;- EPA.97.cadmium.111.df$Cadmium 

  Spike &lt;- EPA.97.cadmium.111.df$Spike 

  calibrate.list &lt;- calibrate(Cadmium ~ Spike, 
    data = EPA.97.cadmium.111.df) 

  newdata &lt;- data.frame(Spike = seq(min(Spike), max(Spike), 
    length.out = 100))

  pred.list &lt;- predict(calibrate.list, newdata = newdata, se.fit = TRUE) 

  pointwise.list &lt;- pointwise(pred.list, coverage = 0.99, 
    individual = TRUE)

  plot(Spike, Cadmium, ylim = c(min(pointwise.list$lower), 
    max(pointwise.list$upper)), xlab = "True Concentration (ng/L)", 
    ylab = "Observed Concentration (ng/L)") 

  abline(calibrate.list, lwd=2) 

  lines(newdata$Spike, pointwise.list$lower, lty=8, lwd=2) 

  lines(newdata$Spike, pointwise.list$upper, lty=8, lwd=2) 

  title(paste("Calibration Line and 99% Prediction Limits", 
    "for US EPA Cadmium 111 Data", sep = "\n")) 

 
  # Now estimate the true concentration based on 
  # an observed signal of 60 ng/L. 

  inversePredictCalibrate(calibrate.list, obs.y = 60, 
    intervals = TRUE, coverage = 0.99, individual = TRUE) 

  #     obs.y   pred.x   lpl.x    upl.x 
  #[1,]    60 59.97301 53.8301 66.15422 
  #attr(, "coverage"): 
  #[1] 0.99 
  #attr(, "simultaneous"): 
  #[1] FALSE

  rm(Cadmium, Spike, calibrate.list, newdata, pred.list, pointwise.list) 
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
