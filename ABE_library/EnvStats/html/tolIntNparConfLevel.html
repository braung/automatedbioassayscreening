<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Confidence Level for Nonparametric Tolerance Interval for...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for tolIntNparConfLevel {EnvStats}"><tr><td>tolIntNparConfLevel {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Confidence Level for Nonparametric Tolerance Interval for Continuous Distribution
</h2>

<h3>Description</h3>

<p>Compute the confidence level associated with a nonparametric <i>&beta;</i>-content tolerance 
interval for a continuous distribution given the sample size, coverage, and ranks of the 
order statistics used for the interval.
</p>


<h3>Usage</h3>

<pre>
  tolIntNparConfLevel(n, coverage = 0.95, 
    ltl.rank = ifelse(ti.type == "upper", 0, 1), 
    n.plus.one.minus.utl.rank = ifelse(ti.type == "lower", 0, 1), 
    ti.type = "two.sided")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n</code></td>
<td>

<p>vector of positive integers specifying the sample sizes.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are not allowed.  
</p>
</td></tr>
<tr valign="top"><td><code>coverage</code></td>
<td>

<p>numeric vector of values between 0 and 1 indicating the desired coverage of the 
<i>&beta;</i>-content tolerance interval.
</p>
</td></tr>
<tr valign="top"><td><code>ltl.rank</code></td>
<td>

<p>vector of positive integers indicating the rank of the order statistic to use for the lower bound 
of the tolerance interval.  If <code>ti.type="two-sided"</code> or <br />
<code>ti.type="lower"</code>, 
the default value is <code>ltl.rank=1</code> (implying the minimum value of <code>x</code> is used 
as the lower bound of the tolerance interval).  If <br />
<code>ti.type="upper"</code>, this argument 
is set equal to <code>0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>n.plus.one.minus.utl.rank</code></td>
<td>

<p>vector of positive integers related to the rank of the order statistic to use for 
the upper bound of the tolerance interval.  A value of 
<code>n.plus.one.minus.utl.rank=1</code> (the default) means use the 
first largest value, and in general a value of <br />
<code>n.plus.one.minus.utl.rank=</code><i>i</i> means use the <i>i</i>'th largest value.  
If <br />
<code>ti.type="lower"</code>, this argument is set equal to <code>0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ti.type</code></td>
<td>

<p>character string indicating what kind of tolerance interval to compute.  
The possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If the arguments <code>n</code>, <code>coverage</code>, <code>ltl.rank</code>, and 
<code>n.plus.one.minus.utl.rank</code> are not all the same length, they are replicated to be the 
same length as the length of the longest argument.
</p>
<p>The help file for <code><a href="../../EnvStats/help/tolIntNpar.html">tolIntNpar</a></code> explains how nonparametric <i>&beta;</i>-content 
tolerance intervals are constructed and how the confidence level 
associated with the tolerance interval is computed based on specified values 
for the sample size, the coverage, and the ranks of the order statistics used for 
the bounds of the tolerance interval. 
</p>


<h3>Value</h3>

<p>vector of values between 0 and 1 indicating the confidence level associated with 
the specified nonparametric tolerance interval.
</p>


<h3>Note</h3>

<p>See the help file for <code><a href="../../EnvStats/help/tolIntNpar.html">tolIntNpar</a></code>.
</p>
<p>In the course of designing a sampling program, an environmental scientist may wish to determine 
the relationship between sample size, coverage, and confidence level if one of the objectives of 
the sampling program is to produce tolerance intervals.  The functions 
<code><a href="../../EnvStats/help/tolIntNparN.html">tolIntNparN</a></code>, <code><a href="../../EnvStats/help/tolIntNparCoverage.html">tolIntNparCoverage</a></code>, <code>tolIntNparConfLevel</code>, and 
<code><a href="../../EnvStats/help/plotTolIntNparDesign.html">plotTolIntNparDesign</a></code> can be used to investigate these relationships for 
constructing nonparametric tolerance intervals.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>See the help file for <code><a href="../../EnvStats/help/tolIntNpar.html">tolIntNpar</a></code>.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/tolIntNpar.html">tolIntNpar</a></code>, <code><a href="../../EnvStats/help/tolIntNparN.html">tolIntNparN</a></code>, <code><a href="../../EnvStats/help/tolIntNparCoverage.html">tolIntNparCoverage</a></code>, 
<code><a href="../../EnvStats/help/plotTolIntNparDesign.html">plotTolIntNparDesign</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Look at how the confidence level of a nonparametric tolerance interval increases with 
  # increasing sample size:

  seq(10, 60, by=10) 
  #[1] 10 20 30 40 50 60 

  round(tolIntNparConfLevel(n = seq(10, 60, by = 10)), 2) 
  #[1] 0.09 0.26 0.45 0.60 0.72 0.81

  #----------

  # Look at how the confidence level of a nonparametric tolerance interval decreases with 
  # increasing coverage:

  seq(0.5, 0.9, by = 0.1) 
  #[1] 0.5 0.6 0.7 0.8 0.9 

  round(tolIntNparConfLevel(n = 10, coverage = seq(0.5, 0.9, by = 0.1)), 2) 
  #[1] 0.99 0.95 0.85 0.62 0.26

  #----------

  # Look at how the confidence level of a nonparametric tolerance interval decreases with the 
  # rank of the lower tolerance limit:

  round(tolIntNparConfLevel(n = 60, ltl.rank = 1:5), 2) 
  #[1] 0.81 0.58 0.35 0.18 0.08

  #==========

  # Example 17-4 on page 17-21 of USEPA (2009) uses copper concentrations (ppb) from 3 
  # background wells to set an upper limit for 2 compliance wells.  There are 6 observations 
  # per well, and the maximum value from the 3 wells is set to the 95% confidence upper 
  # tolerance limit, and we need to determine the coverage of this tolerance interval.  

  tolIntNparCoverage(n = 24, conf.level = 0.95, ti.type = "upper")
  #[1] 0.8826538

  # Here we will modify the example and determine the confidence level of the tolerance 
  # interval when we set the coverage to 95%. 

  tolIntNparConfLevel(n = 24, coverage = 0.95, ti.type = "upper")
  # [1] 0.708011
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
