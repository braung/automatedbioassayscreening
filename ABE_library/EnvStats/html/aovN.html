<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Compute Sample Size Necessary to Achieve Specified Power for...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for aovN {EnvStats}"><tr><td>aovN {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Compute Sample Size Necessary to Achieve Specified Power for One-Way Fixed-Effects Analysis of Variance
</h2>

<h3>Description</h3>

<p>Compute the sample sizes necessary to achieve a specified power 
for a one-way fixed-effects analysis of variance test, given 
the population means, population standard deviation, and 
significance level.
</p>


<h3>Usage</h3>

<pre>
  aovN(mu.vec, sigma = 1, alpha = 0.05, power = 0.95, 
    round.up = TRUE, n.max = 5000, tol = 1e-07, maxiter = 1000)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>mu.vec</code></td>
<td>

<p>required numeric vector of population means. The length of 
<code>mu.vec</code> must be at least 2.  Missing (<code>NA</code>), 
undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>sigma</code></td>
<td>

<p>optional numeric scalar specifying the population standard 
deviation (<i>&sigma;</i>) for each group.  The default value 
is <code>sigma=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>alpha</code></td>
<td>

<p>optional numeric scalar between 0 and 1 indicating the Type I 
error level associated with the hypothesis test.  The default 
value is <code>alpha=0.05</code>.
</p>
</td></tr>
<tr valign="top"><td><code>power</code></td>
<td>

<p>optional numeric scalar between 0 and 1 indicating the power 
associated with the hypothesis test.  The default value 
is <code>power=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>round.up</code></td>
<td>

<p>optional logical scalar indicating whether to round up the value of the 
computed sample size to the next smallest integer.  The default 
value is <code>round.up=TRUE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>n.max</code></td>
<td>

<p>positive integer greater then 1 indicating the maximum sample size per group.  
The default value is <code>n.max=5000</code>.
</p>
</td></tr>
<tr valign="top"><td><code>tol</code></td>
<td>

<p>optional numeric scalar indicating the tolerance to use in the 
<code><a href="../../stats/html/uniroot.html">uniroot</a></code> search algorithm.  The default value is 
<code>tol=1e-7</code>.
</p>
</td></tr>
<tr valign="top"><td><code>maxiter</code></td>
<td>

<p>optional positive integer indicating the maximum number of iterations to use in the 
<code><a href="../../stats/html/uniroot.html">uniroot</a></code> search algorithm.  The default value is 
<code>maxiter=1000</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The F-statistic to test the equality of <i>k</i> population means
assuming each population has a normal distribution with the same
standard deviation <i>&sigma;</i> is presented in most basic 
statistics texts, including Zar (2010, Chapter 10), 
Berthouex and Brown (2002, Chapter 24), and Helsel and Hirsh (1992, pp.164-169).  
The formula for the power of this test is given in Scheffe 
(1959, pp.38-39,62-65).  The power of the one-way fixed-effects ANOVA depends 
on the sample sizes for each of the <i>k</i> groups, the value of the 
population means for each of the <i>k</i> groups, the population 
standard deviation <i>&sigma;</i>, and the significance level
<i>&alpha;</i>.  See the help file for <code><a href="../../EnvStats/help/aovPower.html">aovPower</a></code>.
</p>
<p>The function <code>aovN</code> assumes equal sample 
sizes for each of the <i>k</i> groups and uses a search 
algorithm to determine the sample size <i>n</i> required to 
attain a specified power, given the values of the population 
means and the significance level.
</p>


<h3>Value</h3>

<p>numeric scalar indicating the required sample size for each 
group.  (The number of groups is equal to the length of the 
argument <code>mu.vec</code>.)
</p>


<h3>Note</h3>

<p>The normal and lognormal distribution are probably the two most 
frequently used distributions to model environmental data.  
Sometimes it is necessary to compare several means to determine 
whether any are significantly different from each other 
(e.g., USEPA, 2009, p.6-38).  In this case, assuming 
normally distributed data, you perform a one-way parametric 
analysis of variance.
</p>
<p>In the course of designing a sampling program, an environmental 
scientist may wish to determine the relationship between sample 
size, Type I error level, power, and differences in means if 
one of the objectives of the sampling program is to determine 
whether a particular mean differs from a group of means.  The 
functions <code><a href="../../EnvStats/help/aovPower.html">aovPower</a></code>, <code>aovN</code>, and 
<code><a href="../../EnvStats/help/plotAovDesign.html">plotAovDesign</a></code> can be used to investigate these 
relationships for the case of normally-distributed observations.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002).  
<em>Statistics for Environmental Engineers</em>.  Second Edition.  
Lewis Publishers, Boca Raton, FL.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). 
<em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York, NY, Chapter 7.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995). 
<em>Continuous Univariate Distributions, Volume 2</em>. 
Second Edition. John Wiley and Sons, New York, 
Chapters 27, 29, 30.
</p>
<p>Millard, S.P., and Neerchal, N.K. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, Florida.
</p>
<p>Scheffe, H. (1959). <em>The Analysis of Variance</em>. 
John Wiley and Sons, New York, 477pp.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C. p.6-38.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. 
Fifth Edition. Prentice-Hall, Upper Saddle River, NJ, 
Chapter 10.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/aovPower.html">aovPower</a></code>, <code><a href="../../EnvStats/help/plotAovDesign.html">plotAovDesign</a></code>, 
<code><a href="../../stats/html/Normal.html">Normal</a></code>, <code><a href="../../stats/html/aov.html">aov</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Look at how the required sample size for a one-way ANOVA 
  # increases with increasing power:

  aovN(mu.vec = c(10, 12, 15), sigma = 5, power = 0.8) 
  #[1] 21 

  aovN(mu.vec = c(10, 12, 15), sigma = 5, power = 0.9) 
  #[1] 27 

  aovN(mu.vec = c(10, 12, 15), sigma = 5, power = 0.95) 
  #[1] 33

  #----------------------------------------------------------------

  # Look at how the required sample size for a one-way ANOVA, 
  # given a fixed power, decreases with increasing variability 
  # in the population means:

  aovN(mu.vec = c(10, 10, 11), sigma=5) 
  #[1] 581 

  aovN(mu.vec = c(10, 10, 15), sigma = 5) 
  #[1] 25 

  aovN(mu.vec = c(10, 13, 15), sigma = 5) 
  #[1] 33 

  aovN(mu.vec = c(10, 15, 20), sigma = 5) 
  #[1] 10

  #----------------------------------------------------------------

  # Look at how the required sample size for a one-way ANOVA, 
  # given a fixed power, decreases with increasing values of 
  # Type I error:

  aovN(mu.vec = c(10, 12, 14), sigma = 5, alpha = 0.001) 
  #[1] 89 

  aovN(mu.vec = c(10, 12, 14), sigma = 5, alpha = 0.01) 
  #[1] 67 

  aovN(mu.vec = c(10, 12, 14), sigma = 5, alpha = 0.05) 
  #[1] 50 

  aovN(mu.vec = c(10, 12, 14), sigma = 5, alpha = 0.1) 
  #[1] 42
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
