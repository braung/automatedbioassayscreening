<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Compute the Value of K for a Tolerance Interval for a Normal...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for tolIntNormK {EnvStats}"><tr><td>tolIntNormK {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Compute the Value of <i>K</i> for a Tolerance Interval for a Normal Distribution
</h2>

<h3>Description</h3>

<p>Compute the value of <i>K</i> (the multiplier of estimated standard deviation) used 
to construct a tolerance interval based on data from a normal distribution.
</p>


<h3>Usage</h3>

<pre>
  tolIntNormK(n, df = n - 1, coverage = 0.95, cov.type = "content", 
    ti.type = "two-sided", conf.level = 0.95, method = "exact", 
    rel.tol = 1e-07, abs.tol = rel.tol)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n</code></td>
<td>

<p>a positive integer greater than 2 indicating the sample size upon which the 
tolerance interval is based.
</p>
</td></tr>
<tr valign="top"><td><code>df</code></td>
<td>

<p>the degrees of freedom associated with the tolerance interval.  The default is 
<code>df=n-1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>coverage</code></td>
<td>

<p>a scalar between 0 and 1 indicating the desired coverage of the tolerance interval.  
The default value is <code>coverage=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>cov.type</code></td>
<td>

<p>character string specifying the coverage type for the tolerance interval.  
The possible values are <code>"content"</code> (<i>&beta;</i>-content; the default), and 
<code>"expectation"</code> (<i>&beta;</i>-expectation).  See the help file for <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code> 
for more information on the difference between <i>&beta;</i>-content and <i>&beta;</i>-expectation 
tolerance intervals. 
</p>
</td></tr>
<tr valign="top"><td><code>ti.type</code></td>
<td>

<p>character string indicating what kind of tolerance interval to compute.  
The possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level associated with the tolerance 
interval.  The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>for the case of a two-sided tolerance interval, a character string specifying the method for 
constructing the tolerance interval.  This argument is ignored if <code>ti.type="lower"</code> or 
<code>ti.type="upper"</code>.  The possible values are <br />
<code>"exact"</code> (the default) and <code>"wald.wolfowitz"</code> (the Wald-Wolfowitz approximation).  
See the DETAILS section for more information.
</p>
</td></tr>
<tr valign="top"><td><code>rel.tol</code></td>
<td>

<p>in the case when <code>ti.type="two-sided"</code> and <code>method="exact"</code>, the argument 
<code>rel.tol</code> is passed to the function <code><a href="../../stats/html/integrate.html">integrate</a></code>.  The default value is <br />
<code>rel.tol=1e-07</code>.
</p>
</td></tr>
<tr valign="top"><td><code>abs.tol</code></td>
<td>

<p>in the case when <code>ti.type="two-sided"</code> and <code>method="exact"</code>, the argument 
<code>abs.tol</code> is passed to the function <code><a href="../../stats/html/integrate.html">integrate</a></code>.  The default value is the 
value of <code>rel.tol</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>A tolerance interval for some population is an interval on the real line constructed so as to 
contain <i>100 &beta; \%</i> of the population (i.e., <i>100 &beta; \%</i> of all future observations), 
where <i>0 &lt; &beta; &lt; 1</i>.  The quantity <i>100 &beta; \%</i> is called the coverage.
</p>
<p>There are two kinds of tolerance intervals (Guttman, 1970):
</p>
 
<ul>
<li><p> A <i>&beta;</i>-content tolerance interval with confidence level <i>100(1-&alpha;)\%</i> is 
constructed so that it contains at least <i>100 &beta; \%</i> of the population (i.e., the 
coverage is at least <i>100 &beta; \%</i>) with probability <i>100(1-&alpha;)\%</i>, where 
<i>0 &lt; &alpha; &lt; 1</i>. The quantity <i>100(1-&alpha;)\%</i> is called the confidence level or 
confidence coefficient associated with the tolerance interval.
</p>
</li>
<li><p> A <i>&beta;</i>-expectation tolerance interval is constructed so that the 
<em>average</em> coverage of the interval is <i>100 &beta; \%</i>.
</p>
</li></ul>
 
<p><b>Note:</b> A <i>&beta;</i>-expectation tolerance interval with coverage <i>100 &beta; \%</i> is 
equivalent to a prediction interval for one future observation with associated confidence level 
<i>100 &beta; \%</i>.  Note that there is no explicit confidence level associated with a 
<i>&beta;</i>-expectation tolerance interval.  If a <i>&beta;</i>-expectation tolerance interval is 
treated as a <i>&beta;</i>-content tolerance interval, the confidence level associated with this 
tolerance interval is usually around 50% (e.g., Guttman, 1970, Table 4.2, p.76).  
</p>
<p>For a normal distribution, the form of a two-sided <i>100(1-&alpha;)\%</i> tolerance 
interval is: </p>
<p style="text-align: center;"><i>[\bar{x} - Ks, \, \bar{x} + Ks]</i></p>
<p> where <i>\bar{x}</i> denotes the sample 
mean, <i>s</i> denotes the sample standard deviation, and <i>K</i> denotes a constant 
that depends on the sample size <i>n</i>, the coverage, and, for a <i>&beta;</i>-content 
tolerance interval (but not a <i>&beta;</i>-expectation tolerance interval), 
the confidence level.  
</p>
<p>Similarly, the form of a one-sided lower tolerance interval is: 
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, \, &infin;]</i></p>
 
<p>and the form of a one-sided upper tolerance interval is:
</p>
<p style="text-align: center;"><i>[-&infin;, \, \bar{x} + Ks]</i></p>
 
<p>but <i>K</i> differs for one-sided versus two-sided tolerance intervals.
<br />
</p>
<p><strong>The Derivation of <i>K</i> for a <i>&beta;</i>-Content Tolerance Interval</strong>
</p>
<p><em>One-Sided Case</em>
</p>
<p>When <code>ti.type="upper"</code> or <code>ti.type="lower"</code>, the constant <i>K</i> for a 
<i>100 &beta; \%</i> <i>&beta;</i>-content tolerance interval with associated 
confidence level <i>100(1 - &alpha;)\%</i> is given by: 
</p>
<p style="text-align: center;"><i>K = t(n-1, 1 - &alpha;, z_&beta; &radic;{n}) / &radic;{n}</i></p>

<p>where <i>t(&nu;, p, &delta;)</i> denotes the <i>p</i>'th quantile of a non-central 
t-distribution with <i>&nu;</i> degrees of freedom and noncentrality parameter 
<i>&delta;</i> (see the help file for <a href="../../stats/html/TDist.html">TDist</a>), and <i>z_p</i> denotes the 
<i>p</i>'th quantile of a standard normal distribution.
<br />
<br />
</p>
<p><em>Two-Sided Case</em>
</p>
<p>When <code>ti.type="two-sided"</code> and <code>method="exact"</code>, the exact formula for 
the constant <i>K</i> for a <i>100 &beta; \%</i> <i>&beta;</i>-content tolerance interval 
with associated confidence level <i>100(1-&alpha;)\%</i> requires numerical integration 
and has been derived by several different authors, including Odeh (1978), 
Eberhardt et al. (1989), Jilek (1988), Fujino (1989), and Janiga and Miklos (2001).  
Specifically, for given values of the sample size <i>n</i>, degrees of freedom <i>&nu;</i>, 
confidence level <i>(1-&alpha;)</i>, and coverage <i>&beta;</i>, the constant <i>K</i> is the 
solution to the equation: 
</p>
<p style="text-align: center;"><i>&radic;{\frac{n}{2 &pi;}} \, \int^&infin;_{-&infin;} {F(x, K, &nu;, R) \, e^{(-nx^2)/2}} \, dx = 1 - &alpha;</i></p>

<p>where <i>F(x, K, &nu;, R)</i> denotes the upper-tail area from <i>(&nu; \, R^2) / K^2</i> to 
<i>&infin;</i> of the chi-squared distribution with <i>&nu;</i> degrees of freedom, and 
<i>R</i> is the solution to the equation: 
</p>
<p style="text-align: center;"><i>&Phi; (x + R) - &Phi; (x - R) = &beta;</i></p>
<p> where 
<i>&Phi;()</i> denotes the standard normal cumulative distribuiton function.
<br />
<br />
</p>
<p>When <code>ti.type="two-sided"</code> and <code>method="wald.wolfowitz"</code>, the approximate formula 
due to Wald and Wolfowitz (1946) for the constant <i>K</i> for a <i>100 &beta; \%</i> 
<i>&beta;</i>-content tolerance interval with associated confidence level 
<i>100(1-&alpha;)\%</i> is given by: 
</p>
<p style="text-align: center;"><i>K \approx r \, u</i></p>
 
<p>where <i>r</i> is the solution to the equation:  
</p>
<p style="text-align: center;"><i>&Phi; (\frac{1}{&radic;{n}} + r) - &Phi; (\frac{1}{&radic;{n}} - r) = &beta;</i></p>
 
<p><i>&Phi; ()</i> denotes the standard normal cumulative distribuiton function, and <i>u</i> is 
given by: 
</p>
<p style="text-align: center;"><i>u = &radic;{\frac{n-1}{&chi;^{2} (n-1, &alpha;)}}</i></p>
 
<p>where <i>&chi;^{2} (&nu;, p)</i> denotes the <i>p</i>'th quantile of the chi-squared 
distribution with <i>&nu;</i> degrees of freedom.
<br />
<br />
</p>
<p><strong>The Derivation of <i>K</i> for a <i>&beta;</i>-Expectation Tolerance Interval</strong>
</p>
<p>As stated above, a <i>&beta;</i>-expectation tolerance interval with coverage <i>100 &beta; \%</i> is 
equivalent to a prediction interval for one future observation with associated confidence level 
<i>100 &beta; \%</i>.  This is because the probability that any single future observation will fall 
into this interval is <i>100 &beta; \%</i>, so the distribution of the number of <i>N</i> future 
observations that will fall into this interval is binomial with parameters <code>size =</code> <i>N</i> and 
<code>prob =</code> <i>&beta;</i> (see the help file for <a href="../../stats/html/Binomial.html">Binomial</a>).  Hence the expected proportion 
of future observations that will fall into this interval is <i>100 &beta; \%</i> and is independent of 
the value of <i>N</i>.  See the help file for <code><a href="../../EnvStats/help/predIntNormK.html">predIntNormK</a></code> for information on 
how to derive <i>K</i> for these intervals.
</p>


<h3>Value</h3>

<p>The value of <i>K</i>, a numeric scalar used to construct tolerance intervals for a normal 
(Gaussian) distribution.
</p>


<h3>Note</h3>

<p>Tabled values of <i>K</i> are given in Gibbons et al. (2009), Gilbert (1987), 
Guttman (1970), Krishnamoorthy and Mathew (2009), Owen (1962), Odeh and Owen (1980), 
and USEPA (2009). 
</p>
<p>Tolerance intervals have long been applied to quality control and 
life testing problems (Hahn, 1970b,c; Hahn and Meeker, 1991; Krishnamoorthy and Mathew, 2009).  
References that discuss tolerance intervals in the context of environmental monitoring include:  
Berthouex and Brown (2002, Chapter 21), Gibbons et al. (2009), 
Millard and Neerchal (2001, Chapter 6), Singh et al. (2010b), and USEPA (2009).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). <em>Statistics for Environmental Engineers</em>. 
Lewis Publishers, Boca Raton.
</p>
<p>Draper, N., and H. Smith. (1998). <em>Applied Regression Analysis</em>. Third Edition. 
John Wiley and Sons, New York.
</p>
<p>Eberhardt, K.R., R.W. Mee, and C.P. Reeve. (1989). Computing Factors for 
Exact Two-Sided Tolerance Limits for a Normal Distribution. <em>Communications 
in Statistics, Part B-Simulation and Computation</em> <b>18</b>, 397-413.
</p>
<p>Ellison, B.E. (1964). On Two-Sided Tolerance Intervals for a Normal Distribution. 
<em>Annals of Mathematical Statistics</em> <b>35</b>, 762-772.
</p>
<p>Fujino, T. (1989). Exact Two-Sided Tolerance Limits for a Normal Distribution. 
<em>Japanese Journal of Applied Statistics</em> <b>18</b>, 29-36.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009). 
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.  
John Wiley &amp; Sons, Hoboken.
</p>
<p>Gilbert, R.O. (1987). <em>Statistical Methods for Environmental Pollution Monitoring</em>. 
Van Nostrand Reinhold, New York.
</p>
<p>Guttman, I. (1970). <em>Statistical Tolerance Regions: Classical and Bayesian</em>. 
Hafner Publishing Co., Darien, CT.
</p>
<p>Hahn, G.J. (1970b). Statistical Intervals for a Normal Population, Part I: Tables, Examples 
and Applications. <em>Journal of Quality Technology</em> <b>2</b>(3), 115-125.
</p>
<p>Hahn, G.J. (1970c). Statistical Intervals for a Normal Population, Part II: Formulas, Assumptions, 
Some Derivations. <em>Journal of Quality Technology</em> <b>2</b>(4), 195-206.
</p>
<p>Hahn, G.J., and W.Q. Meeker. (1991). <em>Statistical Intervals: A Guide for Practitioners</em>. 
John Wiley and Sons, New York.
</p>
<p>Jilek, M. (1988). <em>Statisticke Tolerancni Meze</em>. SNTL, Praha.
</p>
<p>Krishnamoorthy K., and T. Mathew. (2009). 
<em>Statistical Tolerance Regions: Theory, Applications, and Computation</em>. 
John Wiley and Sons, Hoboken.
</p>
<p>Janiga, I., and R. Miklos. (2001). Statistical Tolerance Intervals for a Normal 
Distribution.  <em>Measurement Science Review</em> <b>1</b>1, 29-32.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton.
</p>
<p>Odeh, R.E. (1978). Tables of Two-Sided Tolerance Factors for a Normal Distribution. 
<em>Communications in Statistics, Part B-Simulation and Computation</em> <b>7</b>, 183-201.
</p>
<p>Odeh, R.E., and D.B. Owen. (1980). <em>Tables for Normal Tolerance Limits, Sampling Plans, 
and Screening</em>. Marcel Dekker, New York.
</p>
<p>Owen, D.B. (1962). <em>Handbook of Statistical Tables</em>. Addison-Wesley, Reading, MA.
</p>
<p>Singh, A., R. Maichle, and N. Armbya. (2010a). 
<em>ProUCL Version 4.1.00 User Guide (Draft)</em>. EPA/600/R-07/041, May 2010. 
Office of Research and Development, U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Singh, A., N. Armbya, and A. Singh. (2010b). 
<em>ProUCL Version 4.1.00 Technical Guide (Draft)</em>. EPA/600/R-07/041, May 2010.  
Office of Research and Development, U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2010).  <em>Errata Sheet - March 2009 Unified Guidance</em>.
EPA 530/R-09-007a, August 9, 2010.  Office of Resource Conservation and Recovery, Program Information and Implementation Division.
U.S. Environmental Protection Agency, Washington, D.C. 
</p>
<p>Wald, A., and J. Wolfowitz. (1946). Tolerance Limits for a Normal Distribution. 
<em>Annals of Mathematical Statistics</em> <b>17</b>, 208-215.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code>, <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>, <a href="../../stats/html/Normal.html">Normal</a>, 
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>, <code><a href="../../EnvStats/help/enorm.html">enorm</a></code>, <code><a href="../../EnvStats/help/eqnorm.html">eqnorm</a></code>, 
<a href="../../EnvStats/help/Tolerance+20Intervals.html">Tolerance Intervals</a>, <a href="../../EnvStats/help/Prediction+20Intervals.html">Prediction Intervals</a>, 
<a href="../../EnvStats/help/Estimating+20Distribution+20Parameters.html">Estimating Distribution Parameters</a>, 
<a href="../../EnvStats/help/Estimating+20Distribution+20Quantiles.html">Estimating Distribution Quantiles</a>.
</p>


<h3>Examples</h3>

<pre>
  # Compute the value of K for a two-sided 95% beta-content 
  # tolerance interval with associated confidence level 95% 
  # given a sample size of n=20.

  #----------
  # Exact method

  tolIntNormK(n = 20)
  #[1] 2.760346

  #----------
  # Approximate method due to Wald and Wolfowitz (1946)

  tolIntNormK(n = 20, method = "wald")
  # [1] 2.751789


  #--------------------------------------------------------------------

  # Compute the value of K for a one-sided upper tolerance limit 
  # with 99% coverage and associated confidence level 90% 
  # given a samle size of n=20.

  tolIntNormK(n = 20, ti.type = "upper", coverage = 0.99, 
    conf.level = 0.9)
  #[1] 3.051543

  #--------------------------------------------------------------------

  # Example 17-3 of USEPA (2009, p. 17-17) shows how to construct a 
  # beta-content upper tolerance limit with 95% coverage and 95% 
  # confidence  using chrysene data and assuming a lognormal 
  # distribution.  The sample size is n = 8 observations from 
  # the two compliance wells.  Here we will compute the 
  # multiplier for the log-transformed data.

  tolIntNormK(n = 8, ti.type = "upper")
  #[1] 3.187294
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
