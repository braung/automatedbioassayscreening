<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Rate Parameter of an Exponential Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for eexp {EnvStats}"><tr><td>eexp {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Rate Parameter of an Exponential Distribution
</h2>

<h3>Description</h3>

<p>Estimate the rate parameter of an 
<a href="../../stats/help/Exponential.html">exponential distribution</a>, and optionally construct a 
confidence interval for the rate parameter.
</p>


<h3>Usage</h3>

<pre>
  eexp(x, method = "mle/mme", ci = FALSE, ci.type = "two-sided", 
    ci.method = "exact", conf.level = 0.95)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  Currently the only 
possible value is <code>"mle/mme"</code> 
(maximum likelihood/method of moments; the default).  See the DETAILS section for 
more information. 
</p>
</td></tr>
<tr valign="top"><td><code>ci</code></td>
<td>

<p>logical scalar indicating whether to compute a confidence interval for the 
location or scale parameter.  The default value is <code>FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.type</code></td>
<td>

<p>character string indicating what kind of confidence interval to compute.  The 
possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.method</code></td>
<td>

<p>character string indicating what method to use to construct the confidence interval 
for the location or scale parameter.  Currently, the only possible value is 
<code>"exact"</code> (the default).  See the DETAILS section for more information.  
This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the confidence interval.  
The default value is <code>conf.level=0.95</code>. This argument is ignored if 
<code>ci=FALSE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of <i>n</i> 
observations from an <a href="../../stats/help/Exponential.html">exponential distribution</a> with 
parameter <code>rate=</code><i>&lambda;</i>.
</p>
<p><em>Estimation</em> <br />
The maximum likelihood estimator (mle) of <i>&lambda;</i> is given by:
</p>
<p style="text-align: center;"><i>\hat{&lambda;}_{mle} = \frac{1}{\bar{x}}</i></p>

<p>where
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n}&sum;^n_{i=1} x_i</i></p>

<p>(Forbes et al., 2011).  That is, the mle is the reciprocal of the sample mean.
</p>
<p>Sometimes the exponential distribution is parameterized with a scale parameter 
instead of a rate parameter.  The scale parameter is the reciprocal of the rate 
parameter, and the sample mean is both the mle and the minimum variance unbiased 
estimator (mvue) of the scale parameter.
</p>
<p><em>Confidence Interval</em> <br />
When <code>ci=TRUE</code>, an exact <i>(1-&alpha;)100\%</i> confidence intervals for 
<i>&lambda;</i> can be constructed based on the relationship between the 
exponential distribution, the <a href="../../stats/help/GammaDist.html">gamma distribution</a>, and 
the <a href="../../stats/help/Chisquare.html">chi-square distribution</a>.  An exponential distribution 
with parameter <code>rate=</code><i>&lambda;</i> is equivalent to a gamma distribution 
with parameters <code>shape=1</code> and <code>scale=</code><i>1/&lambda;</i>.  The sum of 
<i>n</i> iid gamma random variables with parameters <code>shape=1</code> and 
<code>scale=</code><i>1/&lambda;</i> is a gamma random variable with parameters 
<code>shape=</code><i>n</i> and <code>scale=</code><i>1/&lambda;</i>.  Finally, a gamma 
distribution with parameters <code>shape=</code><i>n</i> and <code>scale=</code><i>1/&lambda;</i> 
is equivalent to 0.5 times a chi-square distribution with degrees of freedom 
<code>df=</code><i>2n</i>. Thus, the quantity <i>2n\bar{x}</i> has a chi-square 
distribution with degrees of freedom <code>df=</code><i>2n</i>.
</p>
<p>A two-sided <i>(1-&alpha;)100\%</i> confidence interval for <i>&lambda;</i> is 
therefore constructed as:
</p>
<p style="text-align: center;"><i>[\frac{&chi;^2(2n, &alpha;/2)}{2n\bar{x}}, \; \frac{chi^2(2n, 1 - &alpha;/2)}{2n\bar{x}} ]</i></p>

<p>where <i>&chi;^2(&nu;,p)</i> is the <i>p</i>'th quantile of a 
<a href="../../stats/help/Chisquare.html">chi-square distribution</a> with <i>&nu;</i> degrees of freedom.
</p>
<p>One-sided confidence intervals are computed in a similar fashion.
</p>


<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other information.  
See <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/help/Exponential.html">exponential distribution</a> is a special case of the 
<a href="../../stats/help/GammaDist.html">gamma distribution</a>, and 
takes on positive real values.  A major use of the exponential distribution is 
in life testing where it is used to model the lifetime of a product, part, 
person, etc.
</p>
<p>The exponential distribution is the only continuous distribution with a 
&ldquo;lack of memory&rdquo; property.  That is, if the lifetime of a part follows 
the exponential distribution, then the distribution of the time until failure 
is the same as the distribution of the time until failure given that the part 
has survived to time <i>t</i>.
</p>
<p>The exponential distribution is related to the double exponential (also called 
Laplace) distribution, and to the <a href="../../EnvStats/help/EVD.html">extreme value distribution</a>.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1994). 
<em>Continuous Univariate Distributions, Volume 1</em>. 
Second Edition. John Wiley and Sons, New York.
</p>


<h3>See Also</h3>

<p><a href="../../stats/help/Exponential.html">Exponential</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from an exponential distribution with parameter 
  # rate=2, then estimate the parameter and construct a 90% confidence interval. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rexp(20, rate = 2) 
  eexp(dat, ci=TRUE, conf = 0.9) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Exponential
  #
  #Estimated Parameter(s):          rate = 2.260587
  #
  #Estimation Method:               mle/mme
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Confidence Interval for:         rate
  #
  #Confidence Interval Method:      Exact
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                90%
  #
  #Confidence Interval:             LCL = 1.498165
  #                                 UCL = 3.151173

  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
