<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Sample Size for a Specified Half-Width of a Prediction...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for predIntNormN {EnvStats}"><tr><td>predIntNormN {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Sample Size for a Specified Half-Width of a Prediction Interval for the next <i>k</i> Observations from a Normal Distribution
</h2>

<h3>Description</h3>

<p>Compute the sample size necessary to achieve a specified half-width of a 
prediction interval for the next <i>k</i> observations from a normal distribution.
</p>


<h3>Usage</h3>

<pre>
  predIntNormN(half.width, n.mean = 1, k = 1, sigma.hat = 1, 
    method = "Bonferroni", conf.level = 0.95, round.up = TRUE, 
    n.max = 5000, tol = 1e-07, maxiter = 1000)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>half.width</code></td>
<td>

<p>numeric vector of (positive) half-widths.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) values are not allowed.
</p>
</td></tr>
<tr valign="top"><td><code>n.mean</code></td>
<td>

<p>numeric vector of positive integers specifying the sample size associated with 
the <i>k</i> future <em>averages</em>.  The default value is 
<code>n.mean=1</code> (i.e., individual observations).  Note that all future averages 
must be based on the same sample size.
</p>
</td></tr>
<tr valign="top"><td><code>k</code></td>
<td>

<p>numeric vector of positive integers specifying the number of future observations 
or averages the prediction interval should contain with confidence level 
<code>conf.level</code>.  The default value is <code>k=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>sigma.hat</code></td>
<td>

<p>numeric vector specifying the value(s) of the estimated standard deviation(s).  
The default value is <code>sigma.hat=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method to use if the number of future observations 
(<code>k</code>) is greater than 1.  The possible values are <code>method="Bonferroni"</code> 
(approximate method based on Bonferonni inequality; the default), and <br />
<code>method="exact"</code> (exact method due to Dunnett, 1955).  
This argument is ignored if <code>k=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>numeric vector of values between 0 and 1 indicating the confidence level of the 
prediction interval.  The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>round.up</code></td>
<td>

<p>logical scalar indicating whether to round up the values of the computed sample 
size(s) to the next smallest integer.  The default value is <code>round.up=TRUE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>n.max</code></td>
<td>

<p>positive integer greater than 1 indicating the maximum possible sample size.  The 
default value is <code>n.max=5000</code>.
</p>
</td></tr>
<tr valign="top"><td><code>tol</code></td>
<td>

<p>numeric scalar indicating the tolerance to use in the <code><a href="../../stats/html/uniroot.html">uniroot</a></code> 
search algorithm.  The default value is <code>tol=1e-7</code>.
</p>
</td></tr>
<tr valign="top"><td><code>maxiter</code></td>
<td>

<p>positive integer indicating the maximum number of iterations to use in the 
<code><a href="../../stats/html/uniroot.html">uniroot</a></code> search algorithm.  The default value is 
<code>maxiter=1000</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If the arguments <code>half.width</code>, <code>k</code>, <code>n.mean</code>, <code>sigma.hat</code>, and 
<code>conf.level</code> are not all the same length, they are replicated to be the same 
length as the length of the longest argument.
</p>
<p>The help files for <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code> and <code><a href="../../EnvStats/help/predIntNormK.html">predIntNormK</a></code> 
give formulas for a two-sided prediction interval based on the sample size, the 
observed sample mean and sample standard deviation, and specified confidence level.  
Specifically, the two-sided prediction interval is given by:
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, \bar{x} + Ks] \;\;\;\;\;\; (1)</i></p>
 
<p>where <i>\bar{x}</i> denotes the sample mean:
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\;\;\; (2)</i></p>

<p><i>s</i> denotes the sample standard deviation:
</p>
<p style="text-align: center;"><i>s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\;\;\; (3)</i></p>

<p>and <i>K</i> denotes a constant that depends on the sample size <i>n</i>, the 
confidence level, the number of future averages <i>k</i>, and the 
sample size associated with the future averages, <i>m</i> (see the help file for 
<code><a href="../../EnvStats/help/predIntNormK.html">predIntNormK</a></code>).  Thus, the half-width of the prediction interval is 
given by:
</p>
<p style="text-align: center;"><i>HW = Ks \;\;\;\;\;\; (4)</i></p>

<p>The function <code>predIntNormN</code> uses the <code><a href="../../stats/html/uniroot.html">uniroot</a></code> search algorithm to 
determine the sample size for specified values of the half-width, number of 
observations used to create a single future average, number of future observations or 
averages, the sample standard deviation, and the confidence level.  <b>Note that 
unlike a confidence interval, the half-width of a prediction interval does <em>not</em> 
approach 0 as the sample size increases.</b>
</p>


<h3>Value</h3>

<p>numeric vector of sample sizes.
</p>


<h3>Note</h3>

<p>See the help file for <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>See the help file for <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>, <code><a href="../../EnvStats/help/predIntNormK.html">predIntNormK</a></code>, 
<code><a href="../../EnvStats/help/predIntNormHalfWidth.html">predIntNormHalfWidth</a></code>, <code><a href="../../EnvStats/help/plotPredIntNormDesign.html">plotPredIntNormDesign</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Look at how the required sample size for a prediction interval increases 
  # with increasing number of future observations:

  1:5 
  #[1] 1 2 3 4 5 

  predIntNormN(half.width = 3, k = 1:5) 
  #[1]  6  9 11 14 18

  #----------

  # Look at how the required sample size for a prediction interval decreases 
  # with increasing half-width:

  2:5 
  #[1] 2 3 4 5 

  predIntNormN(half.width = 2:5) 
  #[1] 86  6  4  3

  predIntNormN(2:5, round = FALSE) 
  #[1] 85.567387  5.122911  3.542393  2.987861

  #----------

  # Look at how the required sample size for a prediction interval increases 
  # with increasing estimated standard deviation for a fixed half-width:

  seq(0.5, 2, by = 0.5) 
  #[1] 0.5 1.0 1.5 2.0 

  predIntNormN(half.width = 4, sigma.hat = seq(0.5, 2, by = 0.5)) 
  #[1]  3  4  7 86

  #----------

  # Look at how the required sample size for a prediction interval increases 
  # with increasing confidence level for a fixed half-width:

  seq(0.5, 0.9, by = 0.1) 
  #[1] 0.5 0.6 0.7 0.8 0.9 

  predIntNormN(half.width = 2, conf.level = seq(0.5, 0.9, by = 0.1)) 
  #[1] 2 2 3 4 9

  #==========

  # The data frame EPA.92c.arsenic3.df contains arsenic concentrations (ppb) 
  # collected quarterly for 3 years at a background well and quarterly for 
  # 2 years at a compliance well.  Using the data from the background well, 
  # compute the required sample size in order to achieve a half-width of 
  # 2.25, 2.5, or 3 times the estimated standard deviation for a two-sided 
  # 90% prediction interval for k=4 future observations.
  #
  # For a half-width of 2.25 standard deviations, the required sample size is 526, 
  # or about 131 years of quarterly observations!  For a half-width of 2.5 
  # standard deviations, the required sample size is 20, or about 5 years of 
  # quarterly observations.  For a half-width of 3 standard deviations, the required 
  # sample size is 9, or about 2 years of quarterly observations.

  EPA.92c.arsenic3.df
  #   Arsenic Year  Well.type
  #1     12.6    1 Background
  #2     30.8    1 Background
  #3     52.0    1 Background
  #...
  #18     3.8    5 Compliance
  #19     2.6    5 Compliance
  #20    51.9    5 Compliance

  mu.hat &lt;- with(EPA.92c.arsenic3.df, 
    mean(Arsenic[Well.type=="Background"])) 

  mu.hat 
  #[1] 27.51667 

  sigma.hat &lt;- with(EPA.92c.arsenic3.df, 
    sd(Arsenic[Well.type=="Background"]))

  sigma.hat 
  #[1] 17.10119 

  predIntNormN(half.width=c(2.25, 2.5, 3) * sigma.hat, k = 4, 
    sigma.hat = sigma.hat, conf.level = 0.9) 
  #[1] 526  20   9 

  #==========

  # Clean up
  #---------
  rm(mu.hat, sigma.hat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
