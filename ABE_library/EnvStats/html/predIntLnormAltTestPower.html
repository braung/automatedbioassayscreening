<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Probability That at Least One Future Observation Falls...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for predIntLnormAltTestPower {EnvStats}"><tr><td>predIntLnormAltTestPower {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Probability That at Least One Future Observation Falls Outside a Prediction Interval for a Lognormal Distribution
</h2>

<h3>Description</h3>

<p>Compute the probability that at least one out of <i>k</i> future observations 
(or geometric means) falls outside a prediction interval for <i>k</i> future 
observations (or geometric means) for a normal distribution.
</p>


<h3>Usage</h3>

<pre>
  predIntLnormAltTestPower(n, df = n - 1, n.geomean = 1, k = 1, 
    ratio.of.means = 1, cv = 1, pi.type = "upper", conf.level = 0.95)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n</code></td>
<td>

<p>vector of positive integers greater than 2 indicating the sample size upon which 
the prediction interval is based.
</p>
</td></tr>
<tr valign="top"><td><code>df</code></td>
<td>

<p>vector of positive integers indicating the degrees of freedom associated with 
the sample size.  The default value is <code>df=n-1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>n.geomean</code></td>
<td>

<p>positive integer specifying the sample size associated with the future 
geometric means.  The default value is <code>n.geomean=1</code> (i.e., individual 
observations).  Note that all future geometric means must be based on the 
same sample size.
</p>
</td></tr>
<tr valign="top"><td><code>k</code></td>
<td>

<p>vector of positive integers specifying the number of future observations that the 
prediction interval should contain with confidence level <code>conf.level</code>.  The 
default value is <code>k=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ratio.of.means</code></td>
<td>

<p>numeric vector specifying the ratio of the mean of the population that will be 
sampled to produce the future observations vs. the mean of the population that 
was sampled to construct the prediction interval.  See the DETAILS section below 
for more information.  The default value is <code>ratio.of.means=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>cv</code></td>
<td>

<p>numeric vector of positive values specifying the coefficient of variation for 
both the population that was sampled to construct the prediction interval <b>and</b> 
the population that will be sampled to produce the future observations.  The 
default value is <code>cv=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>pi.type</code></td>
<td>

<p>character string indicating what kind of prediction interval to compute.  
The possible values are <code>pi.type="upper"</code> (the default), and 
<code>pi.type="lower"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>numeric vector of values between 0 and 1 indicating the confidence level of the 
prediction interval.  The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>A prediction interval for some population is an interval on the real line 
constructed so that it will contain <i>k</i> future observations or averages 
from that population with some specified probability <i>(1-&alpha;)100\%</i>, 
where <i>0 &lt; &alpha; &lt; 1</i> and <i>k</i> is some pre-specified positive integer.  
The quantity <i>(1-&alpha;)100\%</i> is call the confidence coefficient or 
confidence level associated with the prediction interval.  The function 
<code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code> computes a standard prediction interval based on a 
sample from a normal distribution.  
</p>
<p>The function <code><a href="../../EnvStats/help/predIntNormTestPower.html">predIntNormTestPower</a></code> computes the probability that at 
least one out of <i>k</i> future observations or averages will <b>not</b> be contained in 
a prediction interval based on the assumption of normally distributed observations, 
where the population mean for the future observations is allowed to differ from 
the population mean for the observations used to construct the prediction interval.
</p>
<p>The function <code>predIntLnormAltTestPower</code> assumes all observations are 
from a <a href="../../EnvStats/help/LognormalAlt.html">lognormal distribution</a>.  The observations used to 
construct the prediction interval are assumed to come from a lognormal distribution 
with mean <i>&theta;_2</i> and coefficient of variation <i>&tau;</i>.  The future 
observations are assumed to come from a lognormal distribution with mean 
<i>&theta;_1</i> and coefficient of variation <i>&tau;</i>; that is, the means are 
allowed to differ between the two populations, but not the coefficient of variation.
</p>
<p>The function <code>predIntLnormAltTestPower</code> calls the function 
<code><a href="../../EnvStats/help/predIntNormTestPower.html">predIntNormTestPower</a></code>, with the argument <code>delta.over.sigma</code> 
given by:
</p>
<p style="text-align: center;"><i>\frac{&delta;}{&sigma;} = \frac{log(R)}{&radic;{log(&tau;^2 + 1)}} \;\;\;\;\;\; (1)</i></p>

<p>where <i>R</i> is given by:
</p>
<p style="text-align: center;"><i>R = \frac{&theta;_1}{&theta;_2} \;\;\;\;\;\; (2)</i></p>

<p>and corresponds to the argument <code>ratio.of.means</code> for the function 
<code>predIntLnormAltTestPower</code>, and <i>&tau;</i> corresponds to the argument 
<code>cv</code>.
</p>


<h3>Value</h3>

<p>vector of numbers between 0 and 1 equal to the probability that at least one of 
<i>k</i> future observations or geometric means will fall outside the prediction 
interval.
</p>


<h3>Note</h3>

<p>See the help files for <code><a href="../../EnvStats/help/predIntNormTestPower.html">predIntNormTestPower</a></code>.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>See the help files for <code><a href="../../EnvStats/help/predIntNormTestPower.html">predIntNormTestPower</a></code> and 
<code><a href="../../EnvStats/help/tTestLnormAltPower.html">tTestLnormAltPower</a></code>.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/plotPredIntLnormAltTestPowerCurve.html">plotPredIntLnormAltTestPowerCurve</a></code>, 
<code><a href="../../EnvStats/help/predIntLnormAlt.html">predIntLnormAlt</a></code>, 
<code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>, <code><a href="../../EnvStats/help/predIntNormK.html">predIntNormK</a></code>, <br />
<code><a href="../../EnvStats/help/plotPredIntNormTestPowerCurve.html">plotPredIntNormTestPowerCurve</a></code>, 
<code><a href="../../EnvStats/help/predIntLnormAltSimultaneous.html">predIntLnormAltSimultaneous</a></code>, <br />
<code><a href="../../EnvStats/help/predIntLnormAltSimultaneousTestPower.html">predIntLnormAltSimultaneousTestPower</a></code>, <a href="../../EnvStats/help/Prediction+20Intervals.html">Prediction Intervals</a>, 
<a href="../../EnvStats/help/LognormalAlt.html">LognormalAlt</a>.
</p>


<h3>Examples</h3>

<pre>
  # Show how the power increases as ratio.of.means increases.  Assume a 
  # 95% upper prediction interval.

  predIntLnormAltTestPower(n = 4, ratio.of.means = 1:3) 
  #[1] 0.0500000 0.1459516 0.2367793

  #----------

  # Look at how the power increases with sample size for an upper one-sided 
  # prediction interval with k=3, ratio.of.means=4, and a confidence level of 95%.

  predIntLnormAltTestPower(n = c(4, 8), k = 3, ratio.of.means = 4) 
  #[1] 0.2860952 0.4533567

  #----------

  # Show how the power for an upper 95% prediction limit increases as the 
  # number of future observations k increases.  Here, we'll use n=20 and 
  # ratio.of.means=2.

  predIntLnormAltTestPower(n = 20, k = 1:3, ratio.of.means = 2) 
  #[1] 0.1945886 0.2189538 0.2321562
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
