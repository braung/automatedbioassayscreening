<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Test for the Presence of Serial Correlation</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for serialCorrelationTest {EnvStats}"><tr><td>serialCorrelationTest {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Test for the Presence of Serial Correlation
</h2>

<h3>Description</h3>

<p><code>serialCorrelationTest</code> is a generic function used to test for the 
presence of lag-one serial correlation using either the rank 
von Neumann ratio test, the normal approximation based on the Yule-Walker 
estimate of lag-one correlation, or the normal approximation based on the 
MLE of lag-one correlation.  The function invokes particular 
<code><a href="../../utils/html/methods.html">methods</a></code> which depend on the <code><a href="../../base/html/class.html">class</a></code> of the first 
argument. 
</p>
<p>Currently, there is a default method and a method for objects of class <code>"lm"</code>.
</p>


<h3>Usage</h3>

<pre>
  serialCorrelationTest(x, ...)

  ## Default S3 method:
serialCorrelationTest(x, test = "rank.von.Neumann", 
    alternative = "two.sided", conf.level = 0.95, ...) 

  ## S3 method for class 'lm'
serialCorrelationTest(x, test = "rank.von.Neumann", 
    alternative = "two.sided", conf.level = 0.95, ...)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations, a numeric univariate time series of 
class <code>"ts"</code>, or an object of class <code>"lm"</code>.  Undefined (<code>NaN</code>) and 
infinite (<code>Inf</code>, <code>-Inf</code>) values are not allowed for <code>x</code> 
when <code>x</code> is a numeric vector or time series, nor for the residuals 
associated with <code>x</code> when <code>x</code> is an object of class <code>"lm"</code>.
</p>
<p>When <code>test="AR1.mle"</code>, missing (<code>NA</code>) values are allowed, otherwise 
they are not allowed.  When <code>x</code> is a numeric vector of observations 
or a numeric univariate time series of class <code>"ts"</code>, it must contain at least 
3 non-missing values.  When <code>x</code> is an object of class <code>"lm"</code>, the 
residuals must contain at least 3 non-missing values.
</p>
<p>Note:  when <code>x</code> is an object of class <code>"lm"</code>, the linear model 
should have been fit using the argument <code>na.action=na.exclude</code> in the 
call to <code><a href="../../stats/html/lm.html">lm</a></code> in order to correctly deal with missing values.
</p>
</td></tr>
<tr valign="top"><td><code>test</code></td>
<td>

<p>character string indicating which test to use.  The possible values are: <br />
<code>"rank.von.Neumann"</code> (rank von Neumann ratio test; the default), <br />
<code>"AR1.yw"</code> (z-test based on Yule-Walker lag-one estimate of correlation), and <br />
<code>"AR1.mle"</code> (z-test based on MLE of lag-one correlation).  
</p>
</td></tr>
<tr valign="top"><td><code>alternative</code></td>
<td>

<p>character string indicating the kind of alternative hypothesis.  The possible 
values are <code>"two.sided"</code> (the default), <code>"greater"</code>, and <code>"less"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>numeric scalar between 0 and 1 indicating the confidence level associated with 
the confidence interval for the population lag-one autocorrelation.  The default 
value is <code>conf.level=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>...</code></td>
<td>

<p>optional arguments for possible future methods.  Currently not used.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Let <i>\underline{x} = x_1, x_2, &hellip;, x_n</i> denote <i>n</i> observations from a 
stationary time series sampled at equispaced points in time with normal (Gaussian) 
errors.  The function <code>serialCorrelationTest</code> tests the null hypothesis:
</p>
<p style="text-align: center;"><i>H_0: &rho;_1 = 0 \;\;\;\;\;\; (1)</i></p>

<p>where <i>&rho;_1</i> denotes the true lag-1 autocorrelation (also called the lag-1 
serial correlation coefficient).  Actually, the null hypothesis is that the 
lag-<i>k</i> autocorrelation is 0 for all values of <i>k</i> greater than 0 (i.e., 
the time series is purely random).
</p>
<p>In the case when the argument <code>x</code> is a linear model, the function 
<code>serialCorrelationTest</code> tests the null hypothesis (1) for the 
residuals.
</p>
<p>The three possible alternative hypotheses are the upper one-sided alternative 
(<code>alternative="greater"</code>):
</p>
<p style="text-align: center;"><i>H_a: &rho;_1 &gt; 0 \;\;\;\;\;\; (2)</i></p>

<p>the lower one-sided alternative (<code>alternative="less"</code>):
</p>
<p style="text-align: center;"><i>H_a: &rho;_1 &lt; 0 \;\;\;\;\;\; (3)</i></p>

<p>and the two-sided alternative: 
</p>
<p style="text-align: center;"><i>H_a: &rho;_1 \ne 0 \;\;\;\;\;\; (4)</i></p>

<p><b>Testing the Null Hypothesis of No Lag-1 Autocorrelation</b> <br />
There are several possible methods for testing the null hypothesis (1) versus any 
of the three alternatives (2)-(4). The function <code>serialCorrelationTest</code> allows 
you to use one of three possible tests:
</p>

<ul>
<li><p> The rank von Neuman ratio test.
</p>
</li>
<li><p> The test based on the normal approximation for the distribution of the 
Yule-Walker estimate of lag-one correlation.
</p>
</li>
<li><p> The test based on the normal approximation for the distribution of the 
maximum likelihood estimate (MLE) of lag-one correlation.
</p>
</li></ul>

<p>Each of these tests is described below.
<br />
</p>
<p><em>Test Based on Yule-Walker Estimate</em> (<code>test="AR1.yw"</code>) <br />
The Yule-Walker estimate of the lag-1 autocorrelation is given by:
</p>
<p style="text-align: center;"><i>\hat{&rho;}_1 = \frac{\hat{&gamma;}_1}{\hat{&gamma;}_0} \;\;\;\;\;\; (5)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>\hat{&gamma;}_k = \frac{1}{n} &sum;_{t=1}^{n-k} (x_t - \bar{x})(x_{t+k} - \bar{x}) \;\;\;\;\;\; (6)</i></p>

<p>is the estimate of the lag-<i>k</i> autocovariance.  
(This estimator does not allow for missing values.)
</p>
<p>Under the null hypothesis (1), the estimator of lag-1 correlation in Equation (5) is 
approximately distributed as a normal (Gaussian) random variable with mean 0 and 
variance given by:
</p>
<p style="text-align: center;"><i>Var(\hat{&rho;}_1) \approx \frac{1}{n} \;\;\;\;\;\; (7)</i></p>

<p>(Box and Jenkins, 1976, pp.34-35). Thus, the null hypothesis (1) can be tested 
with the statistic
</p>
<p style="text-align: center;"><i>z = &radic;{n} \hat{&rho;_1} \;\;\;\;\;\; (8)</i></p>

<p>which is distributed approximately as a standard normal random variable under the 
null hypothesis that the lag-1 autocorrelation is 0.
<br />
</p>
<p><em>Test Based on the MLE</em> (<code>test="AR1.mle"</code>) <br />
The function <code>serialCorrelationTest</code> the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> function <code><a href="../../stats/html/arima.html">arima</a></code> to 
compute the MLE of the lag-one autocorrelation and the estimated variance of this 
estimator.  As for the test based on the Yule-Walker estimate, the z-statistic is 
computed as the estimated lag-one autocorrelation divided by the square root of the 
estimated variance.
<br />
</p>
<p><em>Test Based on Rank von Neumann Ratio</em> (<code>test="rank.von.Neumann"</code>) <br />
The null distribution of the serial correlation coefficient may be badly affected 
by departures from normality in the underlying process (Cox, 1966; Bartels, 1977).  
It is therefore a good idea to consider using a nonparametric test for randomness if 
the normality of the underlying process is in doubt (Bartels, 1982). 
</p>
<p>Wald and Wolfowitz (1943) introduced the rank serial correlation coefficient, which 
for lag-1 autocorrelation is simply the Yule-Walker estimate (Equation (5) above) 
with the actual observations replaced with their ranks.
</p>
<p>von Neumann et al. (1941) introduced a test for randomness in the context of 
testing for trend in the mean of a process. Their statistic is given by:
</p>
<p style="text-align: center;"><i>V = \frac{&sum;_{i=1}^{n-1}(x_i - x_{i+1})^2}{&sum;_{i=1}^n (x_i - \bar{x})^2} \;\;\;\;\;\; (9)</i></p>

<p>which is the ratio of the square of successive differences to the usual sums of 
squared deviations from the mean.  This statistic is bounded between 0 and 4, and 
for a purely random process is symmetric about 2.  Small values of this statistic 
indicate possible positive autocorrelation, and large values of this statistics 
indicate possible negative autocorrelation.  Durbin and Watson (1950, 1951, 1971) 
proposed using this statistic in the context of checking the independence of 
residuals from a linear regression model and provided tables for the distribution 
of this statistic.  This statistic is therefore often called the 
&ldquo;Durbin-Watson statistic&rdquo; (Draper and Smith, 1998, p.181).
</p>
<p>The rank version of the von Neumann ratio statistic is given by:
</p>
<p style="text-align: center;"><i>V_{rank} = \frac{&sum;_{i=1}^{n-1}(R_i - R_{i+1})^2}{&sum;_{i=1}^n (R_i - \bar{R})^2} \;\;\;\;\;\; (10)</i></p>

<p>where <i>R_i</i> denotes the rank of the <i>i</i>'th observation (Bartels, 1982).  
(This test statistic does not allow for missing values.)  In the absence of ties, 
the denominator of this test statistic is equal to
</p>
<p style="text-align: center;"><i>&sum;_{i=1}^n (R_i - \bar{R})^2 = \frac{n(n^2 - 1)}{12} \;\;\;\;\;\; (11)</i></p>

<p>The range of the <i>V_{rank}</i> test statistic is given by:
</p>
<p style="text-align: center;"><i>[\frac{12}{(n)(n+1)} , 4 - \frac{12}{(n)(n+1)}] \;\;\;\;\;\; (12)</i></p>

<p>if n is even, with a negligible adjustment if n is odd (Bartels, 1982), so 
asymptotically the range is from 0 to 4, just as for the <i>V</i> test statistic in 
Equation (9) above.
</p>
<p>Bartels (1982) shows that asymptotically, the rank von Neumann ratio statistic is a 
linear transformation of the rank serial correlation coefficient, so any asymptotic 
results apply to both statistics.
</p>
<p>For any fixed sample size <i>n</i>, the exact distribution of the <i>V_{rank}</i> 
statistic in Equation (10) above can be computed by simply computing the value of 
<i>V_{rank}</i> for all possible permutations of the serial order of the ranks.  
Based on this exact distribution, Bartels (1982) presents a table of critical 
values for the numerator of the RVN statistic for sample sizes between 4 and 10.
</p>
<p>Determining the exact distribution of <i>V_{rank}</i> becomes impractical as the 
sample size increases.  For values of n between 10 and 100, Bartels (1982) 
approximated the distribution of <i>V_{rank}</i> by a 
<a href="../../stats/help/Beta.html">beta distribution</a> over the range 0 to 4 with shape parameters 
<code>shape1=</code><i>&nu;</i> and <code>shape2=</code><i>&omega;</i> and:
</p>
<p style="text-align: center;"><i>&nu; = &omega; = \frac{5n(n+1)(n-1)^2}{2(n-2)(5n^2 - 2n - 9)} - \frac{1}{2} \;\;\;\;\;\; (13)</i></p>

<p>Bartels (1982) checked this approximation by simulating the distribution of 
<i>V_{rank}</i> for <i>n=25</i> and <i>n=50</i> and comparing the empirical quantiles 
at <i>0.005</i>, <i>0.01</i>, <i>0.025</i>, <i>0.05</i>, and <i>0.1</i> with the 
approximated quantiles based on the beta distribution.  He found that the quantiles 
agreed to 2 decimal places for eight of the 10 values, and differed by <i>0.01</i> 
for the other two values.
</p>
<p><b>Note</b>: The definition of the <a href="../../stats/help/Beta.html">beta distribution</a> assumes the 
random variable ranges from 0 to 1.  This definition can be generalized as follows.  
Suppose the random variable <i>Y</i> has a beta distribution over the range 
<i>a &le; y &le; b</i>, with shape parameters <i>&nu;</i> and <i>&omega;</i>.  Then the 
random variable <i>X</i> defined as:
</p>
<p style="text-align: center;"><i>X = \frac{Y-a}{b-a} \;\;\;\;\;\; (14)</i></p>

<p>has the &ldquo;standard beta distribution&rdquo; as described in the help file for Beta 
(Johnson et al., 1995, p.210).
</p>
<p>Bartels (1982) shows that asymptotically, <i>V_{rank}</i> has normal distribution 
with mean 2 and variance <i>4/n</i>, but notes that a slightly better approximation 
is given by using a variance of <i>20/(5n + 7)</i>.
</p>
<p>To test the null hypothesis (1) when <code>test="rank.von.Neumann"</code>, the function <br />
<code>serialCorrelationTest</code> does the following:
</p>

<ul>
<li><p> When the sample size is between 3 and 10, the exact distribution of <i>V_{rank}</i> 
is used to compute the p-value.
</p>
</li>
<li><p> When the sample size is between 11 and 100, the beta approximation to the 
distribution of <i>V_{rank}</i> is used to compute the p-value.
</p>
</li>
<li><p> When the sample size is larger than 100, the normal approximation to the 
distribution of <i>V_{rank}</i> is used to compute the p-value.  
(This uses the variance <i>20/(5n + 7)</i>.)
</p>
</li></ul>

<p>When ties are present in the observations and midranks are used for the tied 
observations, the distribution of the <i>V_{rank}</i> statistic based on the 
assumption of no ties is not applicable.  If the number of ties is small, however, 
they may not grossly affect the assumed p-value.
</p>
<p>When ties are present, the function <code>serialCorrelationTest</code> issues a warning.  
When the sample size is between 3 and 10, the p-value is computed based on 
rounding up the computed value of <i>V_{rank}</i> to the nearest possible value 
that could be observed in the case of no ties.
<br />
</p>
<p><b>Computing a Confidence Interval for the Lag-1 Autocorrelation</b> <br />
The function <code>serialCorrelationTest</code> computes an approximate 
<i>100(1-&alpha;)\%</i> confidence interval for the lag-1 autocorrelation as follows:
</p>
<p style="text-align: center;"><i>[\hat{&rho;}_1 - z_{1-&alpha;/2}\hat{&sigma;}_{\hat{&rho;}_1},  \hat{&rho;}_1 + z_{1-&alpha;/2}\hat{&sigma;}_{\hat{&rho;}_1}] \;\;\;\;\;\; (15)</i></p>

<p>where <i>\hat{&sigma;}_{\hat{&rho;}_1}</i> denotes the estimated standard deviation of 
the estimated of lag-1 autocorrelation and <i>z_p</i> denotes the <i>p</i>'th quantile 
of the standard <a href="../../stats/help/Normal.html">normal distribution</a>.
</p>
<p>When <code>test="AR1.yw"</code> or <code>test="rank.von.Neumann"</code>, the Yule-Walker 
estimate of lag-1 autocorrelation is used and the variance of the estimated 
lag-1 autocorrelation is approximately:
</p>
<p style="text-align: center;"><i>Var(\hat{&rho;}_1) \approx \frac{1}{n} (1 - &rho;_1^2) \;\;\;\;\;\; (16)</i></p>

<p>(Box and Jenkins, 1976, p.34), so
</p>
<p style="text-align: center;"><i>\hat{&sigma;}_{\hat{&rho;}_1} = &radic;{\frac{1 - \hat{&rho;}_1^2}{n}} \;\;\;\;\;\; (17)</i></p>

<p>When <code>test="AR1.mle"</code>, the MLE of the lag-1 autocorrelation is used, and its 
standard deviation is estimated with the square root of the estimated variance 
returned by <code><a href="../../stats/html/arima.html">arima</a></code>.
</p>


<h3>Value</h3>

<p>A list of class <code>"htest"</code> containing the results of the hypothesis test.  
See the help file for <code><a href="../../EnvStats/help/htest.object.html">htest.object</a></code> for details.
</p>


<h3>Note</h3>

<p>Data collected over time on the same phenomenon are called a time series.  
A time series is usually modeled as a single realization of a stochastic process; 
that is, if we could go back in time and repeat the experiment, we would get 
different results that would vary according to some probabilistic law.  
The simplest kind of time series is a stationary time series, in which the mean 
value is constant over time, the variability of the observations is constant over 
time, etc.  That is, the probability distribution associated with each future 
observation is the same.
</p>
<p>A common concern in applying standard statistical tests to time series data is 
the assumption of independence.  Most conventional statistical hypothesis tests 
assume the observations are independent, but data collected sequentially in time 
may not satisfy this assumption.  For example, high observations may tend to 
follow high observations (positive serial correlation), or low observations may 
tend to follow high observations (negative serial correlation).  One way to 
investigate the assumption of independence is to estimate the lag-one serial 
correlation and test whether it is significantly different from 0.
</p>
<p>The null distribution of the serial correlation coefficient may be badly affected 
by departures from normality in the underlying process (Cox, 1966; Bartels, 1977).  
It is therefore a good idea to consider using a nonparametric test for randomness 
if the normality of the underlying process is in doubt (Bartels, 1982).  
Knoke (1977) showed that under normality, the test based on the rank serial 
correlation coefficient (and hence the test based on the rank von Neumann ratio 
statistic) has asymptotic relative efficiency of 0.91 with respect to using the 
test based on the ordinary serial correlation coefficient against the alternative 
of first-order autocorrelation.
</p>
<p>Bartels (1982) performed an extensive simulation study of the power of the 
rank von Neumann ratio test relative to the standard von Neumann ratio test 
(based on the statistic in Equation (9) above) and the runs test 
(Lehmann, 1975, 313-315).  He generated a first-order autoregressive process for 
sample sizes of 10, 25, and 50, using 6 different parent distributions: normal, 
Cauchy, contaminated normal, Johnson, Stable, and exponential.  Values of 
lag-1 autocorrelation ranged from -0.8 to 0.8.  Bartels (1982) found three 
important results:
</p>

<ul>
<li><p> The rank von Neumann ratio test is far more powerful than the runs test.
</p>
</li>
<li><p> For the normal process, the power of the rank von Neumann ratio test was 
never less than 89% of the power of the standard von Neumann ratio test.
</p>
</li>
<li><p> For non-normal processes, the rank von Neumann ratio test was often much 
more powerful than of the standard von Neumann ratio test.
</p>
</li></ul>



<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Bartels, R. (1982).  The Rank Version of von Neumann's Ratio Test for Randomness.  
<em>Journal of the American Statistical Association</em> <b>77</b>(377), 40&ndash;46.
</p>
<p>Berthouex, P.M., and L.C. Brown. (2002).  
<em>Statistics for Environmental Engineers</em>.  Second Edition.  
Lewis Publishers, Boca Raton, FL.
</p>
<p>Box, G.E.P., and G.M. Jenkins. (1976).  
<em>Time Series Analysis: Forecasting and Control</em>.  Prentice Hall, 
Englewood Cliffs, NJ, Chapter 2.
</p>
<p>Cox, D.R. (1966).  The Null Distribution of the First Serial Correlation Coefficient.  
<em>Biometrika</em> <b>53</b>, 623&ndash;626.
</p>
<p>Draper, N., and H. Smith. (1998).  <em>Applied Regression Analysis</em>.  
Third Edition. John Wiley and Sons, New York, pp.69-70;181-192.
</p>
<p>Durbin, J., and G.S. Watson. (1950).  Testing for Serial Correlation in Least 
Squares Regression I.  <em>Biometrika</em> <b>37</b>, 409&ndash;428.
</p>
<p>Durbin, J., and G.S. Watson. (1951).  Testing for Serial Correlation in Least 
Squares Regression II.  <em>Biometrika</em> <b>38</b>, 159&ndash;178.
</p>
<p>Durbin, J., and G.S. Watson. (1971). Testing for Serial Correlation in Least Squares 
Regression III.  <em>Biometrika</em> <b>58</b>, 1&ndash;19.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992).  <em>Statistical Methods in Water 
Resources Research</em>.  Elsevier, New York, NY, pp.250&ndash;253.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995).  <em>Continuous Univariate 
Distributions, Volume 2</em>.  Second Edition.  John Wiley and Sons, New York, 
Chapter 25.
</p>
<p>Knoke, J.D. (1975).  Testing for Randomness Against Autocorrelation Alternatives:  
The Parametric Case.  <em>Biometrika</em> <b>62</b>, 571&ndash;575.
</p>
<p>Knoke, J.D. (1977).  Testing for Randomness Against Autocorrelation Alternatives:  
Alternative Tests.  <em>Biometrika</em> <b>64</b>, 523&ndash;529.
</p>
<p>Lehmann, E.L. (1975).  <em>Nonparametrics:  Statistical Methods Based on Ranks</em>.  
Holden-Day, Oakland, CA, 457pp.
</p>
<p>von Neumann, J., R.H. Kent, H.R. Bellinson, and B.I. Hart. (1941).  The Mean Square 
Successive Difference.  <em>Annals of Mathematical Statistics</em> <b>12</b>(2), 
153&ndash;162.
</p>
<p>Wald, A., and J. Wolfowitz. (1943).  An Exact Test for Randomness in the 
Non-Parametric Case Based on Serial Correlation.  <em>Annals of Mathematical 
Statistics</em> <b>14</b>, 378&ndash;388.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/htest.object.html">htest.object</a></code>, <code><a href="../../stats/html/acf.html">acf</a></code>, <code><a href="../../stats/html/ar.html">ar</a></code>, 
<code><a href="../../stats/html/arima.html">arima</a></code>, <code><a href="../../stats/html/arima.sim.html">arima.sim</a></code>, 
<code><a href="../../stats/html/ts.plot.html">ts.plot</a></code>, <code><a href="../../stats/html/plot.ts.html">plot.ts</a></code>,  
<code><a href="../../stats/html/lag.plot.html">lag.plot</a></code>, <a href="../../EnvStats/help/Hypothesis+20Tests.html">Hypothesis Tests</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate a purely random normal process, then use serialCorrelationTest 
  # to test for the presence of correlation. 
  # (Note: the call to set.seed allows you to reproduce this example.) 

  set.seed(345) 
  x &lt;- rnorm(100) 

  # Look at the data
  #-----------------
  dev.new()
  ts.plot(x)

  dev.new()
  acf(x)

  # Test for serial correlation
  #----------------------------
  serialCorrelationTest(x) 

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 rho = 0
  #
  #Alternative Hypothesis:          True rho is not equal to 0
  #
  #Test Name:                       Rank von Neumann Test for
  #                                 Lag-1 Autocorrelation
  #                                 (Beta Approximation)
  #
  #Estimated Parameter(s):          rho = 0.02773737
  #
  #Estimation Method:               Yule-Walker
  #
  #Data:                            x
  #
  #Sample Size:                     100
  #
  #Test Statistic:                  RVN = 1.929733
  #
  #P-value:                         0.7253405
  #
  #Confidence Interval for:         rho
  #
  #Confidence Interval Method:      Normal Approximation
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL = -0.1681836
  #                                 UCL =  0.2236584

  # Clean up
  #---------
  rm(x)
  graphics.off()

  #==========

  # Now use the R function arima.sim to generate an AR(1) process with a 
  # lag-1 autocorrelation of 0.8, then test for autocorrelation.

  set.seed(432) 
  y &lt;- arima.sim(model = list(ar = 0.8), n = 100) 

  # Look at the data
  #-----------------
  dev.new()
  ts.plot(y)

  dev.new()
  acf(y)

  # Test for serial correlation
  #----------------------------
  serialCorrelationTest(y)

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 rho = 0
  #
  #Alternative Hypothesis:          True rho is not equal to 0
  #
  #Test Name:                       Rank von Neumann Test for
  #                                 Lag-1 Autocorrelation
  #                                 (Beta Approximation)
  #
  #Estimated Parameter(s):          rho = 0.835214
  #
  #Estimation Method:               Yule-Walker
  #
  #Data:                            y
  #
  #Sample Size:                     100
  #
  #Test Statistic:                  RVN = 0.3743174
  #
  #P-value:                         0
  #
  #Confidence Interval for:         rho
  #
  #Confidence Interval Method:      Normal Approximation
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL = 0.7274307
  #                                 UCL = 0.9429973

  #----------

  # Clean up
  #---------
  rm(y)
  graphics.off()

  #==========

  # The data frame Air.df contains information on ozone (ppb^1/3), 
  # radiation (langleys), temperature (degrees F), and wind speed (mph) 
  # for 153 consecutive days between May 1 and September 30, 1973.  
  # First test for serial correlation in (the cube root of) ozone.  
  # Note that we must use the test based on the MLE because the time series 
  # contains missing values.  Serial correlation appears to be present.  
  # Next fit a linear model that includes the predictor variables temperature, 
  # radiation, and wind speed, and test for the presence of serial correlation 
  # in the residuals.  There is no evidence of serial correlation.

  # Look at the data
  #-----------------

  Air.df
  #              ozone radiation temperature wind
  #05/01/1973 3.448217       190          67  7.4
  #05/02/1973 3.301927       118          72  8.0
  #05/03/1973 2.289428       149          74 12.6
  #05/04/1973 2.620741       313          62 11.5
  #05/05/1973       NA        NA          56 14.3
  #...
  #09/27/1973       NA       145          77 13.2
  #09/28/1973 2.410142       191          75 14.3
  #09/29/1973 2.620741       131          76  8.0
  #09/30/1973 2.714418       223          68 11.5

  #----------

  # Test for serial correlation
  #----------------------------

  with(Air.df, 
    serialCorrelationTest(ozone, test = "AR1.mle"))

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 rho = 0
  #
  #Alternative Hypothesis:          True rho is not equal to 0
  #
  #Test Name:                       z-Test for
  #                                 Lag-1 Autocorrelation
  #                                 (Wald Test Based on MLE)
  #
  #Estimated Parameter(s):          rho = 0.5641616
  #
  #Estimation Method:               Maximum Likelihood
  #
  #Data:                            ozone
  #
  #Sample Size:                     153
  #
  #Number NA/NaN/Inf's:             37
  #
  #Test Statistic:                  z = 7.586952
  #
  #P-value:                         3.28626e-14
  #
  #Confidence Interval for:         rho
  #
  #Confidence Interval Method:      Normal Approximation
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL = 0.4184197
  #                                 UCL = 0.7099034

  #----------

  # Next fit a linear model that includes the predictor variables temperature, 
  # radiation, and wind speed, and test for the presence of serial correlation 
  # in the residuals.  Note setting the argument na.action = na.exclude in the 
  # call to lm to correctly deal with missing values.
  #----------------------------------------------------------------------------

  lm.ozone &lt;- lm(ozone ~ radiation + temperature + wind + 
    I(temperature^2) + I(wind^2), 
    data = Air.df, na.action = na.exclude) 


  # Now test for serial correlation in the residuals.
  #--------------------------------------------------

  serialCorrelationTest(lm.ozone, test = "AR1.mle") 

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 rho = 0
  #
  #Alternative Hypothesis:          True rho is not equal to 0
  #
  #Test Name:                       z-Test for
  #                                 Lag-1 Autocorrelation
  #                                 (Wald Test Based on MLE)
  #
  #Estimated Parameter(s):          rho = 0.1298024
  #
  #Estimation Method:               Maximum Likelihood
  #
  #Data:                            Residuals
  #
  #Data Source:                     lm.ozone
  #
  #Sample Size:                     153
  #
  #Number NA/NaN/Inf's:             42
  #
  #Test Statistic:                  z = 1.285963
  #
  #P-value:                         0.1984559
  #
  #Confidence Interval for:         rho
  #
  #Confidence Interval Method:      Normal Approximation
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL = -0.06803223
  #                                 UCL =  0.32763704

  # Clean up
  #---------
  rm(lm.ozone)

</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
