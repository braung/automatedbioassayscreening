<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Compute the Value of K for a Simultaneous Prediction Interval...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for predIntNormSimultaneousK {EnvStats}"><tr><td>predIntNormSimultaneousK {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Compute the Value of <i>K</i> for a Simultaneous Prediction Interval for a Normal Distribution
</h2>

<h3>Description</h3>

<p>Compute the value of <i>K</i> (the multiplier of estimated standard deviation) used
to construct a simultaneous prediction interval based on data from a
<a href="../../stats/help/Normal.html">normal distribution</a>.
The function <br />
<code>predIntNormSimultaneousK</code> is called by <code><a href="../../EnvStats/help/predIntNormSimultaneous.html">predIntNormSimultaneous</a></code>.
</p>


<h3>Usage</h3>

<pre>
  predIntNormSimultaneousK(n, df = n - 1, n.mean = 1, k = 1, m = 2, r = 1,
    rule = "k.of.m", delta.over.sigma = 0, pi.type = "upper", conf.level = 0.95,
    K.tol = .Machine$double.eps^0.5, integrate.args.list = NULL)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n</code></td>
<td>

<p>a positive integer greater than 2 indicating the sample size upon which the
prediction interval is based.
</p>
</td></tr>
<tr valign="top"><td><code>df</code></td>
<td>

<p>the degrees of freedom associated with the prediction interval.  The default is
<code>df=n-1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>n.mean</code></td>
<td>

<p>positive integer specifying the sample size associated with the future averages.
The default value is <code>n.mean=1</code> (i.e., individual observations).  Note that all
future averages must be based on the same sample size.
</p>
</td></tr>
<tr valign="top"><td><code>k</code></td>
<td>

<p>for the <i>k</i>-of-<i>m</i> rule (<code>rule="k.of.m"</code>), a positive integer
specifying the minimum number of observations (or averages) out of <i>m</i>
observations (or averages) (all obtained on one future sampling &ldquo;occassion&rdquo;)
the prediction interval should contain with confidence level <code>conf.level</code>.
The default value is <code>k=1</code>.  This argument is ignored when the argument
<code>rule</code> is not equal to <code>"k.of.m"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>m</code></td>
<td>

<p>positive integer specifying the maximum number of future observations (or
averages) on one future sampling &ldquo;occasion&rdquo;.
The default value is <code>m=2</code>, except when <code>rule="Modified.CA"</code>, in which
case this argument is ignored and <code>m</code> is automatically set equal to <code>4</code>.
</p>
</td></tr>
<tr valign="top"><td><code>r</code></td>
<td>

<p>positive integer specifying the number of future sampling &ldquo;occasions&rdquo;.
The default value is <code>r=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>rule</code></td>
<td>

<p>character string specifying which rule to use.  The possible values are
<code>"k.of.m"</code> (<i>k</i>-of-<i>m</i> rule; the default), <code>"CA"</code> (California rule),
and <code>"Modified.CA"</code> (modified California rule).
See the DETAILS section below for more information.
</p>
</td></tr>
<tr valign="top"><td><code>delta.over.sigma</code></td>
<td>

<p>numeric scalar indicating the ratio <i>&Delta;/&sigma;</i>.  The quantity
<i>&Delta;</i> (delta) denotes the difference between the mean of the population
that was sampled to construct the prediction interval, and the mean of the
population that will be sampled to produce the future observations.  The quantity
<i>&sigma;</i> (sigma) denotes the population standard deviation for both populations.
See the DETAILS section below for more information.  The default value is
<code>delta.over.sigma=0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>pi.type</code></td>
<td>

<p>character string indicating what kind of prediction interval to compute.
The possible values are <code>pi.type="upper"</code> (the default),
<code>pi.type="lower"</code> and <code>pi.type="two-sided"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the prediction interval.
The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>K.tol</code></td>
<td>

<p>numeric scalar indicating the tolerance to use in the nonlinear search algorithm to
compute <i>K</i>.  The default value is <code>K.tol=.Machine$double.eps^(1/2)</code>.
For many applications, the value of <i>K</i> needs to be known only to the second
decimal place, in which case setting <code>K.tol=1e-4</code> will speed up computation a
bit.
</p>
</td></tr>
<tr valign="top"><td><code>integrate.args.list</code></td>
<td>

<p>a list of arguments to supply to the <code><a href="../../stats/html/integrate.html">integrate</a></code> function.  The
default value is <code>integrate.args.list=NULL</code> which means that the
default values of <code><a href="../../stats/html/integrate.html">integrate</a></code> are used.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p><b>What is a Simultaneous Prediction Interval?</b> <br />
A prediction interval for some population is an interval on the real line constructed
so that it will contain <i>k</i> future observations from that population
with some specified probability <i>(1-&alpha;)100\%</i>, where
<i>0 &lt; &alpha; &lt; 1</i> and <i>k</i> is some pre-specified positive integer.
The quantity <i>(1-&alpha;)100\%</i> is called
the confidence coefficient or confidence level associated with the prediction
interval.  The function <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code> computes a standard prediction
interval based on a sample from a <a href="../../stats/help/Normal.html">normal distribution</a>.
</p>
<p>The function <code>predIntNormSimultaneous</code> computes a simultaneous prediction
interval that will contain a certain number of future observations with probability
<i>(1-&alpha;)100\%</i> for each of <i>r</i> future sampling &ldquo;occasions&rdquo;,
where <i>r</i> is some pre-specified positive integer.  The quantity <i>r</i> may
refer to <i>r</i> distinct future sampling occasions in time, or it may for example
refer to sampling at <i>r</i> distinct locations on one future sampling occasion,
assuming that the population standard deviation is the same at all of the <i>r</i>
distinct locations.
</p>
<p>The function <code><a href="../../EnvStats/help/predIntNormSimultaneous.html">predIntNormSimultaneous</a></code> computes a simultaneous prediction
interval based on one of three possible rules:
</p>

<ul>
<li><p> For the <i>k</i>-of-<i>m</i> rule (<code>rule="k.of.m"</code>), at least <i>k</i> of
the next <i>m</i> future observations will fall in the prediction
interval with probability <i>(1-&alpha;)100\%</i> on each of the <i>r</i> future
sampling occasions.  If obserations are being taken sequentially, for a particular
sampling occasion, up to <i>m</i> observations may be taken, but once
<i>k</i> of the observations fall within the prediction interval, sampling can stop.
Note:  When <i>k=m</i> and <i>r=1</i>, the results of <code>predIntNormSimultaneous</code>
are equivalent to the results of <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>.
</p>
</li>
<li><p> For the California rule (<code>rule="CA"</code>), with probability
<i>(1-&alpha;)100\%</i>, for each of the <i>r</i> future sampling occasions, either
the first observation will fall in the prediction interval, or else all of the next
<i>m-1</i> observations will fall in the prediction interval. That is, if the first
observation falls in the prediction interval then sampling can stop.  Otherwise,
<i>m-1</i> more observations must be taken.
</p>
</li>
<li><p> For the Modified California rule (<code>rule="Modified.CA"</code>), with probability
<i>(1-&alpha;)100\%</i>, for each of the <i>r</i> future sampling occasions, either the
first observation will fall in the prediction interval, or else at least 2 out of
the next 3 observations will fall in the prediction interval.  That is, if the first
observation falls in the prediction interval then sampling can stop.  Otherwise, up
to 3 more observations must be taken.
</p>
</li></ul>

<p>Simultaneous prediction intervals can be extended to using averages (means) in place
of single observations (USEPA, 2009, Chapter 19).  That is, you can create a
simultaneous prediction interval
that will contain a specified number of averages (based on which rule you choose) on
each of <i>r</i> future sampling occassions, where each each average is based on
<i>w</i> individual observations.  For the functions
<code><a href="../../EnvStats/help/predIntNormSimultaneous.html">predIntNormSimultaneous</a></code> and <code>predIntNormSimultaneousK</code>,
the argument <code>n.mean</code> corresponds to <i>w</i>.
<br />
</p>
<p><b>The Form of a Prediction Interval</b> <br />
Let <i>\underline{x} = x_1, x_2, &hellip;, x_n</i> denote a vector of <i>n</i>
observations from a <a href="../../stats/help/Normal.html">normal distribution</a> with parameters
<code>mean=</code><i>&mu;</i> and <code>sd=</code><i>&sigma;</i>.  Also, let <i>w</i> denote the
sample size associated with the future averages (i.e., <code>n.mean=</code><i>w</i>).
When <i>w=1</i>, each average is really just a single observation, so in the rest of
this help file the term &ldquo;averages&rdquo; will sometimes replace the phrase
&ldquo;observations or averages&rdquo;.
</p>
<p>For a normal distribution, the form of a two-sided <i>(1-&alpha;)100\%</i>
simultaneous prediction interval is:
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, \bar{x} + Ks] \;\;\;\;\;\; (1)</i></p>

<p>where <i>\bar{x}</i> denotes the sample mean:
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\;\;\; (2)</i></p>

<p><i>s</i> denotes the sample standard deviation:
</p>
<p style="text-align: center;"><i>s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\;\;\; (3)</i></p>

<p>and <i>K</i> denotes a constant that depends on the sample size <i>n</i>, the
confidence level, the number of future sampling occassions <i>r</i>, and the
sample size associated with the future averages, <i>w</i>.  Do not confuse the
constant <i>K</i> (uppercase K) with the number of future averages <i>k</i>
(lowercase k) in the <i>k</i>-of-<i>m</i> rule.  The symbol <i>K</i> is used here
to be consistent with the notation used for tolerance intervals
(see <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code>).
</p>
<p>Similarly, the form of a one-sided lower prediction interval is:
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, &infin;] \;\;\;\;\;\; (4)</i></p>

<p>and the form of a one-sided upper prediction interval is:
</p>
<p style="text-align: center;"><i>[-&infin;, \bar{x} + Ks] \;\;\;\;\;\; (5)</i></p>

<p><b>Note:</b>  For simultaneous prediction intervals, only lower
(<code>pi.type="lower"</code>) and upper <br />
(<code>pi.type="upper"</code>) prediction intervals are available.
</p>
<p>The derivation of the constant <i>K</i> is explained  below.
<br />
</p>
<p><b>The Derivation of K for Future Observations</b> <br />
First we will show the derivation based on future observations (i.e.,
<i>w=1</i>, <code>n.mean=1</code>), and then extend the formulas to future averages.
</p>
<p><em>The Derivation of K for the k-of-m Rule</em> (<code>rule="k.of.m"</code>) <br />
For the <i>k</i>-of-<i>m</i> rule (<code>rule="k.of.m"</code>) with <i>w=1</i>
(i.e., <code>n.mean=1</code>), at least <i>k</i> of the next <i>m</i> future
observations will fall in the prediction interval
with probability <i>(1-&alpha;)100\%</i> on each of the <i>r</i> future sampling
occasions.  If observations are being taken sequentially, for a particular
sampling occasion, up to <i>m</i> observations may be taken, but once <i>k</i>
of the observations fall within the prediction interval, sampling can stop.
Note: When <i>k=m</i> and <i>r=1</i>, this kind of simultaneous prediction
interval becomes the same as a standard prediction interval for the next
<i>k</i> observations (see <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>).
</p>
<p>For the case when <i>r=1</i> future sampling occasion, both Hall and Prairie (1973)
and Fertig and Mann (1977) discuss the derivation of <i>K</i>.  Davis and McNichols
(1987) extend the derivation to the case where <i>r</i> is a positive integer.  They
show that for a one-sided upper prediction interval (<code>pi.type="upper"</code>), the
probability <i>p</i> that at least <i>k</i> of the next <i>m</i> future observations
will be contained in the interval given in Equation (5) above, for each of <i>r</i>
future sampling occasions, is given by:
</p>
<p style="text-align: center;"><i>p = \int_0^1 T(&radic;{n}K; n-1, &radic;{n}[&Phi;^{-1}(v) + \frac{&Delta;}{&sigma;}]) r[I(v; k, m+1-k)]^{r-1} [\frac{v^{k-1}(1-v)^{m-k}}{B(k, m+1-k)}] dv \;\;\;\;\;\; (6)</i></p>

<p>where <i>T(x; &nu;, &delta;)</i> denotes the cdf of the
<a href="../../stats/help/TDist.html">non-central Student's t-distribution</a> with parameters
<code>df=</code><i>&nu;</i> and <code>ncp=</code><i>&delta;</i> evaluated at <i>x</i>;
<i>&Phi;(x)</i> denotes the cdf of the standard <a href="../../stats/help/Normal.html">normal distribution</a>
evaluated at <i>x</i>; <i>I(x; &nu;, &omega;)</i> denotes the cdf of the
<a href="../../stats/help/Beta.html">beta distribution</a> with parameters <code>shape1=</code><i>&nu;</i> and
<code>shape2=</code><i>&omega;</i>; and <i>B(&nu;, &omega;)</i> denotes the value of the
<a href="../../base/help/Special.html">beta function</a> with parameters <code>a=</code><i>&nu;</i> and
<code>b=</code><i>&omega;</i>.
</p>
<p>The quantity <i>&Delta;</i> (upper case delta) denotes the difference between the
mean of the population that was sampled to construct the prediction interval, and
the mean of the population that will be sampled to produce the future observations.
The quantity <i>&sigma;</i> (sigma) denotes the population standard deviation of both
of these populations.  Usually you assume <i>&Delta;=0</i> unless you are interested
in computing the power of the rule to detect a change in means between the
populations (see <code><a href="../../EnvStats/help/predIntNormSimultaneousTestPower.html">predIntNormSimultaneousTestPower</a></code>).
</p>
<p>For given values of the confidence level (<i>p</i>), sample size (<i>n</i>),
minimum number of future observations to be contained in the interval per
sampling occasion (<i>k</i>), number of future observations per sampling occasion
(<i>m</i>), and number of future sampling occasions (<i>r</i>), Equation (6) can
be solved for <i>K</i>.  The function <code>predIntNormSimultaneousK</code> uses the
<span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> function <code><a href="../../stats/html/nlminb.html">nlminb</a></code> to solve Equation (6) for <i>K</i>.
</p>
<p>When <code>pi.type="lower"</code>, the same value of <i>K</i> is used as when
<code>pi.type="upper"</code>, but Equation (4) is used to construct the prediction
interval.
<br />
</p>
<p><em>The Derivation of K for the California Rule</em> (<code>rule="CA"</code>) <br />
For the California rule (<code>rule="CA"</code>), with probability <i>(1-&alpha;)100\%</i>,
for each of the <i>r</i> future sampling occasions, either the first observation will
fall in the prediction interval, or else all of the next <i>m-1</i> observations will
fall in the prediction interval.  That is, if the first observation falls in the
prediction interval then sampling can stop.  Otherwise, <i>m-1</i> more observations
must be taken.
</p>
<p>The formula for <i>K</i> is the same as for the <i>k</i>-of-<i>m</i> rule, except that
Equation (6) becomes the following (Davis, 1998b):
</p>
<p style="text-align: center;"><i>p = \int_0^1 T(&radic;{n}K; n-1, &radic;{n}[&Phi;^{-1}(v) + \frac{&Delta;}{&sigma;}]) r\{v[1+v^{m-2}(1-v)]\}^{r-1} [1+v^{m-2}(m-1-mv)] dv \;\;\;\;\;\; (7)</i></p>

<p><br />
</p>
<p><em>The Derivation of K for the Modified California Rule</em> (<code>rule="Modified.CA"</code>) <br />
For the Modified California rule (<code>rule="Modified.CA"</code>), with probability
<i>(1-&alpha;)100\%</i>, for each of the <i>r</i> future sampling occasions, either the
first observation will fall in the prediction interval, or else at least 2 out of
the next 3 observations will fall in the prediction interval.  That is, if the first
observation falls in the prediction interval then sampling can stop.  Otherwise, up
to 3 more observations must be taken.
</p>
<p>The formula for <i>K</i> is the same as for the <i>k</i>-of-<i>m</i> rule, except that
Equation (6) becomes the following (Davis, 1998b):
</p>
<p style="text-align: center;"><i>p = \int_0^1 T(&radic;{n}K; n-1, &radic;{n}[&Phi;^{-1}(v) + \frac{&Delta;}{&sigma;}]) r\{v[1+v(3-v[5-2v])]\}^{r-1} \{1+v[6-v(15-8v)]\} dv \;\;\;\;\;\; (8)</i></p>

<p><br />
</p>
<p><b>The Derivation of K for Future Means</b> <br />
For each of the above rules, if we are interested in using averages instead of
single observations, with <i>w &ge; 1</i> (i.e., <code>n.mean</code><i>&ge; 1</i>), the first
term in the integral in Equations (6)-(8) that involves the cdf of the
<a href="../../stats/help/TDist.html">non-central Student's t-distribution</a> becomes:
</p>
<p style="text-align: center;"><i>T(&radic;{n}K; n-1, \frac{&radic;{n}}{&radic;{w}}[&Phi;^{-1}(v) + \frac{&radic;{w}&Delta;}{&sigma;}]) \;\;\;\;\;\; (9)</i></p>



<h3>Value</h3>

<p>A numeric scalar equal to <i>K</i>, the multiplier of estimated standard
deviation that is used to construct the simultaneous prediction interval.
</p>


<h3>Note</h3>

<p><em>Motivation</em> <br />
Prediction and tolerance intervals have long been applied to quality control and
life testing problems (Hahn, 1970b,c; Hahn and Nelson, 1973).  In the context of
environmental statistics, prediction intervals are useful for analyzing data from
groundwater detection monitoring programs at hazardous and solid waste facilities.
</p>
<p>One of the main statistical problems that plague groundwater monitoring programs at
hazardous and solid waste facilities is the requirement of testing several wells and
several constituents at each well on each sampling occasion.  This is an obvious
multiple comparisons problem, and the naive approach of using a standard t-test at
a conventional <i>&alpha;</i>-level (e.g., 0.05 or 0.01) for each test leads to a
very high probability of at least one significant result on each sampling occasion,
when in fact no contamination has occurred.  This problem was pointed out years ago
by Millard (1987) and others.
</p>
<p>Davis and McNichols (1987) proposed simultaneous prediction intervals as a way of
controlling the facility-wide false positive rate (FWFPR) while maintaining adequate
power to detect contamination in the groundwater.  Because of the ubiquitous presence
of spatial variability, it is usually best to use simultaneous prediction intervals
at each well (Davis, 1998a).  That is, by constructing prediction intervals based on
background (pre-landfill) data on each well, and comparing future observations at a
well to the prediction interval for that particular well.  In each of these cases,
the individual <i>&alpha;</i>-level at each well is equal to the FWFRP divided by the
product of the number of wells and constituents.
</p>
<p>Often, observations at downgradient wells are not available prior to the
construction and operation of the landfill.  In this case, upgradient well data can
be combined to create a background prediction interval, and observations at each
downgradient well can be compared to this prediction interval.  If spatial
variability is present and a major source of variation, however, this method is not
really valid (Davis, 1994; Davis, 1998a).
</p>
<p>Chapter 19 of USEPA (2009) contains an extensive discussion of using the
<i>1</i>-of-<i>m</i> rule and the Modified California rule.
</p>
<p>Chapters 1 and 3 of Gibbons et al. (2009) discuss simultaneous prediction intervals
for the normal and lognormal distributions, respectively.
<br />
</p>
<p><em>The k-of-m Rule</em> <br />
For the <i>k</i>-of-<i>m</i> rule, Davis and McNichols (1987) give tables with
&ldquo;optimal&rdquo; choices of <i>k</i> (in terms of best power for a given overall
confidence level) for selected values of <i>m</i>, <i>r</i>, and <i>n</i>.  They found
that the optimal ratios of <i>k</i> to <i>m</i> (i.e., <i>k/m</i>) are generally small,
in the range of 15-50%.
<br />
</p>
<p><em>The California Rule</em> <br />
The California rule was mandated in that state for groundwater monitoring at waste
disposal facilities when resampling verification is part of the statistical program
(Barclay's Code of California Regulations, 1991).  The California code mandates a
&ldquo;California&rdquo; rule with <i>m &ge; 3</i>.  The motivation for this rule may have
been a desire to have a majority of the observations in bounds (Davis, 1998a).  For
example, for a <i>k</i>-of-<i>m</i> rule with <i>k=1</i> and <i>m=3</i>, a monitoring
location will pass if the first observation is out of bounds, the second resample
is out of bounds, but the last resample is in bounds, so that 2 out of 3 observations
are out of bounds.  For the California rule with <i>m=3</i>, either the first
observation must be in bounds, or the next 2 observations must be in bounds in order
for the monitoring location to pass.
</p>
<p>Davis (1998a) states that if the FWFPR is kept constant, then the California rule
offers little increased power compared to the <i>k</i>-of-<i>m</i> rule, and can
actually decrease the power of detecting contamination.
<br />
</p>
<p><em>The Modified California Rule</em> <br />
The Modified California Rule was proposed as a compromise between a 1-of-<i>m</i>
rule and the California rule.  For a given FWFPR, the Modified California rule
achieves better power than the California rule, and still requires at least as many
observations in bounds as out of bounds, unlike a 1-of-<i>m</i> rule.
<br />
</p>
<p><em>Different Notations Between Different References</em> <br />
For the <i>k</i>-of-<i>m</i> rule described in this help file, both
Davis and McNichols (1987) and USEPA (2009, Chapter 19) use the variable <i>p</i> instead of <i>k</i> to represent the minimum number
of future observations the interval should contain on each of the <i>r</i> sampling
occasions.
</p>
<p>Gibbons et al. (2009, Chapter 1) presents extensive lists of the value of
<i>K</i> for both <i>k</i>-of-<i>m</i> rules and California rules.  Gibbons et al.'s
notation reverses the meaning of <i>k</i> and <i>r</i> compared to the notation used
in this help file.  That is, in Gibbons et al.'s notation, <i>k</i> represents the
number of future sampling occasions or monitoring wells, and <i>r</i> represents the
minimum number of observations the interval should contain on each sampling occasion.
</p>
<p>USEPA (2009, Chapter 19) uses <i>p</i> in place of <i>k</i>.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p><b>Barclay's California Code of Regulations</b>. (1991). Title 22,
Section 66264.97 [concerning hazardous waste facilities] and Title 23,
Section 2550.7(e)(8) [concerning solid waste facilities].
Barclay's Law Publishers, San Francisco, CA.
</p>
<p>Davis, C.B. (1998a).  <em>Ground-Water Statistics &amp; Regulations:  Principles,
Progress and Problems</em>.  Second Edition.  Environmetrics &amp; Statistics Limited,
Henderson, NV.
</p>
<p>Davis, C.B. (1998b). Personal Communication, September 3, 1998.
</p>
<p>Davis, C.B., and R.J. McNichols. (1987).  One-sided Intervals for at Least <i>p</i>
of <i>m</i> Observations from a Normal Population on Each of <i>r</i> Future Occasions.
<em>Technometrics</em> <b>29</b>, 359&ndash;370.
</p>
<p>Fertig, K.W., and N.R. Mann. (1977).  One-Sided Prediction Intervals for at Least
<i>p</i> Out of <i>m</i> Future Observations From a Normal Population.
<em>Technometrics</em> <b>19</b>, 167&ndash;177.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009).
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.
John Wiley &amp; Sons, Hoboken.
</p>
<p>Hahn, G.J. (1969). Factors for Calculating Two-Sided Prediction Intervals for
Samples from a Normal Distribution.
<em>Journal of the American Statistical Association</em> <b>64</b>(327), 878-898.
</p>
<p>Hahn, G.J. (1970a). Additional Factors for Calculating Prediction Intervals for
Samples from a Normal Distribution.
<em>Journal of the American Statistical Association</em> <b>65</b>(332), 1668-1676.
</p>
<p>Hahn, G.J. (1970b). Statistical Intervals for a Normal Population, Part I: Tables,
Examples and Applications. <em>Journal of Quality Technology</em> <b>2</b>(3), 115-125.
</p>
<p>Hahn, G.J. (1970c). Statistical Intervals for a Normal Population, Part II:
Formulas, Assumptions, Some Derivations. <em>Journal of Quality Technology</em>
<b>2</b>(4), 195-206.
</p>
<p>Hahn, G.J., and W.Q. Meeker. (1991). <em>Statistical Intervals: A Guide for
Practitioners</em>.  John Wiley and Sons, New York.
</p>
<p>Hahn, G., and W. Nelson. (1973). A Survey of Prediction Intervals and Their
Applications.  <em>Journal of Quality Technology</em> <b>5</b>, 178-188.
</p>
<p>Hall, I.J., and R.R. Prairie. (1973).  One-Sided Prediction Intervals to Contain at
Least <i>m</i> Out of <i>k</i> Future Observations.
<em>Technometrics</em> <b>15</b>, 897&ndash;914.
</p>
<p>Millard, S.P. (1987).  Environmental Monitoring, Statistics, and the Law:  Room for
Improvement (with Comment).  <em>The American Statistician</em> <b>41</b>(4), 249&ndash;259.
</p>
<p>Millard, S.P., and Neerchal, N.K. (2001). <em>Environmental Statistics with S-PLUS</em>.
CRC Press, Boca Raton, Florida.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2010).  <em>Errata Sheet - March 2009 Unified Guidance</em>.
EPA 530/R-09-007a, August 9, 2010.  Office of Resource Conservation and Recovery, Program Information and Implementation Division.
U.S. Environmental Protection Agency, Washington, D.C.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/predIntNormSimultaneous.html">predIntNormSimultaneous</a></code>,
<code><a href="../../EnvStats/help/predIntNormSimultaneousTestPower.html">predIntNormSimultaneousTestPower</a></code>,
<code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>, <code><a href="../../EnvStats/help/predIntNormK.html">predIntNormK</a></code>,
<code><a href="../../EnvStats/help/predIntLnormSimultaneous.html">predIntLnormSimultaneous</a></code>, <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code>,
<a href="../../stats/html/Normal.html">Normal</a>, <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>, <code><a href="../../EnvStats/help/enorm.html">enorm</a></code>
</p>


<h3>Examples</h3>

<pre>
  # Compute the value of K for an upper 95% simultaneous prediction
  # interval to contain at least 1 out of the next 3 observations
  # given a background sample size of n=8.

  predIntNormSimultaneousK(n = 8, k = 1, m = 3)

  #----------

  # Compare the value of K for a 95% 1-of-3 upper prediction interval to
  # the value for the California and Modified California rules.
  # Note that the value of K for the Modified California rule is between
  # the value of K for the 1-of-3 rule and the California rule.

  predIntNormSimultaneousK(n = 8, k = 1, m = 3)

  predIntNormSimultaneousK(n = 8, m = 3, rule = "CA")

  predIntNormSimultaneousK(n = 8, rule = "Modified.CA")

  #----------

  # Show how the value of K for an upper 95% simultaneous prediction
  # limit increases as the number of future sampling occasions r increases.
  # Here, we'll use the 1-of-3 rule.

  predIntNormSimultaneousK(n = 8, k = 1, m = 3)


  predIntNormSimultaneousK(n = 8, k = 1, m = 3, r = 10)

  #==========

  # Example 19-1 of USEPA (2009, p. 19-17) shows how to compute an
  # upper simultaneous prediction limit for the 1-of-3 rule for
  # r = 2 future sampling occasions.  The data for this example are
  # stored in EPA.09.Ex.19.1.sulfate.df.

  # We will pool data from 4 background wells that were sampled on
  # a number of different occasions, giving us a sample size of
  # n = 25 to use to construct the prediction limit.

  # There are 50 compliance wells and we will monitor 10 different
  # constituents at each well at each of the r=2 future sampling
  # occasions.  To determine the confidence level we require for
  # the simultaneous prediction interval, USEPA (2009) recommends
  # setting the individual Type I Error level at each well to

  # 1 - (1 - SWFPR)^(1 / (Number of Constituents * Number of Wells))

  # which translates to setting the confidence limit to

  # (1 - SWFPR)^(1 / (Number of Constituents * Number of Wells))

  # where SWFPR = site-wide false positive rate.  For this example, we
  # will set SWFPR = 0.1.  Thus, the confidence level is given by:

  nc &lt;- 10
  nw &lt;- 50
  SWFPR &lt;- 0.1
  conf.level &lt;- (1 - SWFPR)^(1 / (nc * nw))

  conf.level

  #----------

  # Compute the value of K for the upper simultaneous prediction
  # limit for the 1-of-3 plan.

  predIntNormSimultaneousK(n = 25, k = 1, m = 3, r = 2,
    rule = "k.of.m", pi.type = "upper", conf.level = conf.level)

  #two-sided option
  predIntNormSimultaneousK(n = 25, k = 1, m = 3, r = 2,
   rule = "k.of.m", pi.type = "two-sided", conf.level = conf.level)

</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
