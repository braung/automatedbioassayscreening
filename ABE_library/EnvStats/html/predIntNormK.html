<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Compute the Value of K for a Prediction Interval for a Normal...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for predIntNormK {EnvStats}"><tr><td>predIntNormK {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Compute the Value of <i>K</i> for a Prediction Interval for a Normal Distribution
</h2>

<h3>Description</h3>

<p>Compute the value of <i>K</i> (the multiplier of estimated standard deviation) used 
to construct a prediction interval for the next <i>k</i> observations or next set of 
<i>k</i> means based on data from a <a href="../../stats/help/Normal.html">normal distribution</a>.  
The function 
<code>predIntNormK</code> is called by <code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>.
</p>


<h3>Usage</h3>

<pre>
  predIntNormK(n, df = n - 1, n.mean = 1, k = 1, 
    method = "Bonferroni", pi.type = "two-sided", 
    conf.level = 0.95)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n</code></td>
<td>

<p>a positive integer greater than 2 indicating the sample size upon which the 
prediction interval is based.
</p>
</td></tr>
<tr valign="top"><td><code>df</code></td>
<td>

<p>the degrees of freedom associated with the prediction interval.  The default is 
<code>df=n-1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>n.mean</code></td>
<td>

<p>positive integer specifying the sample size associated with the <i>k</i> future averages.  
The default value is <code>n.mean=1</code> (i.e., individual observations).  Note that all 
future averages must be based on the same sample size.
</p>
</td></tr>
<tr valign="top"><td><code>k</code></td>
<td>

<p>positive integer specifying the number of future observations or averages the 
prediction interval should contain with confidence level <code>conf.level</code>.  
The default value is <code>k=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method to use if the number of future observations 
(<code>k</code>) is greater than 1.  The possible values are <code>method="Bonferroni"</code> 
(approximate method based on Bonferonni inequality; the default), and <br />
<code>method="exact"</code> (exact method due to Dunnett, 1955).  See the DETAILS section for 
more information.  This argument is ignored if <code>k=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>pi.type</code></td>
<td>

<p>character string indicating what kind of prediction interval to compute.  
The possible values are <code>pi.type="two-sided"</code> (the default), <code>pi.type="lower"</code>, 
and <code>pi.type="upper"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the prediction interval.  
The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>A prediction interval for some population is an interval on the real line constructed 
so that it will contain <i>k</i> future observations or averages from that population 
with some specified probability <i>(1-&alpha;)100\%</i>, where 
<i>0 &lt; &alpha; &lt; 1</i> and <i>k</i> is some pre-specified positive integer.  
The quantity <i>(1-&alpha;)100\%</i> is called  
the confidence coefficient or confidence level associated with the prediction 
interval.
</p>
<p>Let <i>\underline{x} = x_1, x_2, &hellip;, x_n</i> denote a vector of <i>n</i> 
observations from a <a href="../../stats/help/Normal.html">normal distribution</a> with parameters 
<code>mean=</code><i>&mu;</i> and <code>sd=</code><i>&sigma;</i>.  Also, let <i>m</i> denote the 
sample size associated with the <i>k</i> future averages (i.e., <code>n.mean=</code><i>m</i>).  
When <i>m=1</i>, each average is really just a single observation, so in the rest of 
this help file the term &ldquo;averages&rdquo; will replace the phrase 
&ldquo;observations or averages&rdquo;.
</p>
<p>For a normal distribution, the form of a two-sided <i>(1-&alpha;)100\%</i> prediction 
interval is: 
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, \bar{x} + Ks] \;\;\;\;\;\; (1)</i></p>
 
<p>where <i>\bar{x}</i> denotes the sample mean:
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\;\;\; (2)</i></p>

<p><i>s</i> denotes the sample standard deviation:
</p>
<p style="text-align: center;"><i>s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\;\;\; (3)</i></p>

<p>and <i>K</i> denotes a constant that depends on the sample size <i>n</i>, the 
confidence level, the number of future averages <i>k</i>, and the 
sample size associated with the future averages, <i>m</i>.  Do not confuse the 
constant <i>K</i> (uppercase K) with the number of future averages <i>k</i> 
(lowercase k).  The symbol <i>K</i> is used here to be consistent with the 
notation used for tolerance intervals (see <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code>).  
</p>
<p>Similarly, the form of a one-sided lower prediction interval is: 
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, &infin;] \;\;\;\;\;\; (4)</i></p>
 
<p>and the form of a one-sided upper prediction interval is:
</p>
<p style="text-align: center;"><i>[-&infin;, \bar{x} + Ks] \;\;\;\;\;\; (5)</i></p>
 
<p>but <i>K</i> differs for one-sided versus two-sided prediction intervals.  
The derivation of the constant <i>K</i> is explained below.  The function 
<code>predIntNormK</code> computes the value of <i>K</i> and is called by 
<code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>.
<br />
</p>
<p><b>The Derivation of K for One Future Observation or Average (k = 1)</b> <br />
Let <i>X</i> denote a random variable from a <a href="../../stats/help/Normal.html">normal distribution</a> 
with parameters <code>mean=</code><i>&mu;</i> and <code>sd=</code><i>&sigma;</i>, and let 
<i>x_p</i> denote the <i>p</i>'th quantile of <i>X</i>.
</p>
<p>A true two-sided <i>(1-&alpha;)100\%</i> prediction interval for the next 
<i>k=1</i> observation of <i>X</i> is given by:
</p>
<p style="text-align: center;"><i>[x_{&alpha;/2}, x_{1-&alpha;/2}] = [&mu; - z_{1-&alpha;/2}&sigma;,  &mu; + z_{1-&alpha;/2}&sigma;] \;\;\;\;\;\; (6)</i></p>

<p>where <i>z_p</i> denotes the <i>p</i>'th quantile of a standard normal distribution.
</p>
<p>More generally, a true two-sided <i>(1-&alpha;)100\%</i> prediction interval for the 
next <i>k=1</i> average based on a sample of size <i>m</i> is given by:
</p>
<p style="text-align: center;"><i>[&mu; - z_{1-&alpha;/2}\frac{&sigma;}{&radic;{m}},  &mu; + z_{1-&alpha;/2}\frac{&sigma;}{&radic;{m}}] \;\;\;\;\;\; (7)</i></p>

<p>Because the values of <i>&mu;</i> and <i>&sigma;</i> are unknown, they must be 
estimated, and a prediction interval then constructed based on the estimated 
values of <i>&mu;</i> and <i>&sigma;</i>.
</p>
<p>For a two-sided prediction interval (<code>pi.type="two-sided"</code>), 
the constant <i>K</i> for a <i>(1-&alpha;)100\%</i> prediction interval for the next 
<i>k=1</i> average based on a sample size of <i>m</i> is computed as: 
</p>
<p style="text-align: center;"><i>K = t_{n-1, 1-&alpha;/2} &radic;{\frac{1}{m} + \frac{1}{n}} \;\;\;\;\;\; (8)</i></p>
 
<p>where <i>t_{&nu;, p}</i> denotes the <i>p</i>'th quantile of the 
<a href="../../stats/help/TDist.html">Student's t-distribution</a> with <i>&nu;</i> 
degrees of freedom.  For a one-sided prediction interval 
(<code>pi.type="lower"</code> or <code>pi.type="lower"</code>), the prediction interval 
is given by: 
</p>
<p style="text-align: center;"><i>K = t_{n-1, 1-&alpha;} &radic;{\frac{1}{m} + \frac{1}{n}} \;\;\;\;\;\; (9)</i></p>
<p>.
</p>
<p>The formulas for these prediction intervals are derived as follows.  Let 
<i>\bar{y}</i> denote the future average based on <i>m</i> observations.  Then 
the quantity <i>\bar{y} - \bar{x}</i> has a normal distribution with expectation 
and variance given by:
</p>
<p style="text-align: center;"><i>E(\bar{y} - \bar{x}) = 0 \;\;\;\;\;\; (10)</i></p>

<p style="text-align: center;"><i>Var(\bar{y} - \bar{x}) = Var(\bar{y}) + Var(\bar{x}) = \frac{&sigma;^2}{m} + \frac{&sigma;^2}{n} = &sigma;^2(\frac{1}{m} + \frac{1}{n}) \;\;\;\;\;\; (11)</i></p>

<p>so the quantity
</p>
<p style="text-align: center;"><i>t = \frac{\bar{y} - \bar{x}}{s&radic;{\frac{1}{m} + \frac{1}{n}}} \;\;\;\;\;\; (12)</i></p>

<p>has a <a href="../../stats/help/TDist.html">Student's t-distribution</a> with <i>n-1</i> degrees of freedom.
<br />
</p>
<p><b>The Derivation of K for More than One Future Observation or Average (k &gt;1)</b> <br />
When <i>k &gt; 1</i>, the function <code>predIntNormK</code> allows for two ways to compute 
<i>K</i>:  an exact method due to Dunnett (1955) (<code>method="exact"</code>), and 
an approximate (conservative) method based on the Bonferroni inequality 
(<code>method="Bonferroni"</code>; see Miller, 1981a, pp.8, 67-70; 
Gibbons et al., 2009, p.4).  Each of these methods is explained below.
<br />
</p>
<p><em>Exact Method Due to Dunnett (1955)</em> (<code>method="exact"</code>) <br />
Dunnett (1955) derived the value of <i>K</i> in the context of the multiple 
comparisons problem of comparing several treatment means to one control mean.  
The value of <i>K</i> is computed as:
</p>
<p style="text-align: center;"><i>K = c &radic;{\frac{1}{m} + \frac{1}{n}} \;\;\;\;\;\; (13)</i></p>

<p>where <i>c</i> is a constant that depends on the sample size <i>n</i>, the number of 
future observations (averages) <i>k</i>, the sample size associated with the 
<i>k</i> future averages <i>m</i>, and the confidence level <i>(1-&alpha;)100\%</i>.
</p>
<p>When <code>pi.type="lower"</code> or <code>pi.type="upper"</code>, the value of <i>c</i> is the 
number that satisfies the following equation (Gupta and Sobel, 1957; Hahn, 1970a):
</p>
<p style="text-align: center;"><i>1 - &alpha; = \int_{0}^{&infin;} F_1(cs, k, &rho;) h(s&radic;{n-1}, n-1) &radic;{n-1} ds \;\;\;\;\;\; (14)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>F_1(x, k, &rho;) = \int_{&infin;}^{&infin;} [&Phi;(\frac{x + &rho;^{1/2}y}{&radic;{1 - &rho;}})]^k &phi;(y) dy \;\;\;\;\;\; (15)</i></p>

<p style="text-align: center;"><i>&rho; = 1 / (\frac{n}{m} + 1) \;\;\;\;\;\; (16)</i></p>

<p style="text-align: center;"><i>h(x, &nu;) = \frac{x^{&nu;-1}e^{-x^2/2}}{2^{(&nu;/2) - 1} &Gamma;(\frac{&nu;}{2})} \;\;\;\;\;\; (17)</i></p>

<p>and <i>&Phi;()</i> and <i>&phi;()</i> denote the cumulative distribution function and 
probability density function, respectively, of the standard normal distribution.  
Note that the function <i>h(x, &nu;)</i> is the probability density function of a 
<a href="../../EnvStats/help/Chi.html">chi random variable</a> with <i>&nu;</i> degrees of freedom.
</p>
<p>When <code>pi.type="two-sided"</code>, the value of <i>c</i> is the number that satisfies 
the following equation:
</p>
<p style="text-align: center;"><i>1 - &alpha; = \int_{0}^{&infin;} F_2(cs, k, &rho;) h(s&radic;{n-1}, n-1) &radic;{n-1} ds \;\;\;\;\;\; (18)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>F_2(x, k, &rho;) = \int_{&infin;}^{&infin;} [&Phi;(\frac{x + &rho;^{1/2}y}{&radic;{1 - &rho;}}) - &Phi;(\frac{-x + &rho;^{1/2}y}{&radic;{1 - &rho;}})]^k &phi;(y) dy \;\;\;\;\;\; (19)</i></p>

<p><br />
</p>
<p><em>Approximate Method Based on the Bonferroni Inequality</em> (<code>method="Bonferroni"</code>) <br />
As shown above, when <i>k=1</i>, the value of <i>K</i> is given by Equation (8) or 
Equation (9) for two-sided or one-sided prediction intervals, respectively.  When 
<i>k &gt; 1</i>, a conservative way to construct a <i>(1-&alpha;^*)100\%</i> prediction 
interval for the next <i>k</i> observations or averages is to use a Bonferroni 
correction (Miller, 1981a, p.8) and set <i>&alpha; = &alpha;^*/k</i> in Equation (8) 
or (9) (Chew, 1968).  This value of <i>K</i> will be conservative in that the computed 
prediction intervals will be wider than the exact predictions intervals.  
Hahn (1969, 1970a) compared the exact values of <i>K</i> with those based on the 
Bonferroni inequality for the case of <i>m=1</i> and found the approximation to be 
quite satisfactory except when <i>n</i> is small, <i>k</i> is large, and <i>&alpha;</i> 
is large.  For example, Gibbons (1987a) notes that for a 99% prediction interval 
(i.e., <i>&alpha; = 0.01</i>) for the next <i>k</i> observations, if <i>n &gt; 4</i>, 
the bias of <i>K</i> is never greater than 1% no matter what the value of <i>k</i>.
</p>


<h3>Value</h3>

<p>A numeric scalar equal to <i>K</i>, the multiplier of estimated standard 
deviation that is used to construct the prediction interval.
</p>


<h3>Note</h3>

<p>Prediction and tolerance intervals have long been applied to quality control and 
life testing problems (Hahn, 1970b,c; Hahn and Nelson, 1973).  
In the context of environmental statistics, prediction intervals are useful for 
analyzing data from groundwater detection monitoring programs at hazardous and 
solid waste facilities (e.g., Gibbons et al., 2009; Millard and Neerchal, 2001; 
USEPA, 2009).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). <em>Statistics for Environmental Engineers</em>. 
Lewis Publishers, Boca Raton.
</p>
<p>Dunnett, C.W. (1955). A Multiple Comparisons Procedure for Comparing Several Treatments 
with a Control. <em>Journal of the American Statistical Association</em> <b>50</b>, 1096-1121.
</p>
<p>Dunnett, C.W. (1964). New Tables for Multiple Comparisons with a Control. 
<em>Biometrics</em> <b>20</b>, 482-491.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009). 
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.  
John Wiley &amp; Sons, Hoboken.
</p>
<p>Hahn, G.J. (1969). Factors for Calculating Two-Sided Prediction Intervals for 
Samples from a Normal Distribution. 
<em>Journal of the American Statistical Association</em> <b>64</b>(327), 878-898.
</p>
<p>Hahn, G.J. (1970a). Additional Factors for Calculating Prediction Intervals for 
Samples from a Normal Distribution. 
<em>Journal of the American Statistical Association</em> <b>65</b>(332), 1668-1676.
</p>
<p>Hahn, G.J. (1970b). Statistical Intervals for a Normal Population, Part I: Tables, 
Examples and Applications. <em>Journal of Quality Technology</em> <b>2</b>(3), 115-125.
</p>
<p>Hahn, G.J. (1970c). Statistical Intervals for a Normal Population, Part II: 
Formulas, Assumptions, Some Derivations. <em>Journal of Quality Technology</em> 
<b>2</b>(4), 195-206.
</p>
<p>Hahn, G.J., and W.Q. Meeker. (1991). <em>Statistical Intervals: A Guide for Practitioners</em>. 
John Wiley and Sons, New York.
</p>
<p>Hahn, G., and W. Nelson. (1973). A Survey of Prediction Intervals and Their Applications. 
<em>Journal of Quality Technology</em> <b>5</b>, 178-188.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). <em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (2002). <em>Statistical Methods in Water Resources</em>. 
Techniques of Water Resources Investigations, Book 4, chapter A3. U.S. Geological Survey. 
(available on-line at:  <a href="http://pubs.usgs.gov/twri/twri4a3/">http://pubs.usgs.gov/twri/twri4a3/</a>).
</p>
<p>Millard, S.P., and Neerchal, N.K. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, Florida.
</p>
<p>Miller, R.G. (1981a). <em>Simultaneous Statistical Inference</em>. McGraw-Hill, New York.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2010).  <em>Errata Sheet - March 2009 Unified Guidance</em>.
EPA 530/R-09-007a, August 9, 2010.  Office of Resource Conservation and Recovery, Program Information and Implementation Division.
U.S. Environmental Protection Agency, Washington, D.C.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/predIntNorm.html">predIntNorm</a></code>, <code><a href="../../EnvStats/help/predIntNormSimultaneous.html">predIntNormSimultaneous</a></code>, 
<code><a href="../../EnvStats/help/predIntLnorm.html">predIntLnorm</a></code>, <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code>, 
<a href="../../stats/html/Normal.html">Normal</a>, <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>, <code><a href="../../EnvStats/help/enorm.html">enorm</a></code>, <code><a href="../../EnvStats/help/eqnorm.html">eqnorm</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Compute the value of K for a two-sided 95% prediction interval 
  # for the next observation given a sample size of n=20.

  predIntNormK(n = 20)
  #[1] 2.144711

  #--------------------------------------------------------------------

  # Compute the value of K for a one-sided upper 99% prediction limit 
  # for the next 3 averages of order 2 (i.e., each of the 3 future 
  # averages is based on a sample size of 2 future observations) given a 
  # samle size of n=20.

  predIntNormK(n = 20, n.mean = 2, k = 3, pi.type = "upper", 
    conf.level = 0.99)
  #[1] 2.258026

  #----------

  # Compare the result above that is based on the Bonferroni method 
  # with the exact method.

  predIntNormK(n = 20, n.mean = 2, k = 3, method = "exact", 
    pi.type = "upper", conf.level = 0.99)
  #[1] 2.251084

  #--------------------------------------------------------------------

  # Example 18-1 of USEPA (2009, p.18-9) shows how to construct a 95% 
  # prediction interval for 4 future observations assuming a 
  # normal distribution based on arsenic concentrations (ppb) in 
  # groundwater at a solid waste landfill.  There were 4 years of 
  # quarterly monitoring, and years 1-3 are considered background, 

  # So the sample size for the prediciton limit is n = 12, 
  # and the number of future samples is k = 4.

  predIntNormK(n = 12, k = 4, pi.type = "upper")
  #[1] 2.698976
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
