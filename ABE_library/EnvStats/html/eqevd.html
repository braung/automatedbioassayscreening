<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Quantiles of an Extreme Value (Gumbel) Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for eqevd {EnvStats}"><tr><td>eqevd {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Quantiles of an Extreme Value (Gumbel) Distribution
</h2>

<h3>Description</h3>

<p>Estimate quantiles of an <a href="../../EnvStats/help/EVD.html">extreme value distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  eqevd(x, p = 0.5, method = "mle", pwme.method = "unbiased", 
    plot.pos.cons = c(a = 0.35, b = 0), digits = 0)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>a numeric vector of observations, or an object resulting from a call to an 
estimating function that assumes an extreme value distribution 
(e.g., <code><a href="../../EnvStats/help/eevd.html">eevd</a></code>).  If <code>x</code> is a numeric vector, 
missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>numeric vector of probabilities for which quantiles will be estimated.  
All values of <code>p</code> must be between 0 and 1.  The default value is <code>p=0.5</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method to use to estimate the location and scale 
parameters.  Possible values are 
<code>"mle"</code> (maximum likelihood; the default), <code>"mme"</code> (methods of moments), 
<code>"mmue"</code> (method of moments based on the unbiased estimator of variance), and 
<code>"pwme"</code> (probability-weighted moments).  See the DETAILS section of the 
help file for <code><a href="../../EnvStats/help/eevd.html">eevd</a></code> for more information on these estimation methods. 
</p>
</td></tr>
<tr valign="top"><td><code>pwme.method</code></td>
<td>

<p>character string specifying what method to use to compute the 
probability-weighted moments when <code>method="pwme"</code>.  The possible values are 
<code>"ubiased"</code> (method based on the U-statistic; the default), or 
<code>"plotting.position"</code> (method based on the plotting position formula).  
See the DETAILS section of the help file for <code><a href="../../EnvStats/help/eevd.html">eevd</a></code> for more information.  
This argument is ignored if <code>method</code> is not equal to <code>"pwme"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>plot.pos.cons</code></td>
<td>

<p>numeric vector of length 2 specifying the constants used in the formula for the 
plotting positions when <code>method="pwme"</code> and <br />
<code>pwme.method="plotting.position"</code>.  The default value is <br />
<code>plot.pos.cons=c(a=0.35, b=0)</code>.  If this vector has a names attribute with 
the value <code>c("a","b")</code> or <code>c("b","a")</code>, then the elements will be 
matched by name in the formula for computing the plotting positions.  Otherwise, 
the first element is mapped to the name <code>"a"</code> and the second element to the 
name <code>"b"</code>.  See the DETAILS section of the help file for <code><a href="../../EnvStats/help/eevd.html">eevd</a></code> 
for more information.  This argument is ignored if 
<code>method</code> is not equal to <code>"pwme"</code> or if <code>pwme.method="ubiased"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>digits</code></td>
<td>

<p>an integer indicating the number of decimal places to round to when printing out 
the value of <code>100*p</code>. The default value is <code>digits=0</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The function <code>eqevd</code> returns estimated quantiles as well as 
estimates of the location and scale parameters.  
</p>
<p>Quantiles are estimated by 1) estimating the location and scale parameters by 
calling <code><a href="../../EnvStats/help/eevd.html">eevd</a></code>, and then 2) calling the function 
<code><a href="../../EnvStats/help/EVD.html">qevd</a></code> and using the estimated values for 
location and scale.
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, <code>eqevd</code> returns a 
list of class <code>"estimate"</code> containing the estimated quantile(s) and other 
information. See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function, <code>eqevd</code> 
returns a list whose class is the same as <code>x</code>.  The list 
contains the same components as <code>x</code>, as well as components called 
<code>quantiles</code> and <code>quantile.method</code>.
</p>


<h3>Note</h3>

<p>There are three families of extreme value distributions.  The one 
described here is the <a href="../../EnvStats/help/EVD.html">Type I, also called the Gumbel extreme value 
distribution or simply Gumbel distribution</a>.  The name 
&ldquo;extreme value&rdquo; comes from the fact that this distribution is 
the limiting distribution (as <i>n</i> approaches infinity) of the 
greatest value among <i>n</i> independent random variables each 
having the same continuous distribution.
</p>
<p>The Gumbel extreme value distribution is related to the 
<a href="../../stats/help/Exponential.html">exponential distribution</a> as follows. 
Let <i>Y</i> be an <a href="../../stats/help/Exponential.html">exponential random variable</a> 
with parameter <code>rate=</code><i>&lambda;</i>.  Then <i>X = &eta; - log(Y)</i> 
has an extreme value distribution with parameters 
<code>location=</code><i>&eta;</i> and <code>scale=</code><i>1/&lambda;</i>.
</p>
<p>The distribution described above and assumed by <code>eevd</code> is the 
<em>largest</em> extreme value distribution.  The smallest extreme value 
distribution is the limiting distribution (as <i>n</i> approaches infinity) 
of the smallest value among 
<i>n</i> independent random variables each having the same continuous distribution. 
If <i>X</i> has a largest extreme value distribution with parameters 
<code>location=</code><i>&eta;</i> and <code>scale=</code><i>&theta;</i>, then 
<i>Y = -X</i> has a smallest extreme value distribution with parameters 
<code>location=</code><i>-&eta;</i> and <code>scale=</code><i>&theta;</i>.  The smallest 
extreme value distribution is related to the <a href="../../stats/help/Weibull.html">Weibull distribution</a> 
as follows.  Let <i>Y</i> be a <a href="../../stats/help/Weibull.html">Weibull random variable</a> with 
parameters 
<code>shape=</code><i>&beta;</i> and <code>scale=</code><i>&alpha;</i>.  Then <i>X = log(Y)</i> 
has a smallest extreme value distribution with parameters <code>location=</code><i>log(&alpha;)</i> 
and <code>scale=</code><i>1/&beta;</i>.
</p>
<p>The extreme value distribution has been used extensively to model the distribution 
of streamflow, flooding, rainfall, temperature, wind speed, and other 
meteorological variables, as well as material strength and life data.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Castillo, E. (1988).  <em>Extreme Value Theory in Engineering</em>.  
Academic Press, New York, pp.184&ndash;198.
</p>
<p>Downton, F. (1966).  Linear Estimates of Parameters in the Extreme Value 
Distribution.  <em>Technometrics</em> <b>8</b>(1), 3&ndash;17.
</p>
<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Greenwood, J.A., J.M. Landwehr, N.C. Matalas, and J.R. Wallis. (1979).  
Probability Weighted Moments: Definition and Relation to Parameters of Several 
Distributions Expressible in Inverse Form.  <em>Water Resources Research</em> 
<b>15</b>(5), 1049&ndash;1054.
</p>
<p>Hosking, J.R.M., J.R. Wallis, and E.F. Wood. (1985).  Estimation of the 
Generalized Extreme-Value Distribution by the Method of 
Probability-Weighted Moments.  <em>Technometrics</em> <b>27</b>(3), 251&ndash;261.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995). 
<em>Continuous Univariate Distributions, Volume 2</em>. 
Second Edition. John Wiley and Sons, New York.
</p>
<p>Landwehr, J.M., N.C. Matalas, and J.R. Wallis. (1979).  Probability Weighted 
Moments Compared With Some Traditional Techniques in Estimating Gumbel 
Parameters and Quantiles.  <em>Water Resources Research</em> <b>15</b>(5), 
1055&ndash;1064.
</p>
<p>Tiago de Oliveira, J. (1963).  Decision Results for the Parameters of the 
Extreme Value (Gumbel) Distribution Based on the Mean and Standard Deviation.  
<em>Trabajos de Estadistica</em> <b>14</b>, 61&ndash;81.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/eevd.html">eevd</a></code>, <a href="../../EnvStats/help/EVD.html">Extreme Value Distribution</a>, 
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from an extreme value distribution with 
  # parameters location=2 and scale=1, then estimate the parameters 
  # and estimate the 90'th percentile.
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- revd(20, location = 2) 
  eqevd(dat, p = 0.9) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Extreme Value
  #
  #Estimated Parameter(s):          location = 1.9684093
  #                                 scale    = 0.7481955
  #
  #Estimation Method:               mle
  #
  #Estimated Quantile(s):           90'th %ile = 3.652124
  #
  #Quantile Estimation Method:      Quantile(s) Based on
  #                                 mle Estimators
  #
  #Data:                            dat
  #
  #Sample Size:                     20

  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
