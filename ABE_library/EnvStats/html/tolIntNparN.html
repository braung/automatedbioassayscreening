<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Sample Size for Nonparametric Tolerance Interval for...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for tolIntNparN {EnvStats}"><tr><td>tolIntNparN {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Sample Size for Nonparametric Tolerance Interval for Continuous Distribution
</h2>

<h3>Description</h3>

<p>Compute the sample size necessary for a nonparametric tolerance interval (for a continuous 
distribution) with a specified coverage and, in the case of a <i>&beta;</i>-content tolerance 
interval, a specified confidence level, given the ranks of the order statistics used for the 
interval.
</p>


<h3>Usage</h3>

<pre>
  tolIntNparN(coverage = 0.95, conf.level = 0.95, cov.type = "content", 
    ltl.rank = ifelse(ti.type == "upper", 0, 1), 
    n.plus.one.minus.utl.rank = ifelse(ti.type == "lower", 0, 1),  
    ti.type = "two.sided")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>coverage</code></td>
<td>

<p>numeric vector of values between 0 and 1 indicating the desired coverage of the 
tolerance interval.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>numeric vector of values between 0 and 1 indicating the confidence level of the 
tolerance interval.
</p>
</td></tr>
<tr valign="top"><td><code>cov.type</code></td>
<td>

<p>character string specifying the coverage type for the tolerance interval.  
The possible values are <code>"content"</code> (<i>&beta;</i>-content; the default), and 
<code>"expectation"</code> (<i>&beta;</i>-expectation).
</p>
</td></tr>
<tr valign="top"><td><code>ltl.rank</code></td>
<td>

<p>vector of positive integers indicating the rank of the order statistic to use for the lower bound 
of the tolerance interval.  If <code>ti.type="two-sided"</code> or <br />
<code>ti.type="lower"</code>, 
the default value is <code>ltl.rank=1</code> (implying the minimum value of <code>x</code> is used 
as the lower bound of the tolerance interval).  If <br />
<code>ti.type="upper"</code>, this argument 
is set equal to <code>0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>n.plus.one.minus.utl.rank</code></td>
<td>

<p>vector of positive integers related to the rank of the order statistic to use for 
the upper bound of the tolerance interval.  A value of 
<code>n.plus.one.minus.utl.rank=1</code> (the default) means use the 
first largest value, and in general a value of <br />
<code>n.plus.one.minus.utl.rank=</code><i>i</i> means use the <i>i</i>'th largest value.  
If <br />
<code>ti.type="lower"</code>, this argument is set equal to <code>0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ti.type</code></td>
<td>

<p>character string indicating what kind of tolerance interval to compute.  
The possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If the arguments <code>coverage</code>, <code>conf.level</code>, <code>ltl.rank</code>, and 
<code>n.plus.one.minus.utl.rank</code> are not all the same length, they are replicated to be the 
same length as the length of the longest argument.
</p>
<p>The help file for <code><a href="../../EnvStats/help/tolIntNpar.html">tolIntNpar</a></code> explains how nonparametric tolerance intervals 
are constructed.  
</p>
<p><em>Computing Required Sample Size for a <i>&beta;</i>-Content Tolerance Interval</em> (<code>cov.type="content"</code>) <br />
For a <i>&beta;</i>-content tolerance interval, if the coverage <i>C=&beta;</i> is specified, then the 
associated confidence level <i>(1-&alpha;)100\%</i> is computed as:
</p>
<p style="text-align: center;"><i>1 - &alpha; = 1 - F(&beta;, v-u, w+u) \;\;\;\;\;\; (1)</i></p>

<p>where <i>F(y, &delta;, &gamma;)</i> denotes the cumulative distribution function of a 
<a href="../../stats/help/Beta.html">beta random variable</a> with parameters <code>shape1=</code><i>&delta;</i> and 
<code>shape2=</code><i>&gamma;</i> evaluated at <i>y</i>.  The value of <i>1-&alpha;</i> is determined by 
the argument <code>conf.level</code>.  The value of <i>&beta;</i> is determined by the argument 
<code>coverage</code>.  The value of <i>u</i> is determined by the argument <code>ltl.rank</code>.  The value 
of <i>w</i> is determined by the argument <br />
<code>n.plus.one.minus.utl.rank</code>.  Once these values 
have been determined, the above equation can be solved implicitly for <i>n</i>, since
</p>
<p style="text-align: center;"><i>v = n + 1 - w \;\;\;\;\;\; (2)</i></p>

<p><em>Computing Required Sample Size for a <i>&beta;</i>-Expectation Tolerance Interval</em> (<code>cov.type="expectation"</code>) <br />
For a <i>&beta;</i>-expectation tolerance interval, the expected coverage is simply the mean of a 
<a href="../../stats/help/Beta.html">beta random variable</a> with parameters <code>shape1=</code><i>v-u</i> and 
<code>shape2=</code><i>w+u</i>, which is given by:
</p>
<p style="text-align: center;"><i>E(C) = \frac{v-u}{n+1} \;\;\;\;\;\; (3)</i></p>

<p>or, using Equation (2) above, we can re-write the formula for the expected coverage as:
</p>
<p style="text-align: center;"><i>E(C) = \frac{n+1-w-u}{n+1} = 1 - \frac{u+w}{n+1} \;\;\;\;\;\; (4)</i></p>

<p>Thus, for user-specified values of <i>u</i> (<code>ltl.rank</code>), 
<i>w</i> (<code>n.plus.one.minus.utl.rank</code>), and expected coverage, the required sample 
size is computed as:
</p>
<p style="text-align: center;"><i>n = Ceiling\{ [ \frac{u+w}{1-E(C)} ] - 1 \} \;\;\;\;\;\; (5)</i></p>

<p>where <i>Ceiling(x)</i> denotes the smallest integer greater than or equal to <i>x</i>.  
(See the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> help file for <code><a href="../../base/html/Round.html">ceiling</a></code>).
</p>


<h3>Value</h3>

<p>A vector of positive integers indicating the required sample size(s) for the specified 
nonparametric tolerance interval(s).
</p>


<h3>Note</h3>

<p>See the help file for <code><a href="../../EnvStats/help/tolIntNpar.html">tolIntNpar</a></code>.
</p>
<p>In the course of designing a sampling program, an environmental scientist may wish to determine 
the relationship between sample size, coverage, and confidence level if one of the objectives of 
the sampling program is to produce tolerance intervals.  The functions 
<code>tolIntNparN</code>, <code><a href="../../EnvStats/help/tolIntNparCoverage.html">tolIntNparCoverage</a></code>, <code><a href="../../EnvStats/help/tolIntNparConfLevel.html">tolIntNparConfLevel</a></code>, and 
<code><a href="../../EnvStats/help/plotTolIntNparDesign.html">plotTolIntNparDesign</a></code> can be used to investigate these relationships for 
constructing nonparametric tolerance intervals.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>See the help file for <code><a href="../../EnvStats/help/tolIntNpar.html">tolIntNpar</a></code>.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/tolIntNpar.html">tolIntNpar</a></code>, <code><a href="../../EnvStats/help/tolIntNparConfLevel.html">tolIntNparConfLevel</a></code>, <code><a href="../../EnvStats/help/tolIntNparCoverage.html">tolIntNparCoverage</a></code>, 
<code><a href="../../EnvStats/help/plotTolIntNparDesign.html">plotTolIntNparDesign</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Look at how the required sample size for a nonparametric tolerance interval increases 
  # with increasing confidence level:

  seq(0.5, 0.9, by = 0.1) 
  #[1] 0.5 0.6 0.7 0.8 0.9 

  tolIntNparN(conf.level = seq(0.5, 0.9, by = 0.1)) 
  #[1] 34 40 49 59 77

  #----------

  # Look at how the required sample size for a nonparametric tolerance interval increases 
  # with increasing coverage:

  tolIntNparN(coverage = seq(0.5, 0.9, by = 0.1)) 
  #[1]  8 10 14 22 46

  #----------

  # Look at how the required sample size for a nonparametric tolerance interval increases 
  # with the rank of the lower tolerance limit:

  tolIntNparN(ltl.rank = 1:5) 
  #[1]  93 124 153 181 208

  #==========

  # Example 17-4 on page 17-21 of USEPA (2009) uses copper concentrations (ppb) from 3 
  # background wells to set an upper limit for 2 compliance wells.  The maximum value from 
  # the 3 wells is set to the 95% confidence upper tolerance limit, and we need to 
  # determine the coverage of this tolerance interval.  

  tolIntNparCoverage(n = 24, conf.level = 0.95, ti.type = "upper")
  #[1] 0.8826538

  # Here we will modify the example and determine the sample size required to produce 
  # a tolerance interval with 95% confidence level AND 95% coverage. 

  tolIntNparN(coverage = 0.95, conf.level = 0.95, ti.type = "upper")
  #[1] 59
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
