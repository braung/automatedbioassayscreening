<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Expected Value of Order Statistics for Random Sample from...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for evNormOrdStats {EnvStats}"><tr><td>evNormOrdStats {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Expected Value of Order Statistics for Random Sample from Standard Normal Distribution
</h2>

<h3>Description</h3>

<p>Compute the expected values of order statistics for a random sample from 
a standard <a href="../../stats/help/Normal.html">normal distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  evNormOrdStats(n = 1, 
    method = "royston", lower = -9, inc = 0.025, warn = TRUE, 
    alpha = 3/8, nmc = 2000, seed = 47, approximate = NULL)

  evNormOrdStatsScalar(r = 1, n = 1, 
    method = "royston", lower = -9, inc = 0.025, warn = TRUE, 
    alpha = 3/8, nmc = 2000, conf.level = 0.95, seed = 47, approximate = NULL) 
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n</code></td>
<td>

<p>positive integer indicating the sample size.
</p>
</td></tr>
<tr valign="top"><td><code>r</code></td>
<td>

<p>positive integer between <code>1</code> and <code>n</code> specifying the order statistic 
for which to compute the expected value.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string indicating what method to use.  The possible values are:
</p>

<ul>
<li> <p><code>"royston"</code>.  Method based on approximating the exact integral as 
given in Royston (1982).
</p>
</li>
<li> <p><code>"blom"</code>.  Method based on the approximation formula proposed by 
Blom (1958).
</p>
</li>
<li> <p><code>"mc"</code>.  Method based on Monte Carlo simulation.
</p>
</li></ul>

<p>See the DETAILS section below.
</p>
</td></tr>
<tr valign="top"><td><code>lower</code></td>
<td>

<p>numeric scalar <i>&le; -9</i> defining the lower bound used for approximating the 
integral when <code>method="royston"</code>.  The upper bound is automatically set 
to <code>-lower</code>.  The default value is <code>lower=-9</code>.
</p>
</td></tr>
<tr valign="top"><td><code>inc</code></td>
<td>

<p>numeric scalar between <code>.Machine$double.eps</code> and <code>0.025</code> that determines 
the width of each subdivision used to approximate the integral when <br />
<code>method="royston"</code>.  The default value is <code>inc=0.025</code>.
</p>
</td></tr>
<tr valign="top"><td><code>warn</code></td>
<td>

<p>logical scalar indicating whether to issue a warning when 
<code>method="royston"</code> and the sample size is greater than 2000.  
The default value is <code>warn=TRUE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>alpha</code></td>
<td>

<p>numeric scalar between 0 and 0.5 that determines the constant used when <br /> 
<code>method="blom"</code>.  The default value is <code>alpha=3/8</code>.
</p>
</td></tr>
<tr valign="top"><td><code>nmc</code></td>
<td>

<p>integer <i>&ge; 100</i> denoting the number of Monte Carlo simulations to use 
when <code>method="mc"</code>.  The default value is <code>nmc=2000</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>numeric scalar between 0 and 1 denoting the confidence level of 
the confidence interval for the expected value of the normal 
order statistic when <code>method="mc"</code>.  
The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>seed</code></td>
<td>

<p>integer between <i>-(2^31 - 1)</i> and <i>2^31 - 1</i> specifying 
the argument to <code><a href="../../base/html/Random.html">set.seed</a></code> (the random number seed) 
when <code>method="mc"</code>.  The default value is <code>seed=47</code>.
</p>
</td></tr>
<tr valign="top"><td><code>approximate</code></td>
<td>

<p>logical scalar included for backwards compatibility with versions of 
<span class="pkg">EnvStats</span> prior to version 2.3.0.  
When <code>method</code> is not supplied and <br />
<code>approximate=FALSE</code>, <code>method</code> is set to <code>method="royston"</code>.  
When <code>method</code> is not supplied and <code>approximate=TRUE</code>, 
<code>method</code> is set to <code>method="blom"</code>.  
This argument is ignored if <code>method</code> is supplied and/or 
<code>approxmiate=NULL</code> (the default).
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Let <i>\underline{z} = z_1, z_2, &hellip;, z_n</i> denote a vector of <i>n</i> 
observations from a <a href="../../stats/help/Normal.html">normal distribution</a> with parameters 
<code>mean=0</code> and <code>sd=1</code>.  That is, <i>\underline{z}</i> denotes a vector of 
<i>n</i> observations from a <em>standard</em> normal distribution.  Let 
<i>z_{(r)}</i> denote the <i>r</i>'th order statistic of <i>\underline{z}</i>, 
for <i>r = 1, 2, &hellip;, n</i>.  The probability density function of 
<i>z_{(r)}</i> is given by:
</p>
<p style="text-align: center;"><i>f_{r,n}(t) = \frac{n!}{(r-1)!(n-r)!} [&Phi;(t)]^{r-1} [1 - &Phi;(t)]^{n-r} &phi;(t) \;\;\;\;\;\; (1)</i></p>

<p>where <i>&Phi;</i> and <i>&phi;</i> denote the cumulative distribution function and 
probability density function of the standard normal distribution, respectively 
(Johnson et al., 1994, p.93). Thus, the expected value of <i>z_{(r)}</i> is given by:
</p>
<p style="text-align: center;"><i>E(r, n) = E[z_{(r)}] = \int_{-&infin;}^{&infin;} t f_{r,n}(t) dt \;\;\;\;\;\; (2)</i></p>

<p>It can be shown that if <i>n</i> is odd, then
</p>
<p style="text-align: center;"><i>E[(n+1)/2, n] = 0 \;\;\;\;\;\; (3)</i></p>

<p>Also, for all values of <i>n</i>,
</p>
<p style="text-align: center;"><i>E(r, n) = -E(n-r+1, n) \;\;\;\;\;\; (4)</i></p>

<p>The function <code>evNormOrdStatsScalar</code> computes the value of <i>E(r,n)</i> for 
user-specified values of <i>r</i> and <i>n</i>.
</p>
<p>The function <code>evNormOrdStats</code> computes the values of <i>E(r,n)</i> for all 
values of <i>r</i> (i.e., for <i>r = 1, 2, &hellip;, n</i>) 
for a user-specified value of <i>n</i>.
</p>
<p><b>Exact Method Based on Royston's Approximation to the Integral</b> (<code>method="royston"</code>) 
</p>
<p>When <code>method="royston"</code>, the integral in Equation (2) above is approximated by 
computing the value of the integrand between the values of <code>lower</code> and 
<code>-lower</code> using increments of <code>inc</code>, then summing these values and 
multiplying by <code>inc</code>.  In particular, the integrand is restructured as:
</p>
<p style="text-align: center;"><i>t \; f_{r,n}(t) = t \; exp\{log(n!) - log[(r-1)!] - log[(n-r)!] +  (r-1)log[&Phi;(t)] + (n-r)log[1 - &Phi;(t)] + log[&phi;(t)]\} \;\;\; (5)</i></p>

<p>By default, as per Royston (1982), the integrand is evaluated between -9 and 9 in 
increments of 0.025.  The approximation is computed this way for values of 
<i>r</i> between <i>1</i> and <i>[n/2]</i>, where <i>[x]</i> denotes the floor of <i>x</i>.  
If <i>r &gt; [n/2]</i>, then the approximation is computed for <i>E(n-r+1, n)</i> and 
Equation (4) is used. 
</p>
<p>Note that Equation (1) in Royston (1982) differs from Equations (1) and (2) above 
because Royston's paper is based on the <i>r^{th}</i> <em>largest</em> value, 
not the <i>r^{th}</i> order statistic. 
</p>
<p>Royston (1982) states that this algorithm &ldquo;is accurate to at least seven decimal 
places on a 36-bit machine,&rdquo; that it has been validated up to a sample size 
of <i>n=2000</i>, and that the accuracy for <i>n &gt; 2000</i> may be improved by 
reducing the value of the argument <code>inc</code>.  Note that making 
<code>inc</code> smaller will increase the computation time.  <br />
</p>
<p><b>Approxmation Based on Blom's Method</b> (<code>method="blom"</code>) 
</p>
<p>When <code>method="blom"</code>, the following approximation to <i>E(r,n)</i>, 
proposed by Blom (1958, pp. 68-75), is used:
</p>
<p style="text-align: center;"><i>E(r, n) \approx &Phi;^{-1}(\frac{r - &alpha;}{n - 2&alpha; + 1}) \;\;\;\;\;\; (5)</i></p>

<p>By default, <i>&alpha; = 3/8 = 0.375</i>.  This approximation is quite accurate.  
For example, for <i>n &ge; 2</i>, the approximation is accurate to the first decimal place, 
and for <i>n &ge; 9</i> it is accurate to the second decimal place.
</p>
<p>Harter (1961) discusses appropriate values of <i>&alpha;</i> for various sample sizes 
<i>n</i> and values of <i>r</i>. <br />
</p>
<p><b>Approximation Based on Monte Carlo Simulation</b> (<code>method="mc"</code>) 
</p>
<p>When <code>method="mc"</code>, Monte Carlo simulation is used to estmate the expected value 
of the <i>r^{th}</i> order statistic.  That is, <i>N =</i> <code>nmc</code> trials are run in which, 
for each trial, a random sample of <i>n</i> standard normal observations is 
generated and the <i>r^{th}</i> order statistic is computed.  Then, the average value 
of this order statistic over all <i>N</i> trials is computed, along with a  
confidence interval for the expected value, assuming an approximately 
normal distribution for the mean of the order statistic (the confidence interval 
is computed by supplying the simulated values of the <i>r^{th}</i> order statistic 
to the function <code><a href="../../EnvStats/help/enorm.html">enorm</a></code>).
</p>
<p><b>NOTE:</b> This method has not been optimized for large sample sizes <i>n</i> 
(i.e., large values of the argument <code>n</code>) and/or a large number of 
Monte Carlo trials <i>N</i> (i.e., large values of the argument <code>nmc</code>) and 
may take a long time to execute in these cases.
</p>


<h3>Value</h3>

<p>For <code>evNormOrdStats</code>: a numeric vector of length <code>n</code> containing the 
expected values of all the order statistics for a random sample of <code>n</code> 
standard normal deviates.
</p>
<p>For <code>evNormOrdStatsScalar</code>: a numeric scalar containing the expected value 
of the <code>r</code>'th order statistic from a random sample of <code>n</code> standard 
normal deviates.  When <code>method="mc"</code>, the returned object also has a 
<code>cont.int</code> attribute that contains the 95
and a <code>nmc</code> attribute indicating the number of Monte Carlo trials run.  
</p>


<h3>Note</h3>

<p>The expected values of normal order statistics are used to construct normal 
quantile-quantile (Q-Q) plots (see <code><a href="../../EnvStats/help/qqPlot.html">qqPlot</a></code>) and to compute 
goodness-of-fit statistics (see <code><a href="../../EnvStats/help/gofTest.html">gofTest</a></code>).  Usually, however, 
approximations are used instead of exact values.  The functions 
<code>evNormOrdStats</code> and <br />
<code>evNormOrdStatsScalar</code> have been included mainly 
because <code>evNormOrdStatsScalar</code> is called by <code><a href="../../EnvStats/help/elnorm3.html">elnorm3</a></code> and 
<code><a href="../../EnvStats/help/predIntNparSimultaneousTestPower.html">predIntNparSimultaneousTestPower</a></code>.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Blom, G. (1958). <em>Statistical Estimates and Transformed Beta Variables</em>. 
John Wiley and Sons, New York.
</p>
<p>Harter, H. L. (1961). <em>Expected Values of Normal Order Statistics</em> 
<b>48</b>, 151&ndash;165.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1994). 
<em>Continuous Univariate Distributions, Volume 1</em>. 
Second Edition. John Wiley and Sons, New York, pp. 93&ndash;99.
</p>
<p>Royston, J.P. (1982).  Algorithm AS 177.  Expected Normal Order Statistics 
(Exact and Approximate).  <em>Applied Statistics</em> <b>31</b>, 161&ndash;165.
</p>


<h3>See Also</h3>

<p><a href="../../stats/html/Normal.html">Normal</a>, <code><a href="../../stats/html/ppoints.html">ppoints</a></code>, <code><a href="../../EnvStats/help/elnorm3.html">elnorm3</a></code>, 
<code><a href="../../EnvStats/help/predIntNparSimultaneousTestPower.html">predIntNparSimultaneousTestPower</a></code>, <code><a href="../../EnvStats/help/gofTest.html">gofTest</a></code>, 
<code><a href="../../EnvStats/help/qqPlot.html">qqPlot</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Compute the expected value of the minimum for a random sample of size 10 
  # from a standard normal distribution:

  # Based on method="royston"
  #--------------------------
  evNormOrdStatsScalar(r = 1, n = 10) 
  #[1] -1.538753


  # Based on method="blom"
  #-----------------------
  evNormOrdStatsScalar(r = 1, n = 10, method = "blom") 
  #[1] -1.546635


  # Based on method="mc" with 10,000 Monte Carlo trials
  #----------------------------------------------------
  evNormOrdStatsScalar(r = 1, n = 10, method = "mc", nmc = 10000) 
  #[1] -1.544318
  #attr(,"confint")
  #   95%LCL    95%UCL 
  #-1.555838 -1.532797 
  #attr(,"nmc")
  #[1] 10000

  #====================

  # Compute the expected values of all of the order statistics 
  # for a random sample of size 10 from a standard normal distribution
  # based on Royston's (1982) method:
  #--------------------------------------------------------------------

  evNormOrdStats(10) 
  #[1] -1.5387527 -1.0013570 -0.6560591 -0.3757647 -0.1226678
  #[6]  0.1226678  0.3757647  0.6560591  1.0013570  1.5387527


  # Compare the above with Blom (1958) scores:
  #-------------------------------------------

  evNormOrdStats(10, method = "blom") 
  #[1] -1.5466353 -1.0004905 -0.6554235 -0.3754618 -0.1225808
  #[6]  0.1225808  0.3754618  0.6554235  1.0004905  1.5466353
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
