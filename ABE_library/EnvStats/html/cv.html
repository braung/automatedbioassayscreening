<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Sample Coefficient of Variation.</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for cv {EnvStats}"><tr><td>cv {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Sample Coefficient of Variation.
</h2>

<h3>Description</h3>

<p>Compute the sample coefficient of variation.
</p>


<h3>Usage</h3>

<pre>
  cv(x, method = "moments", sd.method = "sqrt.unbiased", 
    l.moment.method = "unbiased", plot.pos.cons = c(a = 0.35, b = 0), 
    na.rm = FALSE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying what method to use to compute the sample coefficient 
of variation.  The possible values are <code>"moments"</code> 
(product moment ratio estimator; the default), or <code>"l.moments"</code> 
(L-moment ratio estimator).
</p>
</td></tr>
<tr valign="top"><td><code>sd.method</code></td>
<td>

<p>character string specifying what method to use to compute the sample standard 
deviation when <code>method="moments"</code>.  The possible values are 
<code>"sqrt.ubiased"</code> (the square root of the unbiased estimate of variance; 
the default), or <code>"moments"</code> (the method of moments estimator).
</p>
</td></tr>
<tr valign="top"><td><code>l.moment.method</code></td>
<td>

<p>character string specifying what method to use to compute the 
<i>L</i>-moments when <code>method="l.moments"</code>.  The possible values are 
<code>"ubiased"</code> (method based on the <i>U</i>-statistic; the default), or 
<code>"plotting.position"</code> (method based on the plotting position formula). 
</p>
</td></tr>
<tr valign="top"><td><code>plot.pos.cons</code></td>
<td>

<p>numeric vector of length 2 specifying the constants used in the formula for 
the plotting positions when <code>method="l.moments"</code> and <br />
<code>l.moment.method="plotting.position"</code>.  The default value is <br />
<code>plot.pos.cons=c(a=0.35, b=0)</code>.  If this vector has a names attribute 
with the value <code>c("a","b")</code> or <code>c("b","a")</code>, then the elements will 
be matched by name in the formula for computing the plotting positions.  
Otherwise, the first element is mapped to the name <code>"a"</code> and the second 
element to the name <code>"b"</code>. 
</p>
</td></tr>
<tr valign="top"><td><code>na.rm</code></td>
<td>

<p>logical scalar indicating whether to remove missing values from <code>x</code>.  
If <br />
<code>na.rm=FALSE</code> (the default) and <code>x</code> contains missing values, 
then a missing value (<code>NA</code>) is returned.  If <code>na.rm=TRUE</code>, 
missing values are removed from <code>x</code> prior to computing the coefficient 
of variation.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Let <i>\underline{x}</i> denote a random sample of <i>n</i> observations from 
some distribution with mean <i>&mu;</i> and standard deviation <i>&sigma;</i>.
</p>
<p><em>Product Moment Coefficient of Variation</em> (<code>method="moments"</code>) <br />
The coefficient of variation (sometimes denoted CV) of a distribution is 
defined as the ratio of the standard deviation to the mean. That is:
</p>
<p style="text-align: center;"><i>CV = \frac{&sigma;}{&mu;} \;\;\;\;\;\; (1)</i></p>

<p>The coefficient of variation measures how spread out the distribution is 
relative to the size of the mean.  It is usually used to characterize positive, 
right-skewed distributions such as the lognormal distribution.
</p>
<p>When <code>sd.method="sqrt.unbiased"</code>, the coefficient of variation is estimated 
using the sample mean and the square root of the unbaised estimator of variance:
</p>
<p style="text-align: center;"><i>\widehat{CV} = \frac{s}{\bar{x}} \;\;\;\;\;\; (2)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\;\;\; (3)</i></p>

<p style="text-align: center;"><i>s = [\frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2]^{1/2} \;\;\;\;\;\; (4)</i></p>

<p>Note that the estimator of standard deviation in equation (4) is not unbiased. 
</p>
<p>When <code>sd.method="moments"</code>, the coefficient of variation is estimated using 
the sample mean and the square root of the method of moments estimator of variance:
</p>
<p style="text-align: center;"><i>\widehat{CV} = \frac{s_m}{\bar{x}} \;\;\;\;\;\; (5)</i></p>

<p style="text-align: center;"><i>s = [\frac{1}{n} &sum;_{i=1}^n (x_i - \bar{x})^2]^{1/2} \;\;\;\;\;\; (6)</i></p>

<p><br />
</p>
<p><em>L-Moment Coefficient of Variation</em> (<code>method="l.moments"</code>) <br />
Hosking (1990) defines an <i>L</i>-moment analog of the 
coefficient of variation (denoted the <i>L</i>-CV) as:
</p>
<p style="text-align: center;"><i>&tau; = \frac{l_2}{l_1} \;\;\;\;\;\; (7)</i></p>

<p>that is, the second <i>L</i>-moment divided by the first <i>L</i>-moment.  
He shows that for a positive-valued random variable, the <i>L</i>-CV lies in the 
interval (0, 1).
</p>
<p>When <code>l.moment.method="unbiased"</code>, the <i>L</i>-CV is estimated by:
</p>
<p style="text-align: center;"><i>t = \frac{l_2}{l_1} \;\;\;\;\;\; (8)</i></p>

<p>that is, the unbiased estimator of the second <i>L</i>-moment divided by 
the unbiased estimator of the first <i>L</i>-moment.
</p>
<p>When <code>l.moment.method="plotting.position"</code>, the <i>L</i>-CV is estimated by:
</p>
<p style="text-align: center;"><i>\tilde{t} = \frac{\tilde{l_2}}{\tilde{l_1}} \;\;\;\;\;\; (9)</i></p>

<p>that is, the plotting-position estimator of the second <i>L</i>-moment divided by 
the plotting-position estimator of the first <i>L</i>-moment.
</p>
<p>See the help file for <code><a href="../../EnvStats/help/lMoment.html">lMoment</a></code> for more information on 
estimating <i>L</i>-moments.
</p>


<h3>Value</h3>

<p>A numeric scalar &ndash; the sample coefficient of variation.
</p>


<h3>Note</h3>

<p>Traditionally, the coefficient of variation has been estimated using 
product moment estimators.  Hosking (1990) introduced the idea of 
<i>L</i>-moments and the <i>L</i>-CV.  Vogel and Fennessey (1993) argue that 
<i>L</i>-moment ratios should replace product moment ratios because of their 
superior performance (they are nearly unbiased and better for discriminating 
between distributions).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). 
<em>Statistics for Environmental Engineers, Second Edition</em>. 
Lewis Publishers, Boca Raton, FL.
</p>
<p>Gilbert, R.O. (1987). <em>Statistical Methods for Environmental Pollution 
Monitoring</em>. Van Nostrand Reinhold, NY.
</p>
<p>Ott, W.R. (1995). <em>Environmental Statistics and Data Analysis</em>. 
Lewis Publishers, Boca Raton, FL.
</p>
<p>Taylor, J.K. (1990). <em>Statistical Techniques for Data Analysis</em>.  
Lewis Publishers, Boca Raton, FL.
</p>
<p>Vogel, R.M., and N.M. Fennessey. (1993).  <i>L</i> Moment Diagrams Should Replace 
Product Moment Diagrams.  <em>Water Resources Research</em> <b>29</b>(6), 1745&ndash;1752.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. Fifth Edition. 
Prentice-Hall, Upper Saddle River, NJ.
</p>


<h3>See Also</h3>

<p><a href="../../EnvStats/help/Summary+20Statistics.html">Summary Statistics</a>, <code><a href="../../EnvStats/help/summaryFull.html">summaryFull</a></code>, <code><a href="../../stats/html/cor.html">var</a></code>, 
<code><a href="../../stats/html/sd.html">sd</a></code>, <code><a href="../../EnvStats/help/skewness.html">skewness</a></code>, <code><a href="../../EnvStats/help/kurtosis.html">kurtosis</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a lognormal distribution with 
  # parameters mean=10 and cv=1, and estimate the coefficient of variation. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rlnormAlt(20, mean = 10, cv = 1) 

  cv(dat) 
  #[1] 0.5077981

  cv(dat, sd.method = "moments") 
  #[1] 0.4949403
 
  cv(dat, method = "l.moments") 
  #[1] 0.2804148

  #----------
  # Clean up
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
