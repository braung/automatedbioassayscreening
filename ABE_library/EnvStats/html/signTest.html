<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: One-Sample or Paired-Sample Sign Test on a Median</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for signTest {EnvStats}"><tr><td>signTest {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
One-Sample or Paired-Sample Sign Test on a Median
</h2>

<h3>Description</h3>

<p>Estimate the median, test the null hypothesis that the median is equal to a 
user-specified value based on the sign test, and create a confidence interval 
for the median.
</p>


<h3>Usage</h3>

<pre>
  signTest(x, y = NULL, alternative = "two.sided", mu = 0, paired = FALSE, 
    conf.level = 0.95)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>y</code></td>
<td>

<p>optional numeric vector of observations that are paired with the observations in 
<code>x</code>.  The length of <code>y</code> must be the same as the length of <code>x</code>.  
This argument is ignored if <code>paired=FALSE</code>, and must be supplied if 
<code>paired=TRUE</code>.  The default value is <code>y=NULL</code>.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>alternative</code></td>
<td>

<p>character string indicating the kind of alternative hypothesis.  The possible values 
are <code>"two.sided"</code> (the default), <code>"greater"</code>, and <code>"less"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>mu</code></td>
<td>

<p>numeric scalar indicating the hypothesized value of the median.  The default value is 
<code>mu=0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>paired</code></td>
<td>

<p>logical scalar indicating whether to perform a paired or one-sample sign test.  
The possible values are <code>paired=FALSE</code> (the default; indicates a one-sample 
sign test) and <code>paired=TRUE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>numeric scalar between 0 and 1 indicating the confidence level associated with the 
confidence interval for the population median.  The default value is <br />
<code>conf.level=0.95</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p><b>One-Sample Case</b> (<code>paired=FALSE</code>) <br />
Let <i>\underline{x} = x_1, x_2, &hellip;, x_n</i> be a vector of <i>n</i> 
independent observations from one or more distributions that all have the 
same median <i>&mu;</i>.
</p>
<p>Consider the test of the null hypothesis:
</p>
<p style="text-align: center;"><i>H_0: &mu; = &mu;_0 \;\;\;\;\;\; (1)</i></p>

<p>The three possible alternative hypotheses are the upper one-sided alternative 
(<code>alternative="greater"</code>)
</p>
<p style="text-align: center;"><i>H_a: &mu; &gt; &mu;_0 \;\;\;\;\;\; (2)</i></p>

<p>the lower one-sided alternative (<code>alternative="less"</code>)
</p>
<p style="text-align: center;"><i>H_a: &mu; &lt; &mu;_0 \;\;\;\;\;\; (3)</i></p>

<p>and the two-sided alternative (<code>alternative="two.sided"</code>)
</p>
<p style="text-align: center;"><i>H_a: &mu; \ne &mu;_0 \;\;\;\;\;\; (4)</i></p>

<p>To perform the test of the null hypothesis (1) versus any of the three alternatives 
(2)-(4), the sign test uses the test statistic <i>T</i> which is simply the number of 
observations that are greater than <i>&mu;_0</i> (Conover, 1980, p. 122; 
van Belle et al., 2004, p. 256; Hollander and Wolfe, 1999, p. 60; 
Lehmann, 1975, p. 120; Sheskin, 2011; Zar, 2010, p. 537).  Under the null 
hypothesis, the distribution of <i>T</i> is a 
<a href="../../stats/help/Binomial.html">binomial random variable</a> with 
parameters <code>size=</code><i>n</i> and <code>prob=0.5</code>.  Usually, however, cases for 
which the observations are equal to <i>&mu;_0</i> are discarded, so the distribution 
of <i>T</i> is taken to be binomial with parameters <code>size=</code><i>r</i> and 
<code>prob=0.5</code>, where <i>r</i> denotes the number of observations not equal to 
<i>&mu;_0</i>.  The sign test only requires that the observations are independent 
and that they all come from one or more distributions (not necessarily the same 
ones) that all have the same population median.
</p>
<p>For a two-sided alternative hypothesis (Equation (4)), the p-value is computed as:
</p>
<p style="text-align: center;"><i>p = Pr(X_{r,0.5} &le; r-m) + Pr(X_{r,0.5} &gt; m) \;\;\;\;\;\; (5)</i></p>

<p>where <i>X_{r,p}</i> denotes a <a href="../../stats/help/Binomial.html">binomial</a> random variable 
with parameters <code>size=</code><i>r</i> and <code>prob=</code><i>p</i>, and <i>m</i> 
is defined by:
</p>
<p style="text-align: center;"><i>m = max(T, r-T) \;\;\;\;\;\; (6)</i></p>

<p>For a one-sided lower alternative hypothesis (Equation (3)), the p-value is 
computed as:
</p>
<p style="text-align: center;"><i>p = Pr(X_{m,0.5} &le; T) \;\;\;\;\;\; (7)</i></p>

<p>and for a one-sided upper alternative hypothesis (Equation (2)), the p-value is 
computed as:
</p>
<p style="text-align: center;"><i>p = Pr(X_{m,0.5} &ge; T) \;\;\;\;\;\; (8)</i></p>

<p>It is obvious that the sign test is simply a special case of the 
<a href="../../stats/help/binom.test.html">binomial test</a> with <code>p=0.5</code>.
</p>
<p><em>Computing Confidence Intervals</em> <br />
Based on the relationship between hypothesis tests and confidence intervals, 
we can construct a confidence interval for the population median based on the 
sign test (e.g., Hollander and Wolfe, 1999, p. 72; Lehmann, 1975, p. 182).  
It turns out that this is equivalent to using the formulas for a nonparametric 
confidence intervals for the 0.5 quantile (see <code><a href="../../EnvStats/help/eqnpar.html">eqnpar</a></code>).
<br />
</p>
<p><b>Paired-Sample Case</b> (<code>paired=TRUE</code>) <br />
When the argument <code>paired=TRUE</code>, the arguments <code>x</code> and <code>y</code> are 
assumed to have the same length, and the <i>n</i> differences
<i>d_i = x_i - y_i, \;\; i = 1, 2, &hellip;, n</i> are assumed to be independent 
observations from distributions with the same median <i>&mu;</i>.  The sign test 
can then be applied to the differences.
</p>


<h3>Value</h3>

<p>A list of class <code>"htest"</code> containing the results of the hypothesis test.  
See the help file for <code><a href="../../EnvStats/help/htest.object.html">htest.object</a></code> for details.
</p>


<h3>Note</h3>

<p>A frequent question in environmental statistics is &ldquo;Is the concentration of 
chemical X greater than Y units?&rdquo;.  For example, in groundwater assessment 
(compliance) monitoring at hazardous and solid waste sites, the concentration of a 
chemical in the groundwater at a downgradient well must be compared to a 
groundwater protection standard (GWPS).  If the concentration is &ldquo;above&rdquo; the 
GWPS, then the site enters corrective action monitoring.  As another example, soil 
screening at a Superfund site involves comparing the concentration of a chemical in 
the soil with a pre-determined soil screening level (SSL).  If the concentration is 
&ldquo;above&rdquo; the SSL, then further investigation and possible remedial action is 
required.  Determining what it means for the chemical concentration to be 
&ldquo;above&rdquo; a GWPS or an SSL is a policy decision:  the average of the 
distribution of the chemical concentration must be above the GWPS or SSL, or the 
median must be above the GWPS or SSL, or the 95th percentile must be above the 
GWPS or SSL, or something else.  Often, the first interpretation is used.
</p>
<p>Hypothesis tests you can use to perform tests of location include:  
<a href="../../stats/help/t.test.html">Student's t-test</a>, 
<a href="../../EnvStats/help/oneSamplePermutationTest.html">Fisher's randomization test</a>, the 
<a href="../../stats/help/wilcox.test.html">Wilcoxon signed rank test</a>, 
<a href="../../EnvStats/help/chenTTest.html">Chen's modified t-test</a>, the 
sign test, and a test based on a bootstrap confidence interval.  For a discussion 
comparing the performance of these tests, see Millard and Neerchal (2001, pp.408-409).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Conover, W.J. (1980). <em>Practical Nonparametric Statistics</em>.  
Second Edition. John Wiley and Sons, New York, p.122
</p>
<p>Hollander, M., and D.A. Wolfe. (1999). <em>Nonparametric Statistical Methods</em>.  
Second Edition.  John Wiley and Sons, New York, p.60.
</p>
<p>Lehmann, E.L. (1975).  <em>Nonparametrics: Statistical Methods Based on Ranks</em>.  
Holden-Day, Oakland, CA, p.120.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001).  
<em>Environmental Statistics with S-PLUS</em>.  CRC Press, Boca Raton, FL, pp.404&ndash;406.
</p>
<p>Sheskin, D.J. (2011).  <em>Handbook of Parametric and Nonparametric 
Statistical Procedures</em>  Fifth Edition. CRC Press, Boca Raton, FL.
</p>
<p>van Belle, G., L.D. Fisher, Heagerty, P.J., and Lumley, T. (2004).  
<em>Biostatistics: A Methodology for the Health Sciences</em>  2nd Edition.  
John Wiley &amp; Sons, New York.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>.  Fifth Edition.  
Prentice-Hall, Upper Saddle River, NJ,
</p>


<h3>See Also</h3>

<p><code><a href="../../stats/html/wilcox.test.html">wilcox.test</a></code>, <a href="../../EnvStats/help/Hypothesis+20Tests.html">Hypothesis Tests</a>, <code><a href="../../EnvStats/help/eqnpar.html">eqnpar</a></code>, 
<code><a href="../../EnvStats/help/htest.object.html">htest.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 10 observations from a lognormal distribution with parameters 
  # meanlog=2 and sdlog=1.  The median of this distribution is e^2 (about 7.4). 
  # Test the null hypothesis that the true median is equal to 5 against the 
  # alternative that the true mean is greater than 5. 
  # (Note: the call to set.seed allows you to reproduce this example).

  set.seed(23) 
  dat &lt;- rlnorm(10, meanlog = 2, sdlog = 1) 
  signTest(dat, mu = 5) 

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 median = 5
  #
  #Alternative Hypothesis:          True median is not equal to 5
  #
  #Test Name:                       Sign test
  #
  #Estimated Parameter(s):          median = 19.21717
  #
  #Data:                            dat
  #
  #Test Statistic:                  # Obs &gt; median = 9
  #
  #P-value:                         0.02148438
  #
  #Confidence Interval for:         median
  #
  #Confidence Interval Method:      exact
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                93.45703%
  #
  #Confidence Limit Rank(s):        3 9 
  #
  #Confidence Interval:             LCL =  7.732538
  #                                 UCL = 35.722459

  # Clean up
  #---------
  rm(dat)

  #==========

  # The guidance document "Supplemental Guidance to RAGS: Calculating the 
  # Concentration Term" (USEPA, 1992d) contains an example of 15 observations 
  # of chromium concentrations (mg/kg) which are assumed to come from a 
  # lognormal distribution.  These data are stored in the vector 
  # EPA.92d.chromium.vec.  Here, we will use the sign test to test the null 
  # hypothesis that the median chromium concentration is less than or equal to 
  # 100 mg/kg vs. the alternative that it is greater than 100 mg/kg.  The 
  # estimated median is 110 mg/kg.  There are 8 out of 15 observations greater 
  # than 100 mg/kg, the p-value is equal to 0.5, and the lower 94% confidence 
  # limit is 41 mg/kg.

  signTest(EPA.92d.chromium.vec, mu = 100, alternative = "greater") 

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 median = 100
  #
  #Alternative Hypothesis:          True median is greater than 100
  #
  #Test Name:                       Sign test
  #
  #Estimated Parameter(s):          median = 110
  #
  #Data:                            EPA.92d.chromium.vec
  #
  #Test Statistic:                  # Obs &gt; median = 8
  #
  #P-value:                         0.5
  #
  #Confidence Interval for:         median
  #
  #Confidence Interval Method:      exact
  #
  #Confidence Interval Type:        lower
  #
  #Confidence Level:                94.07654%
  #
  #Confidence Limit Rank(s):        5 
  #
  #Confidence Interval:             LCL =  41
  #                                 UCL = Inf
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
