<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Coefficient of Skewness</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for skewness {EnvStats}"><tr><td>skewness {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Coefficient of Skewness
</h2>

<h3>Description</h3>

<p>Compute the sample coefficient of skewness.
</p>


<h3>Usage</h3>

<pre>
  skewness(x, na.rm = FALSE, method = "fisher", l.moment.method = "unbiased", 
    plot.pos.cons = c(a = 0.35, b = 0))
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.
</p>
</td></tr>
<tr valign="top"><td><code>na.rm</code></td>
<td>

<p>logical scalar indicating whether to remove missing values from <code>x</code>.  If <br />
<code>na.rm=FALSE</code> (the default) and <code>x</code> contains missing values, 
then a missing value (<code>NA</code>) is returned.  If <code>na.rm=TRUE</code>, 
missing values are removed from <code>x</code> prior to computing the coefficient 
of variation.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying what method to use to compute the sample coefficient 
of skewness.  The possible values are 
<code>"fisher"</code> (ratio of unbiased moment estimators; the default), 
<code>"moments"</code> (ratio of product moment estimators), or 
<code>"l.moments"</code> (ratio of <i>L</i>-moment estimators).
</p>
</td></tr>
<tr valign="top"><td><code>l.moment.method</code></td>
<td>

<p>character string specifying what method to use to compute the 
<i>L</i>-moments when <code>method="l.moments"</code>.  The possible values are 
<code>"ubiased"</code> (method based on the <i>U</i>-statistic; the default), or 
<code>"plotting.position"</code> (method based on the plotting position formula). 
</p>
</td></tr>
<tr valign="top"><td><code>plot.pos.cons</code></td>
<td>

<p>numeric vector of length 2 specifying the constants used in the formula for 
the plotting positions when <code>method="l.moments"</code> and <br />
<code>l.moment.method="plotting.position"</code>.  The default value is <br />
<code>plot.pos.cons=c(a=0.35, b=0)</code>.  If this vector has a names attribute 
with the value <code>c("a","b")</code> or <code>c("b","a")</code>, then the elements will 
be matched by name in the formula for computing the plotting positions.  
Otherwise, the first element is mapped to the name <code>"a"</code> and the second 
element to the name <code>"b"</code>. 
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Let <i>\underline{x}</i> denote a random sample of <i>n</i> observations from 
some distribution with mean <i>&mu;</i> and standard deviation <i>&sigma;</i>.
</p>
<p><em>Product Moment Coefficient of Skewness</em> (<code>method="moment"</code> or <code>method="fisher"</code>) <br />
The <b><em>coefficient of skewness</em></b> of a distribution is the third 
standardized moment about the mean:
</p>
<p style="text-align: center;"><i>&eta;_3 = &radic;{&beta;_1} = \frac{&mu;_3}{&sigma;^3} \;\;\;\;\;\; (1)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>&eta;_r = E[(\frac{X-&mu;}{&sigma;})^r] = \frac{1}{&sigma;^r} E[(X-&mu;)^r] = \frac{&mu;_r}{&sigma;^r} \;\;\;\;\;\; (2)</i></p>

<p>and
</p>
<p style="text-align: center;"><i>&mu;_r = E[(X-&mu;)^r] \;\;\;\;\;\; (3)</i></p>

<p>denotes the <i>r</i>'th moment about the mean (central moment).
That is, the coefficient of skewness is the third central moment divided by the 
cube of the standard deviation.  The coefficient of skewness is 0 for a symmetric 
distribution.  Distributions with positive skew have heavy right-hand tails, and 
distributions with negative skew have heavy left-hand tails.
</p>
<p>When <code>method="moment"</code>, the coefficient of skewness is estimated using the 
method of moments estimator for the third central moment and and the method of 
moments estimator for the variance:
</p>
<p style="text-align: center;"><i>\hat{&eta;}_3 = \frac{\hat{&mu;}_3}{&sigma;^3} = \frac{\frac{1}{n} &sum;_{i=1}^n (x_i - \bar{x})^3}{[\frac{1}{n} &sum;_{i=1}^n (x_i - \bar{x})^2]^{3/2}} \;\;\;\;\; (5)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>\hat{&sigma;}^2_m = s^2_m = \frac{1}{n} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\;\;\; (6)</i></p>

<p>This form of estimation should be used when resampling (bootstrap or jackknife).
</p>
<p>When <code>method="fisher"</code>, the coefficient of skewness is estimated using the 
unbiased estimator for the third central moment 
(Serfling, 1980, p.73; Chen, 1995, p.769) and the unbiased estimator for the 
variance.
</p>
<p style="text-align: center;"><i>\hat{&eta;}_3 = \frac{\frac{n}{(n-1)(n-2)} &sum;_{i=1}^n (x_i - \bar{x})^3}{s^3} \;\;\;\;\;\; (7)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>\hat{&sigma;}^2 = s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\;\;\; (8)</i></p>

<p>(Note that Serfling, 1980, p.73 contains a typographical error in the numerator for 
the unbiased estimator of the third central moment.) 
<br />
</p>
<p><em>L-Moment Coefficient of skewness</em> (<code>method="l.moments"</code>) <br />
Hosking (1990) defines the <i>L</i>-moment analog of the coefficient of skewness as:
</p>
<p style="text-align: center;"><i>&tau;_3 = \frac{&lambda;_3}{&lambda;_2} \;\;\;\;\;\; (9)</i></p>

<p>that is, the third <i>L</i>-moment divided by the second <i>L</i>-moment.  He shows 
that this quantity lies in the interval (-1, 1).
</p>
<p>When <code>l.moment.method="unbiased"</code>, the <i>L</i>-skewness is estimated by:
</p>
<p style="text-align: center;"><i>t_3 = \frac{l_3}{l_2} \;\;\;\;\;\; (10)</i></p>

<p>that is, the unbiased estimator of the third <i>L</i>-moment divided by the 
unbiased estimator of the second <i>L</i>-moment.
</p>
<p>When <code>l.moment.method="plotting.position"</code>, the <i>L</i>-skewness is estimated by:
</p>
<p style="text-align: center;"><i>\tilde{&tau;}_3 = \frac{\tilde{&lambda;}_3}{\tilde{&lambda;}_2} \;\;\;\;\;\; (11)</i></p>

<p>that is, the plotting-position estimator of the third <i>L</i>-moment divided by the 
plotting-position estimator of the second <i>L</i>-moment.
</p>
<p>See the help file for <code><a href="../../EnvStats/help/lMoment.html">lMoment</a></code> for more information on 
estimating <i>L</i>-moments.
</p>


<h3>Value</h3>

<p>A numeric scalar &ndash; the sample coefficient of skewness.
</p>


<h3>Note</h3>

<p>Traditionally, the coefficient of skewness has been estimated using product 
moment estimators.  Sometimes an estimate of skewness is used in a 
goodness-of-fit test for normality (e.g., set <br />
<code>test="skew"</code> in the call to <code><a href="../../EnvStats/help/gofTest.html">gofTest</a></code>).
</p>
<p>Hosking (1990) introduced the idea of <i>L</i>-moments and <i>L</i>-skewness.  
</p>
<p>Vogel and Fennessey (1993) argue that <i>L</i>-moment ratios should replace 
product moment ratios because of their superior performance (they are nearly 
unbiased and better for discriminating between distributions).  
They compare product moment diagrams with <i>L</i>-moment diagrams.
</p>
<p>Hosking and Wallis (1995) recommend using unbiased estimators of <i>L</i>-moments 
(vs. plotting-position estimators) for almost all applications.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). 
<em>Statistics for Environmental Engineers, Second Edition</em>. 
Lewis Publishers, Boca Raton, FL.
</p>
<p>Chen, L. (1995).  Testing the Mean of Skewed Distributions.  
<em>Journal of the American Statistical Association</em> <b>90</b>(430), 
767&ndash;772.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). 
<em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York, NY.
</p>
<p>Ott, W.R. (1995). <em>Environmental Statistics and Data Analysis</em>. 
Lewis Publishers, Boca Raton, FL.
</p>
<p>Serfling, R.J. (1980).  <em>Approximation Theorems of Mathematical Statistics</em>.  
John Wiley and Sons, New York, p.73.
</p>
<p>Taylor, J.K. (1990). <em>Statistical Techniques for Data Analysis</em>.  
Lewis Publishers, Boca Raton, FL.
</p>
<p>Vogel, R.M., and N.M. Fennessey. (1993).  <i>L</i> Moment Diagrams Should Replace 
Product Moment Diagrams.  <em>Water Resources Research</em> <b>29</b>(6), 1745&ndash;1752.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. Fifth Edition. 
Prentice-Hall, Upper Saddle River, NJ.
</p>


<h3>See Also</h3>

<p><code><a href="../../stats/html/cor.html">var</a></code>, <code><a href="../../stats/html/sd.html">sd</a></code>, <code><a href="../../EnvStats/help/cv.html">cv</a></code>, 
<code><a href="../../EnvStats/help/kurtosis.html">kurtosis</a></code>, <code><a href="../../EnvStats/help/summaryFull.html">summaryFull</a></code>, 
<a href="../../EnvStats/help/Summary+20Statistics.html">Summary Statistics</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a lognormal distribution with parameters 
  # mean=10 and cv=1, and estimate the coefficient of skewness. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 

  dat &lt;- rlnormAlt(20, mean = 10, cv = 1) 

  skewness(dat) 
  #[1] 0.9876632
 
  skewness(dat, method = "moment") 
  #[1] 0.9119889
 
  skewness(dat, meth = "l.moment") 
  #[1] 0.2656674

  #----------
  # Clean up
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
