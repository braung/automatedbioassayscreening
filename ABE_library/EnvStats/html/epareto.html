<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Parameters of a Pareto Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for epareto {EnvStats}"><tr><td>epareto {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Parameters of a Pareto Distribution
</h2>

<h3>Description</h3>

<p>Estimate the location and shape parameters of a <a href="../../EnvStats/help/Pareto.html">Pareto distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  epareto(x, method = "mle", plot.pos.con = 0.375)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  Possible values are 
<code>"mle"</code> (maximum likelihood; the default), and <code>"lse"</code> (least-squares).  
See the DETAILS section for more information on these estimation methods. 
</p>
</td></tr>
<tr valign="top"><td><code>plot.pos.con</code></td>
<td>

<p>numeric scalar between 0 and 1 containing the value of the plotting position 
constant used to construct the values of the empirical cdf.  The default value is 
<code>plot.pos.con=0.375</code>.  This argument is used only when <code>method="lse"</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of 
<i>n</i> observations from a <a href="../../EnvStats/help/Pareto.html">Pareto distribution</a> with 
parameters <code>location=</code><i>&eta;</i> and <code>shape=</code><i>&theta;</i>.
</p>
<p><em>Maximum Likelihood Estimatation</em> (<code>method="mle"</code>) <br />
The maximum likelihood estimators (mle's) of <i>&eta;</i> and <i>&theta;</i> are 
given by (Evans et al., 1993; p.122; Johnson et al., 1994, p.581):
</p>
<p style="text-align: center;"><i>\hat{&eta;}_{mle} = x_{(1)} \;\;\;\; (1)</i></p>

<p style="text-align: center;"><i>\hat{&theta;}_{mle} = n [&sum;_{i=1}^n log(\frac{x_i}{\hat{&eta;}_{mle}}) ]^{-1} \;\;\;\; (2)</i></p>

<p>where <i>x_(1)</i> denotes the first order statistic (i.e., the minimum value).
<br />
</p>
<p><em>Least-Squares Estimation</em> (<code>method="lse"</code>) <br />
The least-squares estimators (lse's) of <i>&eta;</i> and <i>&theta;</i> are derived as 
follows.  Let <i>X</i> denote a <a href="../../EnvStats/help/Pareto.html">Pareto</a> random variable with parameters 
<code>location=</code><i>&eta;</i> and <code>shape=</code><i>&theta;</i>.  It can be shown that
</p>
<p style="text-align: center;"><i>log[1 - F(x)] = &theta; log(&eta;) - &theta; log(x) \;\;\;\; (3)</i></p>

<p>where <i>F</i> denotes the cumulative distribution function of <i>X</i>.  Set
</p>
<p style="text-align: center;"><i>y_i = log[1 - \hat{F}(x_i)] \;\;\;\; (4)</i></p>

<p style="text-align: center;"><i>z_i = log(x_i) \;\;\;\; (5)</i></p>

<p>where <i>\hat{F}(x)</i> denotes the empirical cumulative distribution function 
evaluated at <i>x</i>.  The least-squares estimates of <i>&eta;</i> and <i>&theta;</i> 
are obtained by solving the regression equation
</p>
<p style="text-align: center;"><i>y_i = &beta;_{0} + &beta;_{1} z_i \;\;\;\; (6)</i></p>

<p>and setting
</p>
<p style="text-align: center;"><i>\hat{&theta;}_{lse} = -\hat{&beta;}_{1} \;\;\;\; (7)</i></p>

<p style="text-align: center;"><i>\hat{&eta;}_{lse} = exp(\frac{\hat{&beta;}_0}{\hat{&theta;}_{lse}}) \;\;\;\; (8)</i></p>

<p>(Johnson et al., 1994, p.580).
</p>


<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other information. <br />
See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>


<h3>Note</h3>

<p>The Pareto distribution is named after Vilfredo Pareto (1848-1923), a professor 
of economics.  It is derived from Pareto's law, which states that the number of 
persons <i>N</i> having income <i>&ge; x</i> is given by:
</p>
<p style="text-align: center;"><i>N = A x^{-&theta;}</i></p>

<p>where <i>&theta;</i> denotes Pareto's constant and is the shape parameter for the 
probability distribution.
</p>
<p>The Pareto distribution takes values on the positive real line.  All values must be 
larger than the &ldquo;location&rdquo; parameter <i>&eta;</i>, which is really a threshold 
parameter.  There are three kinds of Pareto distributions.  The one described here 
is the Pareto distribution of the first kind.  Stable Pareto distributions have 
<i>0 &lt; &theta; &lt; 2</i>.  Note that the <i>r</i>'th moment only exists if 
<i>r &lt; &theta;</i>.
</p>
<p>The Pareto distribution is related to the 
<a href="../../stats/help/Exponential.html">exponential distribution</a> and 
<a href="../../stats/help/Logistic.html">logistic distribution</a> as follows.  
Let <i>X</i> denote a Pareto random variable with <code>location=</code><i>&eta;</i> and 
<code>shape=</code><i>&theta;</i>.  Then <i>log(X/&eta;)</i> has an exponential distribution 
with parameter <code>rate=</code><i>&theta;</i>, and <i>-log\{ [(X/&eta;)^&theta;] - 1 \}</i> 
has a logistic distribution with parameters <code>location=</code><i>0</i> and 
<code>scale=</code><i>1</i>.
</p>
<p>The Pareto distribution has a very long right-hand tail.  It is often applied in 
the study of socioeconomic data, including the distribution of income, firm size, 
population, and stock price fluctuations.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1994). 
<em>Continuous Univariate Distributions, Volume 1</em>. 
Second Edition. John Wiley and Sons, New York.
</p>


<h3>See Also</h3>

<p><a href="../../EnvStats/help/Pareto.html">Pareto</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 30 observations from a Pareto distribution with parameters 
  # location=1 and shape=1 then estimate the parameters. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rpareto(30, location = 1, shape = 1) 
  epareto(dat) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Pareto
  #
  #Estimated Parameter(s):          location = 1.009046
  #                                 shape    = 1.079850
  #
  #Estimation Method:               mle
  #
  #Data:                            dat
  #
  #Sample Size:                     30

  #----------

  # Compare the results of using the least-squares estimators:

  epareto(dat, method="lse")$parameters 
  #location    shape 
  #1.085924 1.144180

  #----------

  # Clean up
  #---------

  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
