<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Quantiles of a Weibull Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for eqweibull {EnvStats}"><tr><td>eqweibull {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Quantiles of a Weibull Distribution
</h2>

<h3>Description</h3>

<p>Estimate quantiles of a <a href="../../stats/help/Weibull.html">Weibull distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  eqweibull(x, p = 0.5, method = "mle", digits = 0)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>a numeric vector of observations, or an object resulting from a call to an 
estimating function that assumes a Weibull distribution 
(e.g., <code><a href="../../EnvStats/help/eweibull.html">eweibull</a></code>).  If <code>x</code> is a numeric vector, 
missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>numeric vector of probabilities for which quantiles will be estimated.  
All values of <code>p</code> must be between 0 and 1.  The default value is <code>p=0.5</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimating the distribution parameters.  
Possible values are 
<code>"mle"</code> (maximum likelihood; the default), <code>"mme"</code> (methods of moments), 
and <code>"mmue"</code> (method of moments based on the unbiased estimator of variance).  
See the DETAILS section of the help file for <code><a href="../../EnvStats/help/eweibull.html">eweibull</a></code> for more 
information.
</p>
</td></tr>
<tr valign="top"><td><code>digits</code></td>
<td>

<p>an integer indicating the number of decimal places to round to when printing out 
the value of <code>100*p</code>. The default value is <code>digits=0</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The function <code>eqweibull</code> returns estimated quantiles as well as 
estimates of the shape and scale parameters.  
</p>
<p>Quantiles are estimated by 1) estimating the shape and scale parameters by 
calling <code><a href="../../EnvStats/help/eweibull.html">eweibull</a></code>, and then 2) calling the function 
<code><a href="../../stats/help/Weibull.html">qweibull</a></code> and using the estimated values for 
shape and scale.
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, <code>eqweibull</code> returns a 
list of class <code>"estimate"</code> containing the estimated quantile(s) and other 
information. See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function, <code>eqweibull</code> 
returns a list whose class is the same as <code>x</code>.  The list 
contains the same components as <code>x</code>, as well as components called 
<code>quantiles</code> and <code>quantile.method</code>.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/help/Weibull.html">Weibull distribution</a> is named after the Swedish physicist 
Waloddi Weibull, who used this distribution to model breaking strengths of 
materials.  The Weibull distribution has been extensively applied in the fields 
of reliability and quality control.
</p>
<p>The <a href="../../stats/help/Exponential.html">exponential distribution</a> is a special case of the 
Weibull distribution: a Weibull random variable with parameters <code>shape=</code><i>1</i> 
and <code>scale=</code><i>&beta;</i> is equivalent to an exponential random variable with 
parameter <code>rate=</code><i>1/&beta;</i>.
</p>
<p>The Weibull distribution is related to the 
<a href="../../EnvStats/help/EVD.html">Type I extreme value (Gumbel) distribution</a> as follows: 
if <i>X</i> is a random variable from a Weibull distribution with parameters 
<code>shape=</code><i>&alpha;</i> and <code>scale=</code><i>&beta;</i>, then 
</p>
<p style="text-align: center;"><i>Y = -log(X) \;\;\;\; (10)</i></p>
 
<p>is a random variable from an extreme value distribution with parameters 
<code>location=</code><i>-log(&beta;)</i> and <code>scale=</code><i>1/&alpha;</i>.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1994). 
<em>Continuous Univariate Distributions, Volume 1</em>. 
Second Edition. John Wiley and Sons, New York.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/eweibull.html">eweibull</a></code>, <a href="../../stats/help/Weibull.html">Weibull</a>, <a href="../../stats/help/Exponential.html">Exponential</a>, 
<a href="../../EnvStats/help/EVD.html">EVD</a>, <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a Weibull distribution with parameters 
  # shape=2 and scale=3, then estimate the parameters via maximum likelihood,
  # and estimate the 90'th percentile. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- rweibull(20, shape = 2, scale = 3) 
  eqweibull(dat, p = 0.9) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Weibull
  #
  #Estimated Parameter(s):          shape = 2.673098
  #                                 scale = 3.047762
  #
  #Estimation Method:               mle
  #
  #Estimated Quantile(s):           90'th %ile = 4.163755
  #
  #Quantile Estimation Method:      Quantile(s) Based on
  #                                 mle Estimators
  #
  #Data:                            dat
  #
  #Sample Size:                     20

  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
