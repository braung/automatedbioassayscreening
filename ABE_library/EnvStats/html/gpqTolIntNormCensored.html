<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Generalized Pivotal Quantity for Tolerance Interval for a...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for gpqTolIntNormCensored {EnvStats}"><tr><td>gpqTolIntNormCensored {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Generalized Pivotal Quantity for Tolerance Interval for a Normal Distribution Based on Censored Data
</h2>

<h3>Description</h3>

<p>Generate a generalized pivotal quantity (GPQ) for a tolerance interval for a Normal 
distribution based on singly or multiply censored data.
</p>


<h3>Usage</h3>

<pre>
  gpqTolIntNormSinglyCensored(n, n.cen, p, probs, nmc, method = "mle", 
    censoring.side = "left", seed = NULL, names = TRUE)

  gpqTolIntNormMultiplyCensored(n, cen.index, p, probs, nmc, method = "mle", 
    censoring.side = "left", seed = NULL, names = TRUE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n</code></td>
<td>

<p>positive integer <i>&ge; 3</i> indicating the sample size.
</p>
</td></tr>
<tr valign="top"><td><code>n.cen</code></td>
<td>

<p>for the case of singly censored data, a positive integer indicating the number of 
censored observations.  The value of <code>n.cen</code> must be between <code>1</code> and 
<code>n-2</code>, inclusive.
</p>
</td></tr>
<tr valign="top"><td><code>cen.index</code></td>
<td>

<p>for the case of multiply censored data, a sorted vector of unique integers indicating the 
indices of the censored observations when the observations are &ldquo;ordered&rdquo;.  
The length of <code>cen.index</code> must be between <code>1</code> and <code>n-2</code>, inclusive, and 
the values of <code>cen.index</code> must be between <code>1</code> and <code>n</code>.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>numeric scalar strictly greater than 0 and strictly less than 1 indicating the quantile 
for which to generate the GPQ(s) (i.e., the coverage associated with a one-sided 
tolerance interval).
</p>
</td></tr>
<tr valign="top"><td><code>probs</code></td>
<td>

<p>numeric vector of values between 0 and 1 indicating the confidence level(s) associated 
with the GPQ(s).
</p>
</td></tr>
<tr valign="top"><td><code>nmc</code></td>
<td>

<p>positive integer <i>&ge; 10</i> indicating the number of Monte Carlo trials to run in order 
to compute the GPQ(s). 
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string indicating the method to use for parameter estimation.  <br />
<br />
For singly censored data, possible values are <code>"mle"</code> (the default), <code>"bcmle"</code>, 
<code>"qq.reg"</code>, <code>"qq.reg.w.cen.level"</code>, <code>"impute.w.qq.reg"</code>, <br />
<code>"impute.w.qq.reg.w.cen.level"</code>, <code>"impute.w.mle"</code>, <br />
<code>"iterative.impute.w.qq.reg"</code>, 
<code>"m.est"</code>, and <code>"half.cen.level"</code>.  See the help file for <code><a href="../../EnvStats/help/enormCensored.html">enormCensored</a></code> 
for details. <br />
<br />
For multiply censored data, possible values are <code>"mle"</code> (the default), <code>"qq.reg"</code>, 
<code>"impute.w.qq.reg"</code>, and <code>"half.cen.level"</code>.  See the help file for <br />
<code><a href="../../EnvStats/help/enormCensored.html">enormCensored</a></code> for details.
</p>
</td></tr>
<tr valign="top"><td><code>censoring.side</code></td>
<td>

<p>character string indicating on which side the censoring occurs.  The possible values are 
<code>"left"</code> (the default) and <code>"right"</code>. 
</p>
</td></tr>
<tr valign="top"><td><code>seed</code></td>
<td>

<p>positive integer to pass to the function <code><a href="../../base/html/Random.html">set.seed</a></code>.  This argument is 
ignored if <code>seed=NULL</code> (the default).  Using the <code>seed</code> argument lets you 
reproduce the exact same result if all other arguments stay the same.
</p>
</td></tr>
<tr valign="top"><td><code>names</code></td>
<td>

<p>a logical scalar passed to <code><a href="../../stats/html/quantile.html">quantile</a></code> indicating whether to add a 
names attribute to the resulting GPQ(s).  The default value is <code>names=TRUE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The function <code>gpqTolIntNormSinglyCensored</code> generates GPQs as described in Algorithm 12.1 
of Krishnamoorthy and Mathew (2009, p. 329).  The function 
<code>gpqTolIntNormMultiplyCensored</code> is an extension of this idea to multiply censored data.  
These functions are called by <br />
<code><a href="../../EnvStats/help/tolIntNormCensored.html">tolIntNormCensored</a></code> when <code>ti.method="gpq"</code>, 
and also by <code><a href="../../EnvStats/help/eqnormCensored.html">eqnormCensored</a></code> when <code>ci=TRUE</code> and <code>ci.method="gpq"</code>.  See 
the help files for these functions for an explanation of GPQs.
</p>
<p>Note that technically these are only GPQs if the data are Type II censored.  However, 
Krishnamoorthy and Mathew (2009, p. 328) state that in the case of Type I censored data these 
quantities should approximate the true GPQs and the results appear to be satisfactory, even 
for small sample sizes.
</p>
<p>The functions <code>gpqTolIntNormSinglyCensored</code> and <code>gpqTolIntNormMultiplyCensored</code> are 
computationally intensive and provided to the user to allow you to create your own tables.
</p>


<h3>Value</h3>

<p>a numeric vector containing the GPQ(s).
</p>


<h3>Note</h3>

<p>Tolerance intervals have long been applied to quality control and 
life testing problems (Hahn, 1970b,c; Hahn and Meeker, 1991; Krishnamoorthy and Mathew, 2009).  
References that discuss tolerance intervals in the context of environmental monitoring include:  
Berthouex and Brown (2002, Chapter 21), Gibbons et al. (2009), 
Millard and Neerchal (2001, Chapter 6), Singh et al. (2010b), and USEPA (2009).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Krishnamoorthy K., and T. Mathew. (2009). 
<em>Statistical Tolerance Regions: Theory, Applications, and Computation</em>. 
John Wiley and Sons, Hoboken.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/tolIntNormCensored.html">tolIntNormCensored</a></code>, <code><a href="../../EnvStats/help/eqnormCensored.html">eqnormCensored</a></code>,  
<code><a href="../../EnvStats/help/enormCensored.html">enormCensored</a></code>, <code><a href="../../EnvStats/help/estimateCensored.object.html">estimateCensored.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Reproduce the entries for n=10 observations with n.cen=1 in Table 12.2 
  # of Krishnamoorthy and Mathew (2009, p.331).
  #
  # (Note: passing a value for the argument "seed" simply allows you to 
  # reproduce this example.)  
  #
  # NOTE:  Here to save computing time for the sake of example, we will specify 
  #        just 100 Monte Carlos, whereas Krishnamoorthy and Mathew (2009) 
  #        suggest *10,000* Monte Carlos.

  gpqTolIntNormSinglyCensored(n = 10, n.cen = 1, p = 0.05, probs = 0.05, 
    nmc = 100, seed = 529)
  #       5% 
  #-3.483403


  gpqTolIntNormSinglyCensored(n = 10, n.cen = 1, p = 0.1, probs = 0.05, 
    nmc = 100, seed = 497)
  #      5% 
  #-2.66705


  gpqTolIntNormSinglyCensored(n = 10, n.cen = 1, p = 0.9, probs = 0.95, 
    nmc = 100, seed = 623)
  #     95% 
  #2.478654

  gpqTolIntNormSinglyCensored(n = 10, n.cen = 1, p = 0.95, probs = 0.95, 
    nmc = 100, seed = 623)
  #     95% 
  #3.108452

  #==========

  # Example of using gpqTolIntNormMultiplyCensored
  #-----------------------------------------------

  # Consider the following set of multiply left-censored data:
  dat &lt;- 12:16
  censored &lt;- c(TRUE, FALSE, TRUE, FALSE, FALSE)

  # Since the data are "ordered" we can identify the indices of the 
  # censored observations in the ordered data as follow:

  cen.index &lt;- (1:length(dat))[censored]
  cen.index
  #[1] 1 3

  # Now we can generate a GPQ using gpqTolIntNormMultiplyCensored.
  # Here we'll generate a GPQ corresponding to an upper tolerance 
  # interval with coverage 90% with 95% confidence for 
  # left-censored data.
  # NOTE:  Here to save computing time for the sake of example, we will specify 
  #        just 100 Monte Carlos, whereas Krishnamoorthy and Mathew (2009) 
  #        suggest *10,000* Monte Carlos.

  gpqTolIntNormMultiplyCensored(n = 5, cen.index = cen.index, p = 0.9, 
    probs = 0.95, nmc = 100, seed = 237)
  #     95% 
  #3.952052

  #==========

  # Clean up
  #---------
  rm(dat, censored, cen.index)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
