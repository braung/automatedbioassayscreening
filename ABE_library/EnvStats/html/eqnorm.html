<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Quantiles of a Normal Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for eqnorm {EnvStats}"><tr><td>eqnorm {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Quantiles of a Normal Distribution
</h2>

<h3>Description</h3>

<p>Estimate quantiles of a <a href="../../stats/help/Normal.html">normal distribution</a>, and optionally construct a 
confidence interval for a quantile.
</p>


<h3>Usage</h3>

<pre>
  eqnorm(x, p = 0.5, method = "qmle", ci = FALSE, 
    ci.method = "exact", ci.type = "two-sided", conf.level = 0.95, 
    digits = 0, warn = TRUE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>a numeric vector of observations, or an object resulting from a call to an 
estimating function that assumes a normal (Gaussian) distribution 
(i.e., <code><a href="../../EnvStats/help/enorm.html">enorm</a></code>, <code><a href="../../EnvStats/help/enormCensored.html">enormCensored</a></code>). 
If <code>x</code> is a numeric vector, 
missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>numeric vector of probabilities for which quantiles will be estimated.  
All values of <code>p</code> must be between 0 and 1.  When <code>ci=TRUE</code>, <code>p</code> 
must be a scalar. The default value is <code>p=0.5</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string indicating what method to use to estimate the quantile(s).  
Currently the only possible value is <code>method="qmle"</code> (quasi maximum likelihood).  
See the DETAILS section for more information.
</p>
</td></tr>
<tr valign="top"><td><code>ci</code></td>
<td>

<p>logical scalar indicating whether to compute a confidence interval for the quantile.  
The default value is <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.method</code></td>
<td>

<p>character string indicating what method to use to construct the confidence interval 
for the quantile.  The possible values are <code>"exact"</code> (exact method; the default) 
and <code>"normal.approx"</code> (normal approximation).  See the DETAILS section for more 
information.
</p>
</td></tr>
<tr valign="top"><td><code>ci.type</code></td>
<td>

<p>character string indicating what kind of confidence interval for the quantile to compute.  
The possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the confidence interval.  
The default value is <code>conf.level=0.95</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>digits</code></td>
<td>

<p>an integer indicating the number of decimal places to round to when printing out 
the value of <code>100*p</code>. The default value is <code>digits=0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>warn</code></td>
<td>

<p>logical scalar indicating whether to warn in the case when <code>ci=TRUE</code>, <br />
<code>ci.method="exact"</code>, and the supplied object <code>x</code> is of class 
<code>"estimate"</code> but did not use <code>method="mvue"</code> for estimation.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Quantiles are estimated by 1) estimating the mean and standard deviation parameters by 
calling <code><a href="../../EnvStats/help/enorm.html">enorm</a></code> with <code>method="mvue"</code>, and then 
2) calling the function <code><a href="../../stats/html/Normal.html">qnorm</a></code> and using the estimated values 
for mean and standard deviation.  This estimator of the <i>p</i>'th quantile is 
sometimes called the quasi-maximum likelihood estimator (qmle; Cohn et al., 1989) 
because if the maximum likelihood estimator of standard deviation were used 
in place of the minimum variaince unbiased one, then this estimator of the quantile 
would be the mle of the <i>p</i>'th quantile.
</p>
<p>When <code>ci=TRUE</code> and <code>ci.method="exact"</code>, the confidence interval for a 
quantile is computed by using the relationship between a confidence interval for 
a quantile and a tolerance interval.  Specifically, it can be shown 
(e.g., Conover, 1980, pp.119-121) that an upper confidence interval for the 
<i>p</i>'th quantile with confidence level <i>100(1-&alpha;)\%</i> is equivalent to 
an upper <i>&beta;</i>-content tolerance interval with coverage <i>100p\%</i> and 
confidence level <i>100(1-&alpha;)\%</i>.  Also, a lower confidence interval for 
the <i>p</i>'th quantile with confidence level <i>100(1-&alpha;)\%</i> is equivalent 
to a lower <i>&beta;</i>-content tolerance interval with coverage <i>100(1-p)\%</i> and 
confidence level <i>100(1-&alpha;)\%</i>.  See the help file for <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code> 
for information on tolerance intervals for a normal distribution.
</p>
<p>When <code>ci=TRUE</code> and <code>ci.method="normal.approx"</code>, the confidence interval for a 
quantile is computed by assuming the estimated quantile has an approximately normal 
distribution and using the asymptotic variance to construct the confidence interval 
(see Stedinger, 1983; Stedinger et al., 1993).
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, <code>eqnorm</code> returns a list of class 
<code>"estimate"</code> containing the estimated quantile(s) and other information.  
See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function, <code>eqnorm</code> 
returns a list whose class is the same as <code>x</code>.  The list contains the same 
components as <code>x</code>, as well as components called <code>quantiles</code> and 
<code>quantile.method</code>.  In addition, if <code>ci=TRUE</code>, the returned list 
contains a component called <code>interval</code> containing the confidence interval 
information. If <code>x</code> already has a component called <code>interval</code>, this 
component is replaced with the confidence interval information.
</p>


<h3>Note</h3>

<p>Percentiles are sometimes used in environmental standards and regulations.  
For example, Berthouex and Brown (2002, p.71) note that England has water 
quality limits based on the 90th and 95th percentiles of monitoring data not 
exceeding specified levels.  They also note that the U.S. EPA has specifications 
for air quality monitoring, aquatic standards on toxic chemicals, and maximum 
daily limits for industrial effluents that are all based on percentiles.  Given 
the importance of these quantities, it is essential to characterize the amount 
of uncertainty associated with the estimates of these quantities.  This is done 
with confidence intervals.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). <em>Statistics for Environmental Engineers</em>. 
Lewis Publishers, Boca Raton.
</p>
<p>Conover, W.J. (1980). <em>Practical Nonparametric Statistics</em>. Second Edition. 
John Wiley and Sons, New York.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009). 
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.  
John Wiley &amp; Sons, Hoboken.
</p>
<p>Gilbert, R.O. (1987). <em>Statistical Methods for Environmental Pollution Monitoring</em>. 
Van Nostrand Reinhold, New York, NY, pp.132-136.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). <em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York, NY, pp.88-90.
</p>
<p>Johnson, N.L., and B.L. Welch. (1940). Applications of the Non-Central t-Distribution. 
<em>Biometrika</em> <b>31</b>, 362-389.
</p>
<p>Millard, S.P., and Neerchal, N.K. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, Florida.
</p>
<p>Owen, D.B. (1962). <em>Handbook of Statistical Tables</em>. Addison-Wesley, Reading, MA.
</p>
<p>Stedinger, J. (1983). Confidence Intervals for Design Events. 
<em>Journal of Hydraulic Engineering</em> <b>109</b>(1), 13-27.
</p>
<p>Stedinger, J.R., R.M. Vogel, and E. Foufoula-Georgiou. (1993). 
Frequency Analysis of Extreme Events. In: Maidment, D.R., ed. <em>Handbook of Hydrology</em>. 
McGraw-Hill, New York, Chapter 18, pp.29-30.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2010).  <em>Errata Sheet - March 2009 Unified Guidance</em>.
EPA 530/R-09-007a, August 9, 2010.  Office of Resource Conservation and Recovery, Program Information and Implementation Division.
U.S. Environmental Protection Agency, Washington, D.C. 
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/enorm.html">enorm</a></code>, <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code>, <code><a href="../../stats/html/Normal.html">Normal</a></code>,  
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a normal distribution with 
  # parameters mean=10 and sd=2, then estimate the 90th 
  # percentile and create a one-sided upper 95% confidence interval 
  # for that percentile. 
  # (Note: the call to set.seed simply allows you to reproduce this 
  # example.)

  set.seed(47) 
  dat &lt;- rnorm(20, mean = 10, sd = 2) 
  eqnorm(dat, p = 0.9, ci = TRUE, ci.type = "upper")

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Normal
  #
  #Estimated Parameter(s):          mean = 9.792856
  #                                 sd   = 1.821286
  #
  #Estimation Method:               mvue
  #
  #Estimated Quantile(s):           90'th %ile = 12.12693
  #
  #Quantile Estimation Method:      qmle
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Confidence Interval for:         90'th %ile
  #
  #Confidence Interval Method:      Exact
  #
  #Confidence Interval Type:        upper
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL =     -Inf
  #                                 UCL = 13.30064

  #----------
  # Compare these results with the true 90'th percentile:

  qnorm(p = 0.9, mean = 10, sd = 2)
  #[1] 12.56310

  #----------

  # Clean up
  rm(dat)

  #==========

  # Example 21-4 of USEPA (2009, p. 21-13) shows how to construct a 
  # 99% lower confidence limit for the 95th percentile using chrysene 
  # data and assuming a lognormal distribution.  The data for this 
  # example are stored in EPA.09.Ex.21.1.aldicarb.df.

  # The facility permit has established an ACL of 30 ppb that should not 
  # be exceeded more than 5% of the time.  Thus, if the lower confidence limit 
  # for the 95th percentile is greater than 30 ppb, the well is deemed to be 
  # out of compliance.

  # Look at the data
  #-----------------

  head(EPA.09.Ex.21.1.aldicarb.df)
  #  Month   Well Aldicarb.ppb
  #1     1 Well.1         19.9
  #2     2 Well.1         29.6
  #3     3 Well.1         18.7
  #4     4 Well.1         24.2
  #5     1 Well.2         23.7
  #6     2 Well.2         21.9

  longToWide(EPA.09.Ex.21.1.aldicarb.df, 
    "Aldicarb.ppb", "Month", "Well", paste.row.name = TRUE)
  #        Well.1 Well.2 Well.3
  #Month.1   19.9   23.7    5.6
  #Month.2   29.6   21.9    3.3
  #Month.3   18.7   26.9    2.3
  #Month.4   24.2   26.1    6.9

  # Estimate the 95th percentile and compute the lower 
  # 99% confidence limit for Well 1.
  #---------------------------------------------------

  with(EPA.09.Ex.21.1.aldicarb.df, 
    eqnorm(Aldicarb.ppb[Well == "Well.1"], p = 0.95, ci = TRUE, 
      ci.type = "lower", conf.level = 0.99))

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Normal
  #
  #Estimated Parameter(s):          mean = 23.10000
  #                                 sd   =  4.93491
  #
  #Estimation Method:               mvue
  #
  #Estimated Quantile(s):           95'th %ile = 31.2172
  #
  #Quantile Estimation Method:      qmle
  #
  #Data:                            Aldicarb.ppb[Well == "Well.1"]
  #
  #Sample Size:                     4
  #
  #Confidence Interval for:         95'th %ile
  #
  #Confidence Interval Method:      Exact
  #
  #Confidence Interval Type:        lower
  #
  #Confidence Level:                99%
  #
  #Confidence Interval:             LCL = 25.2855
  #                                 UCL =     Inf
 

  # Now compute the 99% lower confidence limit for each of the three 
  # wells all at once.
  #------------------------------------------------------------------

  LCLs &lt;- with(EPA.09.Ex.21.1.aldicarb.df, 
    sapply(split(Aldicarb.ppb, Well), 
      function(x) eqnorm(x, p = 0.95, method = "qmle", ci = TRUE, 
      ci.type = "lower", conf.level = 0.99)$interval$limits["LCL"]))

  round(LCLs, 2)
  #Well.1.LCL Well.2.LCL Well.3.LCL 
  #     25.29      25.66       5.46 

  LCLs &gt; 30
  #Well.1.LCL Well.2.LCL Well.3.LCL 
  #     FALSE      FALSE      FALSE


  # Clean up
  #---------

  rm(LCLs)

  
  #==========

  # Example 17-3 of USEPA (2009, p. 17-17) shows how to construct a 
  # beta-content upper tolerance limit with 95% coverage and 95% 
  # confidence using chrysene data and assuming a lognormal 
  # distribution.

  # A beta-content upper tolerance limit with 95% coverage and 95% 
  # confidence is equivalent to the 95% upper confidence limit for the 
  # 95th percentile.

  # Here we will construct a 95% upper confidence limit for the 95th 
  # percentile based on the log-transformed data, then exponentiate the 
  # result to get the confidence limit on the original scale.  Note that 
  # it is easier to just use the function eqlnorm with the original data 
  # to achieve the same result.

  attach(EPA.09.Ex.17.3.chrysene.df)
  log.Chrysene &lt;- log(Chrysene.ppb[Well.type == "Background"])
  eqnorm(log.Chrysene, p = 0.95, ci = TRUE, ci.type = "upper")

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Normal
  #
  #Estimated Parameter(s):          mean = 2.5085773
  #                                 sd   = 0.6279479
  #
  #Estimation Method:               mvue
  #
  #Estimated Quantile(s):           95'th %ile = 3.54146
  #
  #Quantile Estimation Method:      qmle
  #
  #Data:                            log.Chrysene
  #
  #Sample Size:                     8
  #
  #Confidence Interval for:         95'th %ile
  #
  #Confidence Interval Method:      Exact
  #
  #Confidence Interval Type:        upper
  #
  #Confidence Level:                95%
  #
  #Confidence Interval:             LCL =     -Inf
  #                                 UCL = 4.510032

  exp(4.510032)
  #[1] 90.92473

  #----------
  # Clean up

  rm(log.Chrysene)
  detach("EPA.09.Ex.17.3.chrysene.df")
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
