<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Nonparametric Tolerance Interval for a Continuous...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for tolIntNpar {EnvStats}"><tr><td>tolIntNpar {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Nonparametric Tolerance Interval for a Continuous Distribution
</h2>

<h3>Description</h3>

<p>Construct a <i>&beta;</i>-content or <i>&beta;</i>-expectation tolerance interval 
nonparametrically without making any assumptions about the form of the 
distribution except that it is continuous.
</p>


<h3>Usage</h3>

<pre>
  tolIntNpar(x, coverage, conf.level, cov.type = "content", 
    ltl.rank = ifelse(ti.type == "upper", 0, 1), 
    n.plus.one.minus.utl.rank = ifelse(ti.type == "lower", 0, 1), 
    lb = -Inf, ub = Inf, ti.type = "two-sided")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations. Missing (<code>NA</code>), undefined (<code>NaN</code>), and 
infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>coverage</code></td>
<td>

<p>a scalar between 0 and 1 indicating the desired coverage of the <i>&beta;</i>-content 
tolerance interval.  
The default value is <code>coverage=0.95</code>.  If <code>cov.type="content"</code>, you must 
supply a value for <code>coverage</code> or a value for <code>conf.level</code>, but not both.  
If <code>cov.type="expectation"</code>, this argument is ignored.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level associated with the <i>&beta;</i>-content 
tolerance interval.  The default value is <code>conf.level=0.95</code>.  If <br />
<code>cov.type="content"</code>, 
you must supply a value for <code>coverage</code> or a value for <code>conf.level</code>, but not both.  
If <code>cov.type="expectation"</code>, this argument is ignored.
</p>
</td></tr>
<tr valign="top"><td><code>cov.type</code></td>
<td>

<p>character string specifying the coverage type for the tolerance interval.  
The possible values are <code>"content"</code> (<i>&beta;</i>-content; the default), and 
<code>"expectation"</code> (<i>&beta;</i>-expectation).  See the DETAILS section for more 
information.
</p>
</td></tr>
<tr valign="top"><td><code>ltl.rank</code></td>
<td>

<p>positive integer indicating the rank of the order statistic to use for the lower bound 
of the tolerance interval.  If <code>ti.type="two-sided"</code> or <code>ti.type="lower"</code>, 
the default value is <code>ltl.rank=1</code> (implying the minimum value of <code>x</code> is used 
as the lower bound of the tolerance interval).  If <code>ti.type="upper"</code>, this argument 
is set equal to <code>0</code> and the value of <code>lb</code> is used as the lower bound of the 
tolerance interval.
</p>
</td></tr>
<tr valign="top"><td><code>n.plus.one.minus.utl.rank</code></td>
<td>

<p>positive integer related to the rank of the order statistic to use for 
the upper bound of the toleracne interval.  A value of 
<code>n.plus.one.minus.utl.rank=1</code> (the default) means use the 
first largest value of <code>x</code>, and in general a value of 
<code>n.plus.one.minus.utl.rank=</code><i>i</i> means use the <i>i</i>'th largest value.  If <br />
<code>ti.type="lower"</code>, 
this argument is set equal to <code>0</code> and the value of <code>ub</code> is used as the upper 
bound of the tolerance interval.
</p>
</td></tr>
<tr valign="top"><td><code>lb, ub</code></td>
<td>

<p>scalars indicating lower and upper bounds on the distribution.  By default, <code>lb=-Inf</code> and 
<code>ub=Inf</code>.  If you are constructing a tolerance interval for a distribution 
that you know has a lower bound other than <code>-Inf</code> (e.g., <code>0</code>), set <code>lb</code> to this 
value.  Similarly, if you know the distribution has an upper bound other than <code>Inf</code>, set 
<code>ub</code> to this value.  The argument <code>lb</code> is ignored if <code>ti.type="two-sided"</code> or 
<code>ti.type="lower"</code>.  The argument <code>ub</code> is ignored if <code>ti.type="two-sided"</code> or 
<code>ti.type="upper"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ti.type</code></td>
<td>

<p>character string indicating what kind of tolerance interval to compute.  
The possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>A tolerance interval for some population is an interval on the real line constructed so as to 
contain <i>100 &beta; \%</i> of the population (i.e., <i>100 &beta; \%</i> of all 
future observations), where <i>0 &lt; &beta; &lt; 1</i>.  The quantity <i>100 &beta; \%</i> is called 
the <em>coverage</em>.
</p>
<p>There are two kinds of tolerance intervals (Guttman, 1970):
</p>
 
<ul>
<li><p> A <i>&beta;</i>-content tolerance interval with confidence level <i>100(1-&alpha;)\%</i> is 
constructed so that it contains at least <i>100 &beta; \%</i> of the population (i.e., the 
coverage is at least <i>100 &beta; \%</i>) with probability <i>100(1-&alpha;)\%</i>, where 
<i>0 &lt; &alpha; &lt; 1</i>. The quantity <i>100(1-&alpha;)\%</i> is called the confidence level or 
confidence coefficient associated with the tolerance interval.
</p>
</li>
<li><p> A <i>&beta;</i>-expectation tolerance interval is constructed so that the <em>average</em> coverage of 
the interval is <i>100 &beta; \%</i>.
</p>
</li></ul>
 
<p><b>Note:</b> A <i>&beta;</i>-expectation tolerance interval with coverage <i>100 &beta; \%</i> is 
equivalent to a prediction interval for one future observation with associated confidence level 
<i>100 &beta; \%</i>.  Note that there is no explicit confidence level associated with a 
<i>&beta;</i>-expectation tolerance interval.  If a <i>&beta;</i>-expectation tolerance interval is 
treated as a <i>&beta;</i>-content tolerance interval, the confidence level associated with this 
tolerance interval is usually around 50% (e.g., Guttman, 1970, Table 4.2, p.76).  
<br />
</p>
<p><b>The Form of a Nonparametric Tolerance Interval</b> <br />
Let <i>\underline{x}</i> denote a random sample of <i>n</i> independent observations 
from some continuous distribution and let <i>x_{(i)}</i> denote the <i>i</i>'th order 
statistic in <i>\underline{x}</i>.  A two-sided nonparametric tolerance interval is 
constructed as:
</p>
<p style="text-align: center;"><i>[x_{(u)}, x_{(v)}] \;\;\;\;\;\; (1)</i></p>

<p>where <i>u</i> and <i>v</i> are positive integers between <i>1</i> and <i>n</i>, and 
<i>u &lt; v</i>.  That is, <i>u</i> denotes the rank of the lower tolerance limit, and 
<i>v</i> denotes the rank of the upper tolerance limit.  To make it easier to write 
some equations later on, we can also write the tolerance interval (1) in a slightly 
different way as:
</p>
<p style="text-align: center;"><i>[x_{(u)}, x_{(n+1-w)}] \;\;\;\;\;\; (2)</i></p>

<p>where
</p>
<p style="text-align: center;"><i>w = n + 1 - v \;\;\;\;\;\; (3)</i></p>

<p>so that <i>w</i> is a positive integer between <i>1</i> and <i>n-1</i>, and <i>u &lt; n+1-w</i>.  
In terms of the arguments to the function <code>tolIntNpar</code>, the argument 
<code>ltl.rank</code> corresponds to <i>u</i>, and the argument <code>n.plus.one.minus.utl.rank</code> 
corresponds to <i>w</i>.
</p>
<p>If we allow <i>u=0</i> and <i>w=0</i> and define lower and upper bounds as:
</p>
<p style="text-align: center;"><i>x_{(0)} = lb \;\;\;\;\;\; (4)</i></p>

<p style="text-align: center;"><i>x_{(n+1)} = ub \;\;\;\;\;\; (5)</i></p>

<p>then equation (2) above can also represent a one-sided lower or one-sided upper 
tolerance interval as well.  That is, a one-sided lower nonparametric tolerance interval 
is constructed as:
</p>
<p style="text-align: center;"><i>[x_{(u)}, x_{(n+1)}] = [x_{(u)}, ub] \;\;\;\;\;\; (6)</i></p>

<p>and a one-sided upper nonparametric tolerance interval is constructed as:
</p>
<p style="text-align: center;"><i>[x_{(0)}, x_{(v)}] = [lb, x_{(v)}] \;\;\;\;\;\; (7)</i></p>

<p>Usually, <i>lb = -&infin;</i> or <i>lb = 0</i> and <i>ub = &infin;</i>.
</p>
<p>Let <i>C</i> be a random variable denoting the coverage of the above nonparametric 
tolerance intervals.  Wilks (1941) showed that the distribution of <i>C</i> follows a 
<a href="../../stats/help/Beta.html">beta distribution</a> with parameters <code>shape1=</code><i>v-u</i> and 
<code>shape2=</code><i>w+u</i> when the unknown distribution is continuous.
<br />
</p>
<p><b>Computations for a <i>&beta;</i>-Content Tolerance Interval</b> <br />
For a <i>&beta;</i>-content tolerance interval, if the coverage <i>C = &beta;</i> is specified, 
then the associated confidence level <i>(1-&alpha;)100\%</i> is computed as:
</p>
<p style="text-align: center;"><i>1 - &alpha; = 1 - F(&beta;, v-u, w+u) \;\;\;\;\;\; (8)</i></p>

<p>where <i>F(y, &delta;, &gamma;)</i> denotes the cumulative distribution function of a 
<a href="../../stats/help/Beta.html">beta random variable</a> with parameters <code>shape1=</code><i>&delta;</i> and 
<code>shape2=</code><i>&gamma;</i> evaluated at <i>y</i>.
</p>
<p>Similarly, if the confidence level associated with the tolerance interval is specified as 
<i>(1-&alpha;)100\%</i>, then the coverage <i>C = &beta;</i> is computed as:
</p>
<p style="text-align: center;"><i>&beta; = B(&alpha;, v-u, w+u) \;\;\;\;\;\; (9)</i></p>

<p>where <i>B(p, &delta;, &gamma;)</i> denotes the <i>p</i>'th quantile of a 
<a href="../../stats/help/Beta.html">beta distribution</a> with parameters <code>shape1=</code><i>&delta;</i> 
and <code>shape2=</code><i>&gamma;</i>. 
<br /> 
</p>
<p><b>Computations for a <i>&beta;</i>-Expectation Tolerance Interval</b> <br />
For a <i>&beta;</i>-expectation tolerance interval, the expected coverage is simply 
the mean of a <a href="../../stats/help/Beta.html">beta random variable</a> with parameters 
<code>shape1=</code><i>v-u</i> and <code>shape2=</code><i>w+u</i>, which is given by:
</p>
<p style="text-align: center;"><i>E(C) = \frac{v-u}{n+1} \;\;\;\;\;\; (10)</i></p>

<p>As stated above, a <i>&beta;</i>-expectation tolerance interval with coverage 
<i>&beta; 100\%</i> is equivalent to a prediction interval for one future observation 
with associated confidence level <i>&beta; 100\%</i>.  This is because the probability 
that any single future observation will fall into this interval is <i>&beta; 100\%</i>, 
so the distribution of the number of <i>N</i> future observations that will fall into 
this interval is <a href="../../stats/help/Binomial.html">binomial</a> with parameters <code>size=</code><i>N</i> 
and <code>prob=</code><i>&beta;</i>.  Hence the expected proportion of future observations 
that fall into this interval is <i>&beta; 100\%</i> and is independent of the value of <i>N</i>. 
See the help file for <code><a href="../../EnvStats/help/predIntNpar.html">predIntNpar</a></code> for more information on constructing 
a nonparametric prediction interval.
</p>


<h3>Value</h3>

<p>A list of class <code>"estimate"</code> containing the estimated parameters, 
the tolerance interval, and other information.  See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> 
for details.
</p>


<h3>Note</h3>

<p>Tolerance intervals have long been applied to quality control and 
life testing problems (Hahn, 1970b,c; Hahn and Meeker, 1991; Krishnamoorthy and Mathew, 2009).  
References that discuss tolerance intervals in the context of environmental monitoring include:  
Berthouex and Brown (2002, Chapter 21), Gibbons et al. (2009), 
Millard and Neerchal (2001, Chapter 6), Singh et al. (2010b), and USEPA (2009).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Conover, W.J. (1980). <em>Practical Nonparametric Statistics</em>. Second Edition. 
John Wiley and Sons, New York.
</p>
<p>Danziger, L., and S. Davis. (1964).  Tables of Distribution-Free Tolerance Limits.  
<em>Annals of Mathematical Statistics</em> <b>35</b>(5), 1361&ndash;1365.
</p>
<p>Davis, C.B. (1994).  Environmental Regulatory Statistics. In Patil, G.P., and C.R. Rao, eds., 
<em>Handbook of Statistics, Vol. 12: Environmental Statistics</em>. 
North-Holland, Amsterdam, a division of Elsevier, New York, NY, Chapter 26, 817&ndash;865.
</p>
<p>Davis, C.B., and R.J. McNichols. (1994a).  Ground Water Monitoring Statistics Update: Part I: 
Progress Since 1988.  <em>Ground Water Monitoring and Remediation</em> <b>14</b>(4), 148&ndash;158.
</p>
<p>Gibbons, R.D. (1991b).  Statistical Tolerance Limits for Ground-Water Monitoring.  
<em>Ground Water</em> <b>29</b>, 563&ndash;570.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009). 
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.  
John Wiley &amp; Sons, Hoboken.
</p>
<p>Guttman, I. (1970).  <em>Statistical Tolerance Regions: Classical and Bayesian</em>.  
Hafner Publishing Co., Darien, CT, Chapter 2.
</p>
<p>Hahn, G.J., and W.Q. Meeker. (1991).  <em>Statistical Intervals: A Guide for Practitioners</em>.  
John Wiley and Sons, New York, 392pp.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). <em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York, NY, pp.88-90.
</p>
<p>Krishnamoorthy K., and T. Mathew. (2009). 
<em>Statistical Tolerance Regions: Theory, Applications, and Computation</em>. 
John Wiley and Sons, Hoboken.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Wilks, S.S. (1941).  Determination of Sample Sizes for Setting Tolerance Limits.  
<em>Annals of Mathematical Statistics</em> <b>12</b>, 91&ndash;96.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/eqnpar.html">eqnpar</a></code>, <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>, 
<code><a href="../../EnvStats/help/tolIntNparN.html">tolIntNparN</a></code>, <a href="../../EnvStats/help/Tolerance+20Intervals.html">Tolerance Intervals</a>, 
<a href="../../EnvStats/help/Estimating+20Distribution+20Parameters.html">Estimating Distribution Parameters</a>, <a href="../../EnvStats/help/Estimating+20Distribution+20Quantiles.html">Estimating Distribution Quantiles</a>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a lognormal mixture distribution
  # with parameters mean1=1, cv1=0.5, mean2=5, cv2=1, and p.mix=0.1.  
  # The exact two-sided interval that contains 90% of this distribution is given by: 
  # [0.682312, 13.32052].  Use tolIntNpar to construct a two-sided 90% 
  # \eqn{\beta}-content tolerance interval.  Note that the associated confidence level 
  # is only 61%.  A larger sample size is required to obtain a larger confidence 
  # level (see the help file for tolIntNparN). 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(23) 
  dat &lt;- rlnormMixAlt(20, 1, 0.5, 5, 1, 0.1) 
  tolIntNpar(dat, coverage = 0.9) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            None
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Tolerance Interval Coverage:     90%
  #
  #Coverage Type:                   content
  #
  #Tolerance Interval Method:       Exact
  #
  #Tolerance Interval Type:         two-sided
  #
  #Confidence Level:                60.8253%
  #
  #Tolerance Limit Rank(s):         1 20 
  #
  #Tolerance Interval:              LTL = 0.5035035
  #                                 UTL = 9.9504662

  #----------

  # Clean up
  rm(dat)

  #----------

  # Reproduce Example 17-4 on page 17-21 of USEPA (2009).  This example uses 
  # copper concentrations (ppb) from 3 background wells to set an upper 
  # limit for 2 compliance wells.  The maximum value from the 3 wells is set 
  # to the 95% confidence upper tolerance limit, and we need to determine the 
  # coverage of this tolerance interval.  The data are stored in EPA.92c.copper2.df.  
  # Note that even though these data are Type I left singly censored, it is still 
  # possible to compute an upper tolerance interval using any of the uncensored 
  # observations as the upper limit. 

  EPA.92c.copper2.df
  #   Copper.orig Copper Censored Month Well  Well.type
  #1           &lt;5    5.0     TRUE     1    1 Background
  #2           &lt;5    5.0     TRUE     2    1 Background
  #3          7.5    7.5    FALSE     3    1 Background
  #...
  #9          9.2    9.2    FALSE     1    2 Background
  #10          &lt;5    5.0     TRUE     2    2 Background
  #11          &lt;5    5.0     TRUE     3    2 Background
  #...
  #17          &lt;5    5.0     TRUE     1    3 Background
  #18         5.4    5.4    FALSE     2    3 Background
  #19         6.7    6.7    FALSE     3    3 Background
  #...
  #29         6.2    6.2    FALSE     5    4 Compliance
  #30          &lt;5    5.0     TRUE     6    4 Compliance
  #31         7.8    7.8    FALSE     7    4 Compliance
  #...
  #38          &lt;5    5.0     TRUE     6    5 Compliance
  #39         5.6    5.6    FALSE     7    5 Compliance
  #40          &lt;5    5.0     TRUE     8    5 Compliance

  with(EPA.92c.copper2.df, 
    tolIntNpar(Copper[Well.type=="Background"], 
      conf.level = 0.95, lb = 0, ti.type = "upper")) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            None
  #
  #Data:                            Copper[Well.type == "Background"]
  #
  #Sample Size:                     24
  #
  #Tolerance Interval Coverage:     88.26538%
  #
  #Coverage Type:                   content
  #
  #Tolerance Interval Method:       Exact
  #
  #Tolerance Interval Type:         upper
  #
  #Confidence Level:                95%
  #
  #Tolerance Limit Rank(s):         24 
  #
  #Tolerance Interval:              LTL = 0.0
  #                                 UTL = 9.2

  #----------

  # Repeat the last example, except compute an upper 
  # \eqn{\beta}-expectation tolerance interval:

  with(EPA.92c.copper2.df, 
    tolIntNpar(Copper[Well.type=="Background"], 
      cov.type = "expectation", lb = 0, ti.type = "upper")) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            None
  #
  #Data:                            Copper[Well.type == "Background"]
  #
  #Sample Size:                     24
  #
  #Tolerance Interval Coverage:     96%
  #
  #Coverage Type:                   expectation
  #
  #Tolerance Interval Method:       Exact
  #
  #Tolerance Interval Type:         upper
  #
  #Tolerance Limit Rank(s):         24 
  #
  #Tolerance Interval:              LTL = 0.0
  #                                 UTL = 9.2
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
