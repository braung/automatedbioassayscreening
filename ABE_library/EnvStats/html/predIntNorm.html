<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Prediction Interval for a Normal Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for predIntNorm {EnvStats}"><tr><td>predIntNorm {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Prediction Interval for a Normal Distribution
</h2>

<h3>Description</h3>

<p>Estimate the mean and standard deviation of a 
<a href="../../stats/help/Normal.html">normal distribution</a>, and 
construct a prediction interval for the next <i>k</i> observations or 
next set of <i>k</i> means.
</p>


<h3>Usage</h3>

<pre>
  predIntNorm(x, n.mean = 1, k = 1, method = "Bonferroni", 
    pi.type = "two-sided", conf.level = 0.95)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>a numeric vector of observations, or an object resulting from a call to an estimating 
function that assumes a normal (Gaussian) distribution (e.g., <code><a href="../../EnvStats/help/enorm.html">enorm</a></code>, 
<code><a href="../../EnvStats/help/eqnorm.html">eqnorm</a></code>, <code><a href="../../EnvStats/help/enormCensored.html">enormCensored</a></code>, etc.).  If <code>x</code> is a 
numeric vector, missing (<code>NA</code>), undefined (<code>NaN</code>), and 
infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>n.mean</code></td>
<td>

<p>positive integer specifying the sample size associated with the <i>k</i> future averages.  
The default value is <code>n.mean=1</code> (i.e., individual observations).  Note that all 
future averages must be based on the same sample size.
</p>
</td></tr>
<tr valign="top"><td><code>k</code></td>
<td>

<p>positive integer specifying the number of future observations or averages the 
prediction interval should contain with confidence level <code>conf.level</code>.  
The default value is <code>k=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method to use if the number of future observations 
(<code>k</code>) is greater than 1.  The possible values are <code>method="Bonferroni"</code> 
(approximate method based on Bonferonni inequality; the default), and <br />
<code>method="exact"</code> (exact method due to Dunnett, 1955).  See the DETAILS section 
of <code><a href="../../EnvStats/help/predIntNormK.html">predIntNormK</a></code> for more information.  
This argument is ignored if <code>k=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>pi.type</code></td>
<td>

<p>character string indicating what kind of prediction interval to compute.  
The possible values are <code>pi.type="two-sided"</code> (the default), <code>pi.type="lower"</code>, 
and <code>pi.type="upper"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the prediction interval.  
The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p><em>What is a Prediction Interval?</em> <br />
A prediction interval for some population is an interval on the real line constructed 
so that it will contain <i>k</i> future observations or averages from that population 
with some specified probability <i>(1-&alpha;)100\%</i>, where 
<i>0 &lt; &alpha; &lt; 1</i> and <i>k</i> is some pre-specified positive integer.  
The quantity <i>(1-&alpha;)100\%</i> is called  
the confidence coefficient or confidence level associated with the prediction 
interval.
<br />
</p>
<p><em>The Form of a Prediction Interval</em> <br />
Let <i>\underline{x} = x_1, x_2, &hellip;, x_n</i> denote a vector of <i>n</i> 
observations from a <a href="../../stats/help/Normal.html">normal distribution</a> with parameters 
<code>mean=</code><i>&mu;</i> and <code>sd=</code><i>&sigma;</i>.  Also, let <i>m</i> denote the 
sample size associated with the <i>k</i> future averages (i.e., <code>n.mean=</code><i>m</i>).  
When <i>m=1</i>, each average is really just a single observation, so in the rest of 
this help file the term &ldquo;averages&rdquo; will replace the phrase 
&ldquo;observations or averages&rdquo;.
</p>
<p>For a normal distribution, the form of a two-sided <i>(1-&alpha;)100\%</i> prediction 
interval is: 
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, \bar{x} + Ks] \;\;\;\;\;\; (1)</i></p>
 
<p>where <i>\bar{x}</i> denotes the sample mean:
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\;\;\; (2)</i></p>

<p><i>s</i> denotes the sample standard deviation:
</p>
<p style="text-align: center;"><i>s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\;\;\; (3)</i></p>

<p>and <i>K</i> denotes a constant that depends on the sample size <i>n</i>, the 
confidence level, the number of future averages <i>k</i>, and the 
sample size associated with the future averages, <i>m</i>.  Do not confuse the 
constant <i>K</i> (uppercase K) with the number of future averages <i>k</i> 
(lowercase k).  The symbol <i>K</i> is used here to be consistent with the 
notation used for tolerance intervals (see <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code>).  
</p>
<p>Similarly, the form of a one-sided lower prediction interval is: 
</p>
<p style="text-align: center;"><i>[\bar{x} - Ks, &infin;] \;\;\;\;\;\; (4)</i></p>
 
<p>and the form of a one-sided upper prediction interval is:
</p>
<p style="text-align: center;"><i>[-&infin;, \bar{x} + Ks] \;\;\;\;\;\; (5)</i></p>
 
<p>but <i>K</i> differs for one-sided versus two-sided prediction intervals.  
The derivation of the constant <i>K</i> is explained in the help file for 
<code><a href="../../EnvStats/help/predIntNormK.html">predIntNormK</a></code>.
<br />
</p>
<p><em>A Prediction Interval is a Random Interval</em> <br />
A prediction interval is a <em>random</em> interval; that is, the lower and/or 
upper bounds are random variables computed based on sample statistics in the 
baseline sample.  Prior to taking one specific baseline sample, the probability 
that the prediction interval will contain the next <i>k</i> averages is 
<i>(1-&alpha;)100\%</i>.  Once a specific baseline sample is taken and the 
prediction interval based on that sample is computed, the probability that that 
prediction interval will contain the next <i>k</i> averages is not necessarily 
<i>(1-&alpha;)100\%</i>, but it should be close.
</p>
<p>If an experiment is repeated <i>N</i> times, and for each experiment:
</p>

<ol>
<li><p> A sample is taken and a <i>(1-&alpha;)100\%</i> prediction interval for <i>k=1</i> 
future observation is computed, and 
</p>
</li>
<li><p> One future observation is generated and compared to the prediction interval,
</p>
</li></ol>

<p>then the number of prediction intervals that actually contain the future observation 
generated in step 2 above is a <a href="../../stats/help/Binomial.html">binomial random variable</a> 
with parameters <code>size=</code><i>N</i> and <code>prob=</code><i>(1-&alpha;)100\%</i>.
</p>
<p>If, on the other hand, only one baseline sample is taken and only one prediction 
interval for <i>k=1</i> future observation is computed, then the number of 
future observations out of a total of <i>N</i> future observations that will be 
contained in that one prediction interval is a binomial random variable with 
parameters <code>size=</code><i>N</i> and <code>prob=</code><i>(1-&alpha;^*)100\%</i>, where 
<i>&alpha;^*</i> depends on the true population parameters and the computed 
bounds of the prediction interval.
</p>


<h3>Value</h3>

<p>If <code>x</code> is a numeric vector, <code>predIntNorm</code> returns a list of class 
<code>"estimate"</code> containing the estimated parameters, the prediction interval, 
and other information.  See the help file for <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>
<p>If <code>x</code> is the result of calling an estimation function, 
<code>predIntNorm</code> returns a list whose class is the same as <code>x</code>.  
The list contains the same components as <code>x</code>, as well as a component called 
<code>interval</code> containing the prediction interval information.  
If <code>x</code> already has a component called <code>interval</code>, this component is 
replaced with the prediction interval information.
</p>


<h3>Note</h3>

<p>Prediction and tolerance intervals have long been applied to quality control and 
life testing problems (Hahn, 1970b,c; Hahn and Nelson, 1973; Krishnamoorthy and Mathew, 2009).  
In the context of environmental statistics, prediction intervals are useful for 
analyzing data from groundwater detection monitoring programs at hazardous and 
solid waste facilities (e.g., Gibbons et al., 2009; Millard and Neerchal, 2001; 
USEPA, 2009).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). <em>Statistics for Environmental Engineers</em>. 
Lewis Publishers, Boca Raton.
</p>
<p>Dunnett, C.W. (1955). A Multiple Comparisons Procedure for Comparing Several Treatments 
with a Control. <em>Journal of the American Statistical Association</em> <b>50</b>, 1096-1121.
</p>
<p>Dunnett, C.W. (1964). New Tables for Multiple Comparisons with a Control. 
<em>Biometrics</em> <b>20</b>, 482-491.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009). 
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.  
John Wiley &amp; Sons, Hoboken.
</p>
<p>Hahn, G.J. (1969). Factors for Calculating Two-Sided Prediction Intervals for 
Samples from a Normal Distribution. 
<em>Journal of the American Statistical Association</em> <b>64</b>(327), 878-898.
</p>
<p>Hahn, G.J. (1970a). Additional Factors for Calculating Prediction Intervals for 
Samples from a Normal Distribution. 
<em>Journal of the American Statistical Association</em> <b>65</b>(332), 1668-1676.
</p>
<p>Hahn, G.J. (1970b). Statistical Intervals for a Normal Population, Part I: Tables, 
Examples and Applications. <em>Journal of Quality Technology</em> <b>2</b>(3), 115-125.
</p>
<p>Hahn, G.J. (1970c). Statistical Intervals for a Normal Population, Part II: 
Formulas, Assumptions, Some Derivations. <em>Journal of Quality Technology</em> 
<b>2</b>(4), 195-206.
</p>
<p>Hahn, G.J., and W.Q. Meeker. (1991). <em>Statistical Intervals: A Guide for Practitioners</em>. 
John Wiley and Sons, New York.
</p>
<p>Hahn, G., and W. Nelson. (1973). A Survey of Prediction Intervals and Their Applications. 
<em>Journal of Quality Technology</em> <b>5</b>, 178-188.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (1992). <em>Statistical Methods in Water Resources Research</em>. 
Elsevier, New York.
</p>
<p>Helsel, D.R., and R.M. Hirsch. (2002). <em>Statistical Methods in Water Resources</em>. 
Techniques of Water Resources Investigations, Book 4, chapter A3. U.S. Geological Survey. 
(available on-line at:  <a href="http://pubs.usgs.gov/twri/twri4a3/">http://pubs.usgs.gov/twri/twri4a3/</a>).
</p>
<p>Krishnamoorthy K., and T. Mathew. (2009). 
<em>Statistical Tolerance Regions: Theory, Applications, and Computation</em>. 
John Wiley and Sons, Hoboken.
</p>
<p>Millard, S.P., and Neerchal, N.K. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, Florida.
</p>
<p>Miller, R.G. (1981a). <em>Simultaneous Statistical Inference</em>. McGraw-Hill, New York.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2010).  <em>Errata Sheet - March 2009 Unified Guidance</em>.
EPA 530/R-09-007a, August 9, 2010.  Office of Resource Conservation and Recovery, Program Information and Implementation Division.
U.S. Environmental Protection Agency, Washington, D.C.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/predIntNormK.html">predIntNormK</a></code>, <code><a href="../../EnvStats/help/predIntNormSimultaneous.html">predIntNormSimultaneous</a></code>, 
<code><a href="../../EnvStats/help/predIntLnorm.html">predIntLnorm</a></code>, <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code>, 
<a href="../../stats/html/Normal.html">Normal</a>, <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>, <code><a href="../../EnvStats/help/enorm.html">enorm</a></code>, 
<code><a href="../../EnvStats/help/eqnorm.html">eqnorm</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a normal distribution with parameters 
  # mean=10 and sd=2, then create a two-sided 95% prediction interval for 
  # the next observation. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(47) 
  dat &lt;- rnorm(20, mean = 10, sd = 2) 
  predIntNorm(dat)

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Normal
  #
  #Estimated Parameter(s):          mean = 9.792856
  #                                 sd   = 1.821286
  #
  #Estimation Method:               mvue
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Prediction Interval Method:      exact
  #
  #Prediction Interval Type:        two-sided
  #
  #Confidence Level:                95%
  #
  #Number of Future Observations:   1
  #
  #Prediction Interval:             LPL =  5.886723
  #                                 UPL = 13.698988

  #----------

  # Using the same data from the last example, create a one-sided 
  # upper 99% prediction limit for the next 3 averages of order 2 
  # (i.e., each of the 3 future averages is based on a sample size 
  # of 2 future observations).

  predIntNorm(dat, n.mean = 2, k = 3, conf.level = 0.99, 
    pi.type = "upper")

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Normal
  #
  #Estimated Parameter(s):          mean = 9.792856
  #                                 sd   = 1.821286
  #
  #Estimation Method:               mvue
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Prediction Interval Method:      Bonferroni
  #
  #Prediction Interval Type:        upper
  #
  #Confidence Level:                99%
  #
  #Number of Future Averages:       3
  #
  #Sample Size for Averages:        2
  #
  #Prediction Interval:             LPL =     -Inf
  #                                 UPL = 13.90537

  #----------

  # Compare the result above that is based on the Bonferroni method 
  # with the exact method

  predIntNorm(dat, n.mean = 2, k = 3, conf.level = 0.99, 
    pi.type = "upper", method = "exact")$interval$limits["UPL"]

  #     UPL 
  #13.89272

  #----------

  # Clean up
  rm(dat)
  
  #--------------------------------------------------------------------

  # Example 18-1 of USEPA (2009, p.18-9) shows how to construct a 95% 
  # prediction interval for 4 future observations assuming a 
  # normal distribution based on arsenic concentrations (ppb) in 
  # groundwater at a solid waste landfill.  There were 4 years of 
  # quarterly monitoring, and years 1-3 are considered background.  
  # The question to be answered is whether there is evidence of 
  # contamination in year 4.  

  # The data for this example is stored in EPA.09.Ex.18.1.arsenic.df.

  EPA.09.Ex.18.1.arsenic.df

  #   Year Sampling.Period Arsenic.ppb
  #1     1      Background        12.6
  #2     1      Background        30.8
  #3     1      Background        52.0
  #4     1      Background        28.1
  #5     2      Background        33.3
  #6     2      Background        44.0
  #7     2      Background         3.0
  #8     2      Background        12.8
  #9     3      Background        58.1
  #10    3      Background        12.6
  #11    3      Background        17.6
  #12    3      Background        25.3
  #13    4      Compliance        48.0
  #14    4      Compliance        30.3
  #15    4      Compliance        42.5
  #16    4      Compliance        15.0 

  As.bkgd &lt;- with(EPA.09.Ex.18.1.arsenic.df, 
    Arsenic.ppb[Sampling.Period == "Background"])
  As.cmpl &lt;- with(EPA.09.Ex.18.1.arsenic.df, 
    Arsenic.ppb[Sampling.Period == "Compliance"])

  # A Shapiro-Wilks goodness-of-fit test for normality indicates  
  # there is no evidence to reject the assumption of normality
  # for the background data:

  gofTest(As.bkgd)

  #Results of Goodness-of-Fit Test
  #-------------------------------
  #
  #Test Method:                     Shapiro-Wilk GOF
  #
  #Hypothesized Distribution:       Normal
  #
  #Estimated Parameter(s):          mean = 27.51667
  #                                 sd   = 17.10119
  #
  #Estimation Method:               mvue
  #
  #Data:                            As.bkgd
  #
  #Sample Size:                     12
  #
  #Test Statistic:                  W = 0.94695
  #
  #Test Statistic Parameter:        n = 12
  #
  #P-value:                         0.5929102
  #
  #Alternative Hypothesis:          True cdf does not equal the
  #                                 Normal Distribution.

  # Here is the one-sided 95% upper prediction limit:

  UPL &lt;- predIntNorm(As.bkgd, k = 4, 
    pi.type = "upper")$interval$limits["UPL"]
  UPL
  #     UPL 
  #73.67237  

  # Are any of the compliance observations above the prediction limit?

  any(As.cmpl &gt; UPL)
  #[1] FALSE

  #==========

  # Cleanup
  #--------

  rm(As.bkgd, As.cmpl, UPL)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
