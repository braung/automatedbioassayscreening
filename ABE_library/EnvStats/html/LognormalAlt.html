<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: The Lognormal Distribution (Alternative Parameterization)</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for LognormalAlt {EnvStats}"><tr><td>LognormalAlt {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
The Lognormal Distribution (Alternative Parameterization)
</h2>

<h3>Description</h3>

<p>Density, distribution function, quantile function, and random generation 
for the lognormal distribution with parameters <code>mean</code> and <code>cv</code>.
</p>


<h3>Usage</h3>

<pre>
  dlnormAlt(x, mean = exp(1/2), cv = sqrt(exp(1) - 1), log = FALSE)
  plnormAlt(q, mean = exp(1/2), cv = sqrt(exp(1) - 1), 
      lower.tail = TRUE, log.p = FALSE)
  qlnormAlt(p, mean = exp(1/2), cv = sqrt(exp(1) - 1), 
      lower.tail = TRUE, log.p = FALSE)
  rlnormAlt(n, mean = exp(1/2), cv = sqrt(exp(1) - 1))
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>q</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>vector of probabilities between 0 and 1.
</p>
</td></tr>
<tr valign="top"><td><code>n</code></td>
<td>

<p>sample size.  If <code>length(n)</code> is larger than 1, then <code>length(n)</code> 
random values are returned.
</p>
</td></tr>
<tr valign="top"><td><code>mean</code></td>
<td>

<p>vector of (positive) means of the distribution of the random variable.
</p>
</td></tr>
<tr valign="top"><td><code>cv</code></td>
<td>

<p>vector of (positive) coefficients of variation of the random variable.
</p>
</td></tr>
<tr valign="top"><td><code>log, log.p</code></td>
<td>

<p>logical; if <code>TRUE</code>, probabilities/densities <i>p</i> are returned as <i>log(p)</i>.
</p>
</td></tr>
<tr valign="top"><td><code>lower.tail</code></td>
<td>

<p>logical; if <code>TRUE</code> (default), probabilities are <i>P[X &le; x]</i>, 
otherwise, <i>P[X &gt; x]</i>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Let <i>X</i> be a random variable with a <a href="../../stats/help/Lognormal.html">lognormal distribution</a> 
with parameters <code>meanlog=</code><i>&mu;</i> and <code>sdlog=</code><i>&sigma;</i>.  That is, 
<i>&mu;</i> and <i>&sigma;</i> denote the mean and standard deviation of the random variable 
on the log scale.  The relationship between these parameters and the 
mean (<code>mean=</code><i>&theta;</i>) and coefficient of variation (<code>cv=</code><i>&tau;</i>) 
of the distribution on the original scale is given by:
</p>
<p style="text-align: center;"><i>&mu; = log(\frac{&theta;}{&radic;{&tau;^2 + 1}}) \;\;\;\; (1)</i></p>

<p style="text-align: center;"><i>&sigma; = [log(&tau;^2 + 1)]^{1/2} \;\;\;\; (2)</i></p>

<p style="text-align: center;"><i>&theta; = exp[&mu; + (&sigma;^2/2)] \;\;\;\; (3)</i></p>

<p style="text-align: center;"><i>&tau; = [exp(&sigma;^2) - 1]^{1/2} \;\;\;\; (4)</i></p>

<p>Thus, the functions <code>dlnormAlt</code>, <code>plnormAlt</code>, <code>qlnormAlt</code>, and 
<code>rlnormAlt</code> call the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> functions <code><a href="../../stats/html/Lognormal.html">dlnorm</a></code>, 
<code><a href="../../stats/html/Lognormal.html">plnorm</a></code>, <code><a href="../../stats/html/Lognormal.html">qlnorm</a></code>, and <code><a href="../../stats/html/Lognormal.html">rlnorm</a></code>, 
respectively using the following values for the <code>meanlog</code> and <code>sdlog</code> 
parameters: <br /> 
<code>sdlog &lt;- sqrt(log(1 + cv^2))</code>, <br />
<code>meanlog &lt;- log(mean) - (sdlog^2)/2</code>
</p>


<h3>Value</h3>

<p><code>dlnormAlt</code> gives the density, <code>plnormAlt</code> gives the distribution function, 
<code>qlnormAlt</code> gives the quantile function, and <code>rlnormAlt</code> generates random 
deviates. 
</p>


<h3>Note</h3>

<p>The two-parameter <a href="../../stats/help/Lognormal.html">lognormal distribution</a> is the distribution 
of a random variable whose logarithm is normally distributed.  The two major 
characteristics of the lognormal distribution are that it is bounded below at 0, 
and it is skewed to the right.
</p>
<p>Because the empirical distribution of many variables is inherently positive and 
skewed to the right (e.g., size of organisms, amount of rainfall, size of income, 
etc.), the lognormal distribution has been widely applied in several fields, 
including economics, business, industry, biology, ecology, atmospheric science, and 
geology (Aitchison and Brown, 1957; Crow and Shimizu, 1988).
</p>
<p>Gibrat (1930) derived the lognormal distribution from theoretical assumptions, 
calling it the &quot;law of proportionate effect&quot;, but Kapteyn (1903) had described a 
machine that was the mechanical equivalent.  The basic idea is that the 
Central Limit Theorem states that the distribution of the sum of several 
independent random variables tends to look like a normal distribution, no matter 
what the underlying distribution(s) of the original random variables, hence the 
product of several independent random variables tends to look like a lognormal 
distribution.
</p>
<p>The lognormal distribution is often used to characterize chemical concentrations 
in the environment.  Ott (1990) has shown mathematically how a series of 
successive random dilutions gives rise to a distribution that can be approximated 
by a lognormal distribution.
</p>
<p>A lognormal distribution starts to resemble a normal distribution as the 
parameter <i>&sigma;</i> (the standard deviation of the log of the distribution) 
tends to 0.
</p>
<p>Some EPA guidance documents (e.g., Singh et al., 2002; Singh et al., 2010a,b) 
discourage using the assumption of a lognormal distribution for some types of 
environmental data and recommend instead assessing whether the data appear to 
fit a gamma distribution.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1994). 
<em>Continuous Univariate Distributions, Volume 1</em>. 
Second Edition. John Wiley and Sons, New York.
</p>
<p>Limpert, E., W.A. Stahel, and M. Abbt. (2001).  Log-Normal Distributions Across the 
Sciences:  Keys and Clues.  <em>BioScience</em> <b>51</b>, 341&ndash;352.
</p>
<p>Ott, W.R. (1995). <em>Environmental Statistics and Data Analysis</em>. 
Lewis Publishers, Boca Raton, FL.
</p>
<p>Singh, A., R. Maichle, and N. Armbya. (2010a).  <em>ProUCL Version 4.1.00 
User Guide (Draft)</em>.  EPA/600/R-07/041, May 2010.  Office of Research and 
Development, U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Singh, A., N. Armbya, and A. Singh. (2010b).  <em>ProUCL Version 4.1.00 
Technical Guide (Draft)</em>.  EPA/600/R-07/041, May 2010.  Office of Research and 
Development, U.S. Environmental Protection Agency, Washington, D.C.
</p>


<h3>See Also</h3>

<p><a href="../../stats/help/Lognormal.html">Lognormal</a>, <code><a href="../../EnvStats/help/elnormAlt.html">elnormAlt</a></code>,  
<a href="../../EnvStats/help/Probability+20Distributions+20and+20Random+20Numbers.html">Probability Distributions and Random Numbers</a>.
</p>


<h3>Examples</h3>

<pre>
  # Density of the lognormal distribution with parameters 
  # mean=10 and cv=1, evaluated at 5: 

  dlnormAlt(5, mean = 10, cv = 1) 
  #[1] 0.08788173

  #----------

  # The cdf of the lognormal distribution with parameters mean=2 and cv=3, 
  # evaluated at 4: 

  plnormAlt(4, 2, 3) 
  #[1] 0.8879132

  #----------

  # The median of the lognormal distribution with parameters 
  # mean=10 and cv=1: 

  qlnormAlt(0.5, mean = 10, cv = 1) 
  #[1] 7.071068

  #----------

  # Random sample of 3 observations from a lognormal distribution with 
  # parameters mean=10 and cv=1. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(20) 
  rlnormAlt(3, mean = 10, cv = 1) 
  #[1] 18.615797  4.341402 31.265293
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
