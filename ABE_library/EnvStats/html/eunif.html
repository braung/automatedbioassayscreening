<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Parameters of a Uniform Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for eunif {EnvStats}"><tr><td>eunif {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Parameters of a Uniform Distribution
</h2>

<h3>Description</h3>

<p>Estimate the minimum and maximum parameters of a 
<a href="../../stats/help/Uniform.html">uniform distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  eunif(x, method = "mle")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.  Missing (<code>NA</code>), undefined (<code>NaN</code>), and 
infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  The possible values are 
<code>"mle"</code> (maximum likelihood; the default), <code>"mme"</code> (method of moments), 
and <code>"mmue"</code> (method of moments based on the unbiased estimator of variance).  
See the DETAILS section for more information on these estimation methods.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of 
<i>n</i> observations from an <a href="../../stats/help/Uniform.html">uniform distribution</a> with 
parameters <code>min=</code><i>a</i> and <code>max=</code><i>b</i>.  Also, let <i>x_{(i)}</i> 
denote the <i>i</i>'th order statistic.
</p>
<p><b>Estimation</b> <br />
</p>
<p><em>Maximum Likelihood Estimation</em> (<code>method="mle"</code>) <br />
The maximum likelihood estimators (mle's) of <i>a</i> and <i>b</i> are given by
(Johnson et al, 1995, p.286):
</p>
<p style="text-align: center;"><i>\hat{a}_{mle} = x_{(1)} \;\;\;\; (1)</i></p>

<p style="text-align: center;"><i>\hat{b}_{mle} = x_{(n)} \;\;\;\; (2)</i></p>

<p><br />
</p>
<p><em>Method of Moments Estimation</em> (<code>method="mme"</code>) <br />
The method of moments estimators (mme's) of <i>a</i> and <i>b</i> are given by
(Forbes et al., 2011):
</p>
<p style="text-align: center;"><i>\hat{a}_{mme} = \bar{x} - &radic;{3} s_m \;\;\;\; (3)</i></p>

<p style="text-align: center;"><i>\hat{b}_{mme} = \bar{x} + &radic;{3} s_m \;\;\;\; (4)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>\bar{x} = \frac{1}{n} &sum;_{i=1}^n x_i \;\;\;\; (5)</i></p>

<p style="text-align: center;"><i>s^2_m = \frac{1}{n} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\; (6)</i></p>

<p><br />
</p>
<p><em>Method of Moments Estimation Based on the Unbiased Estimator of Variance</em> (<code>method="mmue"</code>) <br />
The method of moments estimators based on the unbiased estimator of variance are 
exactly the same as the method of moments estimators given in equations (3-6) above, 
except that the method of moments estimator of variance in equation (6) is replaced 
with the unbiased estimator of variance:
</p>
<p style="text-align: center;"><i>\hat{a}_{mmue} = \bar{x} - &radic;{3} s \;\;\;\; (7)</i></p>

<p style="text-align: center;"><i>\hat{b}_{mmue} = \bar{x} + &radic;{3} s \;\;\;\; (8)</i></p>

<p>where 
</p>
<p style="text-align: center;"><i>s^2 = \frac{1}{n-1} &sum;_{i=1}^n (x_i - \bar{x})^2 \;\;\;\; (9)</i></p>



<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other 
information.  See <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/help/Uniform.html">uniform distribution</a> (also called the rectangular 
distribution) with parameters <code>min</code> and <code>max</code> takes on values on the 
real line between <code>min</code> and <code>max</code> with equal probability.  It has been 
used to represent the distribution of round-off errors in tabulated values.  Another 
important application is that the distribution of the cumulative distribution 
function (cdf) of any kind of continuous random variable follows a uniform 
distribution with parameters <code>min=0</code> and <code>max=1</code>.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995). 
<em>Continuous Univariate Distributions, Volume 2</em>. 
Second Edition. John Wiley and Sons, New York.
</p>


<h3>See Also</h3>

<p><a href="../../stats/help/Uniform.html">Uniform</a>, <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a uniform distribution with parameters 
  # min=-2 and max=3, then estimate the parameters via maximum likelihood. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(250) 
  dat &lt;- runif(20, min = -2, max = 3) 
  eunif(dat) 

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Uniform
  #
  #Estimated Parameter(s):          min = -1.574529
  #                                 max =  2.837006
  #
  #Estimation Method:               mle
  #
  #Data:                            dat
  #
  #Sample Size:                     20

  #----------

  # Compare the three methods of estimation:

  eunif(dat, method = "mle")$parameters 
  #      min       max 
  #-1.574529  2.837006 
 
 
  eunif(dat, method = "mme")$parameters 
  #      min       max 
  #-1.988462  2.650737 
 
 
  eunif(dat, method = "mmue")$parameters 
  #      min       max 
  #-2.048721  2.710996 

  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
