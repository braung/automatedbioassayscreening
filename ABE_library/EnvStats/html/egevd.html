<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate Parameters of a Generalized Extreme Value...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for egevd {EnvStats}"><tr><td>egevd {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Estimate Parameters of a Generalized Extreme Value Distribution
</h2>

<h3>Description</h3>

<p>Estimate the location, scale and shape parameters of a 
<a href="../../EnvStats/help/GEVD.html">generalized extreme value distribution</a>, and optionally construct a 
confidence interval for one of the parameters.
</p>


<h3>Usage</h3>

<pre>
  egevd(x, method = "mle", pwme.method = "unbiased", tsoe.method = "med", 
    plot.pos.cons = c(a = 0.35, b = 0), ci = FALSE, ci.parameter = "location", 
    ci.type = "two-sided", ci.method = "normal.approx", information = "observed", 
    conf.level = 0.95)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>character string specifying the method of estimation.  Possible values are 
<code>"mle"</code> (maximum likelihood; the default), 
<code>"pwme"</code> (probability-weighted moments), and 
<code>"tsoe"</code> (two-stage order-statistics estimator of Castillo and Hadi (1994)).  
See the DETAILS section for more information on these estimation methods.
</p>
</td></tr>
<tr valign="top"><td><code>pwme.method</code></td>
<td>

<p>character string specifying what method to use to compute the 
probability-weighted moments when <code>method="pwme"</code>.  The possible values are 
<code>"ubiased"</code> (method based on the U-statistic; the default), or 
<code>"plotting.position"</code> (method based on the plotting position formula).  
See the DETAILS section in this help file and the help file for <code><a href="../../EnvStats/help/pwMoment.html">pwMoment</a></code> 
for more information.  This argument is ignored if <code>method</code> is not equal to 
<code>"pwme"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>tsoe.method</code></td>
<td>

<p>character string specifying the robust function to apply in the second stage of 
the two-stage order-statistics estimator when <code>method="tsoe"</code>.  Possible 
values are <code>"med"</code> (median; the default), and <code>"lms"</code> 
(least median of squares).  See the DETAILS section for more information on 
these estimation methods.  This argument is ignored if <code>method</code> is not 
equal to <code>"tsoe"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>plot.pos.cons</code></td>
<td>

<p>numeric vector of length 2 specifying the constants used in the formula for the 
plotting positions when <code>method="pwme"</code> and <br />
<code>pwme.method="plotting.position"</code>.  The default value is <br />
<code>plot.pos.cons=c(a=0.35, b=0)</code>.  If this vector has a names attribute with 
the value <code>c("a","b")</code> or <code>c("b","a")</code>, then the elements will be 
matched by name in the formula for computing the plotting positions.  Otherwise, 
the first element is mapped to the name <code>"a"</code> and the second element to the 
name <code>"b"</code>.  See the DETAILS section in this help file and the help file 
for <code><a href="../../EnvStats/help/pwMoment.html">pwMoment</a></code> for more information.  This argument is used only if 
<code>method="tsoe"</code>, or if both <code>method="pwme"</code> and 
<code>pwme.method="plotting.position"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci</code></td>
<td>

<p>logical scalar indicating whether to compute a confidence interval for the 
location, scale, or shape parameter.  The default value is <code>FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.parameter</code></td>
<td>

<p>character string indicating the parameter for which the confidence interval is 
desired.  The possible values are <code>"location"</code> (the default), <code>"scale"</code>, 
or <code>"shape"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.type</code></td>
<td>

<p>character string indicating what kind of confidence interval to compute.  The 
possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.  This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>ci.method</code></td>
<td>

<p>character string indicating what method to use to construct the confidence interval 
for the location or scale parameter.  Currently, the only possible value is 
<code>"normal.approx"</code> (the default).  See the DETAILS section for more information.  
This argument is ignored if <code>ci=FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>information</code></td>
<td>

<p>character string indicating which kind of Fisher information to use when 
computing the variance-covariance matrix of the maximum likelihood estimators.  
The possible values are <code>"observed"</code> (the default) and <code>"expected"</code>.  
See the DETAILS section for more information.  This argument is used only when 
<code>method="mle"</code> and <code>ci=TRUE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level of the confidence interval.  
The default value is <code>conf.level=0.95</code>. This argument is ignored if 
<code>ci=FALSE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If <code>x</code> contains any missing (<code>NA</code>), undefined (<code>NaN</code>) or 
infinite (<code>Inf</code>, <code>-Inf</code>) values, they will be removed prior to 
performing the estimation.
</p>
<p>Let <i>\underline{x} = (x_1, x_2, &hellip;, x_n)</i> be a vector of 
<i>n</i> observations from a <a href="../../EnvStats/help/GEVD.html">generalized extreme value distribution</a> with 
parameters <code>location=</code><i>&eta;</i>, <code>scale=</code><i>&theta;</i>, and 
<code>shape=</code><i>&kappa;</i>.
</p>
<p><b>Estimation</b>
</p>
<p><em>Maximum Likelihood Estimation</em> (<code>method="mle"</code>) <br /> 
The log likelihood function is given by:
</p>
<p style="text-align: center;"><i>L(&eta;, &theta;, &kappa;) = -n \, log(&theta;) - (1 - &kappa;) &sum;^n_{i=1} y_i - &sum;^n_{i=1} e^{y_i}</i></p>

<p>where
</p>
<p style="text-align: center;"><i>y_i = -\frac{1}{&kappa;} log[\frac{1 - &kappa;(x_i - &eta;)}{&theta;}]</i></p>

<p>(see, for example, Jenkinson, 1969; Prescott and Walden, 1980; Prescott and Walden, 
1983; Hosking, 1985; MacLeod, 1989).  The maximum likelihood estimators (MLE's) of 
<i>&eta;</i>, <i>&theta;</i>, and <i>&kappa;</i> are those values that maximize the 
likelihood function, subject to the following constraints:
</p>
<p style="text-align: center;"><i>&theta; &gt; 0</i></p>

<p style="text-align: center;"><i>&kappa; &le; 1</i></p>

<p style="text-align: center;"><i>x_i &lt; &eta; + \frac{&theta;}{&kappa;} \; if &kappa; &gt; 0</i></p>

<p style="text-align: center;"><i>x_i &gt; &eta; + \frac{&theta;}{&kappa;} \; if &kappa; &lt; 0</i></p>

<p>Although in theory the value of <i>&kappa;</i> may lie anywhere in the interval 
<i>(-&infin;, &infin;)</i> (see <a href="../../EnvStats/help/GEVD.html">GEVD</a>), the constraint <i>&kappa; &le; 1</i> is 
imposed because when <i>&kappa; &gt; 1</i> the likelihood can be made infinite and 
thus the MLE does not exist (Castillo and Hadi, 1994).  Hence, <b>this method of 
estimation is not valid when the true value of <i>&kappa;</i> is larger than 1</b>.  
Hosking (1985) and Hosking et al. (1985) note that in practice the value of 
<i>&kappa;</i> tends to lie in the interval <i>-1/2 &lt; &kappa; &lt; 1/2</i>.
</p>
<p>The value of <i>-L</i> is minimized using the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> function <code><a href="../../stats/html/nlminb.html">nlminb</a></code>.  
Prescott and Walden (1983) give formulas for the gradient and Hessian.  Only 
the gradient is supplied in the call to <code><a href="../../stats/html/nlminb.html">nlminb</a></code>.  The values of 
the PWME (see below) are used as the starting values.  If the starting value of 
<i>&kappa;</i> is less than 0.001 in absolute value, it is reset to 
<code>sign(k) * 0.001</code>, as suggested by Hosking (1985).
</p>
<p><em>Probability-Weighted Moments Estimation</em> (<code>method="pwme"</code>)<br />
The idea of probability-weighted moments was introduced by Greenwood et al. (1979).  
Landwehr et al. (1979) derived probability-weighted moment estimators (PWME's) for 
the parameters of the <a href="../../EnvStats/help/EVD.html">Type I (Gumbel) extreme value distribution</a>.  
Hosking et al. (1985) extended these results to the generalized extreme value 
distribution.  See the <a href="../../EnvStats/help/HoskingEtAl1985.html">abstract for Hosking et al. (1985)</a> 
for details on how these estimators are computed.
</p>
<p><em>Two-Stage Order Statistics Estimation</em> (<code>method="tsoe"</code>)<br />
The two-stage order statistics estimator (TSOE) was introduced by 
Castillo and Hadi (1994) as an alternative to the MLE and PWME.  Unlike the 
MLE and PWME, the TSOE of <i>&kappa;</i> exists for all combinations of sample 
values and possible values of <i>&kappa;</i>.  See the 
<a href="../../EnvStats/help/CastilloAndHadi1994.html">abstract for Castillo and Hadi (1994)</a> for details 
on how these estimators are computed.  In the second stage, 
Castillo and Hadi (1984) suggest using either the median or the least median of 
squares as the robust function.  The function <code>egevd</code> allows three options 
for the robust function:  median (<code>tsoe.method="med"</code>; see the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> help file for 
<code><a href="../../stats/html/median.html">median</a></code>), least median of squares (<code>tsoe.method="lms"</code>; 
see the help file for <code><a href="../../MASS/help/lmsreg.html">lmsreg</a></code> in the package <span class="pkg">MASS</span>), 
and least trimmed squares (<code>tsoe.method="lts"</code>; see the help file for 
<code><a href="../../MASS/help/ltsreg.html">ltsreg</a></code> in the package <span class="pkg">MASS</span>).
</p>
<p><b>Confidence Intervals</b> <br />
When <code>ci=TRUE</code>, an approximate <i>(1-&alpha;)</i>100% confidence intervals 
for <i>&eta;</i> can be constructed assuming the distribution of the estimator of 
<i>&eta;</i> is approximately normally distributed.  A two-sided confidence 
interval is constructed as:
</p>
<p style="text-align: center;"><i>[\hat{&eta;} - t(n-1, 1-&alpha;/2) \hat{&sigma;}_{\hat{&eta;}}, \, \hat{&eta;} + t(n-1, 1-&alpha;/2) \hat{&sigma;}_{\hat{&eta;}}]</i></p>

<p>where <i>t(&nu;, p)</i> is the <i>p</i>'th quantile of Student's t-distribution with 
<i>&nu;</i> degrees of freedom, and the quantity 
</p>
<p style="text-align: center;"><i>\hat{&sigma;}_{\hat{&eta;}}</i></p>

<p>denotes the estimated asymptotic standard deviation of the estimator of <i>&eta;</i>.
</p>
<p>Similarly, a two-sided confidence interval for <i>&theta;</i> is constructed as:
</p>
<p style="text-align: center;"><i>[\hat{&theta;} - t(n-1, 1-&alpha;/2) \hat{&sigma;}_{\hat{&theta;}}, \, \hat{&theta;} + t(n-1, 1-&alpha;/2) \hat{&sigma;}_{\hat{&theta;}}]</i></p>

<p>and a two-sided confidence interval for <i>&kappa;</i> is constructed as:
</p>
<p style="text-align: center;"><i>[\hat{&kappa;} - t(n-1, 1-&alpha;/2) \hat{&sigma;}_{\hat{&kappa;}}, \, \hat{&kappa;} + t(n-1, 1-&alpha;/2) \hat{&sigma;}_{\hat{&kappa;}}]</i></p>

<p>One-sided confidence intervals for <i>&eta;</i>, <i>&theta;</i>, and <i>&kappa;</i> are 
computed in a similar fashion.
</p>
<p><em>Maximum Likelihood Estimator</em> (<code>method="mle"</code>) <br />
Prescott and Walden (1980) derive the elements of the Fisher information matrix 
(the expected information).  The inverse of this matrix, evaluated at the values 
of the MLE, is the estimated asymptotic variance-covariance matrix of the MLE.  
This method is used to estimate the standard deviations of the estimated 
distribution parameters when <code>information="expected"</code>.  The necessary 
regularity conditions hold for <i>&kappa; &lt; 1/2</i>.  Thus, <b>this method of 
constructing confidence intervals is not valid when the true value of 
<i>&kappa;</i> is greater than or equal to 1/2</b>.
</p>
<p>Prescott and Walden (1983) derive expressions for the observed information matrix 
(i.e., the Hessian).  This matrix is used to compute the estimated asymptotic 
variance-covariance matrix of the MLE when <code>information="observed"</code>.
</p>
<p>In computer simulations, Prescott and Walden (1983) found that the 
variance-covariance matrix based on the observed information gave slightly more 
accurate estimates of the variance of MLE of <i>&kappa;</i> compared to the 
estimated variance based on the expected information.
</p>
<p><em>Probability-Weighted Moments Estimator</em> (<code>method="pwme"</code>) <br />
Hosking et al. (1985) show that these estimators are asymptotically multivariate 
normal and derive the asymptotic variance-covariance matrix.  See the 
<a href="../../EnvStats/help/HoskingEtAl1985.html">abstract for Hosking et al. (1985)</a> for details on how 
this matrix is computed.
</p>
<p><em>Two-Stage Order Statistics Estimator</em> (<code>method="tsoe"</code>) <br />
Currently there is no built-in method in <span class="pkg">EnvStats</span> for computing confidence 
intervals when <br />
<code>method="tsoe"</code>.  <a href="../../EnvStats/help/CastilloAndHadi1994.html">Castillo and Hadi (1994)</a> suggest 
using the bootstrap or jackknife method.
</p>


<h3>Value</h3>

<p>a list of class <code>"estimate"</code> containing the estimated parameters and other information.  
See <br />
<code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> for details.
</p>


<h3>Note</h3>

<p>Two-parameter <a href="../../EnvStats/help/EVD.html">extreme value distributions</a> (EVD) have been 
applied extensively since the 1930's to several fields of study, including 
the distributions of hydrological and meteorological variables, human lifetimes, 
and strength of materials.  The three-parameter 
<a href="../../EnvStats/help/GEVD.html">generalized extreme value distribution</a> (GEVD) was introduced by 
Jenkinson (1955) to model annual maximum and minimum values of meteorological 
events.  Since then, it has been used extensively in the hydological and 
meteorological fields.
</p>
<p>The three families of EVDs are all special kinds of GEVDs.  When the shape 
parameter <i>&kappa;=0</i>, the GEVD reduces to the Type I extreme value (Gumbel) 
distribution.  (The function <code><a href="../../EnvStats/help/zTestGevdShape.html">zTestGevdShape</a></code> allows you to test 
the null hypothesis <i>H_0: &kappa;=0</i>.)  When <i>&kappa; &gt; 0</i>, the GEVD is 
the same as the Type II extreme value distribution, and when <i>&kappa; &lt; 0</i> 
it is the same as the Type III extreme value distribution.
</p>
<p>Hosking et al. (1985) compare the asymptotic and small-sample statistical 
properties of the PWME with the MLE and Jenkinson's (1969) method of sextiles.  
Castillo and Hadi (1994) compare the small-sample statistical properties of the 
MLE, PWME, and TSOE.  Hosking and Wallis (1995) compare the small-sample properties 
of unbaised <i>L</i>-moment estimators vs. plotting-position <i>L</i>-moment 
estimators.  (PWMEs can be written as linear combinations of <i>L</i>-moments and 
thus have equivalent statistical properties.)  Hosking and Wallis (1995) conclude 
that unbiased estimators should be used for almost all applications.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Castillo, E., and A. Hadi. (1994).  Parameter and Quantile Estimation for the 
Generalized Extreme-Value Distribution.  <em>Environmetrics</em> <b>5</b>, 417&ndash;432.
</p>
<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Greenwood, J.A., J.M. Landwehr, N.C. Matalas, and J.R. Wallis. (1979).  
Probability Weighted Moments: Definition and Relation to Parameters of Several 
Distributions Expressible in Inverse Form.  <em>Water Resources Research</em> 
<b>15</b>(5), 1049&ndash;1054.
</p>
<p>Hosking, J.R.M. (1984).  Testing Whether the Shape Parameter is Zero in the 
Generalized Extreme-Value Distribution.  <em>Biometrika</em> <b>71</b>(2), 367&ndash;374.
</p>
<p>Hosking, J.R.M. (1985).  Algorithm AS 215: Maximum-Likelihood Estimation of the 
Parameters of the Generalized Extreme-Value Distribution.  
<em>Applied Statistics</em> <b>34</b>(3), 301&ndash;310.
</p>
<p>Hosking, J.R.M., J.R. Wallis, and E.F. Wood. (1985).  Estimation of the 
Generalized Extreme-Value Distribution by the Method of 
Probability-Weighted Moments.  <em>Technometrics</em> <b>27</b>(3), 251&ndash;261.
</p>
<p>Jenkinson, A.F. (1969).  Statistics of Extremes. <em>Technical Note 98</em>, 
World Meteorological Office, Geneva.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1995). 
<em>Continuous Univariate Distributions, Volume 2</em>. 
Second Edition. John Wiley and Sons, New York.
</p>
<p>Landwehr, J.M., N.C. Matalas, and J.R. Wallis. (1979).  Probability Weighted 
Moments Compared With Some Traditional Techniques in Estimating Gumbel 
Parameters and Quantiles.  <em>Water Resources Research</em> <b>15</b>(5), 
1055&ndash;1064.
</p>
<p>Macleod, A.J. (1989).  Remark AS R76: A Remark on Algorithm AS 215: 
Maximum Likelihood Estimation of the Parameters of the Generalized 
Extreme-Value Distribution.  <em>Applied Statistics</em> <b>38</b>(1), 198&ndash;199.
</p>
<p>Prescott, P., and A.T. Walden. (1980).  Maximum Likelihood Estimation of the 
Parameters of the Generalized Extreme-Value Distribution.  
<em>Biometrika</em> <b>67</b>(3), 723&ndash;724.
</p>
<p>Prescott, P., and A.T. Walden. (1983).  Maximum Likelihood Estimation of the 
Three-Parameter Generalized Extreme-Value Distribution from Censored Samples.  
<em>Journal of Statistical Computing and Simulation</em> <b>16</b>, 241&ndash;250.
</p>


<h3>See Also</h3>

<p><a href="../../EnvStats/help/GEVD.html">Generalized Extreme Value Distribution</a>, 
<code><a href="../../EnvStats/help/zTestGevdShape.html">zTestGevdShape</a></code>, <a href="../../EnvStats/help/EVD.html">Extreme Value Distribution</a>, 
<code><a href="../../EnvStats/help/eevd.html">eevd</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a generalized extreme value distribution 
  # with parameters location=2, scale=1, and shape=0.2, then compute the 
  # MLE and construct a 90% confidence interval for the location parameter. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(498) 
  dat &lt;- rgevd(20, location = 2, scale = 1, shape = 0.2) 
  egevd(dat, ci = TRUE, conf.level = 0.9)

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Generalized Extreme Value
  #
  #Estimated Parameter(s):          location = 1.6144631
  #                                 scale    = 0.9867007
  #                                 shape    = 0.2632493
  #
  #Estimation Method:               mle
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Confidence Interval for:         location
  #
  #Confidence Interval Method:      Normal Approximation
  #                                 (t Distribution) based on
  #                                 observed information
  #
  #Confidence Interval Type:        two-sided
  #
  #Confidence Level:                90%
  #
  #Confidence Interval:             LCL = 1.225249
  #                                 UCL = 2.003677

  #----------

  # Compare the values of the different types of estimators:

  egevd(dat, method = "mle")$parameters 
  # location     scale     shape 
  #1.6144631 0.9867007 0.2632493 

  egevd(dat, method = "pwme")$parameters
  # location     scale     shape 
  #1.5785779 1.0187880 0.2257948 

  egevd(dat, method = "pwme", pwme.method = "plotting.position")$parameters 
  # location     scale     shape 
  #1.5509183 0.9804992 0.1657040

  egevd(dat, method = "tsoe")$parameters 
  # location     scale     shape 
  #1.5372694 1.0876041 0.2927272 

  egevd(dat, method = "tsoe", tsoe.method = "lms")$parameters 
  #location    scale    shape 
  #1.519469 1.081149 0.284863

  egevd(dat, method = "tsoe", tsoe.method = "lts")$parameters 
  # location     scale     shape 
  #1.4840198 1.0679549 0.2691914 

  #----------

  # Clean up
  #---------
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
