<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Compute Confidence Level Associated with a Nonparametric...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for ciNparConfLevel {EnvStats}"><tr><td>ciNparConfLevel {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Compute Confidence Level Associated with a Nonparametric Confidence Interval for a Quantile
</h2>

<h3>Description</h3>

<p>Compute the confidence level associated with a nonparametric confidence 
interval for a quantile, given the sample size and order statistics 
associated with the lower and upper bounds.
</p>


<h3>Usage</h3>

<pre>
  ciNparConfLevel(n, p = 0.5, lcl.rank = ifelse(ci.type == "upper", 0, 1), 
    n.plus.one.minus.ucl.rank = ifelse(ci.type == "lower", 0, 1),  
    ci.type = "two.sided")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n</code></td>
<td>

<p>numeric vector of sample sizes.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) values are not allowed.  
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>numeric vector of probabilities specifying which quantiles to consider for 
the sample size calculation.  All values of <code>p</code> must be between 0 and 
1.  The default value is <code>p=0.5</code>.
</p>
</td></tr>
<tr valign="top"><td><code>lcl.rank, n.plus.one.minus.ucl.rank</code></td>
<td>

<p>numeric vectors of non-negative integers indicating the ranks of the 
order statistics that are used for the lower and upper bounds of the 
confidence interval for the specified quantile(s).  When <code>lcl.rank=1</code> 
that means use the smallest value as the lower bound, when <code>lcl.rank=2</code> 
that means use the second to smallest value as the lower bound, etc.  
When <code>n.plus.one.minus.ucl.rank=1</code> that means use the largest value 
as the upper bound, when <code>n.plus.one.minus.ucl.rank=2</code> that means use 
the second to largest value as the upper bound, etc.  
A value of <code>0</code> for <code>lcl.rank</code> indicates no lower bound 
(i.e., -Inf) and a value of 
<code>0</code> for <code>n.plus.one.minus.ucl.rank</code> indicates no upper bound 
(i.e., <code>Inf</code>).  When <br />
<code>ci.type="upper"</code> then <code>lcl.rank</code> is set to <code>0</code> by default, 
otherwise it is set to <code>1</code> by default.  
When <code>ci.type="lower"</code> then <code>n.plus.one.minus.ucl.rank</code> is set 
to <code>0</code> by default, otherwise it is set to <code>1</code> by default.
</p>
</td></tr>
<tr valign="top"><td><code>ci.type</code></td>
<td>

<p>character string indicating what kind of confidence interval to compute.  The 
possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If the arguments <code>n</code>, <code>p</code>, <code>lcl.rank</code>, and 
<code>n.plus.one.minus.ucl.rank</code> are not all the same length, they are 
replicated to be the 
same length as the length of the longest argument.
</p>
<p>The help file for <code><a href="../../EnvStats/help/eqnpar.html">eqnpar</a></code> explains how nonparametric confidence 
intervals for quantiles are constructed and how the confidence level 
associated with the confidence interval is computed based on specified values 
for the sample size and the ranks of the order statistics used for 
the bounds of the confidence interval.  
</p>


<h3>Value</h3>

<p>A numeric vector of confidence levels.
</p>


<h3>Note</h3>

<p>See the help file for <code><a href="../../EnvStats/help/eqnpar.html">eqnpar</a></code>.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>See the help file for <code><a href="../../EnvStats/help/eqnpar.html">eqnpar</a></code>.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/eqnpar.html">eqnpar</a></code>, <code><a href="../../EnvStats/help/ciNparN.html">ciNparN</a></code>, 
<code><a href="../../EnvStats/help/plotCiNparDesign.html">plotCiNparDesign</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Look at how the confidence level of a nonparametric confidence interval 
  # increases with increasing sample size for a fixed quantile:

  seq(5, 25, by = 5) 
  #[1] 5 10 15 20 25 

  round(ciNparConfLevel(n = seq(5, 25, by = 5), p = 0.9), 2) 
  #[1] 0.41 0.65 0.79 0.88 0.93

  #---------

  # Look at how the confidence level of a nonparametric confidence interval 
  # decreases as the quantile moves away from 0.5:

  seq(0.5, 0.9, by = 0.1) 
  #[1] 0.5 0.6 0.7 0.8 0.9

  round(ciNparConfLevel(n = 10, p = seq(0.5, 0.9, by = 0.1)), 2) 
  #[1] 1.00 0.99 0.97 0.89 0.65

  #==========

  # Reproduce Example 21-6 on pages 21-21 to 21-22 of USEPA (2009).  
  # Use 12 measurements of nitrate (mg/L) at a well used for drinking water 
  # to determine with 95% confidence whether or not the infant-based, acute 
  # risk standard of 10 mg/L has been violated.  Assume that the risk 
  # standard represents an upper 95'th percentile limit on nitrate 
  # concentrations.  So what we need to do is construct a one-sided 
  # lower nonparametric confidence interval for the 95'th percentile 
  # that has associated confidence level of no more than 95%, and we will 
  # compare the lower confidence limit with the MCL of 10 mg/L.  
  #
  # The data for this example are stored in EPA.09.Ex.21.6.nitrate.df.

  # Look at the data:
  #------------------

  EPA.09.Ex.21.6.nitrate.df
  #   Sampling.Date       Date Nitrate.mg.per.l.orig Nitrate.mg.per.l Censored
  #1      7/28/1999 1999-07-28                  &lt;5.0              5.0     TRUE
  #2       9/3/1999 1999-09-03                  12.3             12.3    FALSE
  #3     11/24/1999 1999-11-24                  &lt;5.0              5.0     TRUE
  #4       5/3/2000 2000-05-03                  &lt;5.0              5.0     TRUE
  #5      7/14/2000 2000-07-14                   8.1              8.1    FALSE
  #6     10/31/2000 2000-10-31                  &lt;5.0              5.0     TRUE
  #7     12/14/2000 2000-12-14                    11             11.0    FALSE
  #8      3/27/2001 2001-03-27                  35.1             35.1    FALSE
  #9      6/13/2001 2001-06-13                  &lt;5.0              5.0     TRUE
  #10     9/16/2001 2001-09-16                  &lt;5.0              5.0     TRUE
  #11    11/26/2001 2001-11-26                   9.3              9.3    FALSE
  #12      3/2/2002 2002-03-02                  10.3             10.3    FALSE

  # Determine what order statistic to use for the lower confidence limit
  # in order to achieve no more than 95% confidence.
  #---------------------------------------------------------------------

  conf.levels &lt;- ciNparConfLevel(n = 12, p = 0.95, lcl.rank = 1:12, 
    ci.type = "lower")
  names(conf.levels) &lt;- 1:12

  round(conf.levels, 2)
  #   1    2    3    4    5    6    7    8    9   10   11   12 
  #1.00 1.00 1.00 1.00 1.00 1.00 1.00 1.00 1.00 0.98 0.88 0.54 
  
  # Using the 11'th largest observation for the lower confidence limit 
  # yields a confidence level of 88%.  Using the 10'th largest 
  # observation yields a confidence level of 98%.  The example in 
  # USEPA (2009) uses the 10'th largest observation.
  #
  # The 10'th largest observation is 11 mg/L which exceeds the 
  # MCL of 10 mg/L, so there is evidence of contamination.
  #--------------------------------------------------------------------

  with(EPA.09.Ex.21.6.nitrate.df, 
    eqnpar(Nitrate.mg.per.l, p = 0.95, ci = TRUE, 
      ci.type = "lower", lcl.rank = 10))

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            None
  #
  #Estimated Quantile(s):           95'th %ile = 22.56
  #
  #Quantile Estimation Method:      Nonparametric
  #
  #Data:                            Nitrate.mg.per.l
  #
  #Sample Size:                     12
  #
  #Confidence Interval for:         95'th %ile
  #
  #Confidence Interval Method:      exact
  #
  #Confidence Interval Type:        lower
  #
  #Confidence Level:                98.04317%
  #
  #Confidence Limit Rank(s):        10 
  #
  #Confidence Interval:             LCL =  11
  #                                 UCL = Inf

  #==========

  # Clean up
  #---------
  rm(conf.levels)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
