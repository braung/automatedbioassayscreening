<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Tolerance Interval for a Gamma Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for tolIntGamma {EnvStats}"><tr><td>tolIntGamma {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Tolerance Interval for a Gamma Distribution
</h2>

<h3>Description</h3>

<p>Construct a <i>&beta;</i>-content or <i>&beta;</i>-expectation tolerance 
interval for a <a href="../../stats/help/GammaDist.html">gamma distribution</a>.
</p>


<h3>Usage</h3>

<pre>
  tolIntGamma(x, coverage = 0.95, cov.type = "content", 
    ti.type = "two-sided", conf.level = 0.95, method = "exact", 
    est.method = "mle", normal.approx.transform = "kulkarni.powar")

  tolIntGammaAlt(x, coverage = 0.95, cov.type = "content", 
    ti.type = "two-sided", conf.level = 0.95, method = "exact", 
    est.method = "mle", normal.approx.transform = "kulkarni.powar")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of non-negative observations. Missing (<code>NA</code>), undefined (<code>NaN</code>), and 
infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>coverage</code></td>
<td>

<p>a scalar between 0 and 1 indicating the desired coverage of the tolerance interval.  
The default value is <code>coverage=0.95</code>.  If <code>cov.type="expectation"</code>, 
this argument is ignored.
</p>
</td></tr>
<tr valign="top"><td><code>cov.type</code></td>
<td>

<p>character string specifying the coverage type for the tolerance interval.  
The possible values are <code>"content"</code> (<i>&beta;</i>-content; the default), and 
<code>"expectation"</code> (<i>&beta;</i>-expectation).  See the DETAILS section for more 
information.
</p>
</td></tr>
<tr valign="top"><td><code>ti.type</code></td>
<td>

<p>character string indicating what kind of tolerance interval to compute.  
The possible values are <code>"two-sided"</code> (the default), <code>"lower"</code>, and 
<code>"upper"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>a scalar between 0 and 1 indicating the confidence level associated with the tolerance 
interval.  The default value is <code>conf.level=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>method</code></td>
<td>

<p>for the case of a two-sided tolerance interval, a character string specifying the 
method for constructing the two-sided normal distribution tolerance interval using 
the transformed data.  This argument is ignored if <code>ti.type="lower"</code> or 
<code>ti.type="upper"</code>.  The possible values are <code>"exact"</code> (the default) and <br />
<code>"wald.wolfowitz"</code> (the Wald-Wolfowitz approximation).  See the DETAILS section 
of the help file for <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code> for more information.
</p>
</td></tr>
<tr valign="top"><td><code>est.method</code></td>
<td>

<p>character string specifying the method of estimation for the shape and scale 
distribution parameters.  The possible values are 
<code>"mle"</code> (maximum likelihood; the default), 
<code>"bcmle"</code> (bias-corrected mle), <code>"mme"</code> (method of moments), and 
<code>"mmue"</code> (method of moments based on the unbiased estimator of variance). 
See the DETAILS section of the help file for <code><a href="../../EnvStats/help/egamma.html">egamma</a></code> for more information.
</p>
</td></tr>
<tr valign="top"><td><code>normal.approx.transform</code></td>
<td>

<p>character string indicating which power transformation to use.  
Possible values are <code>"kulkarni.powar"</code> (the default), <code>"cube.root"</code>, and <br />
<code>"fourth.root"</code>.  See the DETAILS section for more informaiton.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The function <code>tolIntGamma</code> returns a tolerance interval as well as 
estimates of the shape and scale parameters.  
The function <code>tolIntGammaAlt</code> returns a tolerance interval as well as 
estimates of the mean and coefficient of variation.
</p>
<p>The tolerance interval is computed by 1) using a power transformation on the original 
data to induce approximate normality, 2) using <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code> to compute 
the tolerance interval, and then 3) back-transforming the interval to create a tolerance 
interval on the original scale.  (Krishnamoorthy et al., 2008).  
The value <code>normal.approx.transform="cube.root"</code> uses 
the cube root transformation suggested by Wilson and Hilferty (1931) and used by 
Krishnamoorthy et al. (2008) and Singh et al. (2010b), and the value 
<code>normal.approx.transform="fourth.root"</code> uses the fourth root transformation suggested 
by Hawkins and Wixley (1986) and used by Singh et al. (2010b).  
The default value <code>normal.approx.transform="kulkarni.powar"</code> 
uses the &quot;Optimum Power Normal Approximation Method&quot; of Kulkarni and Powar (2010).  
The &quot;optimum&quot; power <i>p</i> is determined by:
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
  <i>p = -0.0705 - 0.178 \, shape + 0.475 \, &radic;{shape}</i> </td><td style="text-align: left;"> if <i>shape &le; 1.5</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
  <i>p = 0.246</i> </td><td style="text-align: left;"> if <i>shape &gt; 1.5</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
  </td>
</tr>

</table>

<p>where <i>shape</i> denotes the estimate of the shape parameter.  Although 
Kulkarni and Powar (2010) use the maximum likelihood estimate of shape to 
determine the power <i>p</i>, for the functions <br />
<code>tolIntGamma</code> and <code>tolIntGammaAlt</code> the power <i>p</i> is based on 
whatever estimate of shape is used (e.g., <code>est.method="mle"</code>, <code>est.method="bcmle"</code>, etc.).
</p>


<h3>Value</h3>

<p>A list of class <code>"estimate"</code> containing the estimated parameters, 
the tolerance interval, and other information.  See <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code> 
for details.
</p>
<p>In addition to the usual components contained in an object of class 
<code>"estimate"</code>, the returned value also includes an additional 
component within the <code>"interval"</code> component:
</p>
<table summary="R valueblock">
<tr valign="top"><td><code>normal.transform.power</code></td>
<td>
<p>the value of the power used to 
transform the original data to approximate normality.</p>
</td></tr>  
</table>


<h3>Warning</h3>

<p>It is possible for the lower tolerance limit based on the transformed data to be less than 0.  
In this case, the lower tolerance limit on the original scale is set to 0 and a warning is 
issued stating that the normal approximation is not accurate in this case.
</p>


<h3>Note</h3>

<p>The gamma distribution takes values on the positive real line. 
Special cases of the gamma are the <a href="../../stats/html/Exponential.html">exponential</a> distribution and 
the <a href="../../stats/html/Chisquare.html">chi-square</a> distributions. Applications of the gamma include 
life testing, statistical ecology, queuing theory, inventory control, and precipitation 
processes. A gamma distribution starts to resemble a normal distribution as the 
shape parameter a tends to infinity.
</p>
<p>Some EPA guidance documents (e.g., Singh et al., 2002; Singh et al., 2010a,b) strongly 
recommend against using a lognormal model for environmental data and recommend trying a 
gamma distribuiton instead.
</p>
<p>Tolerance intervals have long been applied to quality control and 
life testing problems (Hahn, 1970b,c; Hahn and Meeker, 1991).  References that 
discuss tolerance intervals in the context of environmental monitoring include:  
Berthouex and Brown (2002, Chapter 21), Gibbons et al. (2009), 
Millard and Neerchal (2001, Chapter 6), Singh et al. (2010b), and USEPA (2009).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Berthouex, P.M., and L.C. Brown. (2002). <em>Statistics for Environmental Engineers</em>. 
Lewis Publishers, Boca Raton.
</p>
<p>Draper, N., and H. Smith. (1998). <em>Applied Regression Analysis</em>. Third Edition. 
John Wiley and Sons, New York.
</p>
<p>Ellison, B.E. (1964). On Two-Sided Tolerance Intervals for a Normal Distribution. 
<em>Annals of Mathematical Statistics</em> <b>35</b>, 762-772.
</p>
<p>Evans, M., N. Hastings, and B. Peacock. (1993). <em>Statistical Distributions</em>. 
Second Edition. John Wiley and Sons, New York, Chapter 18.
</p>
<p>Gibbons, R.D., D.K. Bhaumik, and S. Aryal. (2009). 
<em>Statistical Methods for Groundwater Monitoring</em>, Second Edition.  
John Wiley &amp; Sons, Hoboken.
</p>
<p>Guttman, I. (1970). <em>Statistical Tolerance Regions: Classical and Bayesian</em>. 
Hafner Publishing Co., Darien, CT.
</p>
<p>Hahn, G.J. (1970b). Statistical Intervals for a Normal Population, Part I: Tables, Examples 
and Applications. <em>Journal of Quality Technology</em> <b>2</b>(3), 115-125.
</p>
<p>Hahn, G.J. (1970c). Statistical Intervals for a Normal Population, Part II: Formulas, Assumptions, 
Some Derivations. <em>Journal of Quality Technology</em> <b>2</b>(4), 195-206.
</p>
<p>Hahn, G.J., and W.Q. Meeker. (1991). <em>Statistical Intervals: A Guide for Practitioners</em>. 
John Wiley and Sons, New York.
</p>
<p>Hawkins, D. M., and R.A.J. Wixley. (1986). A Note on the Transformation of 
Chi-Squared Variables to Normality. <em>The American Statistician</em>, <b>40</b>, 
296&ndash;298.
</p>
<p>Johnson, N.L., S. Kotz, and N. Balakrishnan. (1994). 
<em>Continuous Univariate Distributions, Volume 1</em>. Second Edition. 
John Wiley and Sons, New York, Chapter 17.
</p>
<p>Krishnamoorthy K., T. Mathew, and S. Mukherjee. (2008). Normal-Based Methods for a 
Gamma Distribution: Prediction and Tolerance Intervals and Stress-Strength Reliability. 
<em>Technometrics</em>, <b>50</b>(1), 69&ndash;78.
</p>
<p>Krishnamoorthy K., and T. Mathew. (2009). 
<em>Statistical Tolerance Regions: Theory, Applications, and Computation</em>. 
John Wiley and Sons, Hoboken.
</p>
<p>Kulkarni, H.V., and S.K. Powar. (2010). A New Method for Interval Estimation of the Mean 
of the Gamma Distribution. <em>Lifetime Data Analysis</em>, <b>16</b>, 431&ndash;447.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton.
</p>
<p>Singh, A., A.K. Singh, and R.J. Iaci. (2002). 
<em>Estimation of the Exposure Point Concentration Term Using a Gamma Distribution</em>.  
EPA/600/R-02/084. October 2002. Technology Support Center for Monitoring and 
Site Characterization, Office of Research and Development, Office of Solid Waste and 
Emergency Response, U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Singh, A., R. Maichle, and N. Armbya. (2010a). 
<em>ProUCL Version 4.1.00 User Guide (Draft)</em>. EPA/600/R-07/041, May 2010. 
Office of Research and Development, U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Singh, A., N. Armbya, and A. Singh. (2010b). 
<em>ProUCL Version 4.1.00 Technical Guide (Draft)</em>. EPA/600/R-07/041, May 2010.  
Office of Research and Development, U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>Wilson, E.B., and M.M. Hilferty. (1931). The Distribution of Chi-Squares. 
<em>Proceedings of the National Academy of Sciences</em>, <b>17</b>, 684&ndash;688.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>
<p>USEPA. (2010).  <em>Errata Sheet - March 2009 Unified Guidance</em>.
EPA 530/R-09-007a, August 9, 2010.  Office of Resource Conservation and Recovery, Program Information and Implementation Division.
U.S. Environmental Protection Agency, Washington, D.C.
</p>


<h3>See Also</h3>

<p><code><a href="../../stats/html/GammaDist.html">GammaDist</a></code>, <code><a href="../../EnvStats/help/estimate.object.html">estimate.object</a></code>, 
<code><a href="../../EnvStats/help/egamma.html">egamma</a></code>, <code><a href="../../EnvStats/help/tolIntNorm.html">tolIntNorm</a></code>, 
<code><a href="../../EnvStats/help/predIntGamma.html">predIntGamma</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a gamma distribution with parameters 
  # shape=3 and scale=2, then create a tolerance interval. 
  # (Note: the call to set.seed simply allows you to reproduce this 
  # example.)

  set.seed(250) 
  dat &lt;- rgamma(20, shape = 3, scale = 2) 
  tolIntGamma(dat)

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Gamma
  #
  #Estimated Parameter(s):          shape = 2.203862
  #                                 scale = 2.174928
  #
  #Estimation Method:               mle
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Tolerance Interval Coverage:     95%
  #
  #Coverage Type:                   content
  #
  #Tolerance Interval Method:       Exact using
  #                                 Kulkarni &amp; Powar (2010)
  #                                 transformation to Normality
  #                                 based on mle of 'shape'
  #
  #Tolerance Interval Type:         two-sided
  #
  #Confidence Level:                95%
  #
  #Number of Future Observations:   1
  #
  #Tolerance Interval:              LTL =  0.2340438
  #                                 UTL = 21.2996464

  #--------------------------------------------------------------------

  # Using the same data as in the previous example, create an upper 
  # one-sided tolerance interval and use the bias-corrected estimate of 
  # shape.

  tolIntGamma(dat, ti.type = "upper", est.method = "bcmle")

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Gamma
  #
  #Estimated Parameter(s):          shape = 1.906616
  #                                 scale = 2.514005
  #
  #Estimation Method:               bcmle
  #
  #Data:                            dat
  #
  #Sample Size:                     20
  #
  #Tolerance Interval Coverage:     95%
  #
  #Coverage Type:                   content
  #
  #Tolerance Interval Method:       Exact using
  #                                 Kulkarni &amp; Powar (2010)
  #                                 transformation to Normality
  #                                 based on bcmle of 'shape'
  #
  #Tolerance Interval Type:         upper
  #
  #Confidence Level:                95%
  #
  #Tolerance Interval:              LTL =  0.00000
  #                                 UTL = 17.72107

  #----------

  # Clean up
  rm(dat)
  
  #--------------------------------------------------------------------

  # Example 17-3 of USEPA (2009, p. 17-17) shows how to construct a 
  # beta-content upper tolerance limit with 95% coverage and 95% 
  # confidence  using chrysene data and assuming a lognormal 
  # distribution.  Here we will use the same chrysene data but assume a 
  # gamma distribution.

  attach(EPA.09.Ex.17.3.chrysene.df)
  Chrysene &lt;- Chrysene.ppb[Well.type == "Background"]

  #----------
  # First perform a goodness-of-fit test for a gamma distribution

  gofTest(Chrysene, dist = "gamma")

  #Results of Goodness-of-Fit Test
  #-------------------------------
  #
  #Test Method:                     Shapiro-Wilk GOF Based on 
  #                                 Chen &amp; Balakrisnan (1995)
  #
  #Hypothesized Distribution:       Gamma
  #
  #Estimated Parameter(s):          shape = 2.806929
  #                                 scale = 5.286026
  #
  #Estimation Method:               mle
  #
  #Data:                            Chrysene
  #
  #Sample Size:                     8
  #
  #Test Statistic:                  W = 0.9156306
  #
  #Test Statistic Parameter:        n = 8
  #
  #P-value:                         0.3954223
  #
  #Alternative Hypothesis:          True cdf does not equal the
  #                                 Gamma Distribution.

  #----------
  # Now compute the upper tolerance limit

  tolIntGamma(Chrysene, ti.type = "upper", coverage = 0.95, 
    conf.level = 0.95)

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Gamma
  #
  #Estimated Parameter(s):          shape = 2.806929
  #                                 scale = 5.286026
  #
  #Estimation Method:               mle
  #
  #Data:                            Chrysene
  #
  #Sample Size:                     8
  #
  #Tolerance Interval Coverage:     95%
  #
  #Coverage Type:                   content
  #
  #Tolerance Interval Method:       Exact using
  #                                 Kulkarni &amp; Powar (2010)
  #                                 transformation to Normality
  #                                 based on mle of 'shape'
  #
  #Tolerance Interval Type:         upper
  #
  #Confidence Level:                95%
  #
  #Tolerance Interval:              LTL =  0.00000
  #                                 UTL = 69.32425

  #----------
  # Compare this upper tolerance limit of 69 ppb to the upper tolerance limit 
  # assuming a lognormal distribution.

  tolIntLnorm(Chrysene, ti.type = "upper", coverage = 0.95, 
    conf.level = 0.95)$interval$limits["UTL"]

  #    UTL 
  #90.9247

  #----------
  # Clean up

  rm(Chrysene)
  detach("EPA.09.Ex.17.3.chrysene.df")

  #--------------------------------------------------------------------

  # Reproduce some of the example on page 73 of 
  # Krishnamoorthy et al. (2008), which uses alkalinity concentrations 
  # reported in Gibbons (1994) and Gibbons et al. (2009) to construct 
  # two-sided and one-sided upper tolerance limits for various values 
  # of coverage using a 95% confidence level.

  tolIntGamma(Gibbons.et.al.09.Alkilinity.vec, ti.type = "upper", 
    coverage = 0.9, normal.approx.transform = "cube.root")

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Gamma
  #
  #Estimated Parameter(s):          shape = 9.375013
  #                                 scale = 6.202461
  #
  #Estimation Method:               mle
  #
  #Data:                            Gibbons.et.al.09.Alkilinity.vec
  #
  #Sample Size:                     27
  #
  #Tolerance Interval Coverage:     90%
  #
  #Coverage Type:                   content
  #
  #Tolerance Interval Method:       Exact using
  #                                 Wilson &amp; Hilferty (1931) cube-root
  #                                 transformation to Normality
  #
  #Tolerance Interval Type:         upper
  #
  #Confidence Level:                95%
  #
  #Tolerance Interval:              LTL =  0.00000
  #                                 UTL = 97.70502

  tolIntGamma(Gibbons.et.al.09.Alkilinity.vec,  
    coverage = 0.99, normal.approx.transform = "cube.root")

  #Results of Distribution Parameter Estimation
  #--------------------------------------------
  #
  #Assumed Distribution:            Gamma
  #
  #Estimated Parameter(s):          shape = 9.375013
  #                                 scale = 6.202461
  #
  #Estimation Method:               mle
  #
  #Data:                            Gibbons.et.al.09.Alkilinity.vec
  #
  #Sample Size:                     27
  #
  #Tolerance Interval Coverage:     99%
  #
  #Coverage Type:                   content
  #
  #Tolerance Interval Method:       Exact using
  #                                 Wilson &amp; Hilferty (1931) cube-root
  #                                 transformation to Normality
  #
  #Tolerance Interval Type:         two-sided
  #
  #Confidence Level:                95%
  #
  #Tolerance Interval:              LTL =  13.14318
  #                                 UTL = 148.43876
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
