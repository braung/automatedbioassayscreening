<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Randomization (Permutation) Test to Compare Two Proportions...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for twoSamplePermutationTestProportion {EnvStats}"><tr><td>twoSamplePermutationTestProportion {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Randomization (Permutation) Test to Compare Two Proportions (Fisher's Exact Test)
</h2>

<h3>Description</h3>

<p>Perform a two-sample randomization (permutation) test to compare two proportions.  
This is also called Fisher's exact test.
</p>
<p><b>Note:</b> You can perform Fisher's exact test in <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> using the function 
<code><a href="../../stats/html/fisher.test.html">fisher.test</a></code>.
</p>


<h3>Usage</h3>

<pre>
  twoSamplePermutationTestProportion(x, y, x.and.y = "Binomial Outcomes", 
    alternative = "two.sided", tol = sqrt(.Machine$double.eps))
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x, y</code></td>
<td>

<p>When <code>x.and.y="Binomial Outcomes"</code> (the default), <code>x</code> and <code>y</code> are 
vectors of observations from groups 1 and 2, respectively.  The vectors <code>x</code> 
and <code>y</code> must contain no more than 2 unique values (e.g., <code>0</code> and <code>1</code>, 
<code>FALSE</code> and <code>TRUE</code>, <code>"No"</code> and <code>"Yes"</code>, etc.).  In this case, 
the result of <code>sort(unique(x))[2]</code> is taken to be the value that indicates a 
&ldquo;success&rdquo; for <code>x</code> and the result of <code>sort(unique(y))[2]</code> is taken 
to be the value that indicates a &ldquo;success&rdquo; for <code>y</code>.  For example, <br />
<code>x = c(FALSE, TRUE, FALSE, TRUE, TRUE)</code> indicates 3 successes in 5 trials, and 
<code>y = c(1, 0, 0, 0)</code> indicates 1 success in 4 trials.  When <br />
<code>x.and.y="Binomial Outcomes"</code>, missing (<code>NA</code>), undefined (<code>NaN</code>), and 
infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed. 
</p>
<p>When <code>x.and.y="Number of Successes and Trials"</code>, <code>x</code> must be a vector of 
length 2 containing the number of successes for groups 1 and 2, respectively, and 
<code>y</code> must be a vector of length 2 that contains the number of trials for groups 
1 and 2, respectively.  For example, <code>x = c(3, 1)</code> and <code>y = c(5, 4)</code> 
indicates 3 successes in 5 trials for group 1 and 1 success in 4 trials for 
group 2.
</p>
</td></tr>
<tr valign="top"><td><code>x.and.y</code></td>
<td>

<p>character string indicating the kind of data stored in the vectors <code>x</code> and 
<code>y</code>.  The possible values are <code>x.and.y="Binomial Outcomes"</code> (the default), 
and <br />
<code>x.and.y="Number Successes and Trials"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>alternative</code></td>
<td>

<p>character string indicating the kind of alternative hypothesis.  The possible values 
are <code>"two.sided"</code> (the default), <code>"less"</code>, and <code>"greater"</code>. 
</p>
</td></tr>
<tr valign="top"><td><code>tol</code></td>
<td>

<p>numeric scalar indicating the tolerance to use for computing the p-value for the 
two-sample permutation test.  The default value is <br />
<code>tol=sqrt(.Machine$double.eps)</code>.  See the DETAILS section below for more 
information.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p><em>Randomization Tests</em> <br />
In 1935, R.A. Fisher introduced the idea of a <b><em>randomization test</em></b> 
(Manly, 2007, p. 107; Efron and Tibshirani, 1993, Chapter 15), which is based on 
trying to answer the question:  &ldquo;Did the observed pattern happen by chance, 
or does the pattern indicate the null hypothesis is not true?&rdquo;  A randomization 
test works by simply enumerating all of the possible outcomes under the null 
hypothesis, then seeing where the observed outcome fits in.  A randomization test 
is also called a <b><em>permutation test</em></b>, because it involves permuting the 
observations during the enumeration procedure (Manly, 2007, p. 3).
</p>
<p>In the past, randomization tests have not been used as extensively as they are now 
because of the &ldquo;large&rdquo; computing resources needed to enumerate all of the 
possible outcomes, especially for large sample sizes.  The advent of more powerful 
personal computers and software has allowed randomization tests to become much 
easier to perform.  Depending on the sample size, however, it may still be too 
time consuming to enumerate all possible outcomes.  In this case, the randomization 
test can still be performed by sampling from the randomization distribution, and 
comparing the observed outcome to this sampled permutation distribution.
<br />
</p>
<p><em>Two-Sample Randomization Test for Proportions</em> <br />
Let <i>\underline{x} = x_1, x_2, &hellip;, x_{n_1}</i> be a vector of <i>n_1</i> 
independent and identically distributed (i.i.d.) observations
from a <a href="../../stats/help/Binomial.html">binomial distribution</a> with parameter <code>size=1</code> and 
probability of success <code>prob=</code><i>p_1</i>, and let 
<i>\underline{y} = y_1, y_2, &hellip;, y_{n_2}</i> be a vector of <i>n_2</i> 
i.i.d. observations from a <a href="../../stats/help/Binomial.html">binomial distribution</a> with 
parameter <code>size=1</code> and probability of success <code>prob=</code><i>p_2</i>.
</p>
<p>Consider the test of the null hypothesis: 
</p>
<p style="text-align: center;"><i>H_0: p_1 = p_2 \;\;\;\;\;\; (1)</i></p>

<p>The three possible alternative hypotheses are the upper one-sided alternative 
(<code>alternative="greater"</code>)
</p>
<p style="text-align: center;"><i>H_a: p_1 &gt; p_2 \;\;\;\;\;\; (2)</i></p>

<p>the lower one-sided alternative (<code>alternative="less"</code>)
</p>
<p style="text-align: center;"><i>H_a: p_1 &lt; p_2 \;\;\;\;\;\; (3)</i></p>

<p>and the two-sided alternative
</p>
<p style="text-align: center;"><i>H_a: p_1 \ne p_2 \;\;\;\;\;\; (4)</i></p>

<p>To perform the test of the null hypothesis (1) versus any of the three 
alternatives (2)-(4), you can use the two-sample permutation test, which is also 
called <a href="../../stats/help/fisher.test.html">Fisher's exact test</a>.  When the observations are 
from a B(1, <i>p</i>) distribution, the sample mean is an estimate of <i>p</i>.  
Fisher's exact test is simply a permutation test for the difference between two 
means from two different groups (see <code><a href="../../EnvStats/help/twoSamplePermutationTestLocation.html">twoSamplePermutationTestLocation</a></code>), 
where the underlying populations are binomial with size parameter <code>size=1</code>, 
but possibly different values of the <code>prob</code> parameter <i>p</i>.  
Fisher's exact test is usually described in terms of testing hypotheses concerning 
a 2 x 2 contingency table (van Bell et al., 2004, p. 157; 
Hollander and Wolfe, 1999, p. 473; Sheskin, 2011; Zar, 2010, p. 561).  
The probabilities associated with the permutation distribution can be computed by 
using the <a href="../../stats/help/Hypergeometric.html">hypergeometric distribution</a>.
</p>


<h3>Value</h3>

<p>A list of class <code>"permutationTest"</code> containing the results of the hypothesis 
test.  See the help file for <code><a href="../../EnvStats/help/permutationTest.object.html">permutationTest.object</a></code> for details.
</p>


<h3>Note</h3>

<p>Sometimes in environmental data analysis we are interested in determining whether 
two probabilities or rates or proportions differ from each other.  For example, 
we may ask the question: &ldquo;Does exposure to pesticide X increase the risk of 
developing cancer Y?&rdquo;, where cancer Y may be liver cancer, stomach cancer, or some 
other kind of cancer.  One way environmental scientists attempt to answer this kind 
of question is by conducting experiments on rodents in which one group (the 
&ldquo;treatment&rdquo; or &ldquo;exposed&rdquo; group) is exposed to the pesticide and the 
other group (the control group) is not.  The incidence of cancer Y in the exposed 
group is compared with the incidence of cancer Y in the control group.  (See 
Rodricks (2007) for a discussion of extrapolating results from experiments involving 
rodents to consequences in humans and the associated difficulties).
</p>
<p>Hypothesis tests you can use to compare proportions or probability of 
&ldquo;success&rdquo; between two groups include Fisher's exact test and the test 
based on the normal approximation (see the <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> help file for 
<code><a href="../../stats/html/prop.test.html">prop.test</a></code>).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Efron, B., and R.J. Tibshirani. (1993).  <em>An Introduction to the Bootstrap</em>.  
Chapman and Hall, New York, Chapter 15.
</p>
<p>Hollander, M., and D.A. Wolfe. (1999). <em>Nonparametric Statistical Methods</em>.  
Second Edition.  John Wiley and Sons, New York, p.473.
</p>
<p>Manly, B.F.J. (2007).  <em>Randomization, Bootstrap and Monte Carlo Methods in 
Biology</em>.  Third Edition. Chapman &amp; Hall, New York, Chapter 6.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001).  
<em>Environmental Statistics with S-PLUS</em>.  CRC Press, Boca Raton, FL, 
pp.441&ndash;446.
</p>
<p>Graham, S.L., K.J. Davis, W.H. Hansen, and C.H. Graham. (1975).  
Effects of Prolonged Ethylene Thiourea Ingestion on the Thyroid of the Rat.  
<em>Food and Cosmetics Toxicology</em>, <b>13</b>(5), 493&ndash;499.
</p>
<p>Rodricks, J.V. (1992).  <em>Calculated Risks: The Toxicity and Human Health 
Risks of Chemicals in Our Environment</em>. Cambridge University 
Press, New York.
</p>
<p>Rodricks, J.V. (2007).  <em>Calculated Risks: The Toxicity and Human Health 
Risks of Chemicals in Our Environment</em>. Second Edition.  Cambridge University 
Press, New York.
</p>
<p>Sheskin, D.J. (2011).  <em>Handbook of Parametric and Nonparametric 
Statistical Procedures</em>  Fifth Edition. CRC Press, Boca Raton, FL.
</p>
<p>van Belle, G., L.D. Fisher, Heagerty, P.J., and Lumley, T. (2004). 
<em>Biostatistics: A Methodology for the Health Sciences, 2nd Edition</em>. 
John Wiley &amp; Sons, New York, p. 157.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. 
Fifth Edition. Prentice-Hall, Upper Saddle River, NJ, 
p. 561.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/permutationTest.object.html">permutationTest.object</a></code>, <code><a href="../../EnvStats/help/plot.permutationTest.html">plot.permutationTest</a></code>, <br />
<code><a href="../../EnvStats/help/twoSamplePermutationTestLocation.html">twoSamplePermutationTestLocation</a></code>,   
<code><a href="../../EnvStats/help/oneSamplePermutationTest.html">oneSamplePermutationTest</a></code>, 
<a href="../../EnvStats/help/Hypothesis+20Tests.html">Hypothesis Tests</a>, <code><a href="../../boot/help/boot.html">boot</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 10 observations from a binomial distribution with parameters 
  # size=1 and prob=0.3, and 20 observations from a binomial distribution 
  # with parameters size=1 and prob=0.5.  Test the null hypothesis that the 
  # probability of "success" for the two distributions is the same against the 
  # alternative that the probability of "success" for group 1 is less than 
  # the probability of "success" for group 2. 
  # (Note: the call to set.seed allows you to reproduce this example).

  set.seed(23) 
  dat1 &lt;- rbinom(10, size = 1, prob = 0.3) 
  dat2 &lt;- rbinom(20, size = 1, prob = 0.5) 

  test.list &lt;- twoSamplePermutationTestProportion(
    dat1, dat2, alternative = "less") 

  #----------

  # Print the results of the test 
  #------------------------------
  test.list 

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 p.x - p.y = 0
  #
  #Alternative Hypothesis:          True p.x - p.y is less than 0
  #
  #Test Name:                       Two-Sample Permutation Test
  #                                 Based on Differences in Proportions
  #                                 (Fisher's Exact Test)
  #
  #Estimated Parameter(s):          p.hat.x = 0.60
  #                                 p.hat.y = 0.65
  #
  #Data:                            x = dat1
  #                                 y = dat2
  #
  #Sample Sizes:                    nx = 10
  #                                 ny = 20
  #
  #Test Statistic:                  p.hat.x - p.hat.y = -0.05
  #
  #P-value:                         0.548026

  #----------

  # Plot the results of the test 
  #------------------------------
  dev.new()
  plot(test.list)

  #----------

  # Compare to the results of fisher.test
  #--------------------------------------
  x11 &lt;- sum(dat1)
  x21 &lt;- length(dat1) - sum(dat1)
  x12 &lt;- sum(dat2)
  x22 &lt;- length(dat2) - sum(dat2)
  mat &lt;- matrix(c(x11, x12, x21, x22), ncol = 2)
  fisher.test(mat, alternative = "less")

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 odds ratio = 1
  #
  #Alternative Hypothesis:          True odds ratio is less than 1
  #
  #Test Name:                       Fisher's Exact Test for Count Data
  #
  #Estimated Parameter(s):          odds ratio = 0.8135355
  #
  #Data:                            mat
  #
  #P-value:                         0.548026
  #
  #95% Confidence Interval:         LCL = 0.000000
  #                                 UCL = 4.076077

  #==========

  # Rodricks (1992, p. 133) presents data from an experiment by 
  # Graham et al. (1975) in which different groups of rats were exposed to 
  # various concentration levels of ethylene thiourea (ETU), a decomposition 
  # product of a certain class of fungicides that can be found in treated foods.  
  # In the group exposed to a dietary level of 250 ppm of ETU, 16 out of 69 rats 
  # (23%) developed thyroid tumors, whereas in the control group 
  # (no exposure to ETU) only 2 out of 72 (3%) rats developed thyroid tumors.  
  # If we use Fisher's exact test to test the null hypothesis that the proportion 
  # of rats exposed to 250 ppm of ETU who will develop thyroid tumors over their 
  # lifetime is no greater than the proportion of rats not exposed to ETU who will 
  # develop tumors, we get a one-sided upper p-value of 0.0002.  Therefore, we 
  # conclude that the true underlying rate of tumor incidence in the exposed group 
  # is greater than in the control group.
  #
  # The data for this example are stored in Graham.et.al.75.etu.df.

  # Look at the data
  #-----------------

  Graham.et.al.75.etu.df
  #  dose tumors  n proportion
  #1    0      2 72 0.02777778
  #2    5      2 75 0.02666667
  #3   25      1 73 0.01369863
  #4  125      2 73 0.02739726
  #5  250     16 69 0.23188406
  #6  500     62 70 0.88571429

  # Perform the test for a difference in tumor rates
  #-------------------------------------------------

  Num.Tumors &lt;- with(Graham.et.al.75.etu.df, tumors[c(5, 1)])
  Sample.Sizes &lt;- with(Graham.et.al.75.etu.df, n[c(5, 1)]) 

  test.list &lt;- twoSamplePermutationTestProportion( 
    x = Num.Tumors, y = Sample.Sizes, 
    x.and.y="Number Successes and Trials", alternative = "greater") 

  #----------

  # Print the results of the test 
  #------------------------------
  test.list 

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 p.x - p.y = 0
  #
  #Alternative Hypothesis:          True p.x - p.y is greater than 0
  #
  #Test Name:                       Two-Sample Permutation Test
  #                                 Based on Differences in Proportions
  #                                 (Fisher's Exact Test)
  #
  #Estimated Parameter(s):          p.hat.x = 0.23188406
  #                                 p.hat.y = 0.02777778
  #
  #Data:                            x = Num.Tumors  
  #                                 n = Sample.Sizes
  #
  #Sample Sizes:                    nx = 69
  #                                 ny = 72
  #
  #Test Statistic:                  p.hat.x - p.hat.y = 0.2041063
  #
  #P-value:                         0.0002186462

  #----------

  # Plot the results of the test 
  #------------------------------
  dev.new()
  plot(test.list)

  #==========

  # Clean up
  #---------
  rm(test.list, x11, x12, x21, x22, mat, Num.Tumors, Sample.Sizes)
  #graphics.off()
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
