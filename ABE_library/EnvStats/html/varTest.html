<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: One-Sample Chi-Squared Test on Variance</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for varTest {EnvStats}"><tr><td>varTest {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
One-Sample Chi-Squared Test on Variance
</h2>

<h3>Description</h3>

<p>Estimate the variance, test the null hypothesis using the chi-squared test that the variance is equal 
to a user-specified value, and create a confidence interval for the variance.
</p>


<h3>Usage</h3>

<pre>
  varTest(x, alternative = "two.sided", conf.level = 0.95, 
    sigma.squared = 1, data.name = NULL)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>numeric vector of observations.  Missing (<code>NA</code>), undefined (<code>NaN</code>), and 
infinite (<code>Inf</code>, <code>-Inf</code>) values are allowed but will be removed.
</p>
</td></tr>
<tr valign="top"><td><code>alternative</code></td>
<td>

<p>character string indicating the kind of alternative hypothesis.  The possible values are 
<code>"two.sided"</code> (the default), <code>"greater"</code>, and <code>"less"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>conf.level</code></td>
<td>

<p>numeric scalar between 0 and 1 indicating the confidence level associated with the confidence 
interval for the population variance.  The default value is <br /> 
<code>conf.level=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>sigma.squared</code></td>
<td>

<p>a numeric scalar indicating the hypothesized value of the variance.  The default value is 
<code>sigma.squared=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>data.name</code></td>
<td>

<p>character string indicating the name of the data used for the test of variance.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The function <code>varTest</code> performs the one-sample chi-squared test of the hypothesis 
that the population variance is equal to the user specified value given by the argument 
<code>sigma.squared</code>, and it also returns a confidence interval for the population variance.  
The <span style="font-family: Courier New, Courier; color: #666666;"><b>R</b></span> function <code><a href="../../stats/html/var.test.html">var.test</a></code> performs the F-test for comparing two variances.
</p>


<h3>Value</h3>

<p>A list of class <code>"htest"</code> containing the results of the hypothesis test.  
See the help file for <code><a href="../../EnvStats/help/htest.object.html">htest.object</a></code> for details.
</p>


<h3>Note</h3>

<p>Just as you can perform tests of hypothesis on measures of location (mean, median, percentile, etc.), 
you can do the same thing for measures of spread or variability.  Usually, we are interested in 
estimating variability only because we want to quantify the uncertainty of our estimated location or 
percentile.  Sometimes, however, we are interested in estimating variability and quantifying the 
uncertainty in our estimate of variability (for example, for performing a sensitivity analysis for 
power or sample size calculations), or testing whether the population variability is equal to a 
certain value.  There are at least two possible methods of performing a one-sample hypothesis test on 
variability:
</p>

<ul>
<li><p> Perform a hypothesis test for the population variance based on the chi-squared statistic, 
assuming the underlying population is normal.
</p>
</li>
<li><p> Perform a hypothesis test for any kind of measure of spread assuming any kind of underlying 
distribution based on a bootstrap confidence interval (using, for example, the 
package <span class="pkg">boot</span>).
</p>
</li></ul>

<p>You can use <code>varTest</code> for the first method.
</p>
<p><b>Note:</b>  For a one-sample test of location, Student's t-test is fairly robust to departures 
from normality (i.e., the Type I error rate is maintained), as long as the sample size is 
reasonably &quot;large.&quot;  The chi-squared test on the population variance, however, is extremely sensitive 
to departures from normality.  For example, if the underlying population is skewed, the actual 
Type I error rate will be larger than assumed.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>van Belle, G., L.D. Fisher, Heagerty, P.J., and Lumley, T. (2004). 
<em>Biostatistics: A Methodology for the Health Sciences, 2nd Edition</em>. 
John Wiley &amp; Sons, New York.
</p>
<p>Millard, S.P., and N.K. Neerchal. (2001). <em>Environmental Statistics with S-PLUS</em>. 
CRC Press, Boca Raton, FL.
</p>
<p>Zar, J.H. (2010). <em>Biostatistical Analysis</em>. Fifth Edition. 
Prentice-Hall, Upper Saddle River, NJ.
</p>


<h3>See Also</h3>

<p><code><a href="../../stats/html/var.test.html">var.test</a></code>, <code><a href="../../EnvStats/help/varGroupTest.html">varGroupTest</a></code>.
</p>


<h3>Examples</h3>

<pre>
  # Generate 20 observations from a normal distribution with parameters 
  # mean=2 and sd=1.  Test the null hypothesis that the true variance is 
  # equal to 0.5 against the alternative that the true variance is not 
  # equal to 0.5.  
  # (Note: the call to set.seed allows you to reproduce this example).

  set.seed(23) 
  dat &lt;- rnorm(20, mean = 2, sd = 1) 
  varTest(dat, sigma.squared = 0.5) 

  #Results of Hypothesis Test
  #--------------------------
  #
  #Null Hypothesis:                 variance = 0.5
  #
  #Alternative Hypothesis:          True variance is not equal to 0.5
  #
  #Test Name:                       Chi-Squared Test on Variance
  #
  #Estimated Parameter(s):          variance = 0.753708
  #
  #Data:                            dat
  #
  #Test Statistic:                  Chi-Squared = 28.64090
  #
  #Test Statistic Parameter:        df = 19
  #
  #P-value:                         0.1436947
  #
  #95% Confidence Interval:         LCL = 0.4359037
  #                                 UCL = 1.6078623

  # Note that in this case we would not reject the 
  # null hypothesis at the 5% or even the 10% level.

  # Clean up
  rm(dat)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
