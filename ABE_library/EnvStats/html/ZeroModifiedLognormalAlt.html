<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: The Zero-Modified Lognormal (Delta) Distribution (Alternative...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for ZeroModifiedLognormalAlt {EnvStats}"><tr><td>ZeroModifiedLognormalAlt {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
The Zero-Modified Lognormal (Delta) Distribution (Alternative Parameterization)
</h2>

<h3>Description</h3>

<p>Density, distribution function, quantile function, and random generation 
for the zero-modified lognormal distribution with parameters <code>mean</code>, 
<code>cv</code>, and <code>p.zero</code>.
</p>
<p>The zero-modified lognormal (delta) distribution is the mixture of a 
lognormal distribution with a positive probability mass at 0.
</p>


<h3>Usage</h3>

<pre>
  dzmlnormAlt(x, mean = exp(1/2), cv = sqrt(exp(1) - 1), p.zero = 0.5)
  pzmlnormAlt(q, mean = exp(1/2), cv = sqrt(exp(1) - 1), p.zero = 0.5)
  qzmlnormAlt(p, mean = exp(1/2), cv = sqrt(exp(1) - 1), p.zero = 0.5)
  rzmlnormAlt(n, mean = exp(1/2), cv = sqrt(exp(1) - 1), p.zero = 0.5)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>q</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>vector of probabilities between 0 and 1.
</p>
</td></tr>
<tr valign="top"><td><code>n</code></td>
<td>

<p>sample size.  If <code>length(n)</code> is larger than 1, then <code>length(n)</code> 
random values are returned.
</p>
</td></tr>
<tr valign="top"><td><code>mean</code></td>
<td>

<p>vector of means of the lognormal part of the distribution on the.  
The default is <code>mean=exp(1/2)</code>.
</p>
</td></tr>
<tr valign="top"><td><code>cv</code></td>
<td>

<p>vector of (positive) coefficients of variation of the lognormal 
part of the distribution.  The default is <code>cv=sqrt(exp(1) - 1)</code>.
</p>
</td></tr>
<tr valign="top"><td><code>p.zero</code></td>
<td>

<p>vector of probabilities between 0 and 1 indicating the probability the random 
variable equals 0.  For <code>rzmlnormAlt</code> this must be a single, non-missing number.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The zero-modified lognormal (delta) distribution is the mixture of a 
lognormal distribution with a positive probability mass at 0.  This distribution 
was introduced without a name by Aitchison (1955), and the name 
<i>&Delta;</i>-distribution was coined by Aitchison and Brown (1957, p.95).  
It is a special case of a &ldquo;zero-modified&rdquo; distribution 
(see Johnson et al., 1992, p. 312).
</p>
<p>Let <i>f(x; &theta;, &tau;)</i> denote the density of a 
<a href="../../EnvStats/help/LognormalAlt.html">lognormal random variable</a> <i>X</i> with parameters 
<code>mean=</code><i>&theta;</i> and <code>cv=</code><i>&tau;</i>.  The density function of a 
zero-modified lognormal (delta) random variable <i>Y</i> with parameters 
<code>mean=</code><i>&theta;</i>, <code>cv=</code><i>&tau;</i>, and <code>p.zero=</code><i>p</i>, 
denoted <i>h(y; &theta;, &tau;, p)</i>, is given by:
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <i>h(y; &theta;, &tau;, p) =</i> </td><td style="text-align: left;">  <i>p</i>  </td><td style="text-align: left;"> for <i>y = 0</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
                                  </td><td style="text-align: left;">  <i>(1 - p) f(y; &theta;, &tau;)</i> </td><td style="text-align: left;"> for <i>y &gt; 0</i>
  </td>
</tr>

</table>

<p>Note that <i>&theta;</i> is <em>not</em> the mean of the zero-modified lognormal 
distribution; it is the mean of the lognormal part of the distribution.  
Similarly, <i>&tau;</i> is <em>not</em> the coefficient of variation of the 
zero-modified lognormal distribution; it is the coefficient of variation of the 
lognormal part of the distribution.
</p>
<p>Let <i>&gamma;</i>, <i>&delta;</i>, and <i>&omega;</i> denote the mean, 
standard deviation, and coefficient of variation of the overall zero-modified 
lognormal distribution.  Let <i>&eta;</i> denote the standard deviation of the 
lognormal part of the distribution, so that <i>&eta; = &theta; &tau;</i>.  
Aitchison (1955) shows that:
</p>
<p style="text-align: center;"><i>E(Y) = &gamma; = (1 - p) &theta;</i></p>

<p style="text-align: center;"><i>Var(Y) = &delta;^2 = (1 - p) &eta;^2 + p (1-p) &theta;^2</i></p>

<p>so that
</p>
<p style="text-align: center;"><i>&omega; = &radic;{(&tau;^2 + p) / (1 - p)}</i></p>

<p>Note that when <code>p.zero=</code><i>p</i><code>=0</code>, the zero-modified lognormal 
distribution simplifies to the lognormal distribution.
</p>


<h3>Value</h3>

<p><code>dzmlnormAlt</code> gives the density, <code>pzmlnormAlt</code> gives the distribution function, 
<code>qzmlnormAlt</code> gives the quantile function, and <code>rzmlnormAlt</code> generates random 
deviates. 
</p>


<h3>Note</h3>

<p>The zero-modified lognormal (delta) distribution is sometimes used to 
model chemical concentrations for which some observations are reported as 
&ldquo;Below Detection Limit&rdquo; (the nondetects are assumed equal to 0).  
See, for example, Gilliom and Helsel (1986), Owen and DeRouen (1980), and 
Gibbons et al. (2009, Chapter 12).  USEPA (2009, Chapter 15) recommends this 
strategy only in specific situations, and Helsel (2012, Chapter 1) strongly 
discourages this approach to dealing with non-detects.
</p>
<p>A variation of the zero-modified lognormal (delta) distribution is the 
<a href="../../EnvStats/help/ZeroModifiedNormal.html">zero-modified normal distribution</a>, in which a 
normal distribution is mixed with a positive probability mass at 0. 
</p>
<p>One way to try to assess whether a zero-modified lognormal (delta), 
zero-modified normal, censored normal, or censored lognormal is the best 
model for the data is to construct both censored and detects-only probability 
plots (see <code><a href="../../EnvStats/help/qqPlotCensored.html">qqPlotCensored</a></code>).
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Aitchison, J. (1955).  On the Distribution of a Positive Random Variable Having 
a Discrete Probability Mass at the Origin.  <em>Journal of the American 
Statistical Association</em> <b>50</b>, 901-908.
</p>
<p>Aitchison, J., and J.A.C. Brown (1957).  <em>The Lognormal Distribution 
(with special reference to its uses in economics)</em>.  Cambridge University Press, 
London. pp.94-99.
</p>
<p>Crow, E.L., and K. Shimizu. (1988).  <em>Lognormal Distributions: 
Theory and Applications</em>.  Marcel Dekker, New York, pp.47-51.
</p>
<p>Gibbons, RD., D.K. Bhaumik, and S. Aryal. (2009).  <em>Statistical Methods 
for Groundwater Monitoring</em>.  Second Edition.  John Wiley and Sons, Hoboken, NJ.
</p>
<p>Gilliom, R.J., and D.R. Helsel. (1986).  Estimation of Distributional Parameters 
for Censored Trace Level Water Quality Data: 1. Estimation Techniques.  
<em>Water Resources Research</em> <b>22</b>, 135-146.
</p>
<p>Helsel, D.R. (2012).  <em>Statistics for Censored Environmental Data Using 
Minitab and R</em>.  Second Edition.  John Wiley and Sons, Hoboken, NJ, Chapter 1.
</p>
<p>Johnson, N. L., S. Kotz, and A.W. Kemp. (1992).  <em>Univariate Discrete Distributions</em>. 
Second Edition. John Wiley and Sons, New York, p.312.
</p>
<p>Owen, W., and T. DeRouen. (1980).  Estimation of the Mean for Lognormal Data 
Containing Zeros and Left-Censored Values, with Applications to the Measurement 
of Worker Exposure to Air Contaminants.  <em>Biometrics</em> <b>36</b>, 707-719.
</p>
<p>USEPA (1992c).  <em>Statistical Analysis of Ground-Water Monitoring Data at 
RCRA Facilities: Addendum to Interim Final Guidance</em>.  Office of Solid Waste, 
Permits and State Programs Division, US Environmental Protection Agency, 
Washington, D.C.
</p>
<p>USEPA. (2009).  <em>Statistical Analysis of Groundwater Monitoring Data at RCRA Facilities, Unified Guidance</em>.
EPA 530/R-09-007, March 2009.  Office of Resource Conservation and Recovery Program Implementation and Information Division.  
U.S. Environmental Protection Agency, Washington, D.C.
</p>


<h3>See Also</h3>

<p><a href="../../EnvStats/help/ZeroModifiedLognormal.html">Zero-Modified Lognormal</a>, <a href="../../EnvStats/help/LognormalAlt.html">LognormalAlt</a>, 
<code><a href="../../EnvStats/help/ezmlnormAlt.html">ezmlnormAlt</a></code>, <a href="../../EnvStats/help/Probability+20Distributions+20and+20Random+20Numbers.html">Probability Distributions and Random Numbers</a>.
</p>


<h3>Examples</h3>

<pre>
  # Density of the zero-modified lognormal (delta) distribution with 
  # parameters mean=10, cv=1, and p.zero=0.5, evaluated at 
  # 9, 10, and 11:

  dzmlnormAlt(9:11, mean = 10, cv = 1, p.zero = 0.5) 
  #[1] 0.02552685 0.02197043 0.01891924

  #----------

  # The cdf of the zero-modified lognormal (delta) distribution with 
  # parameters mean=10, cv=2, and p.zero=0.1, evaluated at 8:

  pzmlnormAlt(8, 10, 2, .1) 
  #[1] 0.709009

  #----------

  # The median of the zero-modified lognormal (delta) distribution with 
  # parameters mean=10, cv=2, and p.zero=0.1:

  qzmlnormAlt(0.5, 10, 2, 0.1) 
  #[1] 3.74576

  #----------

  # Random sample of 3 observations from the zero-modified lognormal 
  # (delta) distribution with parameters mean=10, cv=2, and p.zero=0.4. 
  # (Note: The call to set.seed simply allows you to reproduce this example.)

  set.seed(20) 
  rzmlnormAlt(3, 10, 2, 0.4)
  #[1] 0.000000 0.000000 4.907131
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
