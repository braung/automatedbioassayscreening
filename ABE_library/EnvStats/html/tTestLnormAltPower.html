<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Power of a One- or Two-Sample t-Test Assuming Lognormal Data</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for tTestLnormAltPower {EnvStats}"><tr><td>tTestLnormAltPower {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Power of a One- or Two-Sample t-Test Assuming Lognormal Data
</h2>

<h3>Description</h3>

<p>Compute the power of a one- or two-sample t-test, given the sample size, 
ratio of means, coefficient of variation, and significance level, assuming 
lognormal data.
</p>


<h3>Usage</h3>

<pre>
  tTestLnormAltPower(n.or.n1, n2 = n.or.n1, ratio.of.means = 1, cv = 1, alpha = 0.05, 
    sample.type = ifelse(!missing(n2), "two.sample", "one.sample"), 
    alternative = "two.sided", approx = FALSE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n.or.n1</code></td>
<td>

<p>numeric vector of sample sizes.  When <code>sample.type="one.sample"</code>, 
<code>n.or.n1</code> denotes <i>n</i>, the number of observations in the single sample.  When <br />
<code>sample.type="two.sample"</code>, <code>n.or.n1</code> denotes <i>n_1</i>, the number 
of observations from group 1.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are <b><em>not</em></b> allowed.
</p>
</td></tr>
<tr valign="top"><td><code>n2</code></td>
<td>

<p>numeric vector of sample sizes for group 2.  The default value is the value of 
<code>n.or.n1</code>. This argument is ignored when <code>sample.type="one.sample"</code>. 
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are <b><em>not</em></b> allowed.
</p>
</td></tr>
<tr valign="top"><td><code>ratio.of.means</code></td>
<td>

<p>numeric vector specifying the ratio of the first mean to the second mean.  
When <code>sample.type="one.sample"</code>, this is the ratio of the population mean to the 
hypothesized mean.  When <code>sample.type="two.sample"</code>, this is the ratio of the 
mean of the first population to the mean of the second population.  The default 
value is <code>ratio.of.means=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>cv</code></td>
<td>

<p>numeric vector of positive value(s) specifying the coefficient of 
variation.  When <code>sample.type="one.sample"</code>, this is the population coefficient 
of variation.  When <code>sample.type="two.sample"</code>, this is the coefficient of 
variation for both the first and second population.  The default value is <code>cv=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>alpha</code></td>
<td>

<p>numeric vector of numbers between 0 and 1 indicating the Type I error level 
associated with the hypothesis test.  The default value is <code>alpha=0.05</code>.
</p>
</td></tr>
<tr valign="top"><td><code>sample.type</code></td>
<td>

<p>character string indicating whether to compute power based on a one-sample or 
two-sample hypothesis test.  When <code>sample.type="one.sample"</code>, the computed 
power is based on a hypothesis test for a single mean.  When <br />
<code>sample.type="two.sample"</code>, the computed power is based on a hypothesis test 
for the difference between two means.  The default value is <br />
<code>sample.type="one.sample"</code> unless the argument <code>n2</code> is supplied.
</p>
</td></tr>
<tr valign="top"><td><code>alternative</code></td>
<td>

<p>character string indicating the kind of alternative hypothesis.  The possible values 
are <code>"two.sided"</code> (the default), <code>"greater"</code>, and <code>"less"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>approx</code></td>
<td>

<p>logical scalar indicating whether to compute the power based on an approximation to 
the non-central t-distribution.  The default value is <code>FALSE</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>If the arguments <code>n.or.n1</code>, <code>n2</code>, <code>ratio.of.means</code>, <code>cv</code>, and 
<code>alpha</code> are not all the same length, they are replicated to be the same length 
as the length of the longest argument.
</p>
<p><em>One-Sample Case</em> (<code>sample.type="one.sample"</code>) <br />
Let <i>\underline{x} = x_1, x_2, &hellip;, x_n</i> denote a vector of <i>n</i> 
observations from a <a href="../../EnvStats/help/LognormalAlt.html">lognormal distribution</a> with mean 
<i>&theta;</i> and coefficient of variation <i>&tau;</i>, and consider the null hypothesis:
</p>
<p style="text-align: center;"><i>H_0: &theta; = &theta;_0 \;\;\;\;\;\; (1)</i></p>

<p>The three possible alternative hypotheses are the upper one-sided alternative 
(<code>alternative="greater"</code>):
</p>
<p style="text-align: center;"><i>H_a: &theta; &gt; &theta;_0 \;\;\;\;\;\; (2)</i></p>

<p>the lower one-sided alternative (<code>alternative="less"</code>)
</p>
<p style="text-align: center;"><i>H_a: &theta; &lt; &theta;_0 \;\;\;\;\;\; (3)</i></p>

<p>and the two-sided alternative (<code>alternative="two.sided"</code>)
</p>
<p style="text-align: center;"><i>H_a: &theta; \ne &theta;_0 \;\;\;\;\;\; (4)</i></p>

<p>To test the null hypothesis (1) versus any of the three alternatives (2)-(4), one 
might be tempted to use <a href="../../stats/help/t.test.html">Student's t-test</a> based on the 
log-transformed observations.  Unlike the two-sample case with equal coefficients of 
variation (see below), in the one-sample case Student's t-test applied to the 
log-transformed observations will not test the correct hypothesis, as now explained.
</p>
<p>Let
</p>
<p style="text-align: center;"><i>y_i = log(x_i), \;\; i = 1, 2, &hellip;, n \;\;\;\;\;\; (5)</i></p>

<p>Then <i>\underline{y} = y_1, y_2, &hellip;, y_n</i> denote <i>n</i> observations from a 
normal distribution with mean <i>&mu;</i> and standard deviation <i>&sigma;</i>, where
</p>
<p style="text-align: center;"><i>&mu; = log(\frac{&theta;}{&radic;{&tau;^2 + 1}}) \;\;\;\;\;\; (6)</i></p>

<p style="text-align: center;"><i>&sigma; = [log(&tau;^2 + 1)]^{1/2} \;\;\;\;\;\; (7)</i></p>

<p style="text-align: center;"><i>&theta; = exp[&mu; + (&sigma;^2/2)] \;\;\;\;\;\; (8)</i></p>

<p style="text-align: center;"><i>&tau; = [exp(&sigma;^2) - 1]^{1/2} \;\;\;\;\;\; (9)</i></p>

<p>(see the help file for <a href="../../EnvStats/help/LognormalAlt.html">LognormalAlt</a>).  Hence, by Equations (6) and (8) above, 
the Student's t-test on the log-transformed data would involve a test of hypothesis 
on both the parameters <i>&theta;</i> and <i>&tau;</i>, not just on <i>&theta;</i>.
</p>
<p>To test the null hypothesis (1) above versus any of the alternatives (2)-(4), you 
can use the function <code><a href="../../EnvStats/help/elnormAlt.html">elnormAlt</a></code> to compute a confidence interval for 
<i>&theta;</i>, and use the relationship between confidence intervals and hypothesis 
tests.  To test the null hypothesis (1) above versus the upper one-sided alternative 
(2), you can also use 
<a href="../../EnvStats/help/chenTTest.html">Chen's modified t-test for skewed distributions</a>.
</p>
<p>Although you can't use Student's t-test based on the log-transformed observations to 
test a hypothesis about <i>&theta;</i>, you can use the t-distribution to estimate the 
power of a test about <i>&theta;</i> that is based on confidence intervals or 
Chen's modified t-test, if you are willing to assume the population coefficient of 
variation <i>&tau;</i> stays constant for all possible values of <i>&theta;</i> you are 
interested in, and you are willing to postulate possible values for <i>&tau;</i>.
</p>
<p>First, let's re-write the hypotheses (1)-(4) as follows.  The null hypothesis (1) 
is equivalent to:
</p>
<p style="text-align: center;"><i>H_0: \frac{&theta;}{&theta;_0} = 1 \;\;\;\;\;\; (10)</i></p>

<p>The three possible alternative hypotheses are the upper one-sided alternative 
(<code>alternative="greater"</code>)
</p>
<p style="text-align: center;"><i>H_a: \frac{&theta;}{&theta;_0} &gt; 1 \;\;\;\;\;\; (11)</i></p>

<p>the lower one-sided alternative (<code>alternative="less"</code>)
</p>
<p style="text-align: center;"><i>H_a: \frac{&theta;}{&theta;_0} &lt; 1 \;\;\;\;\;\; (12)</i></p>

<p>and the two-sided alternative (<code>alternative="two.sided"</code>)
</p>
<p style="text-align: center;"><i>H_a: \frac{&theta;}{&theta;_0} \ne 1 \;\;\;\;\;\; (13)</i></p>

<p>For a constant coefficient of variation <i>&tau;</i>, the standard deviation of the 
log-transformed observations <i>&sigma;</i> is also constant (see Equation (7) above).  
Hence, by Equation (8), the ratio of the true mean to the hypothesized mean can be 
written as:
</p>
<p style="text-align: center;"><i>R = \frac{&theta;}{&theta;_0} = \frac{exp[&mu; + (&sigma;^2/2)]}{exp[&mu;_0 + (&sigma;^2/2)]} = \frac{e^&mu;}{e^&mu;_0} = e^{&mu; - &mu;_0} \;\;\;\;\;\; (14)</i></p>

<p>which only involves the difference
</p>
<p style="text-align: center;"><i>&mu; - &mu;_0 \;\;\;\;\;\; (15)</i></p>

<p>Thus, for given values of <i>R</i> and <i>&tau;</i>, the power of the test of the null 
hypothesis (10) against any of the alternatives (11)-(13) can be computed based on 
the power of a one-sample t-test with
</p>
<p style="text-align: center;"><i>\frac{&delta;}{&sigma;} = \frac{log(R)}{&radic;{log(&tau;^2 + 1)}} \;\;\;\;\;\; (16)</i></p>

<p>(see the help file for <code><a href="../../EnvStats/help/tTestPower.html">tTestPower</a></code>).  Note that for the function 
<code>tTestLnormAltPower</code>, <i>R</i> corresponds to the argument <code>ratio.of.means</code>, 
and <i>&tau;</i> corresponds to the argument <code>cv</code>.
<br />
</p>
<p><em>Two-Sample Case</em> (<code>sample.type="two.sample"</code>) <br />
Let <i>\underline{x}_1 = x_{11}, x_{12}, &hellip;, x_{1n_1}</i> denote a vector of 
<i>n_1</i> observations from a <a href="../../EnvStats/help/LognormalAlt.html">lognormal distribution</a> with mean 
<i>&theta;_1</i> and coefficient of variaiton <i>&tau;</i>, and let 
<i>\underline{x}_2 = x_{21}, x_{22}, &hellip;, x_{2n_2}</i> denote a vector of 
<i>n_2</i> observations from a lognormal distribution with mean <i>&theta;_2</i> and 
coefficient of variation <i>&tau;</i>, and consider the null hypothesis:
</p>
<p style="text-align: center;"><i>H_0: &theta;_1 = &theta;_2 \;\;\;\;\;\; (17)</i></p>

<p>The three possible alternative hypotheses are the upper one-sided alternative 
(<code>alternative="greater"</code>):
</p>
<p style="text-align: center;"><i>H_a: &theta;_1 &gt; &theta;_2 \;\;\;\;\;\; (18)</i></p>

<p>the lower one-sided alternative (<code>alternative="less"</code>)
</p>
<p style="text-align: center;"><i>H_a: &theta;_1 &lt; &theta;_2 \;\;\;\;\;\; (19)</i></p>

<p>and the two-sided alternative (<code>alternative="two.sided"</code>)
</p>
<p style="text-align: center;"><i>H_a: &theta;_1 \ne &theta;_2 \;\;\;\;\;\; (20)</i></p>

<p>Because we are assuming the coefficient of variation <i>&tau;</i> is the same for 
both populations, the test of the null hypothesis (17) versus any of the three 
alternatives (18)-(20) can be based on the Student t-statistic using the 
log-transformed observations.
</p>
<p>To show this, first, let's re-write the hypotheses (17)-(20) as follows.  The 
null hypothesis (17) is equivalent to:
</p>
<p style="text-align: center;"><i>H_0: \frac{&theta;_1}{&theta;_2} = 1 \;\;\;\;\;\; (21)</i></p>

<p>The three possible alternative hypotheses are the upper one-sided alternative 
(<code>alternative="greater"</code>)
</p>
<p style="text-align: center;"><i>H_a: \frac{&theta;_1}{&theta;_2} &gt; 1 \;\;\;\;\;\; (22)</i></p>

<p>the lower one-sided alternative (<code>alternative="less"</code>)
</p>
<p style="text-align: center;"><i>H_a: \frac{&theta;_1}{&theta;_2} &lt; 1 \;\;\;\;\;\; (23)</i></p>

<p>and the two-sided alternative (<code>alternative="two.sided"</code>)
</p>
<p style="text-align: center;"><i>H_a: \frac{&theta;_1}{&theta;_2} \ne 1 \;\;\;\;\;\; (24)</i></p>

<p>If coefficient of variation <i>&tau;</i> is the same for both populations, then the 
standard deviation of the log-transformed observations <i>&sigma;</i> is also the 
same for both populations (see Equation (7) above).  Hence, by Equation (8), the 
ratio of the means can be written as:
</p>
<p style="text-align: center;"><i>R = \frac{&theta;_1}{&theta;_2} = \frac{exp[&mu;_1 + (&sigma;^2/2)]}{exp[&mu;_2 + (&sigma;^2/2)]} = \frac{e^&mu;_1}{e^&mu;_2} = e^{&mu;_1 - &mu;_2} \;\;\;\;\;\; (25)</i></p>

<p>which only involves the difference
</p>
<p style="text-align: center;"><i>&mu;_1 - &mu;_2 \;\;\;\;\;\; (26)</i></p>

<p>Thus, for given values of <i>R</i> and <i>&tau;</i>, the power of the test of the null 
hypothesis (21) against any of the alternatives (22)-(24) can be computed based on 
the power of a two-sample t-test with
</p>
<p style="text-align: center;"><i>\frac{&delta;}{&sigma;} = \frac{log(R)}{&radic;{log(&tau;^2 + 1)}} \;\;\;\;\;\; (27)</i></p>

<p>(see the help file for <code><a href="../../EnvStats/help/tTestPower.html">tTestPower</a></code>).  Note that for the function 
<code>tTestLnormAltPower</code>, <i>R</i> corresponds to the argument <code>ratio.of.means</code>, 
and <i>&tau;</i> corresponds to the argument <code>cv</code>.
</p>


<h3>Value</h3>

<p>a numeric vector powers.
</p>


<h3>Note</h3>

<p>The <a href="../../stats/help/Normal.html">normal distribution</a> and 
<a href="../../stats/help/Lognormal.html">lognormal distribution</a> are probably the two most 
frequently used distributions to model environmental data.  Often, you need to 
determine whether a population mean is significantly different from a specified 
standard (e.g., an MCL or ACL, USEPA, 1989b, Section 6), or whether two different 
means are significantly different from each other (e.g., USEPA 2009, Chapter 16).  
When you have lognormally-distributed data, you have to be careful about making 
statements regarding inference for the mean.  For the two-sample case with 
assumed equal coefficients of variation, you can perform the 
<a href="../../stats/help/t.test.html">Student's t-test</a> on the log-transformed observations.  
For the one-sample case, you can perform a hypothesis test by constructing a 
confidence interval for the mean using <code><a href="../../EnvStats/help/elnormAlt.html">elnormAlt</a></code>, or use 
<a href="../../EnvStats/help/chenTTest.html">Chen's t-test modified for skewed data</a>.
</p>
<p>In the course of designing a sampling program, an environmental scientist may wish 
to determine the relationship between sample size, significance level, power, and 
scaled difference if one of the objectives of the sampling program is to determine 
whether a mean differs from a specified level or two means differ from each other.  
The functions <code>tTestLnormAltPower</code>, <code><a href="../../EnvStats/help/tTestLnormAltN.html">tTestLnormAltN</a></code>, 
<code><a href="../../EnvStats/help/tTestLnormAltRatioOfMeans.html">tTestLnormAltRatioOfMeans</a></code>, and <code><a href="../../EnvStats/help/plotTTestLnormAltDesign.html">plotTTestLnormAltDesign</a></code> 
can be used to investigate these relationships for the case of 
lognormally-distributed observations.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>van Belle, G., and D.C. Martin. (1993).  Sample Size as a Function of Coefficient 
of Variation and Ratio of Means.  <em>The American Statistician</em> <b>47</b>(3), 
165&ndash;167.
</p>
<p>Also see the list of references in the help file for <code><a href="../../EnvStats/help/tTestPower.html">tTestPower</a></code>.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/tTestLnormAltN.html">tTestLnormAltN</a></code>, <code><a href="../../EnvStats/help/tTestLnormAltRatioOfMeans.html">tTestLnormAltRatioOfMeans</a></code>, 
<code><a href="../../EnvStats/help/plotTTestLnormAltDesign.html">plotTTestLnormAltDesign</a></code>, <a href="../../EnvStats/help/LognormalAlt.html">LognormalAlt</a>, 
<code><a href="../../stats/html/t.test.html">t.test</a></code>, <a href="../../EnvStats/help/Hypothesis+20Tests.html">Hypothesis Tests</a>. 
</p>


<h3>Examples</h3>

<pre>
  # Look at how the power of the one-sample test increases with increasing 
  # sample size:

  seq(5, 30, by = 5) 
  #[1]  5 10 15 20 25 30 

  power &lt;- tTestLnormAltPower(n.or.n1 = seq(5, 30, by = 5), 
    ratio.of.means = 1.5, cv = 1) 

  round(power, 2) 
  #[1] 0.14 0.28 0.42 0.54 0.65 0.73

  #----------

  # Repeat the last example, but use the approximation to the power instead of the 
  # exact power.  Note how the approximation underestimates the true power for 
  # the smaller sample sizes:

  power &lt;- tTestLnormAltPower(n.or.n1 = seq(5, 30, by = 5), 
    ratio.of.means = 1.5, cv = 1, approx = TRUE) 

  round(power, 2) 
  #[1] 0.09 0.25 0.40 0.53 0.64 0.73

  #==========

  # Look at how the power of the two-sample t-test increases with increasing 
  # ratio of means:

  power &lt;- tTestLnormAltPower(n.or.n1 = 20, sample.type = "two", 
    ratio.of.means = c(1.1, 1.5, 2), cv = 1) 

  round(power, 2) 
  #[1] 0.06 0.32 0.73

  #----------

  # Look at how the power of the two-sample t-test increases with increasing 
  # values of Type I error:

  power &lt;- tTestLnormAltPower(30, sample.type = "two", ratio.of.means = 1.5, 
    cv = 1, alpha = c(0.001, 0.01, 0.05, 0.1)) 

  round(power, 2) 
  #[1] 0.07 0.23 0.46 0.59

  #==========

  # The guidance document Soil Screening Guidance: Technical Background Document 
  # (USEPA, 1996c, Part 4) discusses sampling design and sample size calculations 
  # for studies to determine whether the soil at a potentially contaminated site 
  # needs to be investigated for possible remedial action. Let 'theta' denote the 
  # average concentration of the chemical of concern.  The guidance document 
  # establishes the following goals for the decision rule (USEPA, 1996c, p.87):
  #
  #     Pr[Decide Don't Investigate | theta &gt; 2 * SSL] = 0.05
  #
  #     Pr[Decide to Investigate | theta &lt;= (SSL/2)] = 0.2
  #
  # where SSL denotes the pre-established soil screening level.
  #
  # These goals translate into a Type I error of 0.2 for the null hypothesis
  #
  #     H0: [theta / (SSL/2)] &lt;= 1
  #
  # and a power of 95% for the specific alternative hypothesis
  #
  #     Ha: [theta / (SSL/2)] = 4
  #
  # Assuming a lognormal distribution with a coefficient of variation of 2, 
  # determine the power associated with various sample sizes for this one-sample test. 
  # Based on these calculations, you need to take at least 6 soil samples to 
  # satisfy the requirements for the Type I and Type II errors.

  power &lt;- tTestLnormAltPower(n.or.n1 = 2:8, ratio.of.means = 4, cv = 2, 
    alpha = 0.2, alternative = "greater") 

  names(power) &lt;- paste("N=", 2:8, sep = "")

  round(power, 2) 
  # N=2  N=3  N=4  N=5  N=6  N=7  N=8 
  #0.65 0.80 0.88 0.93 0.96 0.97 0.98

  #----------

  # Repeat the last example, but use the approximate power calculation instead of 
  # the exact one.  Using the approximate power calculation, you need at least 
  # 7 soil samples instead of 6 (because the approximation underestimates the power).

  power &lt;- tTestLnormAltPower(n.or.n1 = 2:8, ratio.of.means = 4, cv = 2, 
    alpha = 0.2, alternative = "greater", approx = TRUE) 

  names(power) &lt;- paste("N=", 2:8, sep = "")

  round(power, 2)
  # N=2  N=3  N=4  N=5  N=6  N=7  N=8 
  #0.55 0.75 0.84 0.90 0.93 0.95 0.97

  #==========

  # Clean up
  #---------
  rm(power)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
