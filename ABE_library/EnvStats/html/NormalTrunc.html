<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: The Truncated Normal Distribution</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for NormalTrunc {EnvStats}"><tr><td>NormalTrunc {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
The Truncated Normal Distribution
</h2>

<h3>Description</h3>

<p>Density, distribution function, quantile function, and random generation 
for the truncated normal distribution with parameters <code>mean</code>, 
<code>sd</code>, <code>min</code>, and <code>max</code>.
</p>


<h3>Usage</h3>

<pre>
  dnormTrunc(x, mean = 0, sd = 1, min = -Inf, max = Inf)
  pnormTrunc(q, mean = 0, sd = 1, min = -Inf, max = Inf)
  qnormTrunc(p, mean = 0, sd = 1, min = -Inf, max = Inf)
  rnormTrunc(n, mean = 0, sd = 1, min = -Inf, max = Inf)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>q</code></td>
<td>

<p>vector of quantiles.
</p>
</td></tr>
<tr valign="top"><td><code>p</code></td>
<td>

<p>vector of probabilities between 0 and 1.
</p>
</td></tr>
<tr valign="top"><td><code>n</code></td>
<td>

<p>sample size.  If <code>length(n)</code> is larger than 1, then <code>length(n)</code> 
random values are returned.
</p>
</td></tr>
<tr valign="top"><td><code>mean</code></td>
<td>

<p>vector of means of the distribution of the non-truncated random variable.  
The default is <code>mean=0</code>.
</p>
</td></tr>
<tr valign="top"><td><code>sd</code></td>
<td>

<p>vector of (positive) standard deviations of the non-truncated random variable.  
The default is <code>sd=1</code>.
</p>
</td></tr>
<tr valign="top"><td><code>min</code></td>
<td>

<p>vector of minimum values for truncation on the left.  The default value is 
<code>min=-Inf</code>.
</p>
</td></tr>
<tr valign="top"><td><code>max</code></td>
<td>

<p>vector of maximum values for truncation on the right.  The default value is 
<code>max=Inf</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>See the help file for <a href="../../stats/help/Normal.html">the normal distribution</a> 
for information about the density and cdf of a normal distribution.
</p>
<p><em>Probability Density and Cumulative Distribution Function</em> <br />
Let <i>X</i> denote a random variable with density function <i>f(x)</i> and 
cumulative distribution function <i>F(x)</i>, and let 
<i>Y</i> denote the truncated version of <i>X</i> where <i>Y</i> is truncated 
below at <code>min=</code><i>A</i> and above at<code>max=</code><i>B</i>.  Then the density 
function of <i>Y</i>, denoted <i>g(y)</i>, is given by:
</p>
<p style="text-align: center;"><i>g(y) = frac{f(y)}{F(B) - F(A)}, A &le; y &le; B</i></p>

<p>and the cdf of Y, denoted <i>G(y)</i>, is given by:
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <i>G(y) =</i>  </td><td style="text-align: left;">  0                                    </td><td style="text-align: left;"> for <i>y &lt; A</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
                  </td><td style="text-align: left;"> <i>\frac{F(y) - F(A)}{F(B) - F(A)}</i> </td><td style="text-align: left;"> for <i>A &le; y &le; B</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
                  </td><td style="text-align: left;"> 1                                     </td><td style="text-align: left;"> for <i>y &gt; B</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
  </td>
</tr>

</table>

<p><em>Quantiles</em> <br />
The <i>p^{th}</i> quantile <i>y_p</i> of <i>Y</i> is given by:
</p>

<table summary="Rd table">
<tr>
 <td style="text-align: left;">
    <i>y_p =</i>  </td><td style="text-align: left;"> <i>A</i>                                </td><td style="text-align: left;"> for <i>p = 0</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
                 </td><td style="text-align: left;"> <i>F^{-1}\{p[F(B) - F(A)] + F(A)\} </i> </td><td style="text-align: left;"> for <i>0 &lt; p &lt; 1</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
                 </td><td style="text-align: left;"> <i>B</i>                                </td><td style="text-align: left;"> for <i>p = 1</i> </td>
</tr>
<tr>
 <td style="text-align: left;">
  </td>
</tr>

</table>
 
<p><em>Random Numbers</em> <br />
Random numbers are generated using the inverse transformation method:
</p>
<p style="text-align: center;"><i>y = G^{-1}(u)</i></p>

<p>where <i>u</i> is a random deviate from a uniform <i>[0, 1]</i> distribution. 
</p>
<p><em>Mean and Variance</em> <br />
The expected value of a truncated normal random variable with parameters 
<code>mean=</code><i>&mu;</i>, <code>sd=</code><i>&sigma;</i>, <code>min=</code><i>A</i>, and 
<code>max=</code><i>B</i> is given by:
</p>
<p style="text-align: center;"><i>E(Y) = &mu; + &sigma;^2 \frac{f(A) - f(B)}{F(B) - F(A)}</i></p>

<p>(Johnson et al., 1994, p.156; Schneider, 1986, p.17).
</p>
<p>The variance of this random variable is given by:
</p>
<p style="text-align: center;"><i>&sigma;^2 + &sigma;^3 \{z_A f(A) - z_B f(B) - &sigma;[f(A) - f(B)]^2 \}</i></p>

<p>where
</p>
<p style="text-align: center;"><i>z_A = \frac{A - &mu;}{&sigma;}; \, z_B = \frac{B - &mu;}{&sigma;}</i></p>

<p>(Johnson et al., 1994, p.158; Schneider, 1986, p.17).
</p>


<h3>Value</h3>

<p><code>dnormTrunc</code> gives the density, <code>pnormTrunc</code> gives the distribution function, 
<code>qnormTrunc</code> gives the quantile function, and <code>rnormTrunc</code> generates random 
deviates. 
</p>


<h3>Note</h3>

<p>A truncated normal distribution is sometimes used as an input distribution 
for probabilistic risk assessment.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>Forbes, C., M. Evans, N. Hastings, and B. Peacock. (2011).  Statistical Distributions. 
Fourth Edition. John Wiley and Sons, Hoboken, NJ.
</p>
<p>Johnson, N. L., S. Kotz, and N. Balakrishnan. (1994). 
<em>Continuous Univariate Distributions, Volume 1</em>. 
Second Edition. John Wiley and Sons, New York.
</p>
<p>Schneider, H. (1986).  <em>Truncated and Censored Samples from Normal Populations</em>.  
Marcel Dekker, New York, Chapter 2.
</p>


<h3>See Also</h3>

<p><a href="../../stats/help/Normal.html">Normal</a>,   
<a href="../../EnvStats/help/Probability+20Distributions+20and+20Random+20Numbers.html">Probability Distributions and Random Numbers</a>.
</p>


<h3>Examples</h3>

<pre>
  # Density of a truncated normal distribution with parameters 
  # mean=10, sd=2, min=8, max=13, evaluated at 10 and 11.5:

  dnormTrunc(c(10, 11.5), 10, 2, 8, 13) 
  #[1] 0.2575358 0.1943982

  #----------

  # The cdf of a truncated normal distribution with parameters 
  # mean=10, sd=2, min=8, max=13, evaluated at 10 and 11.5:

  pnormTrunc(c(10, 11.5), 10, 2, 8, 13) 
  #[1] 0.4407078 0.7936573

  #----------

  # The median of a truncated normal distribution with parameters 
  # mean=10, sd=2, min=8, max=13:

  qnormTrunc(.5, 10, 2, 8, 13) 
  #[1] 10.23074

  #----------

  # A random sample of 3 observations from a truncated normal distribution 
  # with parameters mean=10, sd=2, min=8, max=13. 
  # (Note: the call to set.seed simply allows you to reproduce this example.)

  set.seed(20) 
  rnormTrunc(3, 10, 2, 8, 13) 
  #[1] 11.975223 11.373711  9.361258
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
