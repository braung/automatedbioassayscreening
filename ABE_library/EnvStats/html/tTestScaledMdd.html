<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Scaled Minimal Detectable Difference for One- or Two-Sample...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body><div class="container">

<table width="100%" summary="page for tTestScaledMdd {EnvStats}"><tr><td>tTestScaledMdd {EnvStats}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>
Scaled Minimal Detectable Difference for One- or Two-Sample t-Test
</h2>

<h3>Description</h3>

<p>Compute the scaled minimal detectable difference necessary to achieve a 
specified power for a one- or two-sample t-test, given the sample size(s) 
and Type I error level.
</p>


<h3>Usage</h3>

<pre>
  tTestScaledMdd(n.or.n1, n2 = n.or.n1, alpha = 0.05, power = 0.95, 
    sample.type = ifelse(!missing(n2) &amp;&amp; !is.null(n2), "two.sample", "one.sample"), 
    alternative = "two.sided", two.sided.direction = "greater", 
    approx = FALSE, tol = 1e-07, maxiter = 1000)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>n.or.n1</code></td>
<td>

<p>numeric vector of sample sizes.  When <code>sample.type="one.sample"</code>, 
<code>n.or.n1</code> denotes <i>n</i>, the number of observations in the single sample.  When <br />
<code>sample.type="two.sample"</code>, <code>n.or.n1</code> denotes <i>n_1</i>, the number 
of observations from group 1.  
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are <b><em>not</em></b> allowed.
</p>
</td></tr>
<tr valign="top"><td><code>n2</code></td>
<td>

<p>numeric vector of sample sizes for group 2.  The default value is the value of 
<code>n.or.n1</code>. This argument is ignored when <code>sample.type="one.sample"</code>. 
Missing (<code>NA</code>), undefined (<code>NaN</code>), and infinite (<code>Inf</code>, <code>-Inf</code>) 
values are <b><em>not</em></b> allowed.
</p>
</td></tr>
<tr valign="top"><td><code>alpha</code></td>
<td>

<p>numeric vector of numbers between 0 and 1 indicating the Type I error level 
associated with the hypothesis test.  The default value is <code>alpha=0.05</code>.
</p>
</td></tr>
<tr valign="top"><td><code>power</code></td>
<td>

<p>numeric vector of numbers between 0 and 1 indicating the power  
associated with the hypothesis test.  The default value is <code>power=0.95</code>.
</p>
</td></tr>
<tr valign="top"><td><code>sample.type</code></td>
<td>

<p>character string indicating whether to compute power based on a one-sample or 
two-sample hypothesis test.  When <code>sample.type="one.sample"</code>, the computed 
power is based on a hypothesis test for a single mean.  When <br />
<code>sample.type="two.sample"</code>, the computed power is based on a hypothesis test 
for the difference between two means.  The default value is <br />
<code>sample.type="one.sample"</code> unless the argument <code>n2</code> is supplied.
</p>
</td></tr>
<tr valign="top"><td><code>alternative</code></td>
<td>

<p>character string indicating the kind of alternative hypothesis.  The possible values 
are: 
</p>

<ul>
<li> <p><code>"two.sided"</code> (the default).  <i>H_a: &mu; \ne &mu;_0</i> for the one-sample case and 
<i>H_a: &mu;_1 \ne &mu;_2</i> for the two-sample case. 
</p>
</li>
<li> <p><code>"greater"</code>.  <i>H_a: &mu; &gt; &mu;_0</i> for the one-sample case and 
<i>H_a: &mu;_1 &gt; &mu;_2</i> for the two-sample case. 
</p>
</li>
<li> <p><code>"less"</code>.  <i>H_a: &mu; &lt; &mu;_0</i> for the one-sample case and 
<i>H_a: &mu;_1 &lt; &mu;_2</i> for the two-sample case.
</p>
</li></ul>

</td></tr>
<tr valign="top"><td><code>two.sided.direction</code></td>
<td>

<p>character string indicating the direction (positive or negative) for the 
scaled minimal detectable difference when <code>alternative="two.sided"</code>.  When <br />
<code>two.sided.direction="greater"</code> (the default), the scaled minimal 
detectable difference is positive.  When <code>two.sided.direction="less"</code>, 
the scaled minimal detectable difference is negative.  This argument 
is ignored if <br />
<code>alternative="less"</code> or <code>alternative="greater"</code>.
</p>
</td></tr>
<tr valign="top"><td><code>approx</code></td>
<td>

<p>logical scalar indicating whether to compute the power based on an approximation to 
the non-central t-distribution.  The default value is <code>FALSE</code>.
</p>
</td></tr>
<tr valign="top"><td><code>tol</code></td>
<td>

<p>numeric scalar indicating the tolerance argument to pass to the  
<code><a href="../../stats/html/uniroot.html">uniroot</a></code> function.  
The default value is <code>tol=1e-7</code>.
</p>
</td></tr>
<tr valign="top"><td><code>maxiter</code></td>
<td>

<p>positive integer indicating the maximum number of iterations 
argument to pass to the <code><a href="../../stats/html/uniroot.html">uniroot</a></code> function.  The default 
value is <code>maxiter=1000</code>.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Formulas for the power of the t-test for specified values of 
the sample size, scaled difference, and Type I error level are given in 
the help file for <code><a href="../../EnvStats/help/tTestPower.html">tTestPower</a></code>.  The function <code>tTestScaledMdd</code> 
uses the <code><a href="../../stats/html/uniroot.html">uniroot</a></code> search algorithm to determine the 
required scaled minimal detectable difference for specified values of the 
sample size, power, and Type I error level.
</p>


<h3>Value</h3>

<p>numeric vector of scaled minimal detectable differences.
</p>


<h3>Note</h3>

<p>See <code><a href="../../EnvStats/help/tTestPower.html">tTestPower</a></code>.
</p>


<h3>Author(s)</h3>

<p>Steven P. Millard (<a href="mailto:EnvStats@ProbStatInfo.com">EnvStats@ProbStatInfo.com</a>)
</p>


<h3>References</h3>

<p>See <code><a href="../../EnvStats/help/tTestPower.html">tTestPower</a></code>.
</p>


<h3>See Also</h3>

<p><code><a href="../../EnvStats/help/tTestPower.html">tTestPower</a></code>, <code><a href="../../EnvStats/help/tTestAlpha.html">tTestAlpha</a></code>, 
<code><a href="../../EnvStats/help/tTestN.html">tTestN</a></code>, 
<code><a href="../../EnvStats/help/plotTTestDesign.html">plotTTestDesign</a></code>, <a href="../../stats/help/Normal.html">Normal</a>, 
<code><a href="../../stats/html/t.test.html">t.test</a></code>, <a href="../../EnvStats/help/Hypothesis+20Tests.html">Hypothesis Tests</a>. 
</p>


<h3>Examples</h3>

<pre>
  # Look at how the scaled minimal detectable difference for the 
  # one-sample t-test increases with increasing required power:

  seq(0.5, 0.9, by = 0.1) 
  #[1] 0.5 0.6 0.7 0.8 0.9 

  scaled.mdd &lt;- tTestScaledMdd(n.or.n1 = 20, power = seq(0.5,0.9,by=0.1)) 

  round(scaled.mdd, 2) 
  #[1] 0.46 0.52 0.59 0.66 0.76

  #----------

  # Repeat the last example, but compute the scaled minimal detectable 
  # differences based on the approximation to the power instead of the 
  # exact formula:

  scaled.mdd &lt;- tTestScaledMdd(n.or.n1 = 20, power = seq(0.5, 0.9, by = 0.1), 
    approx = TRUE) 

  round(scaled.mdd, 2) 
  #[1] 0.47 0.53 0.59 0.66 0.76

  #==========

  # Look at how the scaled minimal detectable difference for the two-sample 
  # t-test decreases with increasing sample size:

  seq(10,50,by=10) 
  #[1] 10 20 30 40 50 

  scaled.mdd &lt;- tTestScaledMdd(seq(10, 50, by = 10), sample.type = "two") 

  round(scaled.mdd, 2) 
  #[1] 1.71 1.17 0.95 0.82 0.73

  #----------

  # Look at how the scaled minimal detectable difference for the two-sample 
  # t-test decreases with increasing values of Type I error:

  scaled.mdd &lt;- tTestScaledMdd(20, alpha = c(0.001, 0.01, 0.05, 0.1), 
    sample.type="two") 

  round(scaled.mdd, 2) 
  #[1] 1.68 1.40 1.17 1.06

  #==========

  # Modifying the example on pages 21-4 to 21-5 of USEPA (2009), 
  # determine the minimal mean level of aldicarb at the third compliance 
  # well necessary to detect a mean level of aldicarb greater than the 
  # MCL of 7 ppb, assuming 90%, 95%, and 99% power.  Use a 99% significance 
  # level and assume an upper one-sided alternative (third compliance well 
  # mean larger than 7).  Use the estimated standard deviation from the 
  # first four months of data to estimate the true population standard 
  # deviation in order to determine the minimal detectable difference based 
  # on the computed scaled minimal detectable difference, then use this 
  # minimal detectable difference to determine the mean level of aldicarb 
  # necessary to detect a difference.  (The data are stored in 
  # EPA.09.Ex.21.1.aldicarb.df.) 
  #
  # Note that the scaled minimal detectable difference changes from 3.4 to 
  # 3.9 to 4.7 as the power changes from 90% to 95% to 99%.  Thus, the 
  # minimal detectable difference changes from 7.2 to 8.1 to 9.8, and the 
  # minimal mean level of aldicarb changes from 14.2 to 15.1 to 16.8.

  EPA.09.Ex.21.1.aldicarb.df
  #   Month   Well Aldicarb.ppb
  #1      1 Well.1         19.9
  #2      2 Well.1         29.6
  #3      3 Well.1         18.7
  #4      4 Well.1         24.2
  #5      1 Well.2         23.7
  #6      2 Well.2         21.9
  #7      3 Well.2         26.9
  #8      4 Well.2         26.1
  #9      1 Well.3          5.6
  #10     2 Well.3          3.3
  #11     3 Well.3          2.3
  #12     4 Well.3          6.9

  sigma &lt;- with(EPA.09.Ex.21.1.aldicarb.df, 
    sd(Aldicarb.ppb[Well == "Well.3"]))

  sigma
  #[1] 2.101388

  scaled.mdd &lt;- tTestScaledMdd(n.or.n1 = 4, alpha = 0.01, 
    power = c(0.90, 0.95, 0.99), sample.type="one", alternative="greater") 

  scaled.mdd 
  #[1] 3.431501 3.853682 4.668749

  mdd &lt;- scaled.mdd * sigma 
  mdd 
  #[1] 7.210917 8.098083 9.810856

  minimal.mean &lt;- mdd + 7 

  minimal.mean 
  #[1] 14.21092 15.09808 16.81086


  #==========

  # Clean up
  #---------
  rm(scaled.mdd, sigma, mdd, minimal.mean)
</pre>

<hr /><div style="text-align: center;">[Package <em>EnvStats</em> version 2.8.0 <a href="00Index.html">Index</a>]</div>
</div></body></html>
